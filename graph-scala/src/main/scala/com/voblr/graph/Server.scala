package com.voblr.graph

import com.googlecode.protobuf.pro.duplex.{RpcClientChannel, PeerInfo}
import com.googlecode.protobuf.pro.duplex.execute.ThreadPoolCallExecutor
import com.googlecode.protobuf.pro.duplex.server.DuplexTcpServerBootstrap
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory
import java.util.concurrent.Executors
import services.v1.impl.{EntityServiceImpl, VobServiceImpl, MovieServiceImpl, UserServiceImpl}
import com.googlecode.protobuf.pro.duplex.listener.TcpConnectionEventListener
import org.neo4j.scala.{GraphDatabaseServiceProvider}
import util.VobLogger

/**
 * User: abhi@voblr.com
 * Date: 10/1/12
 * Time: 2:24 AM
 * (c) Voblr, Inc
 */
class Server(host: String, port: java.lang.Integer, dsp: GraphDatabaseServiceProvider) extends VobLogger {

  val server = new PeerInfo(host, port)

  val executor = new ThreadPoolCallExecutor(1, 10)

  val bootstrap = new DuplexTcpServerBootstrap(
    server, new NioServerSocketChannelFactory(
      Executors.newCachedThreadPool(),
      Executors.newCachedThreadPool()), executor)

  bootstrap.getRpcServiceRegistry.registerService(new UserServiceImpl(dsp.ds))

  bootstrap.getRpcServiceRegistry.registerService(new MovieServiceImpl(dsp.ds))

  bootstrap.getRpcServiceRegistry.registerService(new VobServiceImpl(dsp.ds))

  bootstrap.getRpcServiceRegistry.registerService(new EntityServiceImpl(dsp.ds))

  class Listener extends TcpConnectionEventListener {
    def connectionClosed(clientChannel: RpcClientChannel) {
      logger.debug("connection closed " + clientChannel.getPeerInfo)
    }

    def connectionOpened(clientChannel: RpcClientChannel) {
      logger.debug("connected to " + clientChannel.getPeerInfo)
    }
  }

  val list = new java.util.ArrayList[TcpConnectionEventListener]()
  list.add(new Listener)

  bootstrap.setConnectionEventListeners(list)
  bootstrap.bind()
}
