package com.voblr.graph


import org.neo4j.scala.{Neo4jIndexProvider, Neo4jWrapper}
import org.neo4j.graphdb.{Path, Direction, Node}
import com.voblr.common.constants.GraphConstants
import com.voblr.common.GraphServices.EntityType
import services.v1.User
import sys.ShutdownHookThread
import util.VobLogger
import com.voblr.common.proto.Movie.MovieProto
import com.voblr.common.proto.User.UserProto
import com.voblr.common.proto.Vob.VobProto
import com.voblr.common.proto.Comment.CommentProto
import org.neo4j.helpers.collection.IterableWrapper
import org.neo4j.kernel.Traversal


/**
 * User: abhi@voblr.com
 * Date: 11/4/12
 * Time: 4:53 AM
 * (c) Voblr, Inc
 */
trait VoblrGraphWrapper extends Neo4jWrapper with Neo4jIndexProvider with GraphUtil with VobLogger {

  ShutdownHookThread {
    shutdown(ds)
  }


  final override def NodeIndexConfig = {
    val user = (EntityType.USER.name(), Option(Map("provider" -> "lucene", "type" -> "exact")))
    val movie = (EntityType.MOVIE.name(), Option(Map("provider" -> "lucene", "type" -> "exact")))
    val vob = (EntityType.VOB_ENTITY.name(), Option(Map("provider" -> "lucene", "type" -> "exact")))
    val comment = (EntityType.COMMENT_ENTITY.name(), Option(Map("provider" -> "lucene", "type" -> "exact")))
    user :: movie :: vob :: comment :: Nil
  }


  def getCommentsOnVob(vob_node: Node): IterableWrapper[CommentProto, Path] = {
    val traversal = Traversal.description().depthFirst().
      relationships(GraphConstants.NEXT_COMMENT);
    if (vob_node.hasRelationship(GraphConstants.VOB_COMMENT, out)) {
      val iterable_vobProto = new IterableWrapper[CommentProto, Path](traversal.traverse(vob_node.getSingleRelationship(GraphConstants.VOB_COMMENT, out).getEndNode)) {
        def underlyingObjectToObject(path: Path): CommentProto = {
          val comment_node = path.endNode();
          val proto_builder = getCommentProto(comment_node).toBuilder
          // add some proto properties, if any
          proto_builder.build()
        }
      };
      iterable_vobProto
    } else {
      null
    }
  }

  override def RelationIndexConfig = {
    val follows = (GraphConstants.FOLLOWS, Option(Map("provider" -> "lucene", "type" -> "exact")))
    val user_vob = (GraphConstants.USER_VOB, Option(Map("provider" -> "lucene", "type" -> "exact")))
    val vob_movie = (GraphConstants.VOB_MOVIE, Option(Map("provider" -> "lucene", "type" -> "exact")))
    val comments = (GraphConstants.COMMENTS, Option(Map("provider" -> "lucene", "type" -> "exact")))
    follows :: user_vob :: vob_movie :: comments :: Nil
  }

  def getUserProto(user: Node): UserProto = {
    UserProto.newBuilder()
      .setUserId(user[String](GraphConstants.ENTITY_ID) getOrElse (fail("Not a user Node : no id property")))
      .setFollowersCount(user[Int](GraphConstants.FOLLOWERS_COUNT) getOrElse (fail("Not a user Node : no followers count property")))
      .setFollowingCount(user[Int](GraphConstants.FOLLOWING_COUNT) getOrElse (fail("Not a user Node : no following count property")))
      .setTotalVobs(user[Int](GraphConstants.VOB_COUNT).getOrElse(fail("Not a user Node : no comment count property")))
      .setRecentActivityTs(user[Long](GraphConstants.LATEST_ACTIVITY_TS) getOrElse (fail("Not a user Node : no latest activity ts property")))
      .setCreatedTs(user[Long](GraphConstants.CREATED_TS) getOrElse (fail("Not a user Node : no created ts property")))
      .setReputation(User(user).reputation)
      .build()
  }


  def getMovieProto(movie: Node): MovieProto = {
    MovieProto.newBuilder()
      .setMovieId(movie[String](GraphConstants.ENTITY_ID) getOrElse (fail("Not a movie Node : no id  property")))
      .setTotalVobs(movie[Int](GraphConstants.ENTITY_ID) getOrElse (fail("Not a movie Node : no created ts property")))
      .setCreatedTs(movie[Long](GraphConstants.CREATED_TS) getOrElse (fail("Not a movie Node : no created ts property")))
      .build()
  }


  def getVobProto(vob: Node): VobProto = {
    VobProto.newBuilder()
      .setVobId(vob[String](GraphConstants.ENTITY_ID) getOrElse (fail("Not a vob Node : no id property")))
      .setVobText(vob[String](GraphConstants.VOB_TXT) getOrElse (fail("Not a vob Node : no vob_txt property")))
      .setSubjectId(vob[String](GraphConstants.SUBJECT_ID) getOrElse (fail("Not a vob Node : no subject id property")))
      .setObjectId(vob[String](GraphConstants.OBJECT_ID) getOrElse (fail("Not a vob Node : no subject id property")))
      .setLikeCount(vob[Int](GraphConstants.LIKE_COUNT) getOrElse (fail("Not a vob Node : no like count property")))
      .setVoteUpCount(vob[Int](GraphConstants.VOTE_UP_COUNT) getOrElse (fail("Not a vob Node : no vote up count property")))
      .setVoteDownCount(vob[Int](GraphConstants.VOTE_DOWN_COUNT) getOrElse (fail("Not a vob Node : no vote down count property")))
      .setLatestActivityTs(vob[Long](GraphConstants.LATEST_ACTIVITY_TS) getOrElse (fail("Not a vob Node : no latest activity ts property")))
      .setCreatedTs(vob[Long](GraphConstants.CREATED_TS) getOrElse (fail("Not a vob Node : no created ts property")))
      .setPopularityIdx(vob[Double](GraphConstants.POPULARITY_INDEX) getOrElse (fail("Not a vob Node : no created ts property")))
      .build()
  }

  def getCommentProto(comment: Node): CommentProto = {
    CommentProto.newBuilder()
      .setCommentId(comment[String](GraphConstants.ENTITY_ID) getOrElse (fail("Not a comment Node : no id property")))
      .setFrom(comment[String](GraphConstants.SUBJECT_ID) getOrElse (fail("Not a comment Node : no subject id property")))
      .setMessage(comment[String](GraphConstants.COMMENT_TXT) getOrElse (fail("Not a comment Node : no comment text property")))
      .setCreatedTime(comment[Long](GraphConstants.CREATED_TS) getOrElse (fail("Not a comment Node : no created ts property")))
      .build()
  }

  def fail(msg: String, e: Throwable) = throw new Error(msg, e)

  def fail(msg: String) = throw new Error(msg)


  // FIXME may be returning Option[Node] is good idea?
  final def getNodeByEntityId(entityType: EntityType, entity_id: String): Node = {
//    logger.debug(String.format("getting nodeIndex for node of type <%s> with id <%s>", entityType.toString, entity_id))
    try {

      val nodeIndex = getNodeIndex(entityType.name()).getOrElse(fail("Failed to get Index for " + entityType))
//      logger.debug(String.format("success for nodeIndex of node of type <%s> with id <%s>", entityType, entity_id))

      val hits = nodeIndex.get(GraphConstants.ENTITY_ID, entity_id)
      try {

//        logger.debug("hits for " + entity_id + " : " + hits.size())
        if (hits.size() > 1) fail(" This should never be called. (More than one nodes with unique property)")
        if (hits.size() < 1) null
        hits.getSingle
      } catch {
        case e: Error => fail("", e)
        case e => fail("Unknown error ", e)
      } finally {
        if (hits != null || hits != None)
          hits.close()
      }
    } catch {
      case e: Error => fail("", e)
      case e => fail("Unknown error ", e)
    }
  }

  final def getUserNode(entity_id: String): Node = {
    getNodeByEntityId(EntityType.USER, entity_id)
  }

  final def getMovieNode(entity_id: String): Node = {
    getNodeByEntityId(EntityType.MOVIE, entity_id)
  }

  final def getVobNode(entity_id: String): Node = {
    getNodeByEntityId(EntityType.VOB_ENTITY, entity_id)
  }

  final def getCommentNode(entity_id: String): Node = {
    getNodeByEntityId(EntityType.COMMENT_ENTITY, entity_id)
  }
}

