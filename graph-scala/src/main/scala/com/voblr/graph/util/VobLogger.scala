package com.voblr.graph.util

/**
 * User: abhi@voblr.com
 * Date: 11/12/12
 * Time: 10:11 PM
 * (c) Voblr, Inc
 *
 *
 *
 */

trait VobLogger {
  val logger  = this
  def debug(msg: String) = println("DEBUG > " + msg)

  def info(msg: String) = println("INFO > " + msg)

  def error(msg: String) = println("ERROR > " + msg)

  def warn(msg: String) = println("WARN > " + msg)
}