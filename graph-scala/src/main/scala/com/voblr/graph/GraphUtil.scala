package com.voblr.graph

import org.neo4j.graphdb._
import org.neo4j.kernel.{Uniqueness, Traversal}
import com.voblr.common.constants.GraphConstants
import services.v1.{Comment, User, Vob}
import traversal._
import org.neo4j.graphalgo.GraphAlgoFactory
import org.neo4j.helpers.collection.{IterableWrapper => JIterableWrapper, FilteringIterable}
import traversal.Traverser
import java.lang.{Iterable => JIterable}
import java.util.{Iterator => JIterator}
import scala.Some
import org.neo4j.scala.Neo4jWrapperImplicits
import org.neo4j.helpers.Predicate

/**
 * User: abhi@voblr.com
 * Date: 12/11/12
 * Time: 1:29 AM
 * (c) Voblr, Inc
 *
 *
 *
 */
case class DirectedRelationshipOnNode(node: Node, rel: RelationshipType, dir: Direction) {

  def getSingleRelationship: Option[Relationship] = {
    val x = node.getSingleRelationship(rel, dir)
    if (x == null) None else Some(x)
  }

  def getOtherNode: Option[Node] = getSingleRelationship match {
    case None => None
    case Some(x) => Some(x.getOtherNode(node))
  }

  def getRelationships = node.getRelationships(rel, dir)
}

trait GraphUtil extends Neo4jWrapperImplicits {


  implicit def toIterator[T](ci: java.util.Iterator[T]) =
    new IteratorWrapper[T](ci)

  class IteratorWrapper[A](it: java.util.Iterator[A]) {
    def foreach(f: A => Unit) {
      while (it.hasNext) {
        f(it.next)
      }
    }
  }

  val out = Direction.OUTGOING
  val in = Direction.INCOMING
  val both = Direction.BOTH


  def areConnected(n1: Node, n2: Node, relType: RelationshipType, dir: Direction): Boolean = {

    val dir1 = dir match {
      case null => both
      case _ => dir

    }

    val dir2 = dir1 match {
      case Direction.OUTGOING => in
      case Direction.INCOMING => out
      case Direction.BOTH => both
    }

    val rels1 = n1.getRelationships(relType, dir1).iterator();

    val rels2 = n2.getRelationships(relType, dir2).iterator();

    while (rels1.hasNext && rels2.hasNext) {
      val rel1 = rels1.next();
      val rel2 = rels2.next();


      if (rel1.getEndNode().equals(n2))
        return true
      else if (rel2.getEndNode().equals(n1))
        return true
    }
    return false
  }

  def getConnected(direction: Direction, person: Node): Traverser = {
    Traversal.description()
      .breadthFirst()
      .relationships(DynamicRelationshipType.withName(GraphConstants.FOLLOWS), direction)
      .evaluator(Evaluators.toDepth(1))
      .evaluator(Evaluators.excludeStartPosition()) // required to omit self node #always
      .traverse(person)
  }


  def getDirectedTraversalDescription(rel: RelationshipType, direction: Direction): TraversalDescription = {
    Traversal.description()
      .depthFirst()
      .uniqueness(Uniqueness.NODE_PATH)
      .relationships(rel, direction)
  }

  def getDirectedTraverser(current: DirectedRelationshipOnNode, next: DirectedRelationshipOnNode, first: DirectedRelationshipOnNode): Option[Traverser] = {

    val first_other_node = first.getOtherNode

    if (first_other_node == None) {
      None
    } else {

      val until_node = first_other_node.get

      val start_node: Node = if (next.node == null) current.getOtherNode.get else next.node

      // this rel cant be null, as we will have at-least one vob node here, other wise its an error
      //
      //        if (rel == null) {
      //          new Iterable[Vob] {
      //            def iterator = Iterator.empty
      //          }
      //        }


      val trav_desc = Traversal.description()
        .depthFirst()
        .uniqueness(Uniqueness.NODE_PATH)
        .relationships(next.rel, next.dir)
        .evaluator(
        new Evaluator {
          def evaluate(path: Path) = {
            if (path.endNode().equals(until_node)) {
              Evaluation.INCLUDE_AND_PRUNE;
            }
            else
              Evaluation.INCLUDE_AND_CONTINUE
          }
        }
      )
      //      if (next.node != null)
      //        trav_desc = trav_desc.evaluator(
      //          new Evaluator {
      //            var found: java.lang.Boolean = false
      //
      //            def evaluate(path: Path) = {
      //              if (path.endNode().equals(next.node)) {
      //                found = true;
      //              }
      //              if (found)
      //                Evaluation.INCLUDE_AND_CONTINUE
      //              else Evaluation.EXCLUDE_AND_CONTINUE
      //            }
      //          }
      //        )


      val ret = trav_desc.traverse(start_node)

      Some(ret)
    }

  }

  def getPaginatedTraversalDescription(rel: RelationshipType, direction: Direction = Direction.OUTGOING, untilNode: Node, limit: Int): TraversalDescription = {
    var ret = Traversal.description()
      .depthFirst()
      .uniqueness(Uniqueness.NODE_PATH)
      .relationships(rel, direction);
    if (limit > 0)
      ret = ret.evaluator(Evaluators.toDepth(limit))
    if (untilNode != null) {
      ret = ret.evaluator(
        new Evaluator {
          def evaluate(path: Path) = {
            if (path.endNode().equals(untilNode)) {
              Evaluation.INCLUDE_AND_PRUNE;
            }
            else
              Evaluation.INCLUDE_AND_CONTINUE
          }
        }
      )
    }
    ret

  }

  def shortestSinglePath(implicit depth: Int = 1, start: Node, end: Node, rel: RelationshipType, dir: Direction): Option[Path] = {
    //    PathFinder<Path>
    val finder = GraphAlgoFactory.shortestPath(Traversal.expanderForTypes(rel, dir), depth);
    val ret = finder.findSinglePath(start, end);
    if (ret == null) None
    else Some(ret)
  }

  def processPaginatedAction(actor: Node, otherNode: Node, dir: Direction, currentLinkName: String, next: String, firstLinkName: String) {

    val currentLink = actor.getSingleRelationship(currentLinkName, dir)

    if (currentLink != null) {

      otherNode --> next --> currentLink.getEndNode

      currentLink.delete()

    } else {

      actor --> firstLinkName --> otherNode

    }

    actor --> currentLinkName --> otherNode

  }


  def createPersonsFromPath(iterableToWrap: Traverser): JIterable[User] = {
    val ret = new JIterableWrapper[User, Path](iterableToWrap) {
      def underlyingObjectToObject(path: Path): User = {
        new User(path.endNode());
      }
    }
    def fi(some: JIterable[User]): FilteringIterable[User] = {
      val predicate = new Predicate[User] {
        def accept(item: User) = {
          !item.isDeleted
        }
      }
      new FilteringIterable[User](some, predicate)
    }
    fi(ret)
  }


  def createCommentsFromPath(iterableToWrap: Traverser): JIterable[Comment] = {
    val ret = new JIterableWrapper[Comment, Path](iterableToWrap) {
      def underlyingObjectToObject(path: Path): Comment = {
        new Comment(path.endNode());
      }
    }
    def fi(some: JIterable[Comment]): FilteringIterable[Comment] = {
      val predicate = new Predicate[Comment] {
        def accept(item: Comment) = {
          !item.isDeleted
        }
      }
      new FilteringIterable[Comment](some, predicate)
    }
    fi(ret)
  }

  def createVobsFromPath(iterableToWrap: Traverser): JIterable[Vob] = {
    val ret = new JIterableWrapper[Vob, Path](iterableToWrap) {
      def underlyingObjectToObject(path: Path): Vob = {
        new Vob(path.endNode());
      }
    }
    def fi(some: java.lang.Iterable[Vob]): FilteringIterable[Vob] = {
      val predicate = new Predicate[Vob] {
        def accept(item: Vob) = {
          !item.isDeleted
        }
      }
      new FilteringIterable[Vob](some, predicate)
    }
    fi(ret)
  }

  def createVobsFromRelationships(rels: JIterable[Relationship], dir: Direction): JIterable[Vob] = {
    val ret = new JIterableWrapper[Vob, Relationship](rels) {
      def underlyingObjectToObject(rel: Relationship): Vob = {
        dir match {
          case Direction.OUTGOING => new Vob(rel.getEndNode)
          case Direction.INCOMING => new Vob(rel.getStartNode)
          case _ => throw new Error("Not support both directions")

        }
      }
    }
    def fi(some: java.lang.Iterable[Vob]): FilteringIterable[Vob] = {
      val predicate = new Predicate[Vob] {
        def accept(item: Vob) = {
          !item.isDeleted
        }
      }
      new FilteringIterable[Vob](some, predicate)
    }
    fi(ret)
  }


}
