package com.voblr.graph.services.v1.impl


import com.google.protobuf.{RpcCallback, RpcController}
import org.neo4j.scala.DatabaseService
import org.neo4j.graphdb._
import org.neo4j.helpers.collection.{Iterables, IteratorUtil, PagingIterator, IterableWrapper}
import java.util.concurrent.TimeUnit
import scala.Predef._
import com.voblr.common.GraphServices._
import com.voblr.graph.VoblrGraphWrapper
import com.voblr.common.constants.GraphConstants
import com.voblr.common.proto.Movie.MovieProto
import com.voblr.graph.services.v1.Vob
import com.voblr.graph.services.v1.Movie
import com.voblr.common.proto.Vob.VobProto
import com.voblr.graph.services.v1.Movie
import com.voblr.graph.services.v1.Vob
import com.voblr.common.proto.Comment.Comments

/**
 * User: abhi@voblr.com
 * Date: 11/4/12
 * Time: 4:41 AM
 * (c) Voblr, Inc
 */
class MovieServiceImpl(dbs: DatabaseService) extends MovieService with VoblrGraphWrapper {

  val ds = dbs

  private def createOrGetMovieNode(entity_id: String): Node = {
    getMovieNode(entity_id) match {
      case x => if (x != null) {
        logger.debug(String.format("movieNode %s already exists %s", entity_id, x))
        x
      }
      else {
        val nodeIndex = getNodeIndex(EntityType.MOVIE.name()).getOrElse(fail("Failed to get Movie Index"))
        logger.debug(String.format("movieNodeIndex %s ", nodeIndex))
        withTx {
          implicit neo =>

            val now = System.currentTimeMillis()
            val movie = createNode
            //add to index
            nodeIndex +=(movie, GraphConstants.ENTITY_ID, entity_id)
            // set properties
            movie.setProperty(GraphConstants.ENTITY_ID, entity_id)
            movie.setProperty(GraphConstants.ENTITY_TYPE, EntityType.MOVIE.name())
            movie.setProperty(GraphConstants.LIKE_COUNT, 0)
            movie.setProperty(GraphConstants.VOB_COUNT, 0)
            movie.setProperty(GraphConstants.DELETED_FLAG, 0)
            movie.setProperty(GraphConstants.LATEST_ACTIVITY_TS, now)
            movie.setProperty(GraphConstants.CREATED_TS, now)

            logger.debug(String.format("movieNode %s created", entity_id))
            movie
        }
      }
    }
  }


  // service methods immplemeted from here


  def createMovie(p1: RpcController, p2: MovieProto, p3: RpcCallback[SimpleResponse]) {
    logger.debug("START- createMovie")

    createOrGetMovieNode(p2.getMovieId)
    val msg = "movie successfully created"
    val response = SimpleResponse.newBuilder()
      .setStatus("OK")
      .setMessage(msg).build()
    p3.run(response)

    logger.debug("END- createMovie")
  }

  def getAllVobs(controller: RpcController, request: VobRequest, done: RpcCallback[Vobs]) {
    _getVobs(request, done,false)
  }
  def getPopularVobs(controller: RpcController, request: VobRequest, done: RpcCallback[Vobs]) {
    _getVobs(request, done,true)
  }

  private def _getVobs(request: VobRequest, done: RpcCallback[Vobs], popular: Boolean) {

    val start = System.nanoTime()

    val resp_builder = Vobs.newBuilder()

    val movie_id = request.getObjectId

    val user_id = if (request.getSubjectId == "") null else request.getSubjectId

    val offset = request.getOffset

    val limit = request.getLimit

    try {

      val movieNode = getMovieNode(movie_id)

      val offset_ : Node = offset match {
        case "$start" => null
        case _ => (getVobNode(offset))

      }
      val m = Movie(movieNode)

      val _vobs = if(!popular) m.getVobs(offset_) else m.getPopularVobs(offset_)

      val iterable_vobProto = new IterableWrapper[VobProto, Vob](_vobs) {
        def underlyingObjectToObject(path: Vob): VobProto = {
          val vob_node = path.underlyingNode
          val proto_builder = getVobProto(vob_node).toBuilder
          if (user_id != null) {
            val vob_id = vob_node.getProperty(GraphConstants.ENTITY_ID)
            val t = vob_node.getSingleRelationship(GraphConstants.VOTE_UP + "_" + user_id + "_" + vob_id, in)
            val q = vob_node.getSingleRelationship(GraphConstants.VOTE_DOWN + "_" + user_id + "_" + vob_id, in)
            proto_builder.setUserVotedUp((t != null))
            proto_builder.setUserVotedDown((q != null))
          }
          val comments = getCommentsOnVob(vob_node)
          if (comments != null)
            proto_builder.setComments(Comments.newBuilder().addAllComments(Iterables.cache(comments)))
          proto_builder.build()
        }
      }
      logger.debug(" limit " + limit)
      val limitingIterator = new PagingIterator[VobProto](iterable_vobProto.iterator(), limit)



      logger.debug("build resp " + limitingIterator.position())
      val ret = resp_builder.setStatus("OK")
        .setMessage("Vobs Found")
        .addAllVobs(Iterables.cache(IteratorUtil.asIterable[VobProto](limitingIterator.nextPage())))

      logger.debug(" vobs added " + limitingIterator.position())
      if (limitingIterator.hasNext) {
        logger.debug(" found next " + limitingIterator.position())
        ret.setNext(limitingIterator.next().getVobId)
      } else
        logger.debug(" no next " + limitingIterator.position())
      logger.debug(" vobs done ")
      done.run(ret.build())

    } catch {
      case e => logger.error(e.getMessage)
      resp_builder.setStatus("NG")
      resp_builder.setMessage(e.getMessage)
      done.run(resp_builder.build())
    }

    logger.debug("Movie Vobs(Graph)  " + TimeUnit.NANOSECONDS.toMillis(System.nanoTime - start) + " ms")
  }
}
