package com.voblr.graph.services.v1

import org.neo4j.graphdb.{Path, Direction, Node}
import org.neo4j.scala.Neo4jWrapperImplicits
import com.voblr.graph.GraphUtil
import com.voblr.common.constants.GraphConstants
import org.neo4j.kernel.{Uniqueness, Traversal}
import org.neo4j.graphdb.traversal.{Evaluation, Evaluator, Evaluators}

/**
 * User: abhi@voblr.com
 * Date: 12/17/12
 * Time: 8:40 PM
 * (c) Voblr, Inc
 *
 *
 *
 */

//case class Vob[S <: VoblrNode, O <: VoblrNode](underlyingNode: Node, f1: Node => S, f2: Node => O) extends VoblrNode with Neo4jWrapperImplicits with GraphUtil {
case class Vob(underlyingNode: Node) extends VoblrNode with Neo4jWrapperImplicits with GraphUtil {

  def id = underlyingNode.getProperty(GraphConstants.ENTITY_ID)

  def getSubjectId: String = underlyingNode.getProperty(GraphConstants.SUBJECT_ID).asInstanceOf[String]

  def getSubjectType: String = underlyingNode.getProperty(GraphConstants.SUBJECT_TYPE).asInstanceOf[String]

  def getObjectId: String = underlyingNode.getProperty(GraphConstants.OBJECT_ID).asInstanceOf[String]

  def getObjectType: String = underlyingNode.getProperty(GraphConstants.OBJECT_TYPE).asInstanceOf[String]

  def isDeleted: Boolean = underlyingNode.getProperty(GraphConstants.DELETED_FLAG, 0).asInstanceOf[Int] != 0

  def getPopularityIndex: Double = underlyingNode.getProperty(GraphConstants.POPULARITY_INDEX).asInstanceOf[Double]

  def getCreatedTS: java.lang.Long = underlyingNode.getProperty(GraphConstants.CREATED_TS).asInstanceOf[java.lang.Long]


  def incr_comment_count = {

    val old_count = underlyingNode.getProperty(GraphConstants.COMMENTS_COUNT).asInstanceOf[Int]

    underlyingNode.setProperty(GraphConstants.COMMENTS_COUNT, old_count + 1)

  }

  def incr_vote_up_count = {

    val old_count = underlyingNode.getProperty(GraphConstants.VOTE_UP_COUNT).asInstanceOf[Int]

    underlyingNode.setProperty(GraphConstants.VOTE_UP_COUNT, old_count + 1)

  }

  def incr_vote_down_count = {

    val old_count = underlyingNode.getProperty(GraphConstants.VOTE_DOWN_COUNT).asInstanceOf[Int]

    underlyingNode.setProperty(GraphConstants.VOTE_DOWN_COUNT, old_count + 1)

  }

  // internal method to update link.. note the gets in method name :)
  // this is inward action
  def getsComment(comment: Comment): Unit = {

    incr_comment_count

    processPaginatedAction(underlyingNode, comment.underlyingNode, out, GraphConstants.VOB_COMMENT, GraphConstants.NEXT_COMMENT, GraphConstants.FIRST_COMMENT)

    underlyingNode.setProperty(GraphConstants.LATEST_ACTIVITY_TS, comment.underlyingNode.getProperty(GraphConstants.CREATED_TS).asInstanceOf[Long])
  }


  private def commentsTraversal(implicit limit: Int = 0, dir: Direction) = {
    val ret = Traversal.description()
      .breadthFirst()
      .relationships(GraphConstants.FOLLOWS, dir)
      .uniqueness(Uniqueness.NODE_GLOBAL)
      .evaluator(Evaluators.toDepth(1))
      .evaluator(Evaluators.excludeStartPosition());

    if (limit != 0) ret.evaluator(
      new Evaluator {
        def evaluate(path: Path) = {
          if (path.length() == limit) {
            Evaluation.EXCLUDE_AND_PRUNE
          }
          else Evaluation.INCLUDE_AND_CONTINUE
        }
      })
    else ret
  }

  def getComments(implicit limit: Int = 0, comment: Comment): Unit = {
    // return all comments on Vob, by default

    val trav_desc = commentsTraversal(limit, Direction.OUTGOING)

    createPersonsFromPath(trav_desc.traverse(underlyingNode))

    processPaginatedAction(underlyingNode, comment.underlyingNode, out, GraphConstants.VOB_COMMENT, GraphConstants.NEXT_COMMENT, GraphConstants.FIRST_COMMENT)

    underlyingNode.setProperty(GraphConstants.LATEST_ACTIVITY_TS, comment.underlyingNode.getProperty(GraphConstants.CREATED_TS).asInstanceOf[Long])
  }

  def remove() = {
    //throw new UnsupportedOperationException("Don't know how to do that...");
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG,1)
  }

  def undoRemove() = {
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG,0)
  }

}


