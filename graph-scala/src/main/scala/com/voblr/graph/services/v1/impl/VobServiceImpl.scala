package com.voblr.graph.services.v1.impl

import com.google.protobuf.{RpcCallback, RpcController}
import com.voblr.common.GraphServices._
import com.voblr.common.proto.Movie.MovieProto
import org.neo4j.scala.DatabaseService
import com.voblr.graph.VoblrGraphWrapper
import com.voblr.common.proto.Vob.VobProto
import com.voblr.graph.util.VobLogger
import com.voblr.common.constants.GraphConstants
import org.neo4j.graphdb.{Relationship, Node}
import com.voblr.graph.services.v1.{User, Vob}

/**
 * User: abhi@voblr.com
 * Date: 11/4/12
 * Time: 4:41 AM
 * (c) Voblr, Inc
 */
class VobServiceImpl(dbs: DatabaseService) extends VobService with VoblrGraphWrapper {

  val ds = dbs

  /**
   *
   * Creates or gets existing comment
   */
  private def createOrGetVob(vob_txt: String, objectNode: Node, subjectNode: Node, now: java.lang.Long): Node = {

    val objectType = objectNode.getProperty(GraphConstants.ENTITY_TYPE)
    val objectId = objectNode.getProperty(GraphConstants.ENTITY_ID)
    val subjectType = subjectNode.getProperty(GraphConstants.ENTITY_TYPE)
    val subjectId = subjectNode.getProperty(GraphConstants.ENTITY_ID)

    val entity_id = objectId.toString + "_" + subjectId.toString + "_" + now
    logger.debug("entity_id : " + entity_id)
    getVobNode(entity_id) match {
      case x => if (x != null) {
        logger.debug(String.format("vobNode %s exists %s", entity_id, x))
        x
      }
      else {

        val nodeIndex = getNodeIndex(EntityType.VOB_ENTITY.name()).getOrElse(fail("Failed to get  index " + EntityType.VOB_ENTITY))
        logger.debug(String.format("vobNodeIndex %s ", nodeIndex))
        withTx {
          implicit neo =>
            val vobNode = createNode
            //add to index
            nodeIndex +=(vobNode, GraphConstants.ENTITY_ID, entity_id)
            // set properties
            vobNode.setProperty(GraphConstants.ENTITY_ID, entity_id)
            vobNode.setProperty(GraphConstants.ENTITY_TYPE, EntityType.VOB_ENTITY.name())
            vobNode.setProperty(GraphConstants.VOB_TXT, vob_txt)
            vobNode.setProperty(GraphConstants.OBJECT_TYPE, objectType)
            vobNode.setProperty(GraphConstants.OBJECT_ID, objectId)
            vobNode.setProperty(GraphConstants.SUBJECT_TYPE, subjectType)
            vobNode.setProperty(GraphConstants.SUBJECT_ID, subjectId)
            vobNode.setProperty(GraphConstants.VOB_TS, now)
            // like counts
            vobNode.setProperty(GraphConstants.LIKE_COUNT, 0)
            vobNode.setProperty(GraphConstants.COMMENTS_COUNT, 0)
            vobNode.setProperty(GraphConstants.VOTE_UP_COUNT, 0)
            vobNode.setProperty(GraphConstants.VOTE_DOWN_COUNT, 0)
            vobNode.setProperty(GraphConstants.POPULARITY_INDEX, 1.0)
            vobNode.setProperty(GraphConstants.CREATED_TS, now)
            vobNode.setProperty(GraphConstants.DELETED_FLAG, 0)
            vobNode.setProperty(GraphConstants.LATEST_ACTIVITY_TS, now)

            //relate(subjectNode, vobNode, GraphConstants.USER_VOB)

            // FIXME we should not repeat creating links between vobNode & Movie node if one already exists?
            // relate method can have an extra argument like force=true (by default) for that purpose
            //relate(vobNode, objectNode, GraphConstants.VOB_MOVIE)

            val c = subjectNode[Int](GraphConstants.VOB_COUNT)
            subjectNode(GraphConstants.VOB_COUNT) = c.getOrElse(0) + 1
            subjectNode(GraphConstants.LATEST_ACTIVITY_TS) = now


            // user --> comment
            val recent_user_vob: Relationship = subjectNode.getSingleRelationship(GraphConstants.USER_VOB, out)

            if (recent_user_vob != null) {
              vobNode --> GraphConstants.NEXT_VOB --> recent_user_vob.getEndNode
              recent_user_vob.delete()
            } else {
              subjectNode --> GraphConstants.FIRST_VOB --> vobNode
            }

            // comment --> movie
            val recent_movie_vob: Relationship = objectNode.getSingleRelationship(GraphConstants.VOB_MOVIE, in)

            if (recent_movie_vob != null) {
              vobNode --> GraphConstants.NEXT_VOB_MOVIE --> recent_movie_vob.getStartNode
              recent_movie_vob.delete()
            } else {
              vobNode --> GraphConstants.FIRST_VOB --> objectNode
            }
            subjectNode --> GraphConstants.USER_VOB --> vobNode --> GraphConstants.VOB_MOVIE --> objectNode

            // get subjects current reputation
            val old_rep = User(subjectNode).reputation
            // currently increase reputation by 5, more reputaion for upvoting
            subjectNode.setProperty(GraphConstants.REPUTATION, 5 + old_rep)

            val d = objectNode[Int](GraphConstants.VOB_COUNT)
            objectNode(GraphConstants.VOB_COUNT) = d.getOrElse(0) + 1
            objectNode(GraphConstants.LATEST_ACTIVITY_TS) = now
            logger.debug(String.format("vobNode %s created", vobNode))
            vobNode
        }
      }
    }

  }


  def doVob(p1: RpcController, p2: VobRequest, p3: RpcCallback[SimpleResponse]) {

    logger.debug("START-> doVob")

    logger.debug("START-> doVob --> objectId " + p2.getObjectId)

    logger.debug("START-> doVob --> subjectId " + p2.getSubjectId)

    logger.debug("START-> doVob --> vobtext " + p2.getVobText)


    val objectNode = getMovieNode(p2.getObjectId)

    // since subject is usually (mostly) an comment
    val subjectNode = getUserNode(p2.getSubjectId)

    val vobTxt = p2.getVobText

    val vobTS = p2.getVobTime

    try {
      val vob_node = createOrGetVob(vobTxt, objectNode, subjectNode, vobTS)
      val msg = p2.getSubjectId + " successfully vobbed " + p2.getObjectId
      val response = SimpleResponse.newBuilder()
        .setStatus("OK")
        .setMessage(msg).build()
      p3.run(response)

      val notifierThread = new Thread(FrindsNotifier(subjectNode, vob_node))

      notifierThread.start()

    } catch {
      case e => {
        e.printStackTrace()
        val errMsg = (subjectNode, objectNode) match {
          case (null, null) => p2.getSubjectId + " and " + p2.getObjectId + " doesn't exist"
          case (null, x) => p2.getSubjectId + " doesn't exist"
          case (x, null) => p2.getObjectId + " doesn't exist"
          case _ => "failed to comment : " + e.getMessage
        }
        logger.error(errMsg)
        val response = SimpleResponse.newBuilder()
          .setStatus("NG")
          .setMessage(errMsg).build()
        p3.run(response)
      }
    }
    logger.debug("END-> doVob")
  }

  case class FrindsNotifier(user: Node, vob: Node) extends Runnable {
    val rels = getConnected(in, user)

    def run(): Unit = {
      try {
        logger.debug("START-> doVob FrindsNotifier")
        withTx {
          neo =>
            for (rel <- rels.iterator()) {
              val friend = rel.endNode()
              logger.debug("friend " + friend.getProperty(GraphConstants.ENTITY_ID) + " " + friend)

              val old = friend.getSingleRelationship((friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.CURRENT_FEED), out)

              if (old == null) {
                friend --> (friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.START_FEED) --> vob
              } else {
                vob --> (friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.NEXT_FEED) --> old.getEndNode
                old.delete()
              }
              friend --> (friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.CURRENT_FEED) --> vob

            }
        }
        logger.debug("END-> doVob FrindsNotifier")
      } catch {
        case e => logger.error("Failed to notify friends of " + user + " for vob " + vob)
        e.printStackTrace()
      }
    }
  }

  def doVoteUp(p1: RpcController, p2: VoteOnVobRequest, p3: RpcCallback[SimpleResponse]) {
    logger.debug("START-> doVoteUp")
    val user_id = p2.getUserId
    val vob_id = p2.getVobId
    val vote_type = p2.getType

    val user_node = getUserNode(user_id)

    val vob_node = getVobNode(vob_id)

    val relationship_name = GraphConstants.VOTE_UP + "_" + user_id + "_" + vob_id

    val rel = user_node.getSingleRelationship(relationship_name, out)

    if (rel == null) {
      try {
        withTx {
          neo =>
          //unique relationship
            user_node --> relationship_name --> vob_node

            user_node --> GraphConstants.VOTE_UP --> vob_node

            val old_count = vob_node[Int](GraphConstants.VOTE_UP_COUNT)

            vob_node.setProperty(GraphConstants.VOTE_UP_COUNT, old_count.getOrElse(0) + 1)

            val old_popularity_idx = vob_node[Double](GraphConstants.POPULARITY_INDEX)
            vob_node.setProperty(GraphConstants.POPULARITY_INDEX,
              old_popularity_idx.getOrElse(0.0) + 1.0)
            // user whose vob got upvoted
            val recipient_user_node = getUserNode(Vob(vob_node).getSubjectId)
            val old_rep = User(recipient_user_node).reputation
            // currently increase reputation by 5
            recipient_user_node.setProperty(GraphConstants.REPUTATION, 5 + old_rep)
        }
        val response = SimpleResponse.newBuilder()
          .setStatus("OK")
          .setMessage(user_id + " successfully voted up " + vob_id).build()
        p3.run(response)
      } catch {
        case e => {
          e.printStackTrace()
          val errMsg = (user_node, vob_node) match {
            case (null, null) => p2.getUserId + " and " + p2.getVobId + " doesn't exist"
            case (null, x) => p2.getUserId + " doesn't exist"
            case (x, null) => p2.getVobId + " doesn't exist"
            case _ => "failed to vote up  : " + e.getMessage
          }
          logger.error(errMsg)
          val response = SimpleResponse.newBuilder()
            .setStatus("NG")
            .setMessage(errMsg).build()
          p3.run(response)
        }
      }
    } else {
      val response = SimpleResponse.newBuilder()
        .setStatus("NG")
        .setMessage(user_id + " has already voted up " + vob_id).build()
      p3.run(response)
    }

    logger.debug("END-> doVoteUp")

  }

  def doVoteDown(p1: RpcController, p2: VoteOnVobRequest, p3: RpcCallback[SimpleResponse]) {

    val user_id = p2.getUserId
    val vob_id = p2.getVobId
    val vote_type = p2.getType

    val user_node = getUserNode(user_id)

    val vob_node = getVobNode(vob_id)

    val relationship_name = GraphConstants.VOTE_DOWN + "_" + user_id + "_" + vob_id

    val rel = user_node.getSingleRelationship(relationship_name, out)

    if (rel == null) {
      try {
        withTx {
          neo =>
          //unique relationship
            user_node --> relationship_name --> vob_node

            user_node --> GraphConstants.VOTE_DOWN --> vob_node

            val old_count = vob_node[Int](GraphConstants.VOTE_DOWN_COUNT)

            vob_node.setProperty(GraphConstants.VOTE_DOWN_COUNT, old_count.getOrElse(0) + 1)
            val old_popularity_idx = vob_node[Double](GraphConstants.POPULARITY_INDEX)
            vob_node.setProperty(GraphConstants.POPULARITY_INDEX,
              old_popularity_idx.getOrElse(0.0) - 0.5)
            // user whose vob got upvoted
            val recipient_user_node = getUserNode(Vob(vob_node).getSubjectId)
            val old_rep = User(recipient_user_node).reputation
            // currently increase reputation by 5
            recipient_user_node.setProperty(GraphConstants.REPUTATION, 5 + old_rep)
        }
        val response = SimpleResponse.newBuilder()
          .setStatus("OK")
          .setMessage(user_id + " successfully voted down " + vob_id).build()
        p3.run(response)
      } catch {
        case e => {
          e.printStackTrace()
          val errMsg = (user_node, vob_node) match {
            case (null, null) => p2.getUserId + " and " + p2.getVobId + " doesn't exist"
            case (null, x) => p2.getUserId + " doesn't exist"
            case (x, null) => p2.getVobId + " doesn't exist"
            case _ => "failed to vote down  : " + e.getMessage
          }
          logger.error(errMsg)
          val response = SimpleResponse.newBuilder()
            .setStatus("NG")
            .setMessage(errMsg).build()
          p3.run(response)
        }
      }
    } else {
      val response = SimpleResponse.newBuilder()
        .setStatus("NG")
        .setMessage(user_id + " has already voted up " + vob_id).build()
      p3.run(response)
    }


  }
}
