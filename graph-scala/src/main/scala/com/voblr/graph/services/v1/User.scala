package com.voblr.graph.services.v1

import org.neo4j.graphdb._
import com.voblr.common.constants.GraphConstants
import org.neo4j.kernel.{Uniqueness, Traversal}
import traversal.{Evaluator, Evaluation, Evaluators}
import com.voblr.graph.{DirectedRelationshipOnNode, GraphUtil}
import java.lang.{Iterable => JIterable}
import org.neo4j.helpers.collection.{FilteringIterable, IteratorUtil, PositionedIterator, Iterables}
import java.util.{Collections, Comparator}
import java.util.{Iterator => JIterator}
import org.neo4j.helpers.Predicate

// this gives support for --> <-- notations also infers Strings as Relationships where required

import org.neo4j.scala.Neo4jWrapperImplicits

/**
 * User: abhi@voblr.com
 * Date: 12/9/12
 * Time: 7:23 PM
 * (c) Voblr, Inc
 *
 *
 * This class wraps User node, and gives useful operations on it.
 *
 * Note : This wont do anything unless put into transactions : withTx { .. } , usually
 * taken care by Service classes
 *
 * @param underlyingNode
 */
case class User(underlyingNode: Node) extends VoblrNode with Neo4jWrapperImplicits with GraphUtil {

  require(underlyingNode != null, this.getClass.getName + " is null")

  def id = underlyingNode.getProperty(GraphConstants.ENTITY_ID)

  def isDeleted: Boolean = underlyingNode.getProperty(GraphConstants.DELETED_FLAG, 0).asInstanceOf[Int] != 0

  def reputation: Long = {
    val r : Long = (underlyingNode.getProperty(GraphConstants.REPUTATION, -1L)) match {
      case i: Object => java.lang.Long.parseLong(i+"")
      case _ => -1L
    }
    if (r > -1L) {
      r
    } else {
      var rep = 0L
      import scala.collection.JavaConversions._
      val vobs = getVobs(null)
      for (v <- vobs) {
        rep += (v.getPopularityIndex * 10L).asInstanceOf[Long]
      }
      rep
    }
  }

  private def friendTraversal(implicit limit: Int = 0, dir: Direction) = {
    val ret = Traversal.description()
      .breadthFirst()
      .relationships(GraphConstants.FOLLOWS, dir)
      .uniqueness(Uniqueness.NODE_GLOBAL)
      .evaluator(Evaluators.toDepth(1))
      .evaluator(Evaluators.excludeStartPosition());

    if (limit != 0) ret.evaluator(
      new Evaluator {
        def evaluate(path: Path) = {
          if (path.length() == limit) {
            Evaluation.EXCLUDE_AND_PRUNE
          }
          else Evaluation.INCLUDE_AND_CONTINUE
        }
      })
    else ret
  }


  def incr_comment_count = {

    val old_count = underlyingNode.getProperty(GraphConstants.COMMENTS_COUNT).asInstanceOf[Int]

    underlyingNode.setProperty(GraphConstants.COMMENTS_COUNT, old_count + 1)

  }


  def incr_vob_count = {

    val old_count = underlyingNode.getProperty(GraphConstants.VOB_COUNT).asInstanceOf[Int]

    underlyingNode.setProperty(GraphConstants.VOB_COUNT, old_count + 1)

  }


  def followers(implicit limit: Int = 0): JIterable[User] = {
    // return all my followers   , by default

    val trav_desc = friendTraversal(limit, Direction.INCOMING)

    createPersonsFromPath(trav_desc.traverse(underlyingNode))
  }


  def following(implicit limit: Int = 0): JIterable[User] = {
    // return all my followers, by default

    val trav_desc = friendTraversal(limit, Direction.OUTGOING)

    createPersonsFromPath(trav_desc.traverse(underlyingNode))
  }


  def isFollowedBy(other: User): Boolean = {
    shortestSinglePath(1, underlyingNode, other.underlyingNode, GraphConstants.FOLLOWS, Direction.INCOMING) match {
      case Some(x) => true
      case None => false
    }
  }

  def isFollowing(other: User): Boolean = {
    shortestSinglePath(1, underlyingNode, other.underlyingNode, GraphConstants.FOLLOWS, Direction.OUTGOING) match {
      case Some(x) => true
      case None => false
    }
  }


  def getVobs(fromVob: Node): JIterable[Vob] = {

    val first = DirectedRelationshipOnNode(underlyingNode, GraphConstants.FIRST_VOB, Direction.OUTGOING)

    val next = DirectedRelationshipOnNode(fromVob, GraphConstants.NEXT_VOB, Direction.OUTGOING)

    val current = DirectedRelationshipOnNode(underlyingNode, GraphConstants.USER_VOB, Direction.OUTGOING)


    getDirectedTraverser(current, next, first) match {
      case None => Iterables.empty[Vob]()
      case Some(x) => createVobsFromPath(x)
    }
  }


  def getFeed(fromVob: Node): JIterable[Vob] = {

    val first = DirectedRelationshipOnNode(underlyingNode, id + "_" + GraphConstants.START_FEED, Direction.OUTGOING)

    val next = DirectedRelationshipOnNode(fromVob, id + "_" + GraphConstants.NEXT_FEED, Direction.OUTGOING)

    val current = DirectedRelationshipOnNode(underlyingNode, id + "_" + GraphConstants.CURRENT_FEED, Direction.OUTGOING)


    getDirectedTraverser(current, next, first) match {
      case None => Iterables.empty[Vob]()
      case Some(x) => createVobsFromPath(x)
    }
  }


  def doVoteUp(vob: Vob) {

    val relationship_name = GraphConstants.VOTE_UP + "_" + id + "_" + vob.id

    if (underlyingNode.getSingleRelationship(relationship_name, Direction.OUTGOING) != null)
      throw new Error(id + " has already voted up " + vob.id)

    this.underlyingNode --> relationship_name --> vob.underlyingNode

    this.underlyingNode --> GraphConstants.VOTE_UP --> vob.underlyingNode

    vob.incr_vote_up_count
  }

  def doVoteDown(vob: Vob) {

    val relationship_name = GraphConstants.VOTE_DOWN + "_" + id + "_" + vob.id

    if (underlyingNode.getSingleRelationship(relationship_name, Direction.OUTGOING) != null)
      throw new Error(id + " has already voted down " + vob.id)

    this.underlyingNode --> relationship_name --> vob.underlyingNode

    this.underlyingNode --> GraphConstants.VOTE_DOWN --> vob.underlyingNode

    vob.incr_vote_down_count
  }


  def doComment(vob: Vob, comment: Comment) {

    incr_comment_count

    processPaginatedAction(underlyingNode, vob.underlyingNode, out, GraphConstants.COMMENTS, id + "_" + GraphConstants.NEXT_COMMENT + "_" + vob.id, GraphConstants.FIRST_COMMENT + "_" + vob.id)

    vob.getsComment(comment)

    underlyingNode.setProperty(GraphConstants.LATEST_ACTIVITY_TS, comment.underlyingNode.getProperty(GraphConstants.CREATED_TS).asInstanceOf[Long])

  }


  def doVob(vob: Vob, movie: Movie) {

    incr_vob_count

    processPaginatedAction(underlyingNode, vob.underlyingNode, out, GraphConstants.USER_VOB, GraphConstants.NEXT_VOB, GraphConstants.FIRST_VOB)

    movie.getsVob(vob)

    underlyingNode.setProperty(GraphConstants.LATEST_ACTIVITY_TS, vob.underlyingNode.getProperty(GraphConstants.CREATED_TS).asInstanceOf[Long])

  }


  def getNewsFeed(fromVob: Node): JIterable[Vob] = {

    val ret = IteratorUtil.asIterable(new FriendsNewsFeedUpdateIterator(this))
    if (fromVob == null) {
      ret
    }
    else {

      def fi(some: java.lang.Iterable[Vob]): FilteringIterable[Vob] = {
        val ab = Vob(fromVob)
        val predicate = new Predicate[Vob] {
          def accept(item: Vob) = {
            item.getCreatedTS <= ab.getCreatedTS
          }
        }
        new FilteringIterable[Vob](some, predicate)
      }
      fi(ret)
    }
  }

  def getVotedUpVobs(fromVob: Node): JIterable[Vob] = {

    val rels = underlyingNode.getRelationships(GraphConstants.VOTE_UP, out)

    val ret = createVobsFromRelationships(rels, out)
    if (fromVob == null) {
      ret
    }
    else {

      def fi(some: java.lang.Iterable[Vob]): FilteringIterable[Vob] = {
        val ab = Vob(fromVob)
        val predicate = new Predicate[Vob] {
          def accept(item: Vob) = {
            item.getCreatedTS <= ab.getCreatedTS
          }
        }
        new FilteringIterable[Vob](some, predicate)
      }
      fi(ret)
    }
  }


  private class VobComparator extends Comparator[PositionedIterator[Vob]] {
    def compare(a: PositionedIterator[Vob], b: PositionedIterator[Vob]): Int = {
      return -a.current().getCreatedTS.compareTo(b.current().getCreatedTS);
    }
  }

  // this class's complexity is un-acceptable in long run
  // NOTE it
  class FriendsNewsFeedUpdateIterator() extends JIterator[Vob] {
    private val vobs: java.util.ArrayList[PositionedIterator[Vob]] = new java.util.ArrayList[PositionedIterator[Vob]]
    private val comparator = new VobComparator()

    def this(user: User) = {
      this()
      println(" Getting vobs for ")
      for (friend: User <- user.following.iterator()) {
        val iterator: JIterator[Vob] = friend.getVobs(null).iterator()
        if (iterator.hasNext) {
          println(" getting newsfeed for user id " + friend.id)
          vobs.add(new PositionedIterator[Vob](iterator))
        }
      }
      sort();
    }


    def sort(): Unit = {
      Collections.sort(vobs, comparator);
    }

    def hasNext = vobs.size() > 0

    def next(): Vob = {
      if (vobs.size() == 0) {
        throw new NoSuchElementException();
      }
      val first = vobs.get(0);
      val returnVal = first.current();

      if (!first.hasNext()) {
        vobs.remove(0);
      }
      else {
        first.next();
        sort();
      }

      returnVal
    }

    def remove() = throw new UnsupportedOperationException("Don't know how to do that...");
  }

  def remove() = {
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG, 1)
  }

  def undoRemove() = {
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG, 0)
  }


}