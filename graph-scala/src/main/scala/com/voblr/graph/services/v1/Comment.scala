package com.voblr.graph.services.v1

import org.neo4j.graphdb.Node
import com.voblr.common.constants.GraphConstants

/**
 * User: abhi@voblr.com
 * Date: 12/17/12
 * Time: 11:13 PM
 * (c) Voblr, Inc
 *
 *
 *
 */
case class Comment(underlyingNode :Node) extends VoblrNode{
  def id = underlyingNode.getProperty(GraphConstants.ENTITY_ID)

  def isDeleted: Boolean = underlyingNode.getProperty(GraphConstants.DELETED_FLAG, 0).asInstanceOf[Int] != 0

  def remove() = {
    //throw new UnsupportedOperationException("Don't know how to do that...");
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG,1)
  }

  def undoRemove() = {
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG,0)
  }

}
