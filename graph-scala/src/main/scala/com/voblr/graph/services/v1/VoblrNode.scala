package com.voblr.graph.services.v1

import org.neo4j.graphdb.Node
import org.neo4j.scala.{Neo4jIndexProvider, Neo4jWrapperImplicits}
import com.voblr.common.constants.GraphConstants
import com.voblr.common.GraphServices.EntityType

/**
 * User: abhi@voblr.com
 * Date: 12/17/12
 * Time: 8:38 PM
 * (c) Voblr, Inc
 *
 *
 *
 */
abstract class VoblrNode{
  def underlyingNode : Node
}

object NodeType extends Enumeration(EntityType.USER.name(), EntityType.MOVIE.name(), EntityType.VOB_ENTITY.name()) {
  type NodeType = Value
  val USER, MOVIE, VOB = Value
}

object NodeUtils extends Neo4jWrapperImplicits {

  def f1(node: Node): User = {
    val subject = node.getProperty(GraphConstants.SUBJECT_ID)
    null
  }

  def f2(node: Node): Movie = {
    null
  }

}


object NodeNothing extends VoblrNode {
  def underlyingNode = null
}