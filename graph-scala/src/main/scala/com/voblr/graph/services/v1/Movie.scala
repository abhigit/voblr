package com.voblr.graph.services.v1

import org.neo4j.graphdb.{Direction, Relationship, Node}
import com.voblr.common.constants.GraphConstants
import org.neo4j.scala.Neo4jWrapperImplicits
import com.voblr.graph.{DirectedRelationshipOnNode, GraphUtil}
import java.lang.{Iterable => JIterable}
import collection.mutable
import java.util.{Collections, Comparator}
import org.apache.commons.collections.IteratorUtils
import org.neo4j.helpers.collection.{FilteringIterable, Iterables, IteratorUtil}
import org.neo4j.helpers.Predicate

/**
 * User: abhi@voblr.com
 * Date: 12/17/12
 * Time: 8:48 PM
 * (c) Voblr, Inc
 *
 *
 *
 */


case class Movie(underlyingNode: Node) extends VoblrNode with Neo4jWrapperImplicits with GraphUtil {

  def id = underlyingNode.getProperty(GraphConstants.ENTITY_ID)

  def isDeleted: Boolean = underlyingNode.getProperty(GraphConstants.DELETED_FLAG, 0).asInstanceOf[Int] != 0


  def incr_vob_count = {

    val old_count = underlyingNode.getProperty(GraphConstants.VOB_COUNT).asInstanceOf[Int]

    underlyingNode.setProperty(GraphConstants.VOB_COUNT, old_count + 1)

  }


  def getsVob(vob: Vob): Unit = {

    // vob --> movie
    val recent_vob_on_movie: Relationship = underlyingNode.getSingleRelationship(GraphConstants.VOB_MOVIE, Direction.INCOMING)

    if (recent_vob_on_movie != null) {
      vob.underlyingNode --> GraphConstants.NEXT_VOB_MOVIE --> recent_vob_on_movie.getStartNode

      recent_vob_on_movie.delete()

    } else {

      vob.underlyingNode --> GraphConstants.FIRST_VOB --> underlyingNode

    }

    vob.underlyingNode --> GraphConstants.VOB_MOVIE --> underlyingNode

    underlyingNode.setProperty(GraphConstants.LATEST_ACTIVITY_TS, vob.underlyingNode.getProperty(GraphConstants.CREATED_TS).asInstanceOf[Long])

    incr_vob_count

  }

  def getVobs(from: Node): JIterable[Vob] = {

    val first = DirectedRelationshipOnNode(underlyingNode, GraphConstants.FIRST_VOB, Direction.INCOMING)

    val next = DirectedRelationshipOnNode(from, GraphConstants.NEXT_VOB_MOVIE, Direction.OUTGOING)

    val current = DirectedRelationshipOnNode(underlyingNode, GraphConstants.VOB_MOVIE, Direction.INCOMING)


    getDirectedTraverser(current, next, first) match {
      case None => Iterables.empty[Vob]()
      case Some(x) => createVobsFromPath(x)
    }
  }

  def getPopularVobs(from: Node): JIterable[Vob] = {
    val pq = new Comparator[Vob] {
      def compare(o1: Vob, o2: Vob): Int = {
        val diff = o1.getPopularityIndex - o2.getPopularityIndex
        if (diff > 0) -1
        else if (diff < 0) 1
        else o1.getCreatedTS.compareTo(o2.getCreatedTS)
      }
    }
    if (from != null) {
      val tmp = Vob(from)
      val predicate = new Predicate[Vob] {
        def accept(item: Vob): Boolean =
          (item.getCreatedTS >= tmp.getCreatedTS && item.getPopularityIndex <= tmp.getPopularityIndex)
      }
      val list: java.util.List[Vob] = Iterables.toList(new FilteringIterable[Vob](getVobs(null), predicate))
      Collections.sort(list, pq)
      list
    } else {
      val list: java.util.List[Vob] = Iterables.toList(getVobs(null))
      Collections.sort(list, pq)
      list
    }
  }

  def remove() = {
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG,1)
  }

  def undoRemove() = {
    underlyingNode.setProperty(GraphConstants.DELETED_FLAG,0)
  }

}
