package com.voblr.graph.services.v1.impl

import com.google.protobuf.{RpcCallback, RpcController}
import com.voblr.common.GraphServices._
import org.neo4j.graphdb.{Relationship, Node}
import org.neo4j.scala.DatabaseService
import org.neo4j.helpers.collection._
import com.voblr.common.GraphServices.UserService
import com.voblr.graph.VoblrGraphWrapper
import com.voblr.graph.util.VobLogger
import com.voblr.common.GraphServices.EntityType
import com.voblr.common.constants.GraphConstants
import com.voblr.common.proto.User.UserProto
import com.voblr.common.GraphServices.SimpleResponse
import com.voblr.common.GraphServices.FollowRequest
import com.voblr.common.GraphServices.VobRequest
import com.voblr.common.proto.Vob.VobProto
import com.voblr.common.GraphServices.VoteOnVobRequest
import com.voblr.common.GraphServices.CommentRequest
import com.voblr.graph.services.v1.User
import com.voblr.graph.services.v1.Vob
import java.util.Date
import com.voblr.common.proto.Comment.Comments

/**
 * User: abhi@voblr.com
 * Date: 11/4/12
 * Time: 4:37 AM
 * (c) Voblr, Inc
 *
 *
 * Extending trait VoblrGraphWrapper will include an implicit object ds of type DatabaseService.
 *
 */
class UserServiceImpl(dbs: DatabaseService) extends UserService with VoblrGraphWrapper with VobLogger {


  val ds = dbs


  private def createOrGetUserNode(entity_id: String): Node = {
    getUserNode(entity_id) match {
      case x => if (x != null) {
        logger.debug(String.format("userNode %s already exists %s", entity_id, x))
        x
      }
      else {
        val nodeIndex = getNodeIndex(EntityType.USER.name()).getOrElse(fail("Failed to get  index " + EntityType.USER))
        logger.debug(String.format("userNodeIndex %s ", nodeIndex))
        withTx {
          implicit neo =>
            val now = System.currentTimeMillis()
            val user = createNode
            //add to index
            nodeIndex +=(user, GraphConstants.ENTITY_ID, entity_id)
            // set properties
            user.setProperty(GraphConstants.ENTITY_ID, entity_id)
            user.setProperty(GraphConstants.ENTITY_TYPE, EntityType.USER.name())
            user.setProperty(GraphConstants.FOLLOWERS_COUNT, 0)
            user.setProperty(GraphConstants.FOLLOWING_COUNT, 0)
            user.setProperty(GraphConstants.VOB_COUNT, 0)
            user.setProperty(GraphConstants.DELETED_FLAG, 0)
            user.setProperty(GraphConstants.LATEST_ACTIVITY_TS, now)
            user.setProperty(GraphConstants.CREATED_TS, now)
            user.setProperty(GraphConstants.REPUTATION, 0)
            logger.debug(String.format("userNode %s created", entity_id))
            user
        }
      }
    }


  }


  // TODO add a check to see if fromNode already follows toNode
  private def follow(fromNode: Node, toNode: Node) {

    val relationships = fromNode.getRelationships(GraphConstants.FOLLOWS, out)
    for (rel: Relationship <- relationships.iterator()) {
      if (rel.getEndNode.getId == toNode.getId)
        fail(fromNode.getProperty(GraphConstants.ENTITY_ID) + " already follows " + toNode.getProperty(GraphConstants.ENTITY_ID))
    }
    withTx {
      implicit neo =>
        fromNode --> GraphConstants.FOLLOWS --> toNode

        val c = fromNode[Int](GraphConstants.FOLLOWING_COUNT)
        fromNode(GraphConstants.FOLLOWING_COUNT) = c.getOrElse(0) + 1

        val d = fromNode[Int](GraphConstants.FOLLOWERS_COUNT)
        toNode(GraphConstants.FOLLOWERS_COUNT) = d.getOrElse(0) + 1

        val rf = User(fromNode).reputation
        val rt = User(toNode).reputation
        toNode(GraphConstants.REPUTATION) = rt + (rf/1000)

        fromNode(GraphConstants.LATEST_ACTIVITY_TS) = System.currentTimeMillis()
    }
  }

  // service methods immplemeted from here

  def createUser(p1: RpcController, p2: UserProto, p3: RpcCallback[SimpleResponse]) {
    logger.debug("START- CreateUser")

    val o = createOrGetUserNode(p2.getUserId)

    logger.debug("END- CreateUser " + o)

    val msg = "user successfully created"
    val response = SimpleResponse.newBuilder()
      .setStatus("OK")
      .setMessage(msg).build()
    p3.run(response)
  }

  def doFollow(p1: RpcController, p2: FollowRequest, p3: RpcCallback[SimpleResponse]) {
    logger.debug("START- doFollow")

    val user1 = p2.getUser1
    val user2 = p2.getUser2

    // getUserNode will fail if the comment node doesn't already exist
    val user1Node = getUserNode(user1)
    val user2Node = getUserNode(user2)

    logger.debug(String.format("user1 node = %s , user2 node = %s", user1Node, user2Node))
    try {
      follow(user1Node, user2Node)
      val msg = user1 + " successfully followed " + user2
      val response = SimpleResponse.newBuilder()
        .setStatus("OK")
        .setMessage(msg).build()
      p3.run(response)

    } catch {
      case e => {
        val errMsg = (user1Node, user2Node) match {
          case (null, null) => user1 + " and " + user2 + " doesn't exist"
          case (null, x) => user1 + " doesn't exist"
          case (x, null) => user2 + " doesn't exist"
          case _ => e.getMessage
        }
        logger.error(errMsg)
        val response = SimpleResponse.newBuilder()
          .setStatus("NG")
          .setMessage(errMsg).build()
        p3.run(response)
      }
    }

    logger.debug("END- doFollow")

  }

  case class FrindsNewFeed(user: Node, vob: Node) extends Runnable {
    val rels = getConnected(in, user)

    def run(): Unit = {
      try {
        logger.debug("START-> doVob FrindsNotifier")
        withTx {
          neo =>
            for (rel <- rels.iterator()) {
              val friend = rel.endNode()
              logger.debug("friend " + friend.getProperty(GraphConstants.ENTITY_ID) + " " + friend)

              val old = friend.getSingleRelationship((friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.CURRENT_FEED), out)

              if (old == null) {
                friend --> (friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.START_FEED) --> vob
              } else {
                vob --> (friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.NEXT_FEED) --> old.getEndNode
                old.delete()
              }
              friend --> (friend.getProperty(GraphConstants.ENTITY_ID) + "_" + GraphConstants.CURRENT_FEED) --> vob

            }
        }
        logger.debug("END-> doVob FrindsNotifier")
      } catch {
        case e => logger.error("Failed to notify friends of " + user + " for vob " + vob)
        e.printStackTrace()
      }
    }
  }

  def getFollowing(p1: RpcController, p2: UserProto, p3: RpcCallback[Users]) {
    logger.debug("START- GetFollowing")
    val user = getUserNode(p2.getUserId)
    logger.debug("finding following users for " + user)
    val ret = Users.newBuilder()
    try {
      for (following <- getConnected(out, user).iterator()) {
        val n = following.endNode()
        logger.debug("added " + n)
        ret.addRepeatedField(Users.getDescriptor.findFieldByName("users"), getUserProto(n))
      }
      p3.run(ret.setStatus("OK").setMessage("Found").build())
    } catch {
      case x => {
        logger.error("problem while following" + x.getMessage)
        x.printStackTrace()
        p3.run(ret.setStatus("NG").setMessage("Not Found").build())
      }
    }
    logger.debug("END- GetFollowing")
  }

  def getFollowers(p1: RpcController, p2: UserProto, p3: RpcCallback[Users]) {
    logger.debug("START- GetFollowers")
    val user = getUserNode(p2.getUserId)
    logger.debug("finding followers for " + user)
    val ret = Users.newBuilder()
    val u = User(user)

//    val limitingIterator = new PagingIterator[VobProto](u.followers.iterator(), limit)
    val rels = getConnected(in, user)
    var i = 0
    try {
      for (following <- rels.iterator()) {
        val n = following.endNode()
        ret.addUsers(i, getUserProto(n))
        i = i + 1
        logger.debug("added " + n)
      }
      p3.run(ret.setStatus("OK").setMessage("Found").build())
    } catch {
      case x => {
        logger.debug("problem while following " + x.getMessage)
        x.printStackTrace()
        p3.run(ret.setStatus("NG").setMessage("Not Found").build())
      }
    }
    logger.debug("END- GetFollowers")
  }

  def getAllVobs(controller: RpcController, request: VobRequest, done: RpcCallback[Vobs]) {


    val resp_builder = Vobs.newBuilder()

    val user_id = request.getObjectId

    val logged_in_user = request.getSubjectId

    logger.debug("START- getAllVobs for " + user_id)

    val userNode = getUserNode(user_id)

    val limit = request.getLimit
    logger.debug("userNode " + userNode)
    // fail if user_id doesnt exist in graph
    if (userNode == null) {
      resp_builder.setStatus("NG")
      resp_builder.setMessage("user <" + user_id + "> doesn't exist ")
      done.run(resp_builder.build())
      return
    }

    try {
      val recent_vob = userNode.getSingleRelationship(GraphConstants.USER_VOB, out)

      // simply return if user dosn't have any recent vobs

      if (recent_vob == null) {
        resp_builder.setStatus("OK")
        resp_builder.setMessage("user <" + user_id + "> doesn't have any recent vobs")
        done.run(resp_builder.build())
        return
      }
      val u = User(userNode)

      val iterable_vobProto = new IterableWrapper[VobProto, Vob](u.getVobs(getVobNode(request.getOffset))) {
        def underlyingObjectToObject(path: Vob): VobProto = {
          val vob_node = path.underlyingNode
          val vob_id = vob_node.getProperty(GraphConstants.ENTITY_ID)
          val proto_builder = getVobProto(vob_node).toBuilder
          if (logged_in_user != "") {
            val t = vob_node.getSingleRelationship(GraphConstants.VOTE_UP + "_" + logged_in_user + "_" + vob_id, in)
            val q = vob_node.getSingleRelationship(GraphConstants.VOTE_DOWN + "_" + logged_in_user + "_" + vob_id, in)
            proto_builder.setUserVotedUp(t != null)
            proto_builder.setUserVotedDown(q != null)
          }

          val comments = getCommentsOnVob(vob_node)
          if (comments != null)
            proto_builder.setComments(Comments.newBuilder().addAllComments(Iterables.cache(comments)))
          proto_builder.build()
        }
      }


      println(" start " + request.getOffset)
      println(" limit " + limit)
      val limitingIterator = new PagingIterator[VobProto](iterable_vobProto.iterator(), limit)



      println("build resp " + limitingIterator.position())
      val ret = resp_builder.setStatus("OK")
        .setMessage("Vobs Found")
        .addAllVobs(Iterables.cache(IteratorUtil.asIterable[VobProto](limitingIterator.nextPage())))

      println(" vobs added " + limitingIterator.position())
      if (limitingIterator.hasNext) {
        println(" found next " + limitingIterator.position())
        ret.setNext(limitingIterator.next().getVobId)
      } else
        println(" no next " + limitingIterator.position())
      println(" vobs done ")
      done.run(ret.build())
    } catch {
      case e => e.printStackTrace()
      resp_builder.setStatus("NG")
      resp_builder.setMessage(e.getMessage)
      done.run(resp_builder.build())
    }
    logger.debug("END- getAllVobs")
  }


  def getNewsFeedVobs(controller: RpcController, request: VobRequest, done: RpcCallback[Vobs]) {

    val resp_builder = Vobs.newBuilder()

    try {
      val user_id = request.getSubjectId

      val userNode = getUserNode(user_id)

      logger.debug("START- getNewsFeedVobs for " + userNode)

      val offset = request.getOffset

      val offset_node: Node = if (offset.equals("$start")) null
      else {
        getVobNode(offset)
      }
      val limit = request.getLimit
      val u = User(userNode)

      if (offset_node != null) {
        val t = getVobProto(offset_node)
        logger.debug(">> " + new Date(t.getCreatedTs) + " " + t.getVobId)
      } else logger.debug(">> " + None)

      val iterable_vobProto = new IterableWrapper[VobProto, Vob](u.getNewsFeed(offset_node)) {
        def underlyingObjectToObject(path: Vob): VobProto = {
          val vob_node = path.underlyingNode
          val vob_id = vob_node.getProperty(GraphConstants.ENTITY_ID)

          val proto_builder = getVobProto(vob_node).toBuilder
          if (user_id != "") {
            val t = vob_node.getSingleRelationship(GraphConstants.VOTE_UP + "_" + user_id + "_" + vob_id, in)
            val q = vob_node.getSingleRelationship(GraphConstants.VOTE_DOWN + "_" + user_id + "_" + vob_id, in)
            proto_builder.setUserVotedUp(t != null)
            proto_builder.setUserVotedDown(q != null)
          }

          val comments = getCommentsOnVob(vob_node)
          if (comments != null)
            proto_builder.setComments(Comments.newBuilder().addAllComments(Iterables.cache(comments)))
          proto_builder.build()
        }
      };

      val limitingIterator = new PagingIterator[VobProto]((iterable_vobProto).iterator(), limit)

      //println("build resp " + limitingIterator.position())
      val ret = resp_builder.setStatus("OK")
        .setMessage("Vobs Found")
        .addAllVobs(Iterables.cache(IteratorUtil.asIterable[VobProto](limitingIterator.nextPage())))

      //println(" vobs added " + limitingIterator.position())
      if (limitingIterator.hasNext) {
       // println(" found next " + limitingIterator.position())
        ret.setNext(limitingIterator.next().getVobId)
      } else
       println(" no next " + limitingIterator.position())
     // println(" vobs done ")
      done.run(ret.build())

    } catch {
      case e => e.printStackTrace()
      logger.debug("ERROR- getNewsFeedVobs")
      resp_builder.setStatus("NG")
      resp_builder.setMessage(e.getMessage)
      done.run(resp_builder.build())

    }

    logger.debug("END- getNewsFeedVobs")

  }

  def getVotedUpVobs(p1: RpcController, request: VobRequest, done: RpcCallback[Vobs]) {


    val resp_builder = Vobs.newBuilder()

    try {
      // the acutal entity who did voted up
      val user_id = request.getObjectId
      // the observer
      val logged_in_user = request.getSubjectId

      val userNode = getUserNode(user_id)

      logger.debug("START- getVotedUpVobs for " + userNode)

      val offset = request.getOffset

      val offset_node: Node = if (offset.equals("$start")) null
      else {
        getVobNode(offset)
      }
      val limit = request.getLimit
      val u = User(userNode)

      if (offset_node != null) {
        val t = getVobProto(offset_node)
        logger.debug(">> " + new Date(t.getCreatedTs) + " " + t.getVobId)
      } else logger.debug(">> " + None)

      val iterable_vobProto = new IterableWrapper[VobProto, Vob](u.getVotedUpVobs(offset_node)) {
        def underlyingObjectToObject(path: Vob): VobProto = {
          val vob_node = path.underlyingNode
          val vob_id = vob_node.getProperty(GraphConstants.ENTITY_ID)

          val proto_builder = getVobProto(vob_node).toBuilder
          if (logged_in_user != "") {
            val t = vob_node.getSingleRelationship(GraphConstants.VOTE_UP + "_" + logged_in_user + "_" + vob_id, in)
            val q = vob_node.getSingleRelationship(GraphConstants.VOTE_DOWN + "_" + logged_in_user + "_" + vob_id, in)
            proto_builder.setUserVotedUp(t != null)
            proto_builder.setUserVotedDown(q != null)
          }

          val comments = getCommentsOnVob(vob_node)
          if (comments != null)
            proto_builder.setComments(Comments.newBuilder().addAllComments(Iterables.cache(comments)))
          proto_builder.build()
        }
      };

      val limitingIterator = new PagingIterator[VobProto]((iterable_vobProto).iterator(), limit)

      //println("build resp " + limitingIterator.position())
      val ret = resp_builder.setStatus("OK")
        .setMessage("Vobs Found")
        .addAllVobs(Iterables.cache(IteratorUtil.asIterable[VobProto](limitingIterator.nextPage())))

      //println(" vobs added " + limitingIterator.position())
      if (limitingIterator.hasNext) {
        // println(" found next " + limitingIterator.position())
        ret.setNext(limitingIterator.next().getVobId)
      } else
        println(" no next " + limitingIterator.position())
      // println(" vobs done ")
      done.run(ret.build())

    } catch {
      case e => e.printStackTrace()
      logger.debug("ERROR- getVotedUpVobs")
      resp_builder.setStatus("NG")
      resp_builder.setMessage(e.getMessage)
      done.run(resp_builder.build())

    }
    logger.debug("END- getVotedUpVobs")

  }

  def getPopularVobs(controller: RpcController, request: VobRequest, done: RpcCallback[Vobs]) {


  }

  def doVob(controller: RpcController, request: VobRequest, done: RpcCallback[SimpleResponse]) {}

  def doVoteDown(controller: RpcController, request: VoteOnVobRequest, done: RpcCallback[SimpleResponse]) {}

  def doVoteUp(controller: RpcController, request: VoteOnVobRequest, done: RpcCallback[SimpleResponse]) {}

  def doComment(controller: RpcController, request: CommentRequest, done: RpcCallback[SimpleResponse]) {

    val resp_builder = SimpleResponse.newBuilder()

    val user_id = request.getSubjectId;

    val vob_id = request.getObjectId

    val user_node = getUserNode(user_id)

    val vob_node = getVobNode(vob_id)

    val message = request.getMessage

    logger.debug("comment " + message)

    val now = request.getCommentTime

    val nodeIndex = getNodeIndex(EntityType.COMMENT_ENTITY.name()).getOrElse(fail("Failed to get  index " + EntityType.COMMENT_ENTITY))
    try {

      withTx {
        implicit neo =>

          val comment_node = createNode

          nodeIndex +=(comment_node, GraphConstants.ENTITY_ID, vob_id + request.getCommentTime)
          // set properties
          comment_node.setProperty(GraphConstants.ENTITY_ID, vob_id + request.getCommentTime)
          comment_node.setProperty(GraphConstants.ENTITY_TYPE, EntityType.COMMENT_ENTITY.name())
          comment_node.setProperty(GraphConstants.COMMENT_TXT, message)
          comment_node.setProperty(GraphConstants.OBJECT_TYPE, EntityType.VOB_ENTITY.name())
          comment_node.setProperty(GraphConstants.OBJECT_ID, vob_id)
          comment_node.setProperty(GraphConstants.SUBJECT_TYPE, EntityType.USER.name())
          comment_node.setProperty(GraphConstants.SUBJECT_ID, user_id)
          comment_node.setProperty(GraphConstants.CREATED_TS, now)

          vob_node.setProperty(GraphConstants.LATEST_ACTIVITY_TS, now)

          val old_popularity_idx = vob_node[Double](GraphConstants.POPULARITY_INDEX)
          vob_node.setProperty(GraphConstants.POPULARITY_INDEX,
            old_popularity_idx.getOrElse(0.0) + 0.33)

          user_node.setProperty(GraphConstants.LATEST_ACTIVITY_TS, now)

          // vob <-- comment
          val recent_comment_on_vob = vob_node.getSingleRelationship(GraphConstants.VOB_COMMENT, out)

          if (recent_comment_on_vob != null) {
            comment_node --> GraphConstants.NEXT_COMMENT --> recent_comment_on_vob.getEndNode
            recent_comment_on_vob.delete() // delete the edge
          } else {
            vob_node --> (GraphConstants.FIRST_COMMENT) --> comment_node
          }

          // user -- comments--> vob

          val recent_user_comment_on_vob = user_node.getSingleRelationship(GraphConstants.COMMENTS, out)
          if (recent_user_comment_on_vob != null) {
            comment_node --> (user_id + "_" + GraphConstants.NEXT_COMMENT + "_" + vob_id) --> recent_user_comment_on_vob.getEndNode
            recent_user_comment_on_vob.delete() // delete the edge
          } else {
            user_node --> (GraphConstants.FIRST_COMMENT + "_" + vob_id) --> comment_node
          }

          user_node --> GraphConstants.COMMENTS --> vob_node

          vob_node --> GraphConstants.VOB_COMMENT --> comment_node


          val c = vob_node[Int](GraphConstants.COMMENTS_COUNT)
          vob_node(GraphConstants.COMMENTS_COUNT) = c.getOrElse(0) + 1

      }
      resp_builder.setStatus("OK")
      resp_builder.setMessage("comment successful")
      done.run(resp_builder.build())
    } catch {
      case e => e.printStackTrace()
      resp_builder.setStatus("NG")
      resp_builder.setMessage(e.getMessage)
      done.run(resp_builder.build())
    }

  }


}
