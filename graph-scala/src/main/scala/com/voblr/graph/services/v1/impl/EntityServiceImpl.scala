package com.voblr.graph.services.v1.impl

import com.google.protobuf.{RpcCallback, RpcController}
import com.voblr.common.GraphServices._
import com.voblr.common.proto.Movie.MovieProto
import org.neo4j.scala.DatabaseService
import com.voblr.graph.VoblrGraphWrapper
import org.neo4j.graphdb.{DynamicRelationshipType, RelationshipType}
import com.voblr.common.constants.GraphConstants
import com.voblr.common.proto.User.UserProto
import com.voblr.common.proto.Vob.VobProto
import org.neo4j.helpers.collection.{Iterables, IteratorUtil, IterableWrapper}
import java.lang
import com.voblr.graph.services.v1.{Comment, Vob, Movie, User}
import com.voblr.common.proto.Comment.{CommentProto, Comments}

/**
 * User: abhi@voblr.com
 * Date: 11/4/12
 * Time: 4:41 AM
 * (c) Voblr, Inc
 */
class EntityServiceImpl(dbs: DatabaseService) extends EntityService with VoblrGraphWrapper {

  val ds = dbs


  // service methods immplemeted from here

  def getVob(p1: RpcController, p2: Entity, p3: RpcCallback[VobProto]) {
    logger.debug("START- getVob")

    val vob_id = p2.getEntityId
    logger.debug("getting Vob with entity_id = " + vob_id)
    try {
      val vob = getVobNode(vob_id)
      val v = getVobProto(vob)
      p3.run(v)
      logger.debug("END- getVob " + v)
    } catch {
      case e => logger.error(e.getMessage)
      e.printStackTrace()
      p3.run(null)
    }


  }

  def getUser(p1: RpcController, p2: Entity, p3: RpcCallback[UserProto]) {
    logger.debug("START- getUser")

    val user_id = p2.getEntityId
    logger.debug("getting User with entity_id = " + user_id)
    try {
      val user = getMovieNode(user_id)
      val u = getUserProto(user)
      p3.run(u)
      logger.debug("END- getUser " + u)
    } catch {
      case e => logger.error(e.getMessage)
      e.printStackTrace()
      p3.run(UserProto.getDefaultInstance)
    }
  }

  def getMovie(controller: RpcController, p2: Entity, p3: RpcCallback[MovieProto]) {
    logger.debug("START- getMovie")

    val movie_id = p2.getEntityId
    logger.debug("getting Movie with entity_id = " + movie_id)
    try {
      val movie = getMovieNode(movie_id)
      val m = getMovieProto(movie)
      p3.run(m)
      logger.debug("END- getMovie")
    } catch {
      case e => logger.error(e.getMessage)
      e.printStackTrace()
      p3.run(MovieProto.getDefaultInstance)
    }
  }

  def getComment(controller: RpcController, request: Entity, done: RpcCallback[CommentProto]) {
    logger.debug("START- getComment")

    val comment_id = request.getEntityId
    logger.debug("getting Comment with entity_id = " + comment_id)
    try {
      val comment = getCommentNode(comment_id)
      val m = getCommentProto(comment)
      done.run(m)
      logger.debug("END- getComment")
    } catch {
      case e => logger.error(e.getMessage)
      e.printStackTrace()
      done.run(CommentProto.getDefaultInstance)
    }
  }


  def getVobCount(p1: RpcController, p2: MovieProto, p3: RpcCallback[MovieProto]) {
    logger.debug("START- getVobCount")

    val movie_id = p2.getMovieId
    logger.debug("getting comment count for movie_id = " + movie_id)
    try {
      val movie = getMovieNode(movie_id)
      val m = getMovieProto(movie)
      p3.run(m)
      logger.debug("END- getVobCount")
    } catch {
      case e => logger.error(e.getMessage)
      e.printStackTrace()
      p3.run(null)
    }
  }

  def getCompositeResponse(p1: RpcController, p2: CompositeRequest, p3: RpcCallback[CompositeResponse]) {
    logger.debug("START- getCompositeResponse")

    val resp_builder = CompositeResponse.newBuilder()
    try {
      val subject_node = getNodeByEntityId(p2.getEntityType, p2.getEntityId)

      implicit def f(something: DirectedRelationship): (RelationshipType, org.neo4j.graphdb.Direction) = {
        val dir = something.getDir match {
          case Direction.IN => in
          case Direction.OUT => out
        }
        val rel = DynamicRelationshipType.withName(something.getRelType)
        (rel, dir)
      }

      val outer_iterable = new IterableWrapper[InnerCompositeResponse, DirectedRelationship](p2.getRelsList) {
        def underlyingObjectToObject(something: DirectedRelationship) = {
          val dir = something.getDir match {
            case Direction.IN => in
            case Direction.OUT => out
          }
          val rel = DynamicRelationshipType.withName(something.getRelType)
          val inner_resp_builder = InnerCompositeResponse.newBuilder();

          val single: IterableWrapper[lang.Boolean, String] = new IterableWrapper[java.lang.Boolean, String](p2.getOtherIdsList) {
            def underlyingObjectToObject(other_id: String) = {
              try {
                shortestSinglePath(1, subject_node, getNodeByEntityId(p2.getEntityType, other_id), rel, dir) match {
                  case Some(x) => java.lang.Boolean.TRUE
                  case None => java.lang.Boolean.FALSE
                }
              } catch {
                case _ => java.lang.Boolean.FALSE
              }
            }
          }
          inner_resp_builder.addAllRelatesToOtherIds(Iterables.cache(single)).build()
        }
      }

      resp_builder.setStatus("OK").addAllRelatesToRelTypes(Iterables.cache(outer_iterable))
      p3.run(resp_builder.build())
      logger.debug("END- getCompositeResponse")
    } catch {
      case e => logger.error(e.getMessage)
            e.printStackTrace()

      p3.run(CompositeResponse.newBuilder().setStatus("NG").setMessage(e.getMessage).build())
    }
  }

  def remove(controller: RpcController, request: Entity, done: RpcCallback[SimpleResponse]) {

    val resp = SimpleResponse.newBuilder()
    withTx {
      implicit neo =>
        try {
          resp.setStatus("OK").setMessage("Successfully removed")
          request.getEntityType match {
            case EntityType.USER => User(getUserNode(request.getEntityId)).remove()
            case EntityType.MOVIE => Movie(getMovieNode(request.getEntityId)).remove()
            case EntityType.COMMENT_ENTITY => Comment(getCommentNode(request.getEntityId)).remove()
            case EntityType.VOB_ENTITY => Vob(getVobNode(request.getEntityId)).remove()
            case _ => throw new Error("invalid entity type")
          }
        } catch {
          case e => resp.setMessage("NG").setMessage("Failed to remove : " + e.getMessage)
        }
    }
    done.run(resp.build())
  }

  def undoRemove(controller: RpcController, request: Entity, done: RpcCallback[SimpleResponse]) {

    val resp = SimpleResponse.newBuilder()
    withTx {
      implicit neo =>
        try {
          resp.setStatus("OK").setMessage("Successfully undid remove")
          request.getEntityType match {
            case EntityType.USER => User(getUserNode(request.getEntityId)).undoRemove()
            case EntityType.MOVIE => Movie(getMovieNode(request.getEntityId)).undoRemove()
            case EntityType.COMMENT_ENTITY => Comment(getCommentNode(request.getEntityId)).undoRemove()
            case EntityType.VOB_ENTITY => Vob(getVobNode(request.getEntityId)).undoRemove()
            case _ => throw new Error("invalid entity type")
          }
        } catch {
          case e => resp.setMessage("NG").setMessage("Failed to undo remove : " + e.getMessage)
        }
    }
    done.run(resp.build())
  }

  def getMovies(controller: RpcController, request: Entities, done: RpcCallback[Movies]) {
    val movies = Movies.newBuilder()
    try {
      import scala.collection.JavaConversions._
      for (e <- request.getEntitiesList) {
        if (e.getEntityType != EntityType.MOVIE)
          throw new IllegalArgumentException("getMovies : " + e.getEntityType)
        val node = getNodeByEntityId(e.getEntityType, e.getEntityId)
        movies.addMovies(getMovieProto(node))
      }
      movies.setStatus("OK").setMessage("Successful")
    } catch {
      case e => movies.setStatus("NG").setMessage(e.getMessage)
      logger.error(e.getMessage)
      e.printStackTrace()
    }
    done.run(movies.build())
  }

  def getUsers(controller: RpcController, request: Entities, done: RpcCallback[Users]) {
    val users = Users.newBuilder()
    try {
      import scala.collection.JavaConversions._
      val list = request.getEntitiesList
      var i = 0
      for (e <- list) {
        if (e.getEntityType != EntityType.USER)
          throw new IllegalArgumentException("getUsers : " + e.getEntityType)
        val node = getNodeByEntityId(e.getEntityType, e.getEntityId)
        i += 1
        users.addUsers(getUserProto(node))
      }
      logger.debug("enties size "+i)
      users.setStatus("OK").setMessage("Successful")
    } catch {
      case e => users.setStatus("NG").setMessage(e.getMessage)
      logger.error(e.getMessage)
      e.printStackTrace()
    }
    done.run(users.build())
    logger.debug("getUsers")
  }

  def getVobs(controller: RpcController, request: Entities, done: RpcCallback[Vobs]) {
    val vobs = Vobs.newBuilder()
    try {
      import scala.collection.JavaConversions._
      for (e <- request.getEntitiesList) {
        if (e.getEntityType != EntityType.VOB_ENTITY)
          throw new IllegalArgumentException("getVobs : " + e.getEntityType)
        val node = getNodeByEntityId(e.getEntityType, e.getEntityId)
        vobs.addVobs(getVobProto(node))
      }
      vobs.setStatus("OK").setMessage("Successful")
    } catch {
      case e => vobs.setStatus("NG").setMessage(e.getMessage)
      logger.error(e.getMessage)
      e.printStackTrace()
    }
    done.run(vobs.build())
  }

  def getComments(controller: RpcController, request: Entities, done: RpcCallback[Comments]) {
    val vobs = Comments.newBuilder()
    try {
      import scala.collection.JavaConversions._
      for (e <- request.getEntitiesList) {
        if (e.getEntityType != EntityType.COMMENT_ENTITY)
          throw new IllegalArgumentException("getComments : " + e.getEntityType)
        val node = getNodeByEntityId(e.getEntityType, e.getEntityId)
        vobs.addComments(getCommentProto(node))
      }
      vobs.setStatus("OK").setMessage("Successful")
    } catch {
      case e => vobs.setStatus("NG").setMessage(e.getMessage)
      logger.error(e.getMessage)
      e.printStackTrace()
    }
    done.run(vobs.build())
  }

}
