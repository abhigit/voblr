import com.voblr.graph.Server
import java.net.URI
import org.neo4j.scala.{EmbeddedGraphDatabaseServiceProvider, RestGraphDatabaseServiceProvider}

object Main extends App {

  class RestDB(host: String, port: String) extends RestGraphDatabaseServiceProvider {
    def uri = {
      new URI("http://" + host + ":" + port + "/db/data/")
    }
  }

  class FileDB extends EmbeddedGraphDatabaseServiceProvider {
   
    def neo4jStoreDir = "/home/"+System.getProperty("user.name")+"/data/voblr"
  }

  override def main(args: Array[String]) {

    val host = System.getProperty("HOST")
    val port = System.getProperty("PORT")

    try {
      val neo4j_host = System.getProperty("NEO4J_HOST")
      val neo4j_port = System.getProperty("NEO4J_PORT")

      (neo4j_host, neo4j_port) match {

        case (null, null) => new Server(host, port.toInt, new FileDB())
        case (x, null) => new Server(host, port.toInt, new FileDB())
        case (null, x) => new Server(host, port.toInt, new FileDB())
        case _ => new Server(host, port.toInt, new RestDB(neo4j_host, neo4j_port))

        //new Server(host, port.toInt+1, new RestDB(neo4j_host, neo4j_port))
      }

    } catch {
      case e => println("Error => " + e)
                e.printStackTrace()
    }


  }

}
