#!/bin/bash
# Date: 14 Feb 2013
# Time: 10:45 PM JST
#

# A simple runner script, use it after mvn clean package
# We need a better way to get things working

#
# Explaination

# NEO4J_HOST/PORT for neo4j graph rest api server, not using now
# HOST/PORT 

JAVA_OPTS="-DHOST=0.0.0.0 -DPORT=7040"
#JAVA_OPTS="-DHOST=0.0.0.0 -DPORT=7040 -DNEO4J_HOST=0.0.0.0 -DNEO4J_PORT=7474"

LOG=`date +%y%m%d%H%M%S`.log

# Bootstraping from Main class
MAINCLASS=Main

# Store command to be run first before executing (evaling) it

CP="`echo target/*.jar | tr ' ' ':'`"
CP=$CP:"`echo target/lib/*.jar | tr ' ' ':'`"

cmd="${JAVA_OPTS} -cp $CP ${MAINCLASS}"

# so that we can echo it, for debugging
echo $cmd

# run it now --> boom...
# do some simple log rotating :)
if [ -f ~/logs/voblr.log ];
then
    mv ~/logs/voblr.log ~/logs/$LOG
fi

if [ -f ~/logs/solr.voblr.log ];
then
    mv ~/logs/solr.voblr.log ~/logs/solr.$LOG
fi

nohup java $cmd &> ~/logs/voblr.log &
cd example
nohup java -jar start.jar  &> ~/logs/solr.voblr.log &

