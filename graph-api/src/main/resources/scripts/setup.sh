#!/bin/bash

echo "Starting to deploy Voblr-Graph API package";



svn co svn://dev.ibhupi.com/voblr_backend/trunk/voblr-graph-api  ~/tmp

cd ~/tmp

mvn clean package 

touch target/run

echo "java -Dserver=$HOSTNAME -Dorg.apache.commons.logging.Log=org.apache.commons.logging.impl.NoOpLog -cp lib/*:rpc.server.graph-0.0.1-SNAPSHOT.jar com.voblr.api.rpc.server.graph.Server" > target/run
tar -czf current.tar.gz target/lib/* target/run target/rpc.server.graph-0.0.1-SNAPSHOT.jar


scp -i ~/.ssh/id_rsa current.tar.gz api.iabhi.me:~/tmp.tar.gz


