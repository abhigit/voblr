package com.voblr.api.rpc.server.graph;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.concurrent.Executors;

import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.RpcController;
import com.googlecode.protobuf.pro.duplex.PeerInfo;
import com.googlecode.protobuf.pro.duplex.RpcClientChannel;
import com.googlecode.protobuf.pro.duplex.client.DuplexTcpClientBootstrap;
import com.googlecode.protobuf.pro.duplex.execute.ThreadPoolCallExecutor;
import com.voblr.api.rpc.server.graph.GraphServices.GenericEvent;
import com.voblr.api.rpc.server.graph.GraphServices.GenericResponse;
import com.voblr.api.rpc.server.graph.services.UserServiceImpl;

public class Client {
    static PeerInfo client = new PeerInfo("localhost", 10234);
    static PeerInfo server = new PeerInfo("localhost", 7080);
    static ThreadPoolCallExecutor executor = new ThreadPoolCallExecutor(3, 10);

    static DuplexTcpClientBootstrap bootstrap = new DuplexTcpClientBootstrap(
            client, new NioClientSocketChannelFactory(
                    Executors.newCachedThreadPool(),
                    Executors.newCachedThreadPool()), executor);
    static RpcClientChannel channel;

    public Client() throws IOException {
        channel = bootstrap.peerWith(server);
    }

    public static void main(String[] args) throws Exception {
        new Client();
        UserServiceImpl.BlockingInterface followsService = UserServiceImpl
                .newBlockingStub(channel);

        RpcController controller = channel.newRpcController();

        GenericEvent request = GenericEvent.newBuilder().setObjectId("1")
                .setSubjectId("2").setTimestamp(System.currentTimeMillis())
                .build();

        GenericResponse response = followsService.doFollow(controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        channel.close();
        bootstrap.releaseExternalResources();
    }
}
