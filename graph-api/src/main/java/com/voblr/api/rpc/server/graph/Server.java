package com.voblr.api.rpc.server.graph;

import java.io.IOException;
import java.util.concurrent.Executors;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import com.googlecode.protobuf.pro.duplex.PeerInfo;
import com.googlecode.protobuf.pro.duplex.execute.ThreadPoolCallExecutor;
import com.googlecode.protobuf.pro.duplex.logging.CategoryPerServiceLogger;
import com.googlecode.protobuf.pro.duplex.server.DuplexTcpServerBootstrap;
import com.voblr.api.graph.entity.repository.RepositotyLoader;
import com.voblr.api.rpc.server.graph.services.MovieServiceImpl;
import com.voblr.api.rpc.server.graph.services.UserServiceImpl;

public class Server {
    static PeerInfo server = new PeerInfo(System.getProperty("server",
            "localhost"), 7080);
    static ThreadPoolCallExecutor executor = new ThreadPoolCallExecutor(10, 10);

    static DuplexTcpServerBootstrap bootstrap = new DuplexTcpServerBootstrap(
            server, new NioServerSocketChannelFactory(
                    Executors.newCachedThreadPool(),
                    Executors.newCachedThreadPool()), executor);

    public Server() throws IOException {
        new RepositotyLoader();
        bootstrap.getRpcServiceRegistry()
                .registerService(new UserServiceImpl());

        bootstrap.getRpcServiceRegistry().registerService(
                new MovieServiceImpl());

        bootstrap.bind();

    }

    public static void main(String[] args) throws Exception {
        System.setProperty("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");
        Log log = LogFactory.getLog(CategoryPerServiceLogger.class);

        System.out.println("info enabled : " + log.isInfoEnabled());
        new Server();
    }
}
