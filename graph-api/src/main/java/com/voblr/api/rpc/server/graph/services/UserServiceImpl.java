package com.voblr.api.rpc.server.graph.services;

import static com.voblr.api.graph.entity.repository.RepositotyLoader.gUserRepository;

import java.util.Arrays;
import java.util.Iterator;

import com.google.protobuf.RpcCallback;
import com.google.protobuf.RpcController;
import com.voblr.api.graph.entity.nodes.GUser;
import com.voblr.api.graph.entity.relations.GEvent;
import com.voblr.api.graph.entity.relations.GFollowEvent;
import com.voblr.api.graph.entity.relations.GVobEvent;
import com.voblr.api.rpc.server.graph.GraphServices.GenericEvent;
import com.voblr.api.rpc.server.graph.GraphServices.GenericResponse;
import com.voblr.api.rpc.server.graph.GraphServices.User;
import com.voblr.api.rpc.server.graph.GraphServices.UserService;

public class UserServiceImpl extends UserService {

    @Override
    public void doFollow(RpcController controller, GenericEvent request,
            RpcCallback<GenericResponse> done) {
        String user1 = request.getObjectId();
        String user2 = request.getSubjectId();
        long t = request.getTimestamp();

        GenericResponse gr = null;

        try {
            gUserRepository.doFollow((user1), (user2), t);
            gr = GenericResponse
                    .newBuilder()
                    .setStatus("OK")
                    .setMessage(
                            user1 + " followed " + user2 + " at "
                                    + new java.util.Date(t)).build();
        } catch (Exception e) {
            // e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }

        done.run(gr);

    }

    @Override
    public void getFollowing(RpcController controller, User request,
            RpcCallback<GenericResponse> done) {
        String user = request.getUserId();
        GenericResponse gr = null;
        try {
            GUser u = gUserRepository.get(user);
            GenericResponse.Builder b = GenericResponse.newBuilder();
            b.setStatus("OK");
            int index = 0;
            for (GFollowEvent gfe : u.followees) {
                b.addEvents(index++, gfe.getGenericEvent());
            }
            gr = b.build();
        } catch (Exception e) {
            // e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }
        done.run(gr);

    }

    @Override
    public void doVob(RpcController controller, GenericEvent request,
            RpcCallback<GenericResponse> done) {
        String user = request.getObjectId();
        String movie = request.getSubjectId();
        long t = request.getTimestamp();

        GenericResponse gr = null;

        try {
            gUserRepository.doVob((user), (movie), t);
            gr = GenericResponse
                    .newBuilder()
                    .setStatus("OK")
                    .setMessage(
                            user + " vobbed " + movie + " at "
                                    + new java.util.Date(t)).build();
        } catch (Exception e) {
            // e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }

        done.run(gr);

    }

    @Override
    public void getUserVobsForAllMovies(RpcController controller, User request,
            RpcCallback<GenericResponse> done) {
        String user = request.getUserId();
        GenericResponse gr = null;
        try {
            GUser u = gUserRepository.get(user);
            GenericResponse.Builder b = GenericResponse.newBuilder();
            b.setStatus("OK");
            System.out.println(Arrays.asList(u.vobs));
            for (GVobEvent gfe : u.vobs) {
                b.addEvents(gfe.getGenericEvent());
            }
            gr = b.build();
        } catch (Exception e) {
            e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }
        done.run(gr);
    }

    @Override
    public void getUserVobsForMovie(RpcController controller,
            GenericEvent request, RpcCallback<GenericResponse> done) {
        String user = request.getSubjectId();
        String movie = request.getObjectId();
        GenericResponse gr = null;
        try {
            Iterator<GEvent> u = gUserRepository.getVobs(user, movie);
            GenericResponse.Builder b = GenericResponse.newBuilder();
            b.setStatus("OK");
            while (u.hasNext()) {
                b.addEvents(u.next().getGenericEvent());
            }
            gr = b.build();
        } catch (Exception e) {
            // e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }
        done.run(gr);
    }

    @Override
    public void getNewsFeed(RpcController controller, User request,
            RpcCallback<GenericResponse> done) {
        long start = System.nanoTime();
        String user = request.getUserId();
        GenericResponse gr = null;
        try {
            Iterator<GEvent> u = gUserRepository.getFeeds(user);
            GenericResponse.Builder b = GenericResponse.newBuilder();
            b.setStatus("OK");
            while (u.hasNext()) {
                b.addEvents(u.next().getGenericEvent());
            }
            gr = b.build();
        } catch (Exception e) {
            e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }
        System.out.println("Finished in " + (System.nanoTime() - start));
        done.run(gr);
    }

    @Override
    public void getActivityFeeds(RpcController controller, User request,
            RpcCallback<GenericResponse> done) {
        String user = request.getUserId();
        GenericResponse gr = null;
        try {
            Iterator<GEvent> u = gUserRepository.getActivity(user);
            GenericResponse.Builder b = GenericResponse.newBuilder();
            b.setStatus("OK");
            while (u.hasNext()) {
                b.addEvents(u.next().getGenericEvent());
            }
            gr = b.build();
        } catch (Exception e) {
            // e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }
        done.run(gr);
    }

}
