package com.voblr.api.rpc.server.graph.services;

import static com.voblr.api.graph.entity.repository.RepositotyLoader.gMovieRepository;

import java.util.Iterator;

import com.google.protobuf.RpcCallback;
import com.google.protobuf.RpcController;
import com.voblr.api.graph.entity.relations.GEvent;
import com.voblr.api.rpc.server.graph.GraphServices.GenericResponse;
import com.voblr.api.rpc.server.graph.GraphServices.Movie;
import com.voblr.api.rpc.server.graph.GraphServices.MovieService;
import com.voblr.api.rpc.server.graph.GraphServices.MovieVobCount;

public class MovieServiceImpl extends MovieService {

    @Override
    public void getMovieVobline(RpcController controller, Movie request,
            RpcCallback<GenericResponse> done) {

        String movie = request.getMovieId();
        GenericResponse gr = null;
        try {
            Iterator<GEvent> u = gMovieRepository.getVobline(movie);
            GenericResponse.Builder b = GenericResponse.newBuilder();
            b.setStatus("OK");
            int index = 0;
            while (u.hasNext()) {
                b.setEvents(index++, u.next().getGenericEvent());
            }
            gr = b.build();
        } catch (Exception e) {
            e.printStackTrace();
            gr = GenericResponse.newBuilder().setStatus("NG")
                    .setMessage("Exception : " + e.getMessage()).build();
        }
        done.run(gr);

    }

    @Override
    public void getVobCount(RpcController controller, Movie request,
            RpcCallback<MovieVobCount> done) {
        String movie = request.getMovieId();
        MovieVobCount res = null;
        try {
            int count = gMovieRepository.getVobcount(movie);
            MovieVobCount.Builder b = MovieVobCount.newBuilder();
            b.setMovieId(movie);
            b.setCount(count);
            res = b.build();
        } catch (Exception e) {
            e.printStackTrace();
            MovieVobCount.Builder b = MovieVobCount.newBuilder();
            b.setMovieId(movie);
            b.setCount(0);
            res = b.build();
        }
        done.run(res);
    }

}
