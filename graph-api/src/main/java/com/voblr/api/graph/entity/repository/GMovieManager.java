package com.voblr.api.graph.entity.repository;

import java.util.Iterator;

import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.graph.entity.nodes.GMovie;
import com.voblr.api.graph.entity.relations.GEvent;

/**
 * @author abhi
 * @since 2012.04.07
 */
public interface GMovieManager {
    /*
     * @Transactional is special annotation to denote the modification to the
     * DB, reading however doesnt require it.
     */
    @Transactional
    public GMovie createyOrGetMovie(String name);

    public GMovie get(String name);

    public Iterator<GEvent> getVobline(String movie);

    public int getVobcount(String movie);

}
