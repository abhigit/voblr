package com.voblr.api.graph.entity.nodes;

import java.util.Set;
import java.util.TreeSet;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import com.voblr.api.graph.entity.relations.GEventConstants;
import com.voblr.api.graph.entity.relations.GFollowEvent;
import com.voblr.api.graph.entity.relations.GVobEvent;

@NodeEntity
public class GUser extends GEntity {
    @GraphId
    public Long id;

    @Indexed
    public String entity_id;

    @Fetch
    public final GEntityType type = GEntityType.User;

    @Fetch
    @RelatedToVia(type = GEventConstants.FOLLOWS)
    public Set<GFollowEvent> followees = new TreeSet<GFollowEvent>();

    @Fetch
    @RelatedToVia(type = GEventConstants.VOBS)
    public Set<GVobEvent> vobs = new TreeSet<GVobEvent>();

    public GUser(String id) {
        this.entity_id = id;
    }

    public GUser() {
    }

    @Override
    public GEntityType getGEntityType() {
        return type;
    }

    @Override
    public String getGEntityId() {
        return entity_id;
    }

    @Override
    public String toString() {
        return String.format("'%s'", entity_id);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        GUser other = (GUser) obj;
        if (id == null)
            return other.id == null;
        return id.equals(other.id);
    }
}
