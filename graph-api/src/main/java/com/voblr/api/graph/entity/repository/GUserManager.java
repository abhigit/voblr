package com.voblr.api.graph.entity.repository;

import java.util.Iterator;

import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.graph.entity.nodes.GUser;
import com.voblr.api.graph.entity.relations.GEvent;

/**
 * @author abhi
 * @since 2012.04.07
 */
public interface GUserManager {
    /*
     * @Transactional is special annotation to denote the modification to the
     * DB, reading however doesnt require it.
     */
    @Transactional
    public GUser createyOrGetUser(String user);

    public GUser get(String user);

    @Transactional
    public void doFollow(String user1, String user2, long now);

    @Transactional
    public void doVob(String user, String movie, long now);

    public Iterator<GEvent> getFeeds(String uid);

    public Iterator<GEvent> getActivity(String uid);

    public Iterator<GEvent> getVobs(String user, String movie);
}
