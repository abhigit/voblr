package com.voblr.api.graph.entity.relations;

import org.neo4j.graphdb.RelationshipType;

public class GEventConstants {
    
    public static enum GEventType implements RelationshipType {
        FOLLOWS,
        VOBS
    }
    public static final String FOLLOWS ="FOLLOWS";
    public static final String VOBS ="VOBS";
    public static final String EVENT ="EVENT";
}
