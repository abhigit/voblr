package com.voblr.api.graph.entity.nodes;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import com.voblr.api.graph.entity.relations.GEventConstants;

@NodeEntity
public class GMovie extends GEntity{
	@GraphId
	public Long id;
    
    @Indexed
    public String entity_id;
    
    @Fetch
    public GEntityType type = GEntityType.Movie;
    
    @Fetch
    @RelatedTo(elementClass = GUser.class,type = GEventConstants.VOBS, direction = Direction.INCOMING)
    public Set<GUser> users = new HashSet<GUser>();
    
    public GMovie(String name) {
		this.entity_id = name;
	}
    
    public GMovie() {
	}

	@Override
    public String toString()
    {
        return String.format("Movie{name='%s'}", entity_id);
    }

	@Override
	public GEntityType getGEntityType() {
		return type;
	}

	@Override
	public String getGEntityId() {
		return entity_id;
	}

}
