package com.voblr.api.graph.entity.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.voblr.api.graph.entity.nodes.GEntity;
import com.voblr.api.graph.entity.nodes.GUser;
import com.voblr.api.graph.entity.relations.GEventConstants.GEventType;
import com.voblr.api.rpc.server.graph.GraphServices.GenericEvent;

@RelationshipEntity(type = GEventConstants.FOLLOWS)
public class GFollowEvent implements GEvent {

    @GraphId
    public Long id;

    @Fetch
    @StartNode
    public GUser a;

    @Fetch
    @EndNode
    public GUser b;

    @Fetch
    public long timestamp;

    @Override
    public GEntity getStartEntity() {
        return a;
    }

    public GFollowEvent(GUser user, GUser other, long now) {
        a = user;
        b = other;
        timestamp = now;
    }

    public GFollowEvent() {
    }

    @Override
    public GEntity getEndEntity() {
        return b;
    }

    @Override
    public long getEventTimestamp() {
        return timestamp;
    }

    @Override
    public GEventType getEventType() {
        return GEventType.FOLLOWS;
    }

    @Override
    public String name() {
        return GEventConstants.FOLLOWS;
    }

    @Override
    public int compareTo(GEvent o) {
        if (timestamp == o.getEventTimestamp())
            return 0;
        else if (timestamp > o.getEventTimestamp())
            return 1;
        return -1;
    }

    @Override
    public GenericEvent getGenericEvent() {
        return GenericEvent.newBuilder()
                .setSubjectId(getStartEntity().getGEntityId())
                .setObjectId(getEndEntity().getGEntityId())
                .setTimestamp(getEventTimestamp()).build();
    }
}
