package com.voblr.api.graph.entity.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;

import com.voblr.api.graph.entity.nodes.GMovie;
import com.voblr.api.rpc.server.graph.GraphServices.Movie;

/**
 * @author me
 * @since 01.04.11
 */
public interface GMovieRepository extends GMovieManager, GraphRepository<GMovie>, NamedIndexRepository<Movie> {
}
