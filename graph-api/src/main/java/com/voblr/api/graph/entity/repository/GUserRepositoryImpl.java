package com.voblr.api.graph.entity.repository;

import static com.voblr.api.graph.entity.repository.RepositotyLoader.gMovieRepository;
import static com.voblr.api.graph.entity.repository.RepositotyLoader.gUserRepository;
import static com.voblr.api.graph.entity.repository.RepositotyLoader.template;
import static org.neo4j.graphdb.DynamicRelationshipType.withName;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.StopEvaluator;
import org.neo4j.graphdb.TraversalPosition;
import org.neo4j.graphdb.Traverser;
import org.neo4j.graphdb.Traverser.Order;
import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.graph.entity.nodes.GEntity;
import com.voblr.api.graph.entity.nodes.GEntityType;
import com.voblr.api.graph.entity.nodes.GUser;
import com.voblr.api.graph.entity.relations.GEvent;
import com.voblr.api.graph.entity.relations.GEventConstants;
import com.voblr.api.graph.entity.relations.GEventConstants.GEventType;
import com.voblr.api.graph.entity.relations.GFollowEvent;
import com.voblr.api.graph.entity.relations.GVobEvent;
import com.voblr.api.rpc.server.graph.GraphServices.GenericEvent;

/**
 * Spring Data Neo4j backed application context for Worlds.
 */
public class GUserRepositoryImpl implements GUserManager {

    @Override
    @Transactional
    public GUser createyOrGetUser(String user_id) {

        GUser user = get(user_id);
        if (user == null) {
            user = new GUser(user_id);
            gUserRepository.save(user);
        }
        return user;
    }

    @Override
    public GUser get(String user) {
        return gUserRepository.findByPropertyValue("entity_id", user);
    }

    @Override
    public Iterator<GEvent> getFeeds(String uid) {
        final GUser u = gUserRepository.get(uid);
        System.out.println(uid + " graph id" + u.id);
        Node userNode = template.getNode(u.id);

        Traverser traverser = userNode.traverse(Order.BREADTH_FIRST,
                new StopEvaluator() {

                    @Override
                    public boolean isStopNode(TraversalPosition currentPos) {
                        if (currentPos.depth() > 1)
                            System.out.println("---> last included "
                                    + currentPos.currentNode().getProperty(
                                            "entity_id"));
                        return currentPos.depth() > 1;
                    }
                }, new ReturnableEvaluator() {
                    @Override
                    public boolean isReturnableNode(
                            final TraversalPosition currentPos) {
                        if (currentPos.depth() > -1)
                            System.out.println("---> just included "
                                    + currentPos.currentNode().getProperty(
                                            "entity_id"));
                        return (currentPos.depth() > -1);
                    }
                }, withName(GEventConstants.VOBS), Direction.OUTGOING,
                withName(GEventConstants.FOLLOWS), Direction.OUTGOING);

        Set<GEvent> events = new TreeSet<GEvent>();

        for (final Node n : traverser) {
            for (final Relationship r : n.getRelationships(Direction.OUTGOING)) {
                events.add(new GEvent() {

                    @Override
                    public long getEventTimestamp() {
                        return Long.parseLong((r.getProperty("timestamp")
                                .toString()));
                    }

                    @Override
                    public GEventType getEventType() {
                        return (GEventType) r.getType();
                    }

                    @Override
                    public GEntity getStartEntity() {
                        GEntity e = new GEntity() {

                            @Override
                            public GEntityType getGEntityType() {
                                return (GEntityType) r.getStartNode()
                                        .getProperty("type");
                            }

                            @Override
                            public String getGEntityId() {
                                return (String) r.getStartNode().getProperty(
                                        "entity_id");
                            }

                        };
                        return e;
                    }

                    @Override
                    public GEntity getEndEntity() {
                        GEntity e = new GEntity() {

                            @Override
                            public GEntityType getGEntityType() {
                                return (GEntityType) r.getEndNode()
                                        .getProperty("type");
                            }

                            @Override
                            public String getGEntityId() {
                                return (String) r.getEndNode().getProperty(
                                        "entity_id");
                            }

                        };
                        return e;
                    }

                    @Override
                    public String name() {
                        return getEventType().name();
                    }

                    @Override
                    public GenericEvent getGenericEvent() {
                        return GenericEvent.newBuilder()
                                .setSubjectId(getStartEntity().getGEntityId())
                                .setObjectId(getEndEntity().getGEntityId())
                                .setTimestamp(getEventTimestamp()).build();
                    }

                    @Override
                    public int compareTo(GEvent o) {
                        if (getEventTimestamp() == o.getEventTimestamp())
                            return 0;
                        else if (getEventTimestamp() < o.getEventTimestamp())
                            return 1;
                        return -1;
                    }

                });
            }
        }

        return events.iterator();
    }

    @Override
    public void doFollow(String user1, String user2, long now) {
        GUser me = createyOrGetUser(user1);
        GUser followee = createyOrGetUser(user2);
        for (GFollowEvent gfe : me.followees) {
            if (gfe.getEndEntity().equals(followee))
                throw new RuntimeException(user1 + " already follows " + user2);
        }
        System.out.println("loled");
        GFollowEvent followE = new GFollowEvent(me, followee, now);
        template.save(followE);
    }

    @Override
    public void doVob(String user, String movie, long now) {
        GVobEvent vobE = new GVobEvent(createyOrGetUser(user),
                gMovieRepository.createyOrGetMovie(movie), now);
        template.save(vobE);

    }

    @Override
    public Iterator<GEvent> getVobs(String user, String movie) {
        final GUser u = gUserRepository.get(user);

        Set<GEvent> events = new TreeSet<GEvent>();
        while (u.vobs.iterator().hasNext())
            events.add(u.vobs.iterator().next());

        return events.iterator();
    }

    @Override
    public Iterator<GEvent> getActivity(String user) {
        final GUser u = gUserRepository.get(user);

        Set<GEvent> events = new TreeSet<GEvent>();
        while (u.vobs.iterator().hasNext())
            events.add(u.vobs.iterator().next());
        while (u.followees.iterator().hasNext())
            events.add(u.followees.iterator().next());

        return events.iterator();
    }

}
