package com.voblr.api.graph.entity.repository;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;

import com.voblr.api.graph.entity.nodes.GUser;

/**
 * @author me
 * @since 01.04.11
 */
public interface GUserRepository extends GUserManager, GraphRepository<GUser>, NamedIndexRepository<GUser> {
}
