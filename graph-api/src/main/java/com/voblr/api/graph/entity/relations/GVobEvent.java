package com.voblr.api.graph.entity.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.voblr.api.graph.entity.nodes.GEntity;
import com.voblr.api.graph.entity.nodes.GMovie;
import com.voblr.api.graph.entity.nodes.GUser;
import com.voblr.api.graph.entity.relations.GEventConstants.GEventType;
import com.voblr.api.rpc.server.graph.GraphServices.GenericEvent;

@RelationshipEntity(type = GEventConstants.VOBS)
public class GVobEvent implements GEvent {
    @GraphId
    public Long id;

    @Fetch
    @StartNode
    public GUser a;

    @Fetch
    @EndNode
    public GMovie m;

    @Fetch
    public long timestamp;

    public GVobEvent() {
    }

    public GVobEvent(GUser user, GMovie other, long now) {
        this.a = user;
        this.m = other;
        this.timestamp = now;
    }

    @Override
    public GEntity getStartEntity() {
        return a;
    }

    @Override
    public GEntity getEndEntity() {
        return m;
    }

    @Override
    public long getEventTimestamp() {
        return timestamp;
    }

    @Override
    public String name() {
        return GEventConstants.VOBS;
    }

    @Override
    public GEventType getEventType() {
        return GEventType.VOBS;
    }

    @Override
    public int compareTo(GEvent o) {
        if (timestamp == o.getEventTimestamp())
            return 0;
        else if (timestamp > o.getEventTimestamp())
            return 1;
        return -1;
    }

    @Override
    public GenericEvent getGenericEvent() {
        return GenericEvent.newBuilder()
                .setSubjectId(getStartEntity().getGEntityId())
                .setObjectId(getEndEntity().getGEntityId())
                .setTimestamp(getEventTimestamp()).build();
    }
}
