package com.voblr.api.graph.entity.nodes;

public abstract class GEntity {
	public abstract GEntityType getGEntityType();
	public abstract String getGEntityId();
}
