package com.voblr.api.graph.entity.repository;

import static com.voblr.api.graph.entity.repository.RepositotyLoader.gMovieRepository;
import static com.voblr.api.graph.entity.repository.RepositotyLoader.template;
import static org.neo4j.graphdb.DynamicRelationshipType.withName;

import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.StopEvaluator;
import org.neo4j.graphdb.TraversalPosition;
import org.neo4j.graphdb.Traverser;
import org.neo4j.graphdb.Traverser.Order;
import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.graph.entity.nodes.GEntity;
import com.voblr.api.graph.entity.nodes.GEntityType;
import com.voblr.api.graph.entity.nodes.GMovie;
import com.voblr.api.graph.entity.relations.GEvent;
import com.voblr.api.graph.entity.relations.GEventConstants;
import com.voblr.api.graph.entity.relations.GEventConstants.GEventType;
import com.voblr.api.rpc.server.graph.GraphServices.GenericEvent;

/**
 * Spring Data Neo4j backed application context for Worlds.
 */
public class GMovieRepositoryImpl implements GMovieManager {

    @Override
    @Transactional
    public GMovie createyOrGetMovie(String movie_id) {

        GMovie movie = get(movie_id);
        if (movie == null) {
            movie = new GMovie(movie_id);
            gMovieRepository.save(movie);
        }
        return movie;
    }

    @Override
    public GMovie get(String name) {
        return gMovieRepository.findByPropertyValue("entity_id", name);
    }

    @Override
    public Iterator<GEvent> getVobline(String movie) {
        final GMovie m = gMovieRepository.get(movie);

        Node userNode = template.getNode(m.id);

        Traverser traverser = userNode.traverse(Order.BREADTH_FIRST,
                new StopEvaluator() {

                    @Override
                    public boolean isStopNode(TraversalPosition currentPos) {
                        return currentPos.depth() == 0;
                    }
                }, new ReturnableEvaluator() {
                    @Override
                    public boolean isReturnableNode(
                            final TraversalPosition currentPos) {
                        return (currentPos.depth() > 0 && !currentPos
                                .isStartNode());
                    }
                }, withName(GEventConstants.VOBS), Direction.INCOMING);

        Set<GEvent> events = new TreeSet<GEvent>();

        for (final Node n : traverser) {
            for (final Relationship r : n.getRelationships(Direction.OUTGOING)) {
                if (movie.equals((String) r.getEndNode().getProperty(
                        "entity_id")))
                    events.add(new GEvent() {

                        @Override
                        public long getEventTimestamp() {
                            return Long.parseLong((r.getProperty("timestamp")
                                    .toString()));
                        }

                        @Override
                        public GEventType getEventType() {
                            return (GEventType) r.getType();
                        }

                        @Override
                        public GEntity getStartEntity() {
                            GEntity e = new GEntity() {

                                @Override
                                public GEntityType getGEntityType() {
                                    return (GEntityType) r.getStartNode()
                                            .getProperty("type");
                                }

                                @Override
                                public String getGEntityId() {
                                    return (String) r.getStartNode()
                                            .getProperty("entity_id");
                                }

                            };
                            return e;
                        }

                        @Override
                        public GEntity getEndEntity() {
                            GEntity e = new GEntity() {

                                @Override
                                public GEntityType getGEntityType() {
                                    return (GEntityType) r.getEndNode()
                                            .getProperty("type");
                                }

                                @Override
                                public String getGEntityId() {
                                    return (String) r.getEndNode().getProperty(
                                            "entity_id");
                                }

                            };
                            return e;
                        }

                        @Override
                        public String name() {
                            return getEventType().name();
                        }

                        @Override
                        public GenericEvent getGenericEvent() {
                            return GenericEvent
                                    .newBuilder()
                                    .setSubjectId(
                                            getStartEntity().getGEntityId())
                                    .setObjectId(getEndEntity().getGEntityId())
                                    .setTimestamp(getEventTimestamp()).build();
                        }

                        @Override
                        public int compareTo(GEvent o) {
                            if (getEventTimestamp() == o.getEventTimestamp())
                                return 0;
                            else if (getEventTimestamp() > o
                                    .getEventTimestamp())
                                return 1;
                            return -1;
                        }

                    });
            }
        }

        return events.iterator();
    }

    @Override
    public int getVobcount(String movie) {
        Iterator<GEvent> events = getVobline(movie);
        int c = 0;
        while (events.hasNext()) {
            events.next();
            c++;
        }
        return c;
    }

}
