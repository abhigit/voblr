package com.voblr.api.graph.entity.relations;

import org.neo4j.graphdb.RelationshipType;

import com.voblr.api.graph.entity.nodes.GEntity;
import com.voblr.api.graph.entity.relations.GEventConstants.GEventType;
import com.voblr.api.rpc.server.graph.GraphServices.GenericEvent;

public interface GEvent extends RelationshipType, Comparable<GEvent> {
    public GEntity getStartEntity();

    public GEntity getEndEntity();

    public GEventType getEventType();

    public GenericEvent getGenericEvent();

    public long getEventTimestamp();

}
