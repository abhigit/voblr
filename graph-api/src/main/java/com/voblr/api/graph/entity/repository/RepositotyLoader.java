package com.voblr.api.graph.entity.repository;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.neo4j.support.Neo4jTemplate;

public class RepositotyLoader {
	private static ApplicationContext ctx = new ClassPathXmlApplicationContext(
			"default/context.xml");
	public final static GUserRepository gUserRepository = ctx
			.getBean(GUserRepository.class);
	public final static GMovieRepository gMovieRepository = ctx
			.getBean(GMovieRepository.class);
	public final static Neo4jTemplate template = ctx.getBean(Neo4jTemplate.class);
}
