#!/bin/bash


wget  -q -O .tmp.tar.gz http://resource.voblr.com:8081/nexus/content/repositories/releases/voblr-graph/voblr-graph/1.0/voblr-graph-1.0-bundle.tar.gz


if [ "$?" -ne 0 ]                
then
	echo "Failed to download" 	
	exit
else    echo "Successfully Downloaded"
fi

echo -n "1) Cleaning up logs : "

rm ~/logs/*.log

if [ "$?" -ne 0 ]                
then
	echo "failed : Ignoring" 	
	#exit
else    echo "Success"
fi

echo -n "2) Cleaning old files : "

rm ~/work/target -rf && rm ~/work/*.sh

if [ "$?" -ne 0 ]                
then
	echo "failed : Ignoring" 	
	#exit
else    echo "Success"
fi

echo -n "3) Extracting : "

tar xzf .tmp.tar.gz  -C ~/work

if [ "$?" -ne 0 ]                
then
	echo "failed" 	
	exit
else    echo "Success"
fi


echo -n "4) Killing all java processes : "

killall java

if [ "$?" -ne 0 ]                
then
	echo "failed" 	
	exit
else    echo "Success"
fi

cd ~/work
echo -n "5) Executing runner Script : "

./runner.sh

if [ "$?" -ne 0 ]                
then
	echo "failed" 	
	exit
else    echo "Success"
	sleep 3
	tail -f ~/logs/voblr.log
fi

