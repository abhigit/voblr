package com.voblr.base.api.v1.data.mongo.DAO;

import com.google.code.morphia.Datastore;
import com.google.code.morphia.Morphia;
import com.google.code.morphia.query.Query;
import com.mongodb.Mongo;
import com.voblr.base.api.v1.data.mongo.model.vob.VobData;

public class MongoConnector {

//	static Log log = new Log4JLogger("MongoConnector Log :");
	public static final String DB_NAME = "VoblrTest";
	public static Datastore ds;
	public static Mongo mongo ;
	public static Morphia morphia ;
	
	static {
		try {

			morphia = new Morphia();
			mongo = new Mongo("dev.ibhupi.com", 27017);
//			mongo = new Mongo("localhost", 27017);
			ds = morphia.createDatastore(mongo, DB_NAME);
			morphia.mapPackage("com.voblr.base.api.v1.data.mongo.model", true);
			morphia.map(VobData.class);
			ds.ensureIndexes();
			
		} catch (Exception e) {

			System.out.println("Exception in getting mongo connections" + e);

		}
	}

	public static void saveObjects(Object... obj){
		ds.save(obj);
	}
	
	public static <T> Query<T> createQuery(Class<T> clazz){
		return  (Query<T>) ds.createQuery(clazz);
	}

	public static <T,V> Query<T> find(Class<T> clazz, String property, V value){
		return (Query<T>) ds.find(clazz, property, value);
	}

	/**
	 * @param class1
	 * @param string
	 * @param vobId
	 * @return
	 * VobData
	 */
	public static <T> T getSingleObject(Class<T> clazz, String property,
			String value) {
		Query<T> query = ds.find(clazz, property, value);
		
		return query.get();
	}
}
