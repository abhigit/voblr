package com.voblr.base.api.v1.data.mongo.services;

import java.util.Date;
import java.util.List;

import com.google.code.morphia.query.Query;
import com.google.code.morphia.query.UpdateOperations;
import com.google.code.morphia.query.UpdateResults;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.user.User;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.vob.Comment;
import com.voblr.base.api.v1.data.mongo.model.vob.VobComments;
import com.voblr.base.api.v1.data.mongo.model.vob.VobData;
import com.voblr.base.api.v1.data.mongo.model.vob.VobLikes;
import com.voblr.base.api.v1.data.mongo.response.ResponseCode;
import com.voblr.base.api.v1.data.mongo.response.VobResponse;

/**
 * @author vinit Update Time : Sep 2, 2012 : 10:36:21 PM All user related
 *         services concerning MongoDB
 */
public class UserService {

	/**
	 * This method is used to get first 10 users.
	 * Its temporary method for recommendations
	 * 
	 * @return UserBasicInfo
	 */
	public static List<User> getRandomUserList() {

		Query<User> query = MongoConnector.ds.createQuery(User.class).limit(10);
		List<User> userList = query.asList();
		
		return userList;
	}

	/**
	 * This method takes userID and returns the User object
	 * 
	 * @param userId
	 * @return
	 * User
	 */
	public static User getUserById(String userId){
//		Query<User> query = MongoConnector.ds.createQuery(User.class).filter("_id", userId);
//		User user = query.get();
//		

		return MongoConnector.getSingleObject(User.class, "_id", userId);
	}

	/**
	 * Adds new user information to the database.
	 * Can also be used for updating the existing user.
	 * 
	 * @param userId
	 * @param name
	 * @param picURL
	 * @return ResponseCode
	 * 
	 */
	public static int setUserInfo(String userId, String name, String picURL) {

		UserBasicInfo userBasicInfo = new UserBasicInfo(userId, name, picURL);
		User user = new User(userBasicInfo);

		MongoConnector.saveObjects(user, userBasicInfo);
//		MongoConnector.ds.
		return ResponseCode.SUCCESS;
	}

	/**
	 * This function is used to add a new Vob to Vob Document
	 * Also we add empty Likes document and empty Comments document.
	 * If need, we will add the same Vob to the userVobList and MovieVobList
	 * 
	 * @param userId
	 * @param movieId
	 * @param vobText
	 * @param timestamp
	 * 
	 * @return ResponseCode
	 */
	public static VobResponse doVob(String userId, String movieId, String vobText,
			Date timestamp) {

		VobData vob = new VobData(userId, movieId, vobText, timestamp);

		// Add empty Likes document to the current Vob
		// Actually this should be done when the first like is made
		// But in that case, we will have to check it every time, if it already exists or not.
		VobLikes vobLikes = new VobLikes(VobLikes.generateVobLikeId(vob
				.getVobId()));

		// Add empty Comments document to the current Vob
		// Actually this should be done when the first comment is made
		// But in that case, we will have to check it every time, if it already exists or not.
		VobComments vobComments = new VobComments(
				VobComments.generateVobCommentId(vob.getVobId()));

		
		MongoConnector.saveObjects(vob, vobLikes, vobComments);

		_incPropertyInDocument(User.class, userId , User.NO_OF_VOBS, 1);
		_incPropertyInDocument(MovieBasicInfo.class, movieId, MovieBasicInfo.NO_OF_VOBS, 1);
		
    	VobResponse response = new VobResponse(vob.getVobId(),vob.getTimestamp());
		// If needed add data to Movie Vobs and User vobs collection later
		// here add means append data
		return response;
	}
	
	/**
	 * This function is used to return VobData related to a particular vob
	 * @param vobId
	 * 
	 * @return VobData
	 */
	public static VobData getVobData(String vobId){
		
		VobData vob = MongoConnector.getSingleObject(VobData.class,"_id",vobId);

		return vob;
	}

	/**
	 * Do a comment on particular Vob. 
	 * Also increment number of comments on the Vob by 1
	 * Also adjust popularity index of the Vob accordingly
	 * 
	 * @param vobId
	 * @param userId
	 * @param commentText
	 * @param timestamp
	 * @return ResponseCode
	 */
	public static int doComment(String vobId, String userId, String commentText,
			Date timestamp) {

		String commentId = VobComments.generateVobCommentId(vobId);
		Comment comment = new Comment(timestamp, userId, commentText);

		// may be we should change the popularity index of vob
		if (_doComment(commentId, comment) == ResponseCode.SUCCESS)
			return _incPropertyInDocument(VobComments.class, vobId,
					VobData.NO_OF_COMMENTS, 1);

		return ResponseCode.FAIL;
	}

	/**
	 * This function is to add 1 comment to the given comment Id document
	 * 
	 * @param commentId
	 * @param comment
	 * @return ResponseCode
	 */
	private static int _doComment(String commentId, Comment comment) {

		// query to find the comment list for the given vob
		Query<VobComments> q = MongoConnector.ds.find(VobComments.class, "_id",
				commentId);

		// create update operation to add comment to existing comment
		UpdateOperations<VobComments> uo = MongoConnector.ds
				.createUpdateOperations(VobComments.class).add("comments",
						comment);

		// execute update operation
		UpdateResults<VobComments> res = MongoConnector.ds.update(q, uo);

		if (res.getUpdatedCount() > 0)
			return ResponseCode.SUCCESS;
		else
			return ResponseCode.FAIL;

	}

	/**
	 * This will increment a property in document by the given incrementCount
	 * value. This value can be negative also.
	 * 
	 * @param clazz
	 * @param docId
	 * @param propertyName
	 * @param incrementCount
	 * @return ResponseCode
	 */
	private static <T> int _incPropertyInDocument(Class<T> clazz, String docId,
			String propertyName, int incrementCount) {
		
		Query<T> query = MongoConnector.find(clazz, "_id", docId);
		
		UpdateOperations<T> uo = MongoConnector.ds
				.createUpdateOperations(clazz)
				.inc(propertyName, incrementCount);

		UpdateResults<T> res = MongoConnector.ds.update(query, uo);

		if (res.getUpdatedCount() > 0)
			return ResponseCode.SUCCESS;
		else
			return ResponseCode.FAIL;

	}
}
