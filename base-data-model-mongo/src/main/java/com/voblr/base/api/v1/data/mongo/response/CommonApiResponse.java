/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author vinit
 * Update Time : Nov 2, 2012 : 12:07:43 AM
 * TODO
 */
@XmlRootElement
public class CommonApiResponse {
	private int statusCode;
	private String message;

	public CommonApiResponse(){}
	
	public CommonApiResponse(int status, String string) {
		statusCode = status;
		message=string;
	}

	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}

	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}

}
