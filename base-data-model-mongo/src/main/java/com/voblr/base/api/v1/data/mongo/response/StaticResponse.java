/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import org.jboss.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author vinit Update Time : Nov 4, 2012 : 6:41:22 PM TODO
 */

public class StaticResponse {

	public static final CommonApiResponse successUserCreated = new CommonApiResponse(
			HttpResponseStatus.OK.getCode(), "User created");

	public static final CommonApiResponse noLoggedInUser = new CommonApiResponse(
			HttpResponseStatus.UNAUTHORIZED.getCode(), "User is not logged in");

	public static final CommonApiResponse noUserInfo = new CommonApiResponse(
			HttpResponseStatus.BAD_REQUEST.getCode(),
			"User ID found from cookie/parameters. But User info could not be found. Reason can be that while creating user, user info was not inserted in Mongo DB.");

	public static final CommonApiResponse noMovieInfo = new CommonApiResponse(
			HttpResponseStatus.BAD_REQUEST.getCode(),
			"Movie ID in the request could not be found in the DB");

	public StaticResponse() {

	}
}
