/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.base.api.v1.data.mongo.model.user.User;

/**
 * @author vinit
 * Update Time : Nov 4, 2012 : 7:28:57 PM
 * TODO
 */
@XmlRootElement
public class UserInfoResponse extends CommonApiResponse{

	private User user;
	
	public UserInfoResponse() {
		super(ResponseCode.SUCCESS,"SUCCESS");
		user=null;
	}

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

}
