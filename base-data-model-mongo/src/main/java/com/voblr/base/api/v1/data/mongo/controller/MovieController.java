/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.controller;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.response.MovieInfoResponse;
import com.voblr.base.api.v1.data.mongo.response.MovieListResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.services.MovieService;

/**
 * @author vinit Update Time : Nov 3, 2012 : 11:30:27 AM TODO
 */
@Path("/movies")
public class MovieController {
	@GET
	@Produces("application/javascript")
	public JSONWithPadding getMovieList(@QueryParam("callback") String callback) {

		System.out.println("Movie List called with callback");
		return new JSONWithPadding(getMovieList(), callback);
	}

	public MovieListResponse getMovieList() {
		MovieListResponse movieList = new MovieListResponse();
		movieList.setMovieList(MovieService.getMovieList());
		System.out.println("Movie List called without callback");

		return movieList;
	}

	@GET
	@Produces("application/javascript")
	@Path("{movie_id}")
	public JSONWithPadding getMovieInfo(@PathParam("movie_id") String movieId,
			@QueryParam("callback") String callback) {

		System.out.println("Got a request for movie details for : " + movieId);
		MovieBasicInfo movie = MovieService.getMovieById(movieId);
		if (movie != null) {
			MovieInfoResponse movieInfo = new MovieInfoResponse();
			movieInfo.setMovieInfo(movie);
			return new JSONWithPadding(movieInfo, callback);
		} else {
			return new JSONWithPadding(StaticResponse.noMovieInfo, callback);
		}
	}

	@POST
	@Produces("application/javascript")
	@Path("{movie_id}/vobs")
	public JSONWithPadding doVob(@PathParam("movie_id") String movieId,
			@CookieParam("vob_common") String vobCommon) {

		return null;
	}
}
