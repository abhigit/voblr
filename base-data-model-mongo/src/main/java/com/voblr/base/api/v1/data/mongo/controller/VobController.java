/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.controller;

import java.util.Date;

import javax.ws.rs.CookieParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.auth.AuthUser;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.services.UserService;

/**
 * 
 * @author vinit
 * Update Time : Nov 11, 2012 : 7:24:48 PM
 * TODO
 */
@Path("/vobs")
public class VobController {

	@POST
	@Produces("application/javascript")
	@Path("{movie_id}")
	public JSONWithPadding doVob(@PathParam("movie_id") String movieId,
			@CookieParam("vob_common") String vobCommon,
			@QueryParam("callback") String callback,
			@QueryParam("vobText") String vobText) {

		String userId = AuthUser.getUserID(vobCommon);
		JSONWithPadding json = new UserInfoController().getUserInfo(userId, "callback");
		System.out.println(json.getJsonSource().toString());
		System.out.println("Request came with following parameter");
		System.out.println("movie_id : " + movieId);
        System.out.println("user id : " + userId);
        System.out.println("vobtext : " + vobText);
		if (userId == null){
			return new JSONWithPadding(StaticResponse.noLoggedInUser, callback);
		} 

        Date timestamp = new Date();

		return new JSONWithPadding(UserService.doVob(userId, movieId, vobText, timestamp), callback);
	}
}
