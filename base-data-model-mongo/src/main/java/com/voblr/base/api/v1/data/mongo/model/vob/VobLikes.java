package com.voblr.base.api.v1.data.mongo.model.vob;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;

@Entity(value="VobLikes", noClassnameStored=true)
public class VobLikes {
	@Id String vobLikeId;
	List<Like> likes;

	public VobLikes() {}

	/**
	 * @param vobLikeId
	 */
	public VobLikes(String vobLikeId) {
		this.vobLikeId = vobLikeId;
		this.setLikes(new ArrayList<Like>());
	}

	/**
	 * @return the vobLikeId
	 */
	public String getVobLikeId() {
		return vobLikeId;
	}

	/**
	 * @param vobLikeId the vobLikeId to set
	 */
	public void setVobLikeId(String vobLikeId) {
		this.vobLikeId = vobLikeId;
	}

	/**
	 * @return the likes
	 */
	public List<Like> getLikes() {
		return likes;
	}

	/**
	 * @param likes the likes to set
	 */
	public void setLikes(List<Like> likes) {
		this.likes = likes;
	}

	/**
	 * @param vobId
	 * @return
	 */
	public static String generateVobLikeId(String vobId) {
		return vobId + "_" + "likes";
	}
	

	@Override
	public int hashCode(){
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj){
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}

	
}
