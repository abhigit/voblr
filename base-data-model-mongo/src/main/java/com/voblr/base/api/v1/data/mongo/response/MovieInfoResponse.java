/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;

/**
 * @author vinit
 * Update Time : Nov 4, 2012 : 2:15:46 AM
 * TODO
 */

@XmlRootElement
public class MovieInfoResponse extends CommonApiResponse{
	private MovieBasicInfo movieInfo;
	
	public MovieInfoResponse() {
		super(ResponseCode.SUCCESS,"SUCCESS");
		movieInfo=null;
		
	}

	/**
	 * @return the movieInfo
	 */
	public MovieBasicInfo getMovieInfo() {
		return movieInfo;
	}

	/**
	 * @param movieInfo the movieInfo to set
	 */
	public void setMovieInfo(MovieBasicInfo movieInfo) {
		this.movieInfo = movieInfo;
	}


}
