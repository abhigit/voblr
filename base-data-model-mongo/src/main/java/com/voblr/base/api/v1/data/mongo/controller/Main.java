/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.controller;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.grizzly.http.server.HttpServer;

import com.googlecode.protobuf.pro.duplex.logging.CategoryPerServiceLogger;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;

/**
 * @author vinit Update Time : Nov 1, 2012 : 10:29:33 PM TODO
 */
public class Main {

	private static URI getBaseURI() {
		return UriBuilder
				.fromUri(
						"http://" + System.getProperty("HOST", "api.voblr.com")
								+ "/").port(9998).build();
	}

	public static final URI BASE_URI = getBaseURI();

	protected static HttpServer startServer() throws IOException {
		System.out.println("Starting grizzly...");
		System.out.println("Base uri : " + BASE_URI);
		ResourceConfig rc = new PackagesResourceConfig(
				"com.voblr.base.api.v1.data.mongo.controller");
		return GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
	}

	public static void main(String[] args) throws IOException, InterruptedException {
		Log log = LogFactory.getLog(CategoryPerServiceLogger.class);

		System.out.println("info enabled : " + log.isInfoEnabled());

		startServer();
		System.out.println("server created");
		// System.in.read();
		// httpServer.stop();
		Thread.currentThread().join();
	}
}
