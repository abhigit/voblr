/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.model.vob;

import java.util.Date;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Reference;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;

/**
 * @author vinit
 * Update Time : Sep 17, 2012 : 12:11:53 AM
 * TODO
 */
@Embedded
public class Like{
	Date timestamp;
	@Reference UserBasicInfo user;

	
	public Like() {}

	/**
	 * @param timestamp
	 * @param user
	 */
	public Like(Date timestamp, UserBasicInfo user) {
		this.timestamp = timestamp;
		this.user = user;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}
	/**
	 * @return the user
	 */
	public UserBasicInfo getUser() {
		return user;
	}
	/**
	 * @param user the user to set
	 */
	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

}
