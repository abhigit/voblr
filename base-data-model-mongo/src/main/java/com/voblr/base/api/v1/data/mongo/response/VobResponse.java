/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author vinit Update Time : Nov 11, 2012 : 4:05:01 PM TODO
 */
@XmlRootElement
public class VobResponse extends CommonApiResponse {

	private String vobId;
	private Date vobTimestamp;
	
	public VobResponse() {
		super(ResponseCode.SUCCESS, "SUCCESS");
		vobId = null;
	}

	public VobResponse(String vobId, Date timestamp) {
		super(ResponseCode.SUCCESS, "SUCCESS");
		this.vobId = vobId;
		this.vobTimestamp = timestamp;
	}

	/**
	 * @return the vobId
	 */
	public String getVobId() {
		return vobId;
	}


	/**
	 * @param vobId the vobId to set
	 */
	public void setVobId(String vobId) {
		this.vobId = vobId;
	}


	/**
	 * @return the vobTimestamp
	 */
	public Date getVobTimestamp() {
		return vobTimestamp;
	}

	/**
	 * @param vobTimestamp the vobTimestamp to set
	 */
	public void setVobTimestamp(Date vobTimestamp) {
		this.vobTimestamp = vobTimestamp;
	}

	
}
