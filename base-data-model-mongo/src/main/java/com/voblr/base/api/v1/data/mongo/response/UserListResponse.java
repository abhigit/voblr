/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.base.api.v1.data.mongo.model.user.User;

/**
 * @author vinit Update Time : Nov 4, 2012 : 7:17:08 PM TODO
 */
@XmlRootElement
public class UserListResponse extends CommonApiResponse {

	private List<User> userList;
	private int count;

	public UserListResponse() {
		super(ResponseCode.SUCCESS, "SUCCESS");
		userList = null;
	}

	/**
	 * @return the userList
	 */
	public List<User> getUserList() {
		return userList;
	}

	/**
	 * @param userList the userList to set
	 */
	public void setUserList(List<User> userList) {
		this.userList = userList;
		this.count = userList.size();
	}

	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}
	
	
}
