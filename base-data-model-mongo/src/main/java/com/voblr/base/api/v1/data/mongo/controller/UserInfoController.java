/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.controller;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.auth.AuthUser;
import com.voblr.base.api.v1.data.mongo.model.user.User;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.response.UserInfoResponse;
import com.voblr.base.api.v1.data.mongo.response.UserListResponse;
import com.voblr.base.api.v1.data.mongo.services.UserService;

/**
 * @author vinit Update Time : Nov 4, 2012 : 7:02:46 PM TODO
 */

@Path("/users")
public class UserInfoController {
	@GET
	@Produces("application/javascript")
	public JSONWithPadding getUserListResponse(
			@QueryParam("callback") String callback,
			@CookieParam("vob_common") String cookie) {

		System.out.println("cookie : " + cookie);
		System.out.println("Userid : " + AuthUser.getUserID(cookie));
		System.out.println("User List called with callback");
		return new JSONWithPadding(getUserList(), callback);
	}

	@GET
	@Produces("application/json")
	public UserListResponse getUserList() {
		UserListResponse userList = new UserListResponse();
		userList.setUserList(UserService.getRandomUserList());
		System.out.println("User List called without callback");

		return userList;
	}

	@GET
	@Produces("application/javascript")
	@Path("{user_id}")
	public JSONWithPadding getUserInfo(@PathParam("user_id") String userId,
			@QueryParam("callback") String callback) {

		System.out.println("Got a request for user details for : " + userId);
		User user = UserService.getUserById(userId);
		if (user != null) {
			UserInfoResponse userInfo = new UserInfoResponse();
			userInfo.setUser(user);
			return new JSONWithPadding(userInfo, callback);

		} else {
			return new JSONWithPadding(StaticResponse.noUserInfo, callback);
		}
	}

	
	@POST
	@Produces("application/json")
	@Path("{userId}")
	public CommonApiResponse setUserInfo(@PathParam("userId") String userId,
			@QueryParam("name") String name,
			@QueryParam("picURL") String picURL){
		
		System.out.println("Userid : " + userId);
		System.out.println("Name : " + name);
		System.out.println("picURL : " + picURL);
		UserService.setUserInfo(userId, name, picURL);
		return StaticResponse.successUserCreated;
	}
}
