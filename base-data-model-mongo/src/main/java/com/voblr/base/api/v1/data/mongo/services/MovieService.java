/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.services;

import java.util.List;

import com.google.code.morphia.query.Query;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;

/**
 * @author vinit
 * Update Time : Sep 16, 2012 : 11:51:08 PM
 * TODO
 */
public class MovieService {
	public static List<MovieBasicInfo> getMovieList(){
		
		Query<MovieBasicInfo> q = MongoConnector.ds.createQuery(MovieBasicInfo.class).order("-releaseDate").limit(10);
		List<MovieBasicInfo> list = q.asList();
		
		return list;
		
	}

	public static MovieBasicInfo getMovieById(String movieId){
		
		Query<MovieBasicInfo> q = MongoConnector.ds.createQuery(MovieBasicInfo.class).filter("_id", movieId);
		MovieBasicInfo movie = q.get();
		
		return movie;
		
	}

}
