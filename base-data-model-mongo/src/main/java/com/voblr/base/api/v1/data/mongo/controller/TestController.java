/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.controller;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

import com.sun.jersey.api.json.JSONWithPadding;

import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;

/**
 * @author vinit Update Time : Nov 2, 2012 : 1:07:02 AM TODO
 */
@Path("/test")
public class TestController {
	@GET
	@Produces("application/javascript")
	public JSONWithPadding getTestResults() {

		return new JSONWithPadding(new CommonApiResponse(200, "nothing found"),
				"vinit");

	}
}
