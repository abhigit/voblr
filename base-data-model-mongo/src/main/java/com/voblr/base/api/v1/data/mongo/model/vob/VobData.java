package com.voblr.base.api.v1.data.mongo.model.vob;

import java.util.Date;

import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Indexed;
import com.google.code.morphia.annotations.Reference;
import com.google.code.morphia.utils.IndexDirection;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;

/**
 * 
 * @author vinit Update Time : Sep 2, 2012 : 9:21:32 PM
 */
@Entity(value = "VobData", noClassnameStored = true)
public class VobData {
	@Indexed
	@Id
	String vobId;
	@Indexed
	Date timestamp;

	@Indexed(value = IndexDirection.DESC)
	Date latestActivityTimestamp;

	@Reference
	UserBasicInfo user;
	@Reference
	MovieBasicInfo movie;

	String vobText;

	int noOfLikes;
	int noOfComments;
	int popularityIndex;

	public static final String NO_OF_LIKES = "noOfLikes";
	public static final String NO_OF_COMMENTS = "noOfComments";
	public static final String POPULARITY_INDEX = "popularityIndex";

	public VobData() {
	}

	public VobData(String userId, String movieId, String vobText, Date timestamp) {
		this.vobId = generateVobId(userId, movieId, timestamp);
		this.timestamp = timestamp;
		this.latestActivityTimestamp = timestamp;
		this.user = new UserBasicInfo(userId);
		this.movie = new MovieBasicInfo(movieId);
		this.vobText = vobText;
		noOfLikes = 0;
		noOfComments = 0;
		this.popularityIndex = calculatePopularityIndex(vobText);

	}

	/**
	 * @param vobText
	 * @return
	 */
	private int calculatePopularityIndex(String vobText) {
		int popularity = 0;
		if (vobText.length() == 0)
			popularity = 1;
		else if (vobText.length() < 20)
			popularity = 2;
		else
			popularity = 3;

		return popularity;
	}

	/**
	 * @param userId
	 * @param movieId
	 * @param timestamp
	 * @return
	 */
	public static String generateVobId(String userId, String movieId,
			Date timestamp) {
		return userId + "_" + movieId + "_" + timestamp.getTime();
	}

	/**
	 * @return the vobId
	 */
	public String getVobId() {
		return vobId;
	}

	/**
	 * @param vobId
	 *            the vobId to set
	 */
	public void setVobId(String vobId) {
		this.vobId = vobId;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the latestActivityTimestamp
	 */
	public Date getLatestActivityTimestamp() {
		return latestActivityTimestamp;
	}

	/**
	 * @param latestActivityTimestamp
	 *            the latestActivityTimestamp to set
	 */
	public void setLatestActivityTimestamp(Date latestActivityTimestamp) {
		this.latestActivityTimestamp = latestActivityTimestamp;
	}

	/**
	 * @return the user
	 */
	public UserBasicInfo getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	/**
	 * @return the movie
	 */
	public MovieBasicInfo getMovie() {
		return movie;
	}

	/**
	 * @param movie
	 *            the movie to set
	 */
	public void setMovie(MovieBasicInfo movie) {
		this.movie = movie;
	}

	/**
	 * @return the vobText
	 */
	public String getVobText() {
		return vobText;
	}

	/**
	 * @param vobText
	 *            the vobText to set
	 */
	public void setVobText(String vobText) {
		this.vobText = vobText;
	}

	/**
	 * @return the noOfLikes
	 */
	public int getNoOfLikes() {
		return noOfLikes;
	}

	/**
	 * @param noOfLikes
	 *            the noOfLikes to set
	 */
	public void setNoOfLikes(int noOfLikes) {
		this.noOfLikes = noOfLikes;
	}

	/**
	 * @return the noOfComments
	 */
	public int getNoOfComments() {
		return noOfComments;
	}

	/**
	 * @param noOfComments
	 *            the noOfComments to set
	 */
	public void setNoOfComments(int noOfComments) {
		this.noOfComments = noOfComments;
	}

	/**
	 * @return the popularityIndex
	 */
	public int getPopularityIndex() {
		return popularityIndex;
	}

	/**
	 * @param popularityIndex
	 *            the popularityIndex to set
	 */
	public void setPopularityIndex(int popularityIndex) {
		this.popularityIndex = popularityIndex;
	}


	@Override
	public int hashCode(){
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj){
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}
}
