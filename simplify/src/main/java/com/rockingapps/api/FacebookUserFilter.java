package com.rockingapps.api;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.google.gson.Gson;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.Parameter;
import com.restfb.exception.FacebookException;
import com.restfb.json.JsonObject;
import com.restfb.types.Page;
import com.restfb.types.Post;
import com.restfb.types.User;

public class FacebookUserFilter {

	// private static String api_key = "296437783732677";
	// private static String secret = "90a2123b7186135e29c0049259b91c03";
	static String accessToken = "AAACEdEose0cBAApBowxTERVfMn4ZCx8cajnL4c9RkGsZAum3vtjyO5FWFXuEavklxn8zv8dzqhwAmo6nia5z3yDE15jkQD660zs7DTHceRJe7k7GNN";
	static DefaultFacebookClient facebookClient = new DefaultFacebookClient(
			accessToken);
	static {
	}

	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws FacebookException
	 */
	public static void main(String[] args) throws IOException {
		Calendar cal = new GregorianCalendar(2012, 10, 01);
		long starttime  = System.currentTimeMillis();
		System.out.println(System.currentTimeMillis());
//		Date date = new Date(System.currentTimeMillis() - 1000 * 3600 * 24 * 20);
		Date date = cal.getTime();
		// URL u = new
		// URL("https://graph.facebook.com/me/feed?fields=likes,from&access_token=AAACEdEose0cBAP7xlLalbWpDnCFAvmWjZCqggeR9nJ5xLhrZB9SVrpALYEWCdsVsOPdRUF81G92rm0slpKkgwvCZCHJIltyOV3ACT1ZA7EWxce4z7bVq");
		System.out.println(date);
		URL u = new URL("https://graph.facebook.com/me/feed?access_token="
				+ accessToken + "&fields=likes");
		while (true) {
			int i = 0;
			BufferedReader in = new BufferedReader(new InputStreamReader(
					u.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				System.out.println(inputLine);
				Gson g = new Gson();
				FB fb = g.fromJson(inputLine, FB.class);
				System.out.println(" paging next " + fb.paging.next);
				
				for (D1 d1 : fb.data) {
					if(d1.likes != null){
						System.out.println("count : " + d1.likes.count);
						System.out.println("date : " + d1.created_time);
					}
					if (parseDate(d1.created_time).compareTo(date) < 0){
						i = 1;
						break;
					}
				}
				if (i != 0)
					break;
				u = new URL(fb.paging.next);

			}
			if (i != 0)
				break;
			in.close();
		}
		// System.out.println(b.);
		// p1();
		long endtime = System.currentTimeMillis();
		System.out.println(endtime - starttime);
	}

	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss+SSSS";

	public static Date parseDate(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

		try {
			Date date1 = sdf.parse(str);
			return date1;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

	static void p1() {
		Connection<User> myFriends = facebookClient.fetchConnection(
				"me/friends", User.class);
		Connection<Post> myFeed = facebookClient.fetchConnection(
				"https://graph.facebook.com/me/feed?access_token="
						+ accessToken + "&fields=likes", Post.class);

		System.out
				.println("Count of my friends: " + myFriends.getData().size());
		System.out.println("First item in my feed: " + myFeed.getData().get(0));

		// Connections support paging and are iterable
		Date date = new Date(System.currentTimeMillis() - 1000 * 3600);
		System.out.println("*****************" + date.toString());
		for (List<Post> myFeedConnectionPage : myFeed) {
			int i = 0;
			for (Post post : myFeedConnectionPage) {
				System.out.println("Post: " + post.getLikesCount());
				if (post.getUpdatedTime().compareTo(date) < 0)
					i = 1;
			}
			if (i != 0)
				break;
		}

	}

	static void p2() {
		Connection<User> myFriends = facebookClient.fetchConnection(
				"me/friends", User.class);
		Connection<Post.Likes> myFeed = facebookClient.fetchConnection(
				"me/feed", Post.Likes.class);

		System.out
				.println("Count of my friends: " + myFriends.getData().size());
		System.out.println("First item in my feed: " + myFeed.getData().get(0));

		// Connections support paging and are iterable
		Date date = new Date(System.currentTimeMillis() - 1000 * 3600);
		System.out.println("*****************" + date.toString());
		for (List<Post.Likes> myFeedConnectionPage : myFeed) {
			int i = 0;
			for (Post.Likes post : myFeedConnectionPage) {
				System.out.println("Post: " + post.toString());
				// if (post..compareTo(date) < 0)
				// i=1;
			}
			if (i != 0)
				break;
		}

	}

	// SELECT post_id, fromid, time, text FROM comment WHERE post_id IN "
	static void fetchObject() {
		System.out.println("* Fetching single objects *");

		// User user = facebookClient.fetchObject("id", User.class);
		Page page = facebookClient.fetchObject("cocacola", Page.class);

		// System.out.println("User name: " + user.getName());
		System.out.println("Page likes: " + page.getLikes());

		String query = "SELECT uid, name FROM user WHERE uid=100000298817292";
		List<JsonObject> queryResults = facebookClient.executeQuery(query,
				JsonObject.class);

		if (queryResults.size() > 0)
			System.out.println(queryResults.get(0).getString("name"));
	}

	static void posts() {

		User user = facebookClient.fetchObject("me", User.class,
				Parameter.with("fields", "id"));

		String USER_UID = user.getId();

		List<Post> results = facebookClient.executeQuery(
				"SELECT likes, message FROM status WHERE uid=" + USER_UID,
				Post.class);
		// Like.class
		for (Post message : results) {
			System.out.println("message : " + message.getMessage());
			// System.out.println("message : " + message.getId());
			System.out.println("date : " + message.getLikesCount());
			break;
		}

		List<JsonObject> results1 = facebookClient.executeQuery(
				"SELECT status_id, message FROM status WHERE uid=" + USER_UID,
				JsonObject.class);
		// Like.class
		for (JsonObject message : results1) {
			// System.out.println("message : " + message.getString());
			// System.out.println("message : " + message.getId());
			System.out.println("date : " + message.toString());
			break;
		}

		List<Post.Likes> results2 = facebookClient.executeQuery(
				"SELECT likes, message FROM status WHERE uid=" + USER_UID,
				Post.Likes.class);
		// Like.class
		for (Post.Likes message : results2) {
			// System.out.println("message : " + message.getString());
			// System.out.println("message : " + message.getId());
			System.out.println("date : " + message.toString());
			break;
		}

	}
}

class FB {
	public List<D1> data;
	public D2 paging;
}

class D1 {
	public D1_from from;
	public D1_likes likes;
	public String id;
	public String created_time;
}

class D1_from {
	public String name;
	public String id;
}

class D1_likes {
	public int count;
}

class D2 {
	public String previous;
	public String next;
}