package com.rockingapps.status;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.concurrent.Callable;

import com.google.gson.Gson;

public class CallableExample implements Callable<Posts> {
	public Posts post;
	public String accessToken;
	
	public CallableExample(Posts post,String accessToken){
		this.post = post;
		this.accessToken = accessToken;
	}
	@Override
	public Posts call() throws Exception {
		if (post.likes.count != 25)
			return post;
		
		URL postUrl = new URL("https://graph.facebook.com/" + post.id
				+ "?access_token=" + accessToken
				+ "&fields=likes,message");

		try {
			BufferedReader in = new BufferedReader(
					new InputStreamReader(postUrl.openStream()));
			String inputLine;
			inputLine = in.readLine();
			// System.out.println(inputLine);

			Gson g = new Gson();
			Posts fb = g.fromJson(inputLine, Posts.class);
			if (fb.likes != null) {
				post.likes.count = fb.likes.count;
			}

			in.close();
		} catch (Exception e) {
			e.printStackTrace();
			return post;
		}
		return post;
	}

}
