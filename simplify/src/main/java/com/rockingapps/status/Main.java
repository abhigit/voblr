/**
 * 
 */
package com.rockingapps.status;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.grizzly.http.server.NetworkListener;
import org.glassfish.grizzly.threadpool.GrizzlyExecutorService;
import org.glassfish.grizzly.threadpool.ThreadPoolConfig;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;

/**
 * @author vinit Update Time : Nov 1, 2012 : 10:29:33 PM TODO
 */
public class Main {

	private static URI getBaseURI() {
		return UriBuilder
				.fromUri(
						"http://" + System.getProperty("HOST", "0.0.0.0") + "/")
				.port(9997).build();
	}

	public static final URI BASE_URI = getBaseURI();

	protected static HttpServer startServer() throws IOException {
		System.out.println("Starting grizzly...");
		System.out.println("Base uri : " + BASE_URI);
		ResourceConfig rc = new PackagesResourceConfig("com.rockingapps");
		return GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
	}

	public static void main(String[] args) throws IOException,
			InterruptedException {

		System.out.println("info enabled : ");

		HttpServer server = startServer();
		ThreadPoolConfig config = ThreadPoolConfig.defaultConfig()
				.setPoolName("mypool").setCorePoolSize(10).setMaxPoolSize(300);

		// reconfigure the thread pool
		NetworkListener listener = server.getListeners().iterator().next();
		GrizzlyExecutorService threadPool = (GrizzlyExecutorService) listener
				.getTransport().getWorkerThreadPool();
		threadPool.reconfigure(config);
		System.out.println("server created");
		// System.in.read();
		// httpServer.stop();
		Thread.currentThread().join();
	}
}
