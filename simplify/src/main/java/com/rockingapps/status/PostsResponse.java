package com.rockingapps.status;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PostsResponse {
	public int code;
	public List<Posts> data;
	public int count;
	
	public PostsResponse() {
		code = 200;
		count = 0;
	}

	public PostsResponse(int i) {
		code = i;
		count = 0;
		data = new ArrayList<Posts>();
	}
}
