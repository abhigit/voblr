package com.rockingapps.status;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

//import com.restfb.exception.FacebookException;

@Path("/oldtopstatus")
public class OldFacebookUserStatuses {

	private static final int MAX = 3;
	// private static String api_key = "296437783732677";
	// private static String secret = "90a2123b7186135e29c0049259b91c03";
	static String accessTokenGlobal = "AAACEdEose0cBAHiSmmmnvxYWxDKb38mWRBSqDVNW1ZASZAxr2pyoMShJx2gZBX280bvEaMw1psMgl1IXvdxNdPEoSJSGrtmnYDyC7yrvRcdbkKFG27T";
	// static DefaultFacebookClient facebookClient = new DefaultFacebookClient(
	// accessToken);
	static Calendar cal = new GregorianCalendar(2012, 0, 01);
	static Date date = cal.getTime();

	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws FacebookException
	 */
	public static void main(String[] args) {
		getTopStatuses(accessTokenGlobal, "ndtv");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static PostsResponse getTopStatuses(
			@QueryParam("access_token") String accessToken,
			@QueryParam("user") String user) {
		String userId = "me";

		if (user != null) {
			if (!user.trim().equals(""))
				userId = user;
		}

		long starttime = System.currentTimeMillis();

		try {
			List<Posts> totalStatuses = getLikePosts(userId, accessToken);

			long endtime = System.currentTimeMillis();
			System.out.println("Time till now : " + (endtime - starttime));

			for (Posts post : totalStatuses) {
				if (post.likes.count != 25)
					continue;
				URL postUrl = new URL("https://graph.facebook.com/" + post.id
						+ "?access_token=" + accessToken
						+ "&fields=likes,message");

				try {
					BufferedReader in = new BufferedReader(
							new InputStreamReader(postUrl.openStream()));
					String inputLine;
					inputLine = in.readLine();
					// System.out.println(inputLine);

					Gson g = new Gson();
					Posts fb = g.fromJson(inputLine, Posts.class);
					if (fb.likes != null) {
						post.likes.count = fb.likes.count;
					}

					in.close();
				} catch (Exception e) {
					e.printStackTrace();
					continue;
				}
			}

			Collections.sort(totalStatuses);
			int count = 0;
			System.out.println(totalStatuses.size());
			Iterator<Posts> post2 = totalStatuses.iterator();

			while (post2.hasNext()) {
				count++;
				post2.next();

				if (count > MAX)
					post2.remove();

			}

			System.out.println("likePosts.size() : " + totalStatuses.size());

			endtime = System.currentTimeMillis();
			System.out.println("user id : " + userId);
			System.out.println("Total time taken : " + (endtime - starttime));
			System.out.println(totalStatuses.size());

			PostsResponse p = new PostsResponse();
			p.data = totalStatuses;
			for (Posts p1 : p.data) {
				p1.count = p1.likes.count;
				p1.likes = null;
				System.out.println(" count : " + p1.count);
			}
			p.count = p.data.size();
			return p;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out
					.println("MalformedURLException with User Id : " + userId);
		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("IOException with User Id : " + userId);
		}
		return new PostsResponse(400);
	}

	public static List<Posts> getLikePosts(String userId, String accessToken)
			throws IOException {
		List<Posts> totalStatuses = new ArrayList<Posts>();

		URL u = new URL("https://graph.facebook.com/" + userId
				+ "/statuses?access_token=" + accessToken
				+ "&fields=id,likes,message,from");

		System.out.println(u.toString());

		while (true) {
			int i = 1;
			BufferedReader in = new BufferedReader(new InputStreamReader(
					u.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				// System.out.println(inputLine);
				Gson g = new Gson();
				FB fb = g.fromJson(inputLine, FB.class);

				for (D1 d1 : fb.data) {
					Date d = parseDate(d1.updated_time);
					if (d == null || d.getTime() < 1325376000
							|| d.compareTo(date) < 0) {
						i = 1;
						break;
					}

					i = 0;
					Posts post = new Posts();

					userId = d1.from.id;
					post.id = d1.from.id + "_" + d1.id;
					post.message = d1.message;
					if (d1.likes != null) {
						post.likes = d1.likes;
						post.likes.count = d1.likes.data.size();
						post.created_time = d1.updated_time;
						totalStatuses.add(post);
					}
				}
				if (i != 0)
					break;
				u = new URL(fb.paging.next);

			}
			if (i != 0)
				break;
			in.close();
		}

		System.out.println("totalStatuses.size() : " + totalStatuses.size());

		Collections.sort(totalStatuses);
		int count = 0;
		// System.out.println(totalStatuses.size());
		Iterator<Posts> post1 = totalStatuses.iterator();

		while (post1.hasNext()) {
			count++;
			if (post1.next().likes.count == 25) {
				continue;
			}

			if (count > MAX)
				post1.remove();

		}

		System.out.println("Top statuses with likes : " + totalStatuses.size());

		return totalStatuses;

	}

	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss+SSSS";

	public static Date parseDate(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

		try {
			Date date1 = sdf.parse(str);
			return date1;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return null;
	}

}

class FB {
	public List<D1> data;
	public D2 paging;
}

class D1 {
	public D1_from from;
	public D1_likes likes;
	public String id;
	public String updated_time;
	public String message;
}

class D1_from {
	public String name;
	public String id;
}

class D1_likes {
	public List<D1_from> data;
	public int count;
}

class D2 {
	public String previous;
	public String next;
}