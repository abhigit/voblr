package com.rockingapps.status;

import javax.xml.bind.annotation.XmlRootElement;


@XmlRootElement
public class Posts implements Comparable<Posts> {
	public String id;
	public D1_likes likes;
	public String created_time;
	public String message;
	public int count;
	
	public Posts() {
	}

	public int compareTo(Posts o) {
		if (this.likes == null)
			return -1;
		else if (o.likes == null)
			return 1;
		else {
			if (this.likes.count > o.likes.count)
				return -1;
			else if (this.likes.count < o.likes.count)
				return 1;
			else
				return 0;
		}
	}
}
