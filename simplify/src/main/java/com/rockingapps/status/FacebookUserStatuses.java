package com.rockingapps.status;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;

//import com.restfb.exception.FacebookException;

@Path("/topstatus")
public class FacebookUserStatuses {

	private static final int MAX = 3;
	// private static String api_key = "296437783732677";
	// private static String secret = "90a2123b7186135e29c0049259b91c03";
	static String accessTokenGlobal = "AAAENm8zMIcUBAKXgXXXjZBSGERZCw56yg7Wbfq5B88eVW5nsNrOWVLHcP8D1aCEnasHoIgAoX0Oawr6zt50B6epGJtUfoWpdZB65KnKFhNg496IVw6ZC";
	// static DefaultFacebookClient facebookClient = new DefaultFacebookClient(
	// accessToken);
	static Calendar cal = new GregorianCalendar(2012, 0, 01);
	static Date startDate = cal.getTime();

	static Calendar cal1 = new GregorianCalendar(2013, 0, 01);
	static Date endDate = cal1.getTime();
	public static int userCount = 1;
	/**
	 * @param args
	 * @throws IOException
	 * @throws URISyntaxException
	 * @throws FacebookException
	 */
	public static void main(String[] args) {
		getTopStatuses(accessTokenGlobal, "");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static PostsResponse getTopStatuses(
			@QueryParam("access_token") String accessToken,
			@QueryParam("user") String user) {
		String userId = "me";
		System.out.println("Concurrent request no : " + userCount++);
		if (user != null) {
			if (!user.trim().equals(""))
				userId = user;
		}

		long starttime = System.currentTimeMillis();

		try {
			List<Posts> totalStatuses = getLikePosts(userId, accessToken);

			long endtime = System.currentTimeMillis();
			System.out.println("Time till now : " + (endtime - starttime));

			ExecutorService pool = Executors.newFixedThreadPool(10);
			Set<Future<Posts>> set = new HashSet<Future<Posts>>();
			for (Posts post : totalStatuses) {
				Callable<Posts> callable = new CallableExample(post,
						accessToken);
				Future<Posts> future = pool.submit(callable);
				set.add(future);

			}

			for (Future<Posts> future : set) {
				try {
					future.get();
				} catch (InterruptedException e) {
					e.printStackTrace();
				} catch (ExecutionException e) {
					e.printStackTrace();
				}
			}
			pool.shutdown();

			Collections.sort(totalStatuses);
			int count = 0;
			System.out.println(totalStatuses.size());
			Iterator<Posts> post2 = totalStatuses.iterator();

			while (post2.hasNext()) {
				count++;
				post2.next();

				if (count > MAX)
					post2.remove();

			}

			System.out.println("likePosts.size() : " + totalStatuses.size());

			endtime = System.currentTimeMillis();
			System.out.println("user id : " + userId);
			System.out.println("access token : " + accessToken);
			System.out.println("Total time taken : " + (endtime - starttime));
			System.out.println(totalStatuses.size());

			PostsResponse p = new PostsResponse();
			p.data = totalStatuses;
			for (Posts p1 : p.data) {
				p1.count = p1.likes.count;
				p1.likes = null;
				System.out.println(" count : " + p1.count);
			}
			p.count = p.data.size();
			userCount--;
			return p;
		} catch (MalformedURLException e) {
			e.printStackTrace();
			System.out
					.println("MalformedURLException with User Id : " + userId);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception with User Id : " + userId);
		}
		userCount--;
		return new PostsResponse(400);
	}

	public static List<Posts> getLikePosts(String userId, String accessToken)
			throws IOException {
		List<Posts> totalStatuses = new ArrayList<Posts>();

		URL u = new URL("https://graph.facebook.com/" + userId
				+ "/statuses?access_token=" + accessToken
				+ "&fields=id,likes,message,from");

		System.out.println(u.toString());

		while (true) {
			int i = 1;
			BufferedReader in = new BufferedReader(new InputStreamReader(
					u.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				// System.out.println(inputLine);
				Gson g = new Gson();
				FB fb = g.fromJson(inputLine, FB.class);

				for (D1 d1 : fb.data) {
					Date d = parseDate(d1.updated_time);
					if (d == null || d.getTime() < 1325376000
							|| d.compareTo(startDate) < 0) {
						if (d.compareTo(endDate) > 0)
							continue;
						i = 1;
						break;
					}

					i = 0;
					Posts post = new Posts();

					userId = d1.from.id;
					post.id = d1.from.id + "_" + d1.id;
					post.message = d1.message;
					if (d1.likes != null) {
						post.likes = d1.likes;
						post.likes.count = d1.likes.data.size();
						post.created_time = d1.updated_time;
						totalStatuses.add(post);
					}
				}
				if (i != 0)
					break;
				u = new URL(fb.paging.next);

			}
			if (i != 0)
				break;
			in.close();
		}

		System.out.println("totalStatuses.size() : " + totalStatuses.size());

		Collections.sort(totalStatuses);
		int count = 0;
		// System.out.println(totalStatuses.size());
		Iterator<Posts> post1 = totalStatuses.iterator();

		while (post1.hasNext()) {
			count++;
			if (post1.next().likes.count == 25) {
				continue;
			}

			if (count > MAX)
				post1.remove();

		}

		System.out.println("Top statuses with likes : " + totalStatuses.size());

		return totalStatuses;

	}

	public static final String DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss+SSSS";

	public static Date parseDate(String str) {
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

		try {
			Date date1 = sdf.parse(str);
			return date1;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
