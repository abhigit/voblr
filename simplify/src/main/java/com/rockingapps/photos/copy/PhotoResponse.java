package com.rockingapps.photos.copy;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class PhotoResponse {
	public int code;
	public List<Photo> data;
	public int count;

	public PhotoResponse() {
		code = 200;
		count = 0;
		data = new ArrayList<Photo>();
	}
	
	public PhotoResponse(int i){
		code = i;
		count = 0;
	}
}
