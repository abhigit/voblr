package com.rockingapps.photos.copy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.restfb.DefaultFacebookClient;
import com.restfb.Facebook;
import com.restfb.FacebookClient;
import com.rockingapps.status.FacebookUserStatuses;

@Path("/oldtopphotos")
public class OldTopPhotos {

	private static final int MAX = 4;
	private static final int picNumber = 4;
	static String accessTokenGlobal = "AAAGbYyEGHOoBAJPDetwQ9dZA9j7b29RiaN6gYvkanGsbgi9Iv32uLtgWhwQHHvueYEW1piPNgtMoWTHYGyLE3wGiRX26269eb69tfFk4jXieJRpGM";
	static Calendar cal = new GregorianCalendar(2012, 0, 01);
	static Date date = cal.getTime();
	static long longDate = new Long("1325376000000");
	static long firstJan2012 = 1325376000;
	// static String baseFQL1 = "https://graph.facebook.com/fql?q=";
	static String baseFQL = "SELECT pid, like_info, created, caption, images FROM photo WHERE album_object_id=";
	static Gson g = new Gson();
	static {
		System.out.println(date.getTime());
		System.out.println(date);
		System.out.println(new Date(longDate));
		System.out.println(new Date(longDate).getTime());
		System.out.println(isDateIn2012(date));
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		getTopPhotos(accessTokenGlobal, "", "4");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static PhotoResponse getTopPhotos(
			@QueryParam("access_token") String accessToken,
			@QueryParam("user") String user, @QueryParam("count") String count) {

		long starttime = System.currentTimeMillis();
		String userId = getUserId(user);
		int picCount = getPicCount(count);

		System.out.println("UserId : " + userId + " count : " + count);
		FacebookClient facebookClient = new DefaultFacebookClient(
				accessToken);


		try {
			List<Album> albumList = getAlbums(userId, accessToken);
			System.out.println(albumList.size());
			long endtime = System.currentTimeMillis();
			System.out.println("Time till now : " + (endtime - starttime));

//			for (Album a : albumList)
//				System.out.println(a.updated_time);
			// System.out.println(albumList.size());

			List<PicInfo> picList = getPictures(albumList, facebookClient);
			endtime = System.currentTimeMillis();
			System.out.println("Total time taken : " + (endtime - starttime));

			return getPhotoResponse(picList, picCount);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("IOException with User Id : " + userId);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("IOException with User Id : " + userId);
		}

		return new PhotoResponse(400);

	}

	private static PhotoResponse getPhotoResponse(List<PicInfo> picList,
			int picCount) {
		PhotoResponse pr = new PhotoResponse();
		if (picList.size() == 0)
			return pr;

		int count = (picCount > picList.size()) ? picList.size() : picCount;
		for (int i = 0; i < count; i++) {
			Photo p = new Photo();
			PicInfo pic = picList.get(i);

			p.id = pic.pid;
			p.count = pic.like_info.like_count;
			p.created_time = getDateString(pic.created);
			p.message = pic.caption;

			if (pic.images != null && pic.images.size() != 0) {
				Images image = null;
				if (pic.images.size() > picNumber)
					image = pic.images.get(picNumber);
				else
					image = pic.images.get(pic.images.size() - 1);

				if (image != null) {
					p.height = image.height;
					p.width = image.width;
					p.url = image.source;
				}
			}

			pr.data.add(p);
			pr.count++;
		}

		return pr;
	}

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";
	
	private static String getDateString(long created) {
		try {
			Date d = new Date(created * 1000);

			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

			String date1 = sdf.format(d);
			return date1;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static List<PicInfo> getPictures(List<Album> albumList,
			FacebookClient facebookClient) {
		List<PicInfo> data = new ArrayList<PicInfo>();
		for (Album album : albumList) {
			String query = baseFQL + album.id;
			List<PicInfo> albumPics = facebookClient.executeQuery(query,
					PicInfo.class);
			int c = 0;
			for (PicInfo pic : albumPics) {
				if (pic.created >= firstJan2012 && pic.like_info.like_count != 0) {
					data.add(pic);
					c++;
				}
			}
			System.out.println("album : " + c + " : " + albumPics.size());
		}

		System.out.println("Total pics to be sorted : " + data.size());
		Collections.sort(data);
		for(PicInfo p : data)
			System.out.println("like count : " + p.like_info.like_count);
		return data;
	}

	public static List<PicInfo> getPictures1(List<Album> albumList,
			String accessToken) {

		for (Album album : albumList) {

			URL u;
			try {
				u = new URL(baseFQL + album.id + "&access_token=" + accessToken);
				System.out.println(u.toString());
				BufferedReader in = new BufferedReader(new InputStreamReader(
						u.openStream()));

				String inputLine;
				while ((inputLine = in.readLine()) != null) {

					PicList picList = g.fromJson(inputLine, PicList.class);
					System.out.println(picList.data.size());
				}
			} catch (MalformedURLException e) {
				System.out.println("MalformedURLException");
				e.printStackTrace();
			} catch (JsonSyntaxException e) {
				System.out.println("JsonSyntaxException");
				e.printStackTrace();
			} catch (IOException e) {
				System.out.println("IOException");
				e.printStackTrace();
			}

		}
		return null;
	}

	public static List<Album> getAlbums(String userId, String accessToken)
			throws IOException {

		AlbumList returnList = new AlbumList();
		AlbumList albumList = null;
		URL u = new URL("https://graph.facebook.com/" + userId
				+ "/albums?access_token=" + accessToken
				+ "&fields=updated_time,link,type,count");

		System.out.println(u.toString());

		while (true) {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					u.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {

				albumList = g.fromJson(inputLine, AlbumList.class);
				System.out.println(albumList.data.size());
				if (!(albumList.data == null || albumList.data.size() == 0
						|| albumList.paging == null || albumList.paging.next == null))
					u = new URL(albumList.paging.next);

				Iterator<Album> post1 = albumList.data.iterator();

				while (post1.hasNext()) {
					String s = post1.next().updated_time;
					Date date = FacebookUserStatuses.parseDate(s);

					if (!isDateIn2012(date)) {
//						System.out.println(s);
						post1.remove();
					}

				}

				returnList.data.addAll(albumList.data);

			}

			if ((albumList.data == null || albumList.data.size() == 0
					|| albumList.paging == null || albumList.paging.next == null))
				break;
		}

		return returnList.data;
	}

	public static boolean isDateIn2012(Date d) {
		if (d == null || d.getTime() < longDate || d.compareTo(date) < 0) {
			return false;
		}

		return true;
	}

	public static String getUserId(String user) {
		String userId = "me";

		if (user != null) {
			if (!user.trim().equals(""))
				userId = user;
		}

		return userId;
	}

	private static int getPicCount(String count) {

		if (count == null)
			return MAX;

		try {
			int c = Integer.parseInt(count);

			if (c > 0)
				return c;
		} catch (Exception e) {
			return MAX;
		}

		return MAX;
	}

}

// #######################################################
class PicList {
	@Facebook
	public List<PicInfo> data;
	@Facebook
	public Paging paging;
}

class PicInfo implements Comparable<PicInfo> {
	@Facebook
	public String pid;
	@Facebook
	public LikeInfo like_info;
	@Facebook
	public long created;
	@Facebook
	public String caption;
	@Facebook
	public List<Images> images;

	@Override
	public int compareTo(PicInfo pic) {

		try {
			if (this == null || this.like_info == null)
				return -1;
			if (pic == null || pic.like_info == null)
				return 1;
			if (this.like_info.like_count > pic.like_info.like_count)
				return -1;
			else if (this.like_info.like_count < pic.like_info.like_count)
				return 1;
			else
				return 0;
			
		} catch (Exception e) {
			return 0;
		}
	}

}

class Images {
	@Facebook
	public int height;
	@Facebook
	public int width;
	@Facebook
	public String source;
}

class LikeInfo {
	@Facebook
	public int like_count;
}

// #######################################################
class AlbumList {
	public List<Album> data = new ArrayList<Album>();
	public Paging paging;
}

class Album {
	public String id;
	public int count;
	public String type;
	public String updated_time;
	public String link;

}

// #######################################################
class Paging {
	public String previous;
	public String next;
}
