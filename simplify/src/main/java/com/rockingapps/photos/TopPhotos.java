package com.rockingapps.photos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;
import com.restfb.DefaultFacebookClient;
import com.restfb.Facebook;
import com.restfb.FacebookClient;
import com.rockingapps.status.FacebookUserStatuses;

@Path("/topphotos")
public class TopPhotos {

	private static final int MAX = 4;
	private static final int picNumber = 4;
	static String accessTokenGlobal = "AAAB1UGNOmjwBAM22ZBUbT4dMorUHCP3Yo7s4XLkGWZBflStvBNCr9SRlZAMk7KcszylxozbTeNnWtQiIg1mCHns6W1fjRqRwKHSBZAtK2QZDZD";
	
	static long firstJan2012InMs = new Long("1325376000000");
	static long firstJan2013InMs = new Long("1356998400000");
	
	static long firstJan2012 = 1325376000;
	static long firstJan2013 = 1356998400;

	// static String baseFQL1 = "https://graph.facebook.com/fql?q=";
	static String baseFQL = "SELECT pid, like_info, created, caption, images FROM photo WHERE album_object_id=";
	static Gson g = new Gson();

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		getTopPhotos(accessTokenGlobal, "", "4");
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
	public static PhotoResponse getTopPhotos(
			@QueryParam("access_token") String accessToken,
			@QueryParam("user") String user, @QueryParam("count") String count) {

		System.out.println("Concurrent request no : " + FacebookUserStatuses.userCount++);

		long starttime = System.currentTimeMillis();
		String userId = getUserId(user);
		int picCount = getPicCount(count);

		System.out.println("UserId : " + userId + " count : " + count);
		FacebookClient facebookClient = new DefaultFacebookClient(accessToken);

		try {
			List<Album> albumList = getAlbums(userId, accessToken);
			System.out.println(albumList.size());
			long endtime = System.currentTimeMillis();
			System.out.println("Time till now : " + (endtime - starttime));

			List<PicInfo> picList = getPictures(albumList, facebookClient);
			endtime = System.currentTimeMillis();
			System.out.println("Total time taken : " + (endtime - starttime));

			FacebookUserStatuses.userCount--;
			return getPhotoResponse(picList, picCount);

		} catch (IOException e) {
			e.printStackTrace();
			System.out.println("IOException with User Id : " + userId);
		} catch (Exception e) {
			e.printStackTrace();
			System.out.println("Exception with User Id : " + userId);
		}

		FacebookUserStatuses.userCount--;
		return new PhotoResponse(400);

	}

	private static PhotoResponse getPhotoResponse(List<PicInfo> picList,
			int picCount) {
		PhotoResponse pr = new PhotoResponse();
		if (picList.size() == 0)
			return pr;

		int count = (picCount > picList.size()) ? picList.size() : picCount;
		for (int i = 0; i < count; i++) {
			Photo p = new Photo();
			PicInfo pic = picList.get(i);

			p.id = pic.pid;
			p.count = pic.like_info.like_count;
			p.created_time = getDateString(pic.created);
			p.message = pic.caption;

			if (pic.images != null && pic.images.size() != 0) {
				Images image = null;
				if (pic.images.size() > picNumber)
					image = pic.images.get(picNumber);
				else
					image = pic.images.get(pic.images.size() - 1);

				if (image != null) {
					p.height = image.height;
					p.width = image.width;
					p.url = image.source;
				}
			}

			pr.data.add(p);
			pr.count++;
		}

		return pr;
	}

	private static final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

	private static String getDateString(long created) {
		try {
			Date d = new Date(created * 1000);

			SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);

			String date1 = sdf.format(d);
			return date1;
		} catch (Exception e) {
			e.printStackTrace();
		}

		return null;
	}

	public static List<PicInfo> getPictures(List<Album> albumList,
			FacebookClient facebookClient) {
		List<PicInfo> data = new ArrayList<PicInfo>();

		ExecutorService pool = Executors.newFixedThreadPool(10);
		Set<Future<List<PicInfo>>> set = new HashSet<Future<List<PicInfo>>>();

		for (Album album : albumList) {
			if(album.count == 0)
				continue;
			String query = baseFQL + album.id + "  limit " + album.count;
			CallablePhotos callable = new CallablePhotos(facebookClient, query);
			Future<List<PicInfo>> future = pool.submit(callable);
			set.add(future);
		}

		for (Future<List<PicInfo>> future : set) {

			List<PicInfo> albumPics;
			try {
				albumPics = future.get();

				if (albumPics != null && !albumPics.isEmpty()) {
					int c = 0;
					for (PicInfo pic : albumPics) {
//						System.out.println(pic.created);
//						if(pic.created > firstJan2013){
//							System.out.println(pic.created);
//							System.out.println(pic.images.get(0).source);
//						}

						if (pic.created >= firstJan2012
								&& pic.like_info.like_count != 0 && pic.created < firstJan2013) {
							data.add(pic);
							c++;
						}
					}
					System.out.println("album : " + c + " : "
							+ albumPics.size());
				}

			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
		}
		pool.shutdown();

		System.out.println("Total pics to be sorted : " + data.size());
		Collections.sort(data);
		int c = 0;
		for (PicInfo p : data){
			System.out.println("like count : " + p.like_info.like_count);
			c++;
			if(c > 10)
				break;
		}
		return data;
	}

	public static List<Album> getAlbums(String userId, String accessToken)
			throws IOException {

		AlbumList returnList = new AlbumList();
		AlbumList albumList = null;
		URL u = new URL("https://graph.facebook.com/" + userId
				+ "/albums?access_token=" + accessToken
				+ "&fields=updated_time,link,type,count");

		System.out.println(u.toString());

		while (true) {
			BufferedReader in = new BufferedReader(new InputStreamReader(
					u.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {

				albumList = g.fromJson(inputLine, AlbumList.class);
				System.out.println(albumList.data.size());
				if (!(albumList.data == null || albumList.data.size() == 0
						|| albumList.paging == null || albumList.paging.next == null))
					u = new URL(albumList.paging.next);

				Iterator<Album> post1 = albumList.data.iterator();

				while (post1.hasNext()) {
					Album d = post1.next();
					String s = d.updated_time;
//					System.out.println("pics count :" + d.count);
					Date date = FacebookUserStatuses.parseDate(s);

					if (!isDateIn2012(date)) {
						// System.out.println(s);
						post1.remove();
					}

				}

				returnList.data.addAll(albumList.data);

			}

			if ((albumList.data == null || albumList.data.size() == 0
					|| albumList.paging == null || albumList.paging.next == null))
				break;
		}

		return returnList.data;
	}

	public static boolean isDateIn2012(Date d) {
//		System.out.println(d.getTime());
//		System.out.println(d);
		if (d == null || d.getTime() <= firstJan2012InMs) {
			return false;
		}

		return true;
	}

	public static String getUserId(String user) {
		String userId = "me";

		if (user != null) {
			if (!user.trim().equals(""))
				userId = user;
		}

		return userId;
	}

	private static int getPicCount(String count) {

		if (count == null)
			return MAX;

		try {
			int c = Integer.parseInt(count);

			if (c > 0)
				return c;
		} catch (Exception e) {
			return MAX;
		}

		return MAX;
	}

}

// #######################################################
class PicList {
	@Facebook
	public List<PicInfo> data;
	@Facebook
	public Paging paging;
}

class PicInfo implements Comparable<PicInfo> {
	@Facebook
	public String pid;
	@Facebook
	public LikeInfo like_info;
	@Facebook
	public long created;
	@Facebook
	public String caption;
	@Facebook
	public List<Images> images;

	@Override
	public int compareTo(PicInfo pic) {

		try {
			if (this == null || this.like_info == null)
				return -1;
			if (pic == null || pic.like_info == null)
				return 1;
			if (this.like_info.like_count > pic.like_info.like_count)
				return -1;
			else if (this.like_info.like_count < pic.like_info.like_count)
				return 1;
			else
				return 0;

		} catch (Exception e) {
			return 0;
		}
	}

}

class Images {
	@Facebook
	public int height;
	@Facebook
	public int width;
	@Facebook
	public String source;
}

class LikeInfo {
	@Facebook
	public int like_count;
}

// #######################################################
class AlbumList {
	public List<Album> data = new ArrayList<Album>();
	public Paging paging;
}

class Album {
	public String id;
	public int count;
	public String type;
	public String updated_time;
	public String link;

}

// #######################################################
class Paging {
	public String previous;
	public String next;
}
