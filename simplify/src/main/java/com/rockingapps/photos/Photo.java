package com.rockingapps.photos;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Photo {
	public String id;
	public int count;
	public String created_time;
	public String message;
	
	public int height;
	public int width;
	public String url;

	public Photo() {
	}
}
