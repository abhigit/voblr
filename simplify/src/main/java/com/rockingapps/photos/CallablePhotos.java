package com.rockingapps.photos;

import java.util.List;
import java.util.concurrent.Callable;

import com.restfb.FacebookClient;
import com.rockingapps.status.Posts;

public class CallablePhotos implements Callable<List<PicInfo>> {
	public FacebookClient facebookClient;
	public String query;
	
	
	public CallablePhotos(FacebookClient facebookClient,String query){
		this.facebookClient = facebookClient;
		this.query = query;
	}
	@Override
	public List<PicInfo> call() throws Exception {

		try {
		List<PicInfo> albumPics =  facebookClient.executeQuery(query,
				PicInfo.class);
		
			return albumPics;
		}catch(Exception e){
			e.printStackTrace();
			System.out.println("CallablePhotos Exception with query : " + query);
		}
		
		return null;
	}

}
