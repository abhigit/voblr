package com.voblr.async;

import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

public class Task<V extends Callable<V>> extends FutureTask<V> {

    V v;

    private final TaskCompletionListner<V> listener;

    public Task(V callable, TaskCompletionListner<V> listener) {
        super(callable);
        v = callable;
        this.listener = listener;
    }

    @Override
    protected void done() {
        listener.onComplete(v);
    }

}
