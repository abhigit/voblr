package com.voblr.async;

import java.io.IOException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import com.voblr.cron.SolrSingleton;
import com.voblr.cron.jobs.MovieDbApiClient;
import com.voblr.cron.jobs.SolrMovieUpdatorJob;
import com.voblr.entity.Movie;
import com.voblr.entity.TMDB2MovieMapper;

public class Test {

    public static void main(String[] args) throws Exception {
        SolrMovieUpdatorJob job = new SolrMovieUpdatorJob();

        ExecutorService service = Executors.newFixedThreadPool(1);
        for (int i = 0; i < 100; i++) {
            service.submit(new Task<MovieWrapper>(new MovieWrapper(i + 5100),
                    new TaskTest(job)));
        }

        service.shutdown();
    }
}

class TaskTest implements TaskCompletionListner<MovieWrapper> {
    static AtomicInteger i = new AtomicInteger(0);

    SolrMovieUpdatorJob job;

    public TaskTest(SolrMovieUpdatorJob job) {
        this.job = job;
    }

    @Override
    public void onComplete(MovieWrapper movie) {
        int j = i.incrementAndGet();
        SolrServer solr = SolrSingleton.getUpdateInstace();
        try {
            synchronized (solr) {
                SolrQuery query = new SolrQuery();
                solr.addBean(movie.output);
                solr.commit();
                query.setQuery("*:*");
                QueryResponse rsp = solr.query(query);
                SolrDocumentList docs = rsp.getResults();
                System.out.println(docs);
            }
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        if (movie != null)
            System.out.println("Added : " + j + ") " + movie.output.movie_name);
    }

}

class MovieWrapper implements Callable<MovieWrapper> {
    String id;
    Movie output;

    MovieWrapper(int id) {
        this.id = String.valueOf(id);
    }

    @Override
    public MovieWrapper call() throws Exception {
        output = TMDB2MovieMapper.map(MovieDbApiClient.getClient()
                .getGsonMovie(id));
        return this;
    }

}
