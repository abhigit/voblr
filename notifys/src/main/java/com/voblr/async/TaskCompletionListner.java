package com.voblr.async;

import java.util.concurrent.Callable;

public interface TaskCompletionListner<V extends Callable<V>> {
    /**
     * Called After Task<V> if completed.
     */

    public void onComplete(V callable);
}
