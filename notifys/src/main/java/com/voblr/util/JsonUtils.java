package com.voblr.util;

import java.io.Reader;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class JsonUtils {

    public static JsonElement toGson(String json) {
        return new JsonParser().parse(json);
    }

    public static JsonElement toGson(Reader json) {
        return new JsonParser().parse(json);
    }

    public static JsonElement get(JsonElement obj, String... path) {
        if (obj.isJsonObject()) {
            return (get(obj.getAsJsonObject(), path));
        }
        return null;
    }

    public static JsonElement get(JsonObject obj, String... path) {

        if (path == null || path.length == 0 || obj.get(path[0]) == null) {
            return obj;
        }
        String curr = path[0];
        if (path.length == 1) {
            return obj.get(curr);
        }
        if (obj.has(curr)) {
            String[] temp = new String[path.length - 1];
            System.arraycopy(path, 1, temp, 0, path.length - 1);
            return get(obj.get(curr), temp);
        }

        return null;
    }

}
