package com.voblr.f.notifications;

import java.net.MalformedURLException;

import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.beans.Field;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocumentList;

import com.voblr.entity.Movie;

public class SolrInstance {
    public static void main(String[] args) throws MalformedURLException,
            SolrServerException {
        int queueSize = 10;
        int nThreads = 5;
        String serverUrl = "http://solr.rishabhmhjn.me/solr-voblr/";
        SolrServer solr = new ConcurrentUpdateSolrServer(serverUrl, queueSize,
                nThreads);

        SolrQuery query = new SolrQuery();
        query.setQuery("*:*");
        QueryResponse rsp = solr.query(query);
        SolrDocumentList docs = rsp.getResults();
        System.out.println(docs);

        try {
            SolrServer solrScore = new ConcurrentUpdateSolrServer(
                    "http://solr.rishabhmhjn.me/solr-voblr/", queueSize,
                    nThreads);
            Movie score = new Movie();
            score.movie_id = "5";
            score.movie_name = "iphone";
            solrScore.addBean(score);

            solrScore.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        query = new SolrQuery();
        query.setQuery("*:*");
        rsp = solr.query(query);
        docs = rsp.getResults();
        System.out.println(docs);

    }
}

class ScoreDoc {
    @Field
    public String movie_id;
    public String type;
    @Field
    public String movie_name;
    public int score;
}