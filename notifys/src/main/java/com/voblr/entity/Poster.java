package com.voblr.entity;

import com.google.gson.JsonElement;

public class Poster {
    public String image;

    public Poster() {
    }

    public Poster(JsonElement image) {
        this.image = image.toString();
        System.out.println(image);
    }
}
