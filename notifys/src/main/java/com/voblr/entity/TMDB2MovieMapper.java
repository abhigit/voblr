package com.voblr.entity;

import java.lang.reflect.AccessibleObject;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.voblr.cron.jobs.MovieDbApiClient;
import com.voblr.util.JsonUtils;

public class TMDB2MovieMapper {

    public static Movie map(String tmdbMovieInfoAsString) {
        return null;
    }

    public static Object toMovie(JsonElement obj, Class<?> clazz)
            throws InstantiationException, IllegalAccessException {
        List<TmdbField> fields = getTmdbFields(clazz);
        if (fields.isEmpty()) {
            throw new RuntimeException("class: " + obj.getClass()
                    + " does not define any fields.");
        }
        // System.out.println("Fields " + fields);
        Object doc = clazz.newInstance();
        for (TmdbField field : fields) {
            if ((field.isArray || field.isList)) {
                field.field.set(doc, get(obj, field));
            } else {
                field.field.set(doc, JsonUtils.get(obj, field.name)
                        .getAsString());
            }
        }
        return doc;
    }

    public static Collection<?> get(JsonElement obj, TmdbField field) {
        JsonElement ele = JsonUtils.get(obj, field.name);

        if ((field.isArray || field.isList)) {

            if (field.type != Object.class && ele.isJsonArray()) {
                try {
                    return get(ele, field.type);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                JsonElement resp = JsonUtils.get(obj, field.name);
                if (resp.isJsonArray()) {
                    Set<String> r = new TreeSet<String>();
                    for (JsonElement e : resp.getAsJsonArray()) {
                        r.add(e.toString());
                    }
                    return r;
                }
            }
        }
        return null;

    }

    public static Collection<?> get(JsonElement obj, Class<?> clazz)
            throws Exception {
        Set<String> resp = new TreeSet<String>();
        if (obj.isJsonArray()) {
            Object clazzObj = clazz.newInstance();
            for (JsonElement ele : obj.getAsJsonArray()) {
                try {
                    clazzObj = clazz.getEnclosingConstructor().newInstance(ele);
                } catch (Exception d) {
                }
                for (Field field : clazz.getFields()) {
                    field.setAccessible(true);
                    String tmp = (String) field.get(clazzObj);
                    if (tmp != null) {
                        String f = field.getName();
                        if (tmp.equals(JsonUtils.get(ele, f).getAsString())) {
                            for (Field a : clazz.getFields()) {
                                a.setAccessible(true);
                                if (a.get(clazzObj) == null) {
                                    resp.add(JsonUtils.get(ele, a.getName())
                                            .getAsString());
                                }
                            }
                        }
                    } else {

                    }
                }
            }
        }
        return resp;
    }

    private static List<TmdbField> getTmdbFields(Class<?> clazz) {
        List<TmdbField> fields = new ArrayList<TmdbField>();
        Class<?> superClazz = clazz;
        ArrayList<AccessibleObject> members = new ArrayList<AccessibleObject>();
        while (superClazz != null && superClazz != Object.class) {
            members.addAll(Arrays.asList(superClazz.getDeclaredFields()));
            members.addAll(Arrays.asList(superClazz.getDeclaredMethods()));
            superClazz = superClazz.getSuperclass();
        }
        for (AccessibleObject member : members) {
            if (member.isAnnotationPresent(TMDBField.class)) {
                member.setAccessible(true);
                fields.add(new TmdbField(member));
            }
        }
        return fields;
    }

    public static Movie map(JsonArray obj) {
        for (JsonElement ele : obj) {
            if (ele.isJsonObject()) {
                Movie movie = null;
                try {
                    movie = (Movie) toMovie(ele.getAsJsonObject(), Movie.class);
                } catch (InstantiationException | IllegalAccessException e) {
                    e.printStackTrace();
                }
                return movie;
            }
        }
        return null;
    }

    public static void main(String[] args) throws Exception {
        Movie m = map(MovieDbApiClient.getClient().getGsonMovie("8373"));
        System.out.println(m.movie_trailer);
        for (String image : m.move_posters)
            System.out.println(image);
    }

    private static class TmdbField {
        String name;
        java.lang.reflect.Field field;
        Class<?> fieldType, type;
        boolean isArray = false, isList = false;

        public String toString() {
            if (type == Object.class)
                return name;
            return name + "(" + type.getSimpleName() + ")";
        }

        public TmdbField(AccessibleObject member) {
            if (member instanceof java.lang.reflect.Field) {
                field = (java.lang.reflect.Field) member;
            } else {
                // currently not supporting methods for set/get
                return;
            }
            TMDBField tmdb = member.getAnnotation(TMDBField.class);
            setName(tmdb);
            storeType();

        }

        private void setName(TMDBField annt) {
            if (field == null)
                throw new RuntimeException("field is null");
            name = annt.value();
            type = annt.type();
        }

        private void storeType() {
            if (field != null) {
                fieldType = field.getType();
            }
            if (fieldType == Collection.class || fieldType == List.class
                    || fieldType == ArrayList.class) {
                fieldType = Object.class;
                isList = true;
            } else if (fieldType == byte[].class) {
                // no op
            } else if (fieldType.isArray()) {
                isArray = true;
                fieldType = fieldType.getComponentType();
            }
        }

    }
}
