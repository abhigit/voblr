package com.voblr.entity;

import java.util.Collection;

import org.apache.solr.client.solrj.beans.Field;

public class Movie {
    @Field
    @TMDBField("id")
    public String movie_id;

    @Field("movie_desc")
    @TMDBField("overview")
    public String movie_desc;

    // @Field("movie_directors")
    @TMDBField(value = "cast", type = Director.class)
    public Collection<String> movie_directors;

    @TMDBField(value = "cast", type = Actor.class)
    // @Field("movie_cast")
    public Collection<String> movie_cast;

    @TMDBField(value = "genres", type = Genre.class)
    // @Field("movie_generes")
    public Collection<String> movie_generes;

    @Field("movie_imdb_id")
    @TMDBField("imdb_id")
    public String movie_imdb_id;

    @Field("movie_last_modified")
    @TMDBField("last_modified_at")
    public String movie_last_modified;

    @TMDBField("name")
    public String movie_name;

    // @Field("movie_posters")
    @TMDBField(value = "posters")
    public Collection<String> move_posters;

    @Field("movie_released")
    @TMDBField("released")
    public String movie_released;

    @Field("movie_runtime")
    @TMDBField("runtime")
    public String movie_runtime;

    @Field("movie_tagline")
    @TMDBField("tagline")
    public String movie_tagline;

    // @Field("movie_trailer")
    @TMDBField("trailer")
    public String movie_trailer;
}
