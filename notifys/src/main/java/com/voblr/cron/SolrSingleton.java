package com.voblr.cron;

import java.net.MalformedURLException;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.impl.HttpSolrServer;

public class SolrSingleton {

    private static String server = System.getProperty("solrServer",
            "http://solr.rishabhmhjn.me/solr-voblr/");

    private static final int queueSize = 10;
    private static final int nThreads = 5;

    private static SolrServer instance_query = null;
    private static SolrServer instance_update = null;

    private SolrSingleton() {
    }

    public static SolrServer getQueryInstace() {
        if (instance_query == null) {
            instance_query = new HttpSolrServer(server);
        }
        return instance_query;
    }

    public static SolrServer getUpdateInstace() {
        if (instance_update == null) {
            try {
                instance_update = new ConcurrentUpdateSolrServer(server,
                        queueSize, nThreads);
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
        }
        return instance_update;
    }

}
