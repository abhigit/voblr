package com.voblr.cron.jobs;

import java.io.InputStream;
import java.io.InputStreamReader;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.BasicHttpParams;
import org.json.simple.JSONArray;
import org.json.simple.JSONValue;

import com.google.gson.JsonArray;
import com.voblr.util.JsonUtils;

public class MovieDbApiClient {
    public static void main(String[] args) throws Exception {
        String apiKey = "dcc6ef1530b9b6a6ce80a49fc3a59df0";
        MovieDbApiClient client = new MovieDbApiClient(apiKey);
        long start = System.currentTimeMillis();
        System.out.println(client.getGsonMovie("51162"));
        System.out.println(System.currentTimeMillis() - start);
    }

    private String baseUrl = "http://api.themoviedb.org/";
    private String apiKey;
    private HttpClient httpClient;

    private MovieDbApiClient(String apiKey) {
        this.apiKey = apiKey;

        BasicHttpParams httpParams = new BasicHttpParams();
        httpParams.setBooleanParameter(ClientPNames.HANDLE_REDIRECTS, false);
        httpClient = new DefaultHttpClient(httpParams);
    }

    private static final Object OBJECT_LOCK = new Object();

    private static MovieDbApiClient client;

    public static MovieDbApiClient getClient() {
        if (client == null) {
            synchronized (OBJECT_LOCK) {
                client = new MovieDbApiClient(
                        "dcc6ef1530b9b6a6ce80a49fc3a59df0");
            }
        }
        return client;
    }

    public JsonArray getGsonMovie(String movieId) throws Exception {

        return JsonUtils.toGson(getSimpleJsonMovie(movieId).toJSONString())
                .getAsJsonArray();
    }

    public JSONArray getSimpleJsonMovie(String movieId) throws Exception {
        String movieUrl = buildMovieUrl(movieId);
        JSONArray json = getUrlAsJson(movieUrl);
        if (notFound(json)) {
            // throw new Exception("Movie not found");
            return null;
        }
        return json;
    }

    // The movie db API signals movie not found by returning an array containing
    // the string "Nothing found."
    private boolean notFound(JSONArray json) {
        return json.get(0).equals("Nothing found.");
    }

    private JSONArray getUrlAsJson(String url) throws Exception {
        HttpGet httpGet = new HttpGet(url);
        InputStream inputStream = null;
        try {
            HttpResponse response = httpClient.execute(httpGet);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) {
                throw new RuntimeException("Status code nok OK: " + statusCode);
            }
            HttpEntity entity = response.getEntity();
            inputStream = entity.getContent();
            InputStreamReader responseReader = new InputStreamReader(
                    inputStream);
            Object parsedObject = JSONValue.parseWithException(responseReader);
            if (parsedObject instanceof JSONArray) {
                return (JSONArray) parsedObject;
            } else {
                throw new Exception("Received data are not a JSON array");
            }
        } catch (Exception e) {
            throw new Exception("Failed to get data from " + url + " - "
                    + e.getMessage(), e);
        } finally {
            IOUtils.closeQuietly(inputStream);
        }
    }

    private String buildMovieUrl(String movieId) {
        return String.format("%s2.1/Movie.getInfo/en/json/%s/%s", baseUrl,
                apiKey, movieId);
    }

    public JSONArray getPerson(String personId) throws Exception {
        String personUrl = buildPersonUrl(personId);
        return getUrlAsJson(personUrl);
    }

    private String buildPersonUrl(String personId) {
        return String.format("%s2.1/Person.getInfo/en/json/%s/%s", baseUrl,
                apiKey, personId);
    }
}