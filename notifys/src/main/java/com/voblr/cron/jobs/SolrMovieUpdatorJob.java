package com.voblr.cron.jobs;

import java.io.IOException;
import java.util.Collection;
import java.util.Collections;

import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;

import com.voblr.cron.SolrSingleton;
import com.voblr.entity.Movie;

/**
 * This {@link Job} will update the movies
 * 
 * @author abhi
 * 
 */
public class SolrMovieUpdatorJob implements Job {

    @SuppressWarnings("unchecked")
    @Override
    public void execute(JobExecutionContext arg0) throws JobExecutionException {
        Collection<Movie> movies = null;
        movies = Collections.synchronizedCollection((Collection<Movie>) arg0
                .get("movies"));
        SolrServer solr = SolrSingleton.getUpdateInstace();
        try {
            solr.addBeans(movies);
            solr.commit();
            movies.clear();
        } catch (SolrServerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
