package com.voblr.cron.schedules;

import org.quartz.JobDetail;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

public class MovieUpdateScheduler {
    public static void main(String[] args) throws SchedulerException {
        SchedulerFactory sf = new StdSchedulerFactory();
        Scheduler sched = sf.getScheduler();

        JobDetail job = null;

        Trigger trigger = TriggerBuilder.newTrigger().startNow().forJob(job)
                .withIdentity("trigger1", "group1").withSchedule(null).build();

        sched.scheduleJob(job, trigger);
    }
}
