package com.voblr;

import static com.voblr.api.common.GenericActions.movieRepository;
import static com.voblr.api.common.GenericActions.template;
import static com.voblr.api.common.GenericActions.userRepository;

import java.io.IOException;
import java.util.Iterator;

import com.voblr.api.common.GenericActions;
import com.voblr.api.entity.Movie;
import com.voblr.api.entity.User;
import com.voblr.api.services.common.VobService;

public class App {

    public static void main(String[] args) throws IOException {
        
        
        System.out.println("Before...");
//        ClassPathXmlApplicationContext ctx = 
//                new ClassPathXmlApplicationContext("/default/context.xml");
//        System.out.println("After...");
//        UserRepository repo = ctx.getBean(UserRepository.class);
//        MovieRepository mrepo = ctx.getBean(MovieRepository.class);
//        Neo4jTemplate tmplt = ctx.getBean(Neo4jTemplate.class);
        
        GenericActions ga = new GenericActions();
        
        VobService ser = new VobService();
        
        ser.setMovieRepository(movieRepository);
        ser.setUserRepository(userRepository);
        ser.setNeo4jTemplate(template);
        
//        ser.main(args);
        
        
        Movie m = movieRepository.createyOrGetMovie("abhijeet");
        User u = userRepository.createyOrGetUser("abhijeet");
        
        System.out.println(m.id+" "+u.id);
        
//        Iterator<String> a1 = r.getPropertyKeys().iterator();
//        while (a1.hasNext()) {
//            String temp = a1.next();
//            System.out.println("prop name : " + temp + " : "
//                    + r.getProperty(temp));
//        }
        System.out.println(template.getNode(m.id).getProperty("type")
                +" "+template.getNode(u.id).getProperty("type"));
    }
}
