package com.voblr.api.entity;

import java.util.HashSet;
import java.util.Set;

import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedToVia;

import com.voblr.api.entity.relations.FOLLOW;
import com.voblr.api.entity.relations.VOB;
import com.voblr.api.entity.relations.constants.Rels;

/**
 * A Spring Data Neo4j enhanced World entity.
 * <p/>
 * This is the initial POJO in the Universe.
 */
@NodeEntity
public class User implements Entity {
    @GraphId
    public Long id;

    @Indexed
    public String name;
    
    public final String type = type().name();

    @Fetch
    @RelatedToVia(type = Rels.FOLLOWS)
    public Set<FOLLOW> followees = new HashSet<FOLLOW>();
    //
    // @RelatedTo(elementClass = Movie.class,type = Rels.VOBS)
    // public Set<Movie> movies = new HashSet<Movie>();

    @Fetch
    @RelatedToVia(type = Rels.VOBS)
    public Set<VOB> vobs = new HashSet<VOB>();

    public User(String name) {
        this.name = name;
    }

    public User() {
    }

    @Override
    public String toString() {
        return String.format("User{name='%s'}", name);
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        User other = (User) obj;
        if (id == null)
            return other.id == null;
        return id.equals(other.id);
    }

    @Override
    public String id() {
        return name;
    }

    @Override
    public Type type() {
        return Entity.Type.USER;
    }
}
