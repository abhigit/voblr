package com.voblr.api.entity.relations.constants;

import org.neo4j.graphdb.RelationshipType;

public enum RelationshipTypes implements RelationshipType {
    FOLLOWS ,
    VOBS 
}
