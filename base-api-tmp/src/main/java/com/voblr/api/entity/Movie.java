package com.voblr.api.entity;

import java.util.HashSet;
import java.util.Set;

import org.neo4j.graphdb.Direction;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.Indexed;
import org.springframework.data.neo4j.annotation.NodeEntity;
import org.springframework.data.neo4j.annotation.RelatedTo;

import com.voblr.api.entity.relations.constants.Rels;

@NodeEntity
public class Movie implements Entity{
	@GraphId
	public Long id;
    
    @Indexed
    public String name;

    public String type = type().name();
    
    @Fetch
    @RelatedTo(elementClass = User.class,type = Rels.VOBS, direction = Direction.INCOMING)
    public Set<User> users = new HashSet<User>();
    
    public Movie(String name) {
		this.name = name;
	}
    
    public Movie() {
	}

	@Override
    public String toString()
    {
        return String.format("Movie{name='%s'}", name);
    }

    @Override
    public String id() {
        return name;
    }

    @Override
    public Type type() {
        return Entity.Type.MOVIE;
    }

}
