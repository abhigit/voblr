package com.voblr.api.entity.relations.constants;

public class Rels {
    
    public static final String FOLLOWS ="FOLLOWS";
    public static final String VOBS ="VOBS";
    public static final String EVENT ="EVENT";
}
