package com.voblr.api.entity.relations;

import org.neo4j.graphdb.RelationshipType;

import com.voblr.api.entity.Entity;

public interface EVENT extends RelationshipType{
    public Entity getStartEntity();
    public Entity getEndEntity();
    public long getEventTimestamp();
    
}
