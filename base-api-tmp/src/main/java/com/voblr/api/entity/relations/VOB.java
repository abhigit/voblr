package com.voblr.api.entity.relations;

import javax.xml.bind.annotation.XmlRootElement;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.voblr.api.entity.Entity;
import com.voblr.api.entity.Movie;
import com.voblr.api.entity.User;
import com.voblr.api.entity.relations.constants.Rels;

@RelationshipEntity(type = Rels.VOBS)
@XmlRootElement
public class VOB implements EVENT {
    @GraphId
    public Long id;
 
    @Fetch
    @StartNode
    public User a;
    @Fetch
    @EndNode
    public Movie m;
    public long stamp;

    public VOB() {
    }

    public VOB(User user, Movie other, long now) {
        this.a = user;
        this.m = other;
        this.stamp = now;
    }

    @Override
    public Entity getStartEntity() {
        return a;
    }

    @Override
    public Entity getEndEntity() {
        return m;
    }
    @Override
    public long getEventTimestamp() {
        return stamp;
    }

    @Override
    public String name() {
        return Rels.VOBS;
    }
}
