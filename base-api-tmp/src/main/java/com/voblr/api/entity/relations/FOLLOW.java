package com.voblr.api.entity.relations;

import org.springframework.data.neo4j.annotation.EndNode;
import org.springframework.data.neo4j.annotation.Fetch;
import org.springframework.data.neo4j.annotation.GraphId;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.data.neo4j.annotation.StartNode;

import com.voblr.api.entity.Entity;
import com.voblr.api.entity.User;
import com.voblr.api.entity.relations.constants.Rels;

@RelationshipEntity(type = Rels.FOLLOWS)
public class FOLLOW implements EVENT{
    
    @GraphId
    public Long id;
    
    @StartNode public User a;
    @Fetch
    @EndNode public User b;
    public long stamp;

    public FOLLOW(){}
    
    public FOLLOW(User user, User friend, long now) {
        this.a = user;
        this.b = friend;
        this.stamp = now;
    }

    @Override
    public Entity getStartEntity() {
        return a;
    }

    @Override
    public Entity getEndEntity() {
        return b;
    }

    @Override
    public long getEventTimestamp() {
        return stamp;
    }

    @Override
    public String name() {
        return Rels.FOLLOWS;
    }
}
