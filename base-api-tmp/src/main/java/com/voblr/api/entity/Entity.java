package com.voblr.api.entity;


public interface Entity {

    public static enum Type {
        MOVIE, USER
    }

    public String id();

    public Type type();
}
