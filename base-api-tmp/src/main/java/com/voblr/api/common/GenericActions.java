package com.voblr.api.common;

import java.net.UnknownHostException;
import java.util.Set;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.data.neo4j.support.Neo4jTemplate;

import com.voblr.api.repositories.movie.MovieRepository;
import com.voblr.api.repositories.user.UserRepository;

import com.mongodb.Mongo;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.DBCursor;
import com.mongodb.MongoException;

public class GenericActions {
    private static ApplicationContext ctx = new ClassPathXmlApplicationContext(
            "default/context.xml");
    public static UserRepository userRepository = ctx
            .getBean(UserRepository.class);
    public static MovieRepository movieRepository = ctx
            .getBean(MovieRepository.class);
    public static Neo4jTemplate template = ctx.getBean(Neo4jTemplate.class);

    public static Mongo m;

    public static DB db;

    public GenericActions() {
        System.out.println("Connecting Mongo");
        try {
            m = new Mongo("dev.ibhupi.com", 27017);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (MongoException e) {
            e.printStackTrace();
        }
        db = m.getDB("voblr");
        
        Set<String> colls = db.getCollectionNames();

        for (String s : colls) {
            System.out.println(s);
        }

    }

}
