package com.voblr.api.common.response;

import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.api.entity.relations.FOLLOW;

@XmlRootElement
public class UserFollowees extends APIResponse {
    public int count;
    public Collection<UserStamp> follwees;

    public UserFollowees() {
    }
    public UserFollowees(Collection<FOLLOW> followees) {
        this.count = followees.size();
        this.follwees = new PriorityQueue<UserStamp>(count,
                new Comparator<UserStamp>() {

                    @Override
                    public int compare(UserStamp o1, UserStamp o2) {
                        long diff = o2.timestamp - o1.timestamp;

                        if (diff == 0l) {
                            return 0;
                        }
                        if (diff > 0)
                            return 1;

                        return -1;
                    }
                });
        for (FOLLOW v : followees) {
            follwees.add(new UserStamp(v));
        }
    }
}

@XmlRootElement
class UserStamp {
public String user_id;
public long timestamp;

public UserStamp() {
}

public UserStamp(FOLLOW v) {
    user_id = v.b.name;
    timestamp = v.stamp;
}

}