package com.voblr.api.common.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NewsFeedResponse extends APIResponse{

    
    public List<NewsFeed> newsFeed = new ArrayList<NewsFeed>();
    public NewsFeedResponse() {
        super();
    }

    public NewsFeedResponse(List<NewsFeed> n) {
        super();
        newsFeed = n;
    }

}
