package com.voblr.api.common.response;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.api.common.ResponseConstants;
import com.voblr.api.common.response.VOBStatus;

@XmlRootElement
public class NonLoginUser extends APIResponse{

	public NonLoginUser() {
		super(ResponseConstants.FAILURE,"User is not logged in");
	}

	public NonLoginUser(String status, String message) {
		super(status, message);
	}

	public NonLoginUser(String message) {
		super(message);
	}

}
