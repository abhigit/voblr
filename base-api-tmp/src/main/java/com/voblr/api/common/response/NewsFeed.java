package com.voblr.api.common.response;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class NewsFeed {
	
	public String imageUrl = "Sorry. No image available";
	public int vobCount = 0;
	public String movieName = "No name available";
	public String movieId = "-1";
	
	public String userId;
	public String userName;

	public int moreUsers = 0;

//	public String extendedUsers;

	public long timestamp = System.currentTimeMillis();
	public int notificationType = new Integer(1);

	public NewsFeed() {
		super();
		moreUsers = 0;
		userId="-1";
		userName="no one";
	}

	public NewsFeed(String imageUrl, int vobCount, String movieName,
			String movieId, String userId, String userName, int moreUsers, 
			long timestamp, int notificationType) {

		super();
		this.imageUrl = imageUrl;
		this.vobCount = vobCount;
		this.movieName = movieName;
		this.movieId = movieId;
		this.userId = userId;
		this.userName = userName;
		this.moreUsers = moreUsers;
		this.timestamp = timestamp;
		this.notificationType = notificationType;
	}
	
	

}

