package com.voblr.api.common.response;

import java.text.MessageFormat;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class NGResponse extends APIResponse{
 // just to make JAXB happy
    public NGResponse(){};

    public NGResponse(Exception e) {
        super("NG",MessageFormat.format("'{0}'", e.getMessage()));
    }
    
}
