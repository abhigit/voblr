package com.voblr.api.common.response;

public class Movie {
    int movieId;
    long  timestamp;
    String movieName;
    String movieUrl;
    String movieImageUrl;
    int movieVobs;
    public Movie() {
    }
    
    public Movie(int movieId, long timestamp, String movieName,
            String movieUrl, String movieImageUrl) {
        super();
        this.movieId = movieId;
        this.timestamp = timestamp;
        this.movieName = movieName;
        this.movieUrl = movieUrl;
        this.movieImageUrl = movieImageUrl;
    }

    public Movie(int movieId, long timestamp) {
        super();
        this.movieId = movieId;
        this.timestamp = timestamp;
    }

    public String getMovieName() {
        return movieName;
    }


    public int getMovieVobs() {
        return movieVobs;
    }

    public void setMovieVobs(int movieVobs) {
        this.movieVobs = movieVobs;
    }

    public void setMovieName(String movieName) {
        this.movieName = movieName;
    }


    public String getMovieUrl() {
        return movieUrl;
    }


    public void setMovieUrl(String movieUrl) {
        this.movieUrl = movieUrl;
    }


    public String getMovieImageUrl() {
        return movieImageUrl;
    }


    public void setMovieImageUrl(String movieImageUrl) {
        this.movieImageUrl = movieImageUrl;
    }


    public int getMovieId() {
        return movieId;
    }
    public void setMovieId(int movieId) {
        this.movieId = movieId;
    }
    public long getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    @Override
    public String toString() {
        return "Movie [movieId=" + movieId + ", timestamp=" + timestamp
                + ", movieName=" + movieName + ", movieUrl=" + movieUrl
                + ", movieImageUrl=" + movieImageUrl + ", movieVobs="
                + movieVobs + "]";
    }

}
