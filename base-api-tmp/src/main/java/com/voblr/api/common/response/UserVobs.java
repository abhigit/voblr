package com.voblr.api.common.response;

import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.api.entity.relations.VOB;

@XmlRootElement
public class UserVobs extends APIResponse {
    public int count;
    public Collection<MovieStamp> movies;

    public UserVobs() {
    }

    public UserVobs(Collection<VOB> vobs) {
        this.count = vobs.size();
        this.movies = new PriorityQueue<MovieStamp>(count,
                new Comparator<MovieStamp>() {

                    @Override
                    public int compare(MovieStamp o1, MovieStamp o2) {
                        long diff = o2.timestamp - o1.timestamp;

                        if (diff == 0l) {
                            return 0;
                        }
                        if (diff > 0)
                            return 1;

                        return -1;
                    }
                });
        for (VOB v : vobs) {
            movies.add(new MovieStamp(v));
        }
    }

}

@XmlRootElement
class MovieStamp {
    public String movie_id;
    public long timestamp;

    public MovieStamp() {
    }

    public MovieStamp(VOB v) {
        movie_id = v.m.name;
        timestamp = v.stamp;
    }

}