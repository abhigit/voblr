package com.voblr.api.common.response;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class VoblineResponse extends APIResponse{

    public List<Movie> movieVobEvents = null;

    public VoblineResponse() {
        super();
        movieVobEvents = new ArrayList<Movie>();
    }


//    public VoblineResponse(List<GenericEvent> genericEventList) {
//        super();
//        movieVobEvent = new ArrayList<Movie>();
//        for (GenericEvent genericEvent : genericEventList) {
//            movieVobEvent.add(new Movie(genericEvent.getObjectId(), genericEvent.getTimestamp()));
//        }
//    }


    @Override
    public String toString() {
        return "VoblineResponse [movieVobEvents=" + movieVobEvents.toString() + "]";
    }
    
}