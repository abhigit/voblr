package com.voblr.api.common.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Events {
	public String subject_id;
	public String object_id;
	public long timestamp;
	
	public Events(){
	}
	
	public Events(String subject_id, String object_id, long timestamp) {
		this.subject_id = subject_id;
		this.object_id = object_id;
		this.timestamp = timestamp;
	}

	
}
