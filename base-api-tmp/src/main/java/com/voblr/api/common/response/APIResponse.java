package com.voblr.api.common.response;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class APIResponse {
    public String status = "21";
    public String message = "No message defined";
    public String tmp    = "notRakuten";
    public int vobCount = 0;
    public String movieId;
    public APIResponse(){
    }
    
    public APIResponse(String message){
        this.message = message;
    }
    
    public APIResponse(String status,String message){
        this.status = status;
        this.message = message;
    }
}
