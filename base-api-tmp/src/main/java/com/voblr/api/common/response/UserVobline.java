package com.voblr.api.common.response;

import java.util.Collection;
import java.util.Comparator;
import java.util.PriorityQueue;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.api.entity.Entity;
import com.voblr.api.entity.relations.EVENT;

@XmlRootElement
public class UserVobline extends APIResponse {
    public Collection<EventStamp> vobline;

    public UserVobline() {
    }

    public UserVobline(Collection<EVENT> events) {
        this.vobline = new PriorityQueue<EventStamp>(events.size(),
                new Comparator<EventStamp>() {

                    @Override
                    public int compare(EventStamp o1, EventStamp o2) {
                        long diff = o2.timestamp - o1.timestamp;

                        if (diff == 0l) {
                            return 0;
                        }
                        if (diff > 0)
                            return 1;

                        return -1;
                    }
                });
        for (EVENT v : events) {
            this.vobline.add(new EventStamp(v));
        }
    }

}

@XmlRootElement
class EventStamp {
    public String action;
    public EventType subject;
    public EventType object;
    public long timestamp;
    

    public EventStamp() {
    }

    public EventStamp(EVENT v) {
        this(v,false);
    }
    public EventStamp(EVENT v,boolean subjectRequired) {
        if(subjectRequired)
            subject = new EventType(v.getStartEntity());
        object = new EventType(v.getEndEntity());
        timestamp = v.getEventTimestamp();
        action =v.name();
    }
}

@XmlRootElement
class EventType {
    public String id;
    public String type;

    public EventType() {
    }

    public EventType(Entity v) {
        id = v.id();
        type = v.type().name();
    }

}