package com.voblr.api.common.response;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.api.entity.relations.EVENT;

@XmlRootElement
public class UserFeeds extends APIResponse {
    public Collection<EventStamp> feeds;

    public UserFeeds() {
    }

    public UserFeeds(Iterator<EVENT> events) {
        this.feeds = new ArrayList<EventStamp>();
        while(events.hasNext())
            this.feeds.add(new EventStamp(events.next(),true));
    }

}
