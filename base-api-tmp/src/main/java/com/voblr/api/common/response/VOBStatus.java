package com.voblr.api.common.response;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.core.GenericEntity;
import javax.xml.bind.annotation.XmlRootElement;

import com.voblr.api.rpc.client.GraphServices;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;

@XmlRootElement
public class VOBStatus extends APIResponse {
	public String count;
	public List<Events> vobs;
	public String movieId;
	
	// just to make JAXB happy
	public VOBStatus() {
	};

	public VOBStatus(String status, String message, String count) {
		super(status, message);
		if (Integer.parseInt(count) == -1)
			this.count = "0";
		else
			this.count = count;
	};

	public VOBStatus(String status, String message) {
		super(status, message);
	}

	public VOBStatus(String status, String message,
			List<GenericEvent> eventsList) {
		super("21", message);
//		System.out.println(eventsList);
//		this.movieId = movieId;
		vobs = new ArrayList<Events>();
		for (GenericEvent g : eventsList) {
			vobs.add(new Events(g.getSubjectId(), g.getObjectId(), g
					.getTimestamp()));
//			System.out.println(vobs.get(vobs.size()-1).subject_id + " ");
		}
//		System.out.println(vobs);

	}

}
