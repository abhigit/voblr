package com.voblr.api.common;

import static com.voblr.api.common.GenericActions.userRepository;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.NGResponse;
import com.voblr.api.common.response.UserFeeds;

@Path("/vobfeeds")
public class VobFeeds {
    @GET
    @Produces("application/json")
    @Path("{user_id}")
    public APIResponse getVobFeeds(@PathParam("user_id") String uid) {
        try {
            return new UserFeeds(userRepository.getFeeds(uid));
        } catch (Exception e) {
            e.printStackTrace();
            return new NGResponse(new Exception(
                    "This feature is not yet available."));
        }

    }
}
