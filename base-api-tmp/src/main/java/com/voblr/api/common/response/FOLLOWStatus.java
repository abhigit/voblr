package com.voblr.api.common.response;

import java.text.MessageFormat;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class FOLLOWStatus extends APIResponse {

    // just to make JAXB happy
    public FOLLOWStatus() {
    }
    
    public FOLLOWStatus(String status,String message) {
    	super(status,message);
    
    }

    public FOLLOWStatus(String status,String message, String uid1, String uid2) {
        super(MessageFormat.format("{0} followed {1}", uid1, uid2));
    }
}
