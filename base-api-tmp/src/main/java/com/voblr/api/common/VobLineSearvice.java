package com.voblr.api.common;

import static com.voblr.api.common.GenericActions.userRepository;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.NGResponse;
import com.voblr.api.common.response.UserVobline;
import com.voblr.api.entity.relations.EVENT;

@Path("/vobline")
public class VobLineSearvice {
    @GET
    @Produces("application/json")
    @Path("{user_id}")
    public APIResponse getVobline(@PathParam("user_id") String uid) {
        try {
            Queue<EVENT> events =new PriorityQueue<EVENT>(10,
                    new Comparator<EVENT>() {

                @Override
                public int compare(EVENT o1, EVENT o2) {
                    long diff = o2.getEventTimestamp() - o1.getEventTimestamp();

                    if (diff == 0l) {
                        return 0;
                    }
                    if (diff > 0)
                        return 1;

                    return -1;
                }
            });

            for (EVENT e : userRepository.createyOrGetUser(uid).vobs) {
                System.out.println(e);
                events.add(e);
            }
            for (EVENT e : userRepository.createyOrGetUser(uid).followees) {
                System.out.println(e);
                events.add(e);
            }

            return new UserVobline(events);
        } catch (Exception e) {
            e.printStackTrace();
            return new NGResponse(new Exception(
                    "This feature is not yet available."));
        }

    }
}
