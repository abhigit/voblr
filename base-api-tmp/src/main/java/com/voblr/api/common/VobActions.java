package com.voblr.api.common;

import static com.voblr.api.common.GenericActions.movieRepository;
import static com.voblr.api.common.GenericActions.userRepository;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.NGResponse;
import com.voblr.api.common.response.UserVobs;
import com.voblr.api.common.response.VOBStatus;
import com.voblr.api.entity.relations.VOB;

@Path("/vob")
public class VobActions {
    @GET
    @Produces("application/json")
    @Path("{user_id}/{movie_id}")
    public APIResponse doVob(@PathParam("user_id") String uid,
            @PathParam("movie_id") String mid) {
        try{
            userRepository.vobs(userRepository.createyOrGetUser(uid), movieRepository.createyOrGetMovie(mid));
            return new VOBStatus(uid, mid);
        }catch(Exception e) {
            return new NGResponse(e);
        }
    }

    @GET
    @Produces("application/json")
    @Path("{user_id}")
    public APIResponse getVob(@PathParam("user_id") String uid) {
        try{
            Set<VOB> vobs = userRepository.createyOrGetUser(uid).vobs;
            return new UserVobs(vobs);
        }catch(Exception e) {
            return new NGResponse(e);
        }
    }

}
