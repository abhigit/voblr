package com.voblr.api.common;

import static com.voblr.api.common.GenericActions.movieRepository;
import static com.voblr.api.common.GenericActions.userRepository;

import java.util.Set;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.FOLLOWStatus;
import com.voblr.api.common.response.NGResponse;
import com.voblr.api.common.response.UserFollowees;
import com.voblr.api.common.response.VOBStatus;
import com.voblr.api.entity.relations.FOLLOW;

@Path("/follows")
public class FollowActions {
    @GET
    @Produces("application/json")
    @Path("{user_id1}/{user_id2}")
    public APIResponse doFollows(@PathParam("user_id1") String uid1,
            @PathParam("user_id2") String uid2) {

        try {
            userRepository.follows((userRepository.createyOrGetUser(uid1)),
                    userRepository.createyOrGetUser(uid2));
            return new FOLLOWStatus(uid1, uid2);
        } catch (Exception e) {
            return new NGResponse(e);
        }
    }

    @GET
    @Produces("application/json")
    @Path("{user_id}")
    public APIResponse getFollowees(@PathParam("user_id") String uid) {

        try {
            Set<FOLLOW> followee = userRepository.createyOrGetUser(uid).followees;
            return new UserFollowees(followee);
        } catch (Exception e) {
            return new NGResponse(e);
        }
    }

}
