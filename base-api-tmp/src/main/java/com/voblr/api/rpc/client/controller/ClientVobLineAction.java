package com.voblr.api.rpc.client.controller;

import static com.voblr.api.redis.ClientMain.client;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.api.common.response.Movie;
import com.voblr.api.common.response.VoblineResponse;
import com.voblr.api.mongo.model.MongoConnection;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;
import com.voblr.api.rpc.client.GraphServices.GenericResponse;
import com.voblr.api.rpc.client.GraphServices.UserService;
import com.voblr.api.rpc.client.controller.auth.AuthUser;

@Path("/vobline")
public class ClientVobLineAction {

    /**
     * This is the redis to get the user id from user cookie;
     * 
     */
    @GET
    @Consumes("text/html")
    @Produces("application/javascript")
    public JSONWithPadding getUserVobline(
            @QueryParam("callback") String callback,
            //@CookieParam("vob_common") String userCookie,
            @QueryParam("vob_common") String userCookie,
            @QueryParam("timestamp") long timestamp,
            @QueryParam("count") int count)
            throws ServiceException {
        if (userCookie == null ) {
            System.out.println("cookie is "  + userCookie);
            return new JSONWithPadding(new VoblineResponse(), callback); 
        }
        String userId = AuthUser.getUserID(userCookie);
        RpcController controller = client.channel.newRpcController();
        UserService.BlockingInterface followsService = UserService
                .newBlockingStub(client.channel);
        
        com.voblr.api.rpc.client.GraphServices.User request = com.voblr.api.rpc.client.GraphServices.User.newBuilder().setUserId(userId).build();

        GenericResponse genericResponse = followsService.getUserVobsForAllMovies(controller, request);
        
        List<GenericEvent> genericEventList = genericResponse.getEventsList();


        List<Movie> movieList = new ArrayList<Movie>();
        for (GenericEvent genericEvent : genericEventList) {
            movieList.add(new Movie(Integer.parseInt(genericEvent.getObjectId()),
                                    genericEvent.getTimestamp()));
        }
        int startIndex = 0;
        int endIndex = 0;
        System.out.println("sort complete");
        for (int i = 0; i < movieList.size(); i++) {
            if (movieList.get(i).getTimestamp() > timestamp ) {
                startIndex = i;
                System.out.println("setting the start = " + startIndex);
                break;
            }
        }
        endIndex = startIndex + count;
        endIndex = endIndex > movieList.size() ? movieList.size() : endIndex;
        System.out.println("startIndex = " + startIndex + "EndIndex = " + endIndex + "count is =" + count + " total size = " + movieList.size());
        
        VoblineResponse voblineResponse = new VoblineResponse();
        voblineResponse.movieVobEvents = MongoConnection.getMovieListByIDs(movieList.subList(startIndex, endIndex));

        for (Movie movie : voblineResponse.movieVobEvents) {
            System.out.println(movie.toString());
        }
        System.out.println(voblineResponse.movieVobEvents.size());
        return new JSONWithPadding(voblineResponse, callback);
    }
    
    
    @GET
    @Consumes("text/html")
    @Produces("application/javascript")
    @Path("/vobline/mostwatched")
    public JSONWithPadding getUserVoblineSortedByMostWatched(
            @QueryParam("callback") String callback,
//            @CookieParam("vob_common") String userCookie,
            @QueryParam("vob_common") String userCookie,
            @QueryParam("timestamp") long timestamp,
            @QueryParam("count") int count)
                    throws ServiceException {
        String userId = AuthUser.getUserID(userCookie);
        if (userCookie == null ) {
            System.out.println("cookie is "  + userCookie);
            return new JSONWithPadding(new VoblineResponse(), callback); 
        }
        
        RpcController controller = client.channel.newRpcController();
        UserService.BlockingInterface followsService = UserService
                .newBlockingStub(client.channel);
        
        com.voblr.api.rpc.client.GraphServices.User request = com.voblr.api.rpc.client.GraphServices.User.newBuilder().setUserId(userId).build();

        GenericResponse genericResponse = followsService.getUserVobsForAllMovies(controller, request);
        
        List<GenericEvent> genericEventList = genericResponse.getEventsList();
        Map<String, Movie> uniqMovieIdVobs = new HashMap<String, Movie>();
        for (GenericEvent genericEvent : genericEventList) {
            if (uniqMovieIdVobs.get(genericEvent.getObjectId()) == null) {
                uniqMovieIdVobs.put(genericEvent.getObjectId(), new Movie(Integer.parseInt(genericEvent.getObjectId()),
                        genericEvent.getTimestamp()));
            } else {
                Movie m = uniqMovieIdVobs.get(genericEvent.getObjectId());
                m.setMovieVobs(m.getMovieVobs() + 1);
                uniqMovieIdVobs.put(genericEvent.getObjectId(), m);
            }
        }

        VoblineResponse voblineResponse = new VoblineResponse();
        voblineResponse.movieVobEvents = MongoConnection.getMovieListByIDs(new ArrayList<Movie>(uniqMovieIdVobs.values()));
        return new JSONWithPadding(voblineResponse, callback);

    }

}

