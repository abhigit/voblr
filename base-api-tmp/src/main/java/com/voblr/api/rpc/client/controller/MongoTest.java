package com.voblr.api.rpc.client.controller;

import java.net.UnknownHostException;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.voblr.api.repositories.movie.MovieMongoDbObject;

public class MongoTest {
    /**
     * @param args
     */
    public static void main(String[] args) {
        // TODO Auto-generated method stub
        Mongo mongo =null;
        try {
            mongo = new Mongo("dev.ibhupi.com", 27017);
        } catch (UnknownHostException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (MongoException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println(mongo.getDatabaseNames());

        DB db = mongo.getDB("voblr");

        MovieMongoDbObject  test= new MovieMongoDbObject();
//        test.movie_name= "Masculin feminin"
//        test.movie_desc= "The film stars French New Wave icon Jean-Pierre Léaud as Paul, a romantic young idealist and literary lion-wannabe who chases budding pop star, Madeleine (Chantal Goya, a real life Yé-yé girl)."
//        test.movie_last_modified_at=2012-04-12 09:45:59 UTC, movie_tagline=, movie_released=1966-03-22, movie_trailer=null, movie_runtime=110, movie_homepage=, movie_imdb_id=tt0060675, movie_id=4710, movie_posters=[ "http://cf2.imgobject.com/t/p/original/issm1E827fK7KHMEdRORA9BoTPs.jpg" , "http://cf2.imgobject.com/t/p/original/8kTfggxPLAJUi4bjcuC2MSCRGNn.jpg"], movie_directors=[ "Jean-Luc Godard"], movie_genres=[ "Drama" , "Foreign" , "Romance"]]

        DBCollection coll = db.getCollection("movie");
        BasicDBObject query = new BasicDBObject();
        query.put("movie_id", 4710);
        DBCursor cur = coll.find(query);

        
//        List<Object> colList  = new ArrayList<Object>();
        MovieMongoDbObject  a= new MovieMongoDbObject();
        a.putAll(cur.next().toMap());

        System.out.println(a.toString());
    }

}
