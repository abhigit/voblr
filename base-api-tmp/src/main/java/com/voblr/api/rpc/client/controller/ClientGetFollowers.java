package com.voblr.api.rpc.client.controller;

import static com.voblr.api.redis.ClientMain.client;

import java.util.Map.Entry;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.GenericEntity;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.NonLoginUser;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;
import com.voblr.api.rpc.client.GraphServices.GenericResponse;
import com.voblr.api.rpc.client.GraphServices.User;
import com.voblr.api.rpc.client.GraphServices.UserService;
import com.voblr.api.rpc.client.controller.auth.AuthUser;

@Path("/get")
public class ClientGetFollowers {

	@GET
    @Produces("application/javascript")
	@Path("followers")
    public JSONWithPadding getFollowers(@QueryParam("callback") String callback,
			@CookieParam("vob_common") String userCookie) throws ServiceException {

		String userId = AuthUser.getUserID(userCookie);
        if (userId == null)
            return new JSONWithPadding(new NonLoginUser() , callback);
        
        UserService.BlockingInterface userService = UserService.newBlockingStub(client.channel);
        
        RpcController controller = client.channel.newRpcController();

        User request = User.newBuilder().setUserId(userId).build();

        GenericResponse response = userService.getFollowing(controller,
                request);

//        GenericResponse response = userService.getUserVobsForAllMovies(controller,
//                request);

//        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
//                .entrySet()) {
//            System.out.println(a.getKey() + "\n " + a.getValue());
//        }
//        System.out.println();
//        APIResponse response = _doFollow(userId, follow_id);
        for (GenericEvent a : response.getEventsList()) {
            System.out.println(a.getObjectId() + " ---- " + a.getSubjectId());
        }
        System.out.println();
        
        return new JSONWithPadding(new APIResponse("Sorry no reposponse"), callback);
    }

	@GET
    @Produces("application/javascript")
	@Path("vobs")
    public JSONWithPadding getVobs(@QueryParam("callback") String callback,
			@CookieParam("vob_common") String userCookie) throws ServiceException {

		String userId = AuthUser.getUserID(userCookie);
        if (userId == null)
            return new JSONWithPadding(new NonLoginUser() , callback);
        
        UserService.BlockingInterface userService = UserService.newBlockingStub(client.channel);
        
        RpcController controller = client.channel.newRpcController();

        User request = User.newBuilder().setUserId(userId).build();

//        GenericResponse response = userService.getFollowing(controller,
//                request);

        GenericResponse response = userService.getUserVobsForAllMovies(controller,
                request);

//        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
//                .entrySet()) {
//            System.out.println(a.getKey() + "\n " + a.getValue());
//        }
//        System.out.println();
//        APIResponse response = _doFollow(userId, follow_id);
        for (GenericEvent a : response.getEventsList()) {
            System.out.println(a.getObjectId() + " ---- " + a.getSubjectId());
        }
        System.out.println();
        
        return new JSONWithPadding(new APIResponse("Sorry no reposponse"), callback);
    }

	@GET
    @Produces("application/javascript")
	@Path("activity")
    public JSONWithPadding getActivity(@QueryParam("callback") String callback,
			@CookieParam("vob_common") String userCookie) throws ServiceException {

		String userId = AuthUser.getUserID(userCookie);
        if (userId == null)
            return new JSONWithPadding(new NonLoginUser() , callback);
        
        UserService.BlockingInterface userService = UserService.newBlockingStub(client.channel);
        
        RpcController controller = client.channel.newRpcController();

        User request = User.newBuilder().setUserId(userId).build();

//        GenericResponse response = userService.getFollowing(controller,
//                request);

        GenericResponse response = userService.getActivityFeeds(controller,
                request);

        for (GenericEvent a : response.getEventsList()) {
            System.out.println(a.getObjectId() + " ---- " + a.getSubjectId());
        }
        System.out.println();
//        APIResponse response = _doFollow(userId, follow_id);
        
        return new JSONWithPadding(new APIResponse("Sorry no reposponse"), callback);
    }

}
