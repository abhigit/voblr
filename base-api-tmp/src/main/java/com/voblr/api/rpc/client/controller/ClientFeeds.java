package com.voblr.api.rpc.client.controller;

import static com.voblr.api.redis.ClientMain.client;

import java.util.Map.Entry;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.voblr.api.common.ResponseConstants;
import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.VOBStatus;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;
import com.voblr.api.rpc.client.GraphServices.GenericResponse;
import com.voblr.api.rpc.client.GraphServices.User;
import com.voblr.api.rpc.client.GraphServices.UserService;

@Path("/news")
public class ClientFeeds {
	@GET
//	@POST
//	@Consumes("text/html")
	@Produces("application/json")
	@Path("{user_id}")
	public APIResponse getNewsFeedForUser(
			@PathParam("user_id") String user_id) throws ServiceException {

		System.out.println(user_id);

		UserService.BlockingInterface followsService = UserService
				.newBlockingStub(client.channel);

		RpcController controller = client.channel.newRpcController();


		User request = User.newBuilder().setUserId(user_id).build();

		GenericResponse response = followsService.getNewsFeed(
				controller, request);

		for (Entry<FieldDescriptor, Object> a : response.getAllFields()
				.entrySet()) {
			System.out.println(a.getKey() + "\n " + a.getValue());
		}

//		return new VOBStatus(response.getStatus(), response.getMessage(),
//				response.getEventsList());

		String responseCode;
		if (response.getStatus().equals("OK"))
			responseCode = ResponseConstants.SUCCESS;
		else
			responseCode = ResponseConstants.FAILURE;

		System.out.println("status :" + response.getStatus());
		System.out.println(responseCode);
		return new VOBStatus(responseCode, response.getMessage(),response.getEventsList());
	}

}
