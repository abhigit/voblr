package com.voblr.api.rpc.client.controller;

import static com.voblr.api.redis.ClientMain.client;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import redis.clients.jedis.Jedis;

import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.api.common.ResponseConstants;
import com.voblr.api.common.response.NewsFeed;
import com.voblr.api.common.response.NewsFeedResponse;
import com.voblr.api.common.response.NonLoginUser;
import com.voblr.api.mongo.model.MongoConnection;
import com.voblr.api.repositories.movie.MovieMongoDbObject;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;
import com.voblr.api.rpc.client.GraphServices.GenericResponse;
import com.voblr.api.rpc.client.GraphServices.MovieService;
import com.voblr.api.rpc.client.GraphServices.MovieVobCount;
import com.voblr.api.rpc.client.GraphServices.User;
import com.voblr.api.rpc.client.GraphServices.UserService;
import com.voblr.api.rpc.client.controller.auth.AuthUser;

@Path("/user/newsfeed")
public class ClientNewsFeeds {
	private static final int NOTIFICATION_VOB = 1;
	private static Jedis jedis = new Jedis("localhost", 6379);

	@GET
	@Produces("application/javascript")
	public JSONWithPadding getNewsFeedForUser(
			@CookieParam("vob_common") String userCookie,
			@QueryParam("callback") String callback) throws ServiceException {

		long start = System.currentTimeMillis();
		System.out.println("Req came " + userCookie);
		String userId = AuthUser.getUserID(userCookie);

		// System.out.println("Movie ID " + movie_id);
		if (userId == null)
			return new JSONWithPadding(new NonLoginUser(), callback);

		System.out.println("userId = " + userId);
		// userId="vinit";
		UserService.BlockingInterface followsService = UserService
				.newBlockingStub(client.channel);

		MovieService.BlockingInterface followsServiceMovie = MovieService
				.newBlockingStub(client.channel);

		RpcController controller = client.channel.newRpcController();

		User request = User.newBuilder().setUserId(userId).build();

		// System.out.println("Time start for newsfeed : "+
		// System.currentTimeMillis());
		GenericResponse response = followsService.getNewsFeed(controller,
				request);
		System.out.println("Time taken for newsfeed : "
				+ (System.currentTimeMillis() - start) / 1000);

//		for (Entry<FieldDescriptor, Object> a : response.getAllFields()
//				.entrySet()) {
//			 System.out.println("nothing " + a.getKey() + "\n " +
//			 a.getValue());
//		}

		String responseCode;
		if (response.getStatus().equals("OK"))
			responseCode = ResponseConstants.SUCCESS;
		else
			responseCode = ResponseConstants.FAILURE;

		System.out.println("status :" + response.getStatus());
		System.out.println(responseCode);

		Mongo mongo = MongoConnection.mongo;
		DB db = MongoConnection.voblrDB;

		NewsFeedResponse newsFeedResponse = new NewsFeedResponse();
		DBCollection coll = db.getCollection("movie");
		// List<String> movieIdList = new ArrayList<String>();

		Map<String, NewsFeed> movieList = new HashMap<String, NewsFeed>();

		for (GenericEvent g : response.getEventsList()) {

			String movieId = g.getObjectId();
			// System.out.println("movie id :" + movieId);
			BasicDBObject query = new BasicDBObject();

			try {
				query.put("movie_id", Integer.parseInt(movieId));

				DBCursor cur = coll.find(query);
				MovieMongoDbObject a = new MovieMongoDbObject();
				if (cur.hasNext()) {
					a.putAll(cur.next().toMap());

					if (movieList.containsKey(movieId)) {
						NewsFeed newsfeed = movieList.get(movieId);
						newsfeed.moreUsers += 1;
						continue;
					}

					NewsFeed newsFeed = new NewsFeed(
							(String) a.movie_posters.get(0), 0, a.movie_name,
							movieId, g.getSubjectId(), g.getSubjectId(), -1,
							g.getTimestamp(), NOTIFICATION_VOB);

					com.voblr.api.rpc.client.GraphServices.Movie requestMovieId = com.voblr.api.rpc.client.GraphServices.Movie
							.newBuilder().setMovieId(movieId.toString())
							.build();

					MovieVobCount responseMovieVobCount = followsServiceMovie
							.getVobCount(controller, requestMovieId);

//					for (Entry<FieldDescriptor, Object> ab : responseMovieVobCount
//							.getAllFields().entrySet()) {
//						 System.out.println(ab.getKey() + "\n " +
//						 ab.getValue());
//					}

					newsFeed.vobCount = responseMovieVobCount.getCount();

					movieList.put(movieId, newsFeed);
					newsFeedResponse.newsFeed.add(newsFeed);
				} else {

					NewsFeed newsFeed = new NewsFeed();
					newsFeed.vobCount = -1;

					newsFeedResponse.newsFeed.add(newsFeed);

				}

			} catch (Exception e) {
				// query.put("movie_id", 4710);
			}
		}

//		new MongoConnection().getMovieListByIDs(movieList.keySet());
		System.out.println("eeeeeeee : " + newsFeedResponse.newsFeed.size());
		System.out.println("eeeeeeee : " + newsFeedResponse.newsFeed);

		// for (NewsFeed n : newsFeedResponse.newsFeed)
		// System.out.println(n.movieName);

		JSONWithPadding pad = new JSONWithPadding(new NewsFeedResponse(
				newsFeedResponse.newsFeed), callback);
		System.out.println(pad);
		System.out.println("Time end for newsfeed : "
				+ (System.currentTimeMillis()-start)/1000);
		return pad;
	}

	@GET
	@Produces("application/javascript")
	@Path("test")
	public JSONWithPadding getNewsFeedForUserTest(
			@QueryParam("vob_common") String userCookie,
			@QueryParam("callback") String callback) {
		// System.out.println("Req came "+ callback);
		String userId = AuthUser.getUserID(userCookie);

		System.out.println("userId = " + userId);

		NewsFeedResponse response1 = new NewsFeedResponse();
		// for (int i = 0; i<10; i++) {
		// NewsFeed newsFeed = new NewsFeed();
		// response.newsFeed.add(newsFeed);
		// }
		// response.status = "21";

		Mongo mongo = null;
		try {
			mongo = new Mongo("dev.ibhupi.com", 27017);
		} catch (UnknownHostException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (MongoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println(mongo.getDatabaseNames());

		DB db = mongo.getDB("voblr");

		DBCollection coll = db.getCollection("movie");
		BasicDBObject query = new BasicDBObject();
		query.put("movie_id", 4710);
		DBCursor cur = coll.find(query);

		List<Object> colList = new ArrayList<Object>();
		while (cur.hasNext()) {
			colList.add(cur.next());
		}
		System.out.println(colList);

		JSONWithPadding pad = new JSONWithPadding(colList, callback);
		System.out.println(pad);
		return pad;
	}

}
