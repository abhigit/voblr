package com.voblr.api.rpc.client.controller;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.Map.Entry;
import java.util.concurrent.Executors;

import javax.ws.rs.CookieParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import org.glassfish.jersey.media.json.JsonWithPadding;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import redis.clients.jedis.Jedis;

import com.esotericsoftware.kryo.Kryo;
import com.google.protobuf.RpcController;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.ServiceException;
import com.googlecode.protobuf.pro.duplex.RpcClientChannel;
import com.googlecode.protobuf.pro.duplex.client.DuplexTcpClientBootstrap;
import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.api.common.ResponseConstants;
import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.FOLLOWStatus;
import com.voblr.api.common.response.NonLoginUser;
import com.voblr.api.redis.data.ShortMovieCompleteResponse;
import com.voblr.api.rpc.client.Client;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;
import com.voblr.api.rpc.client.GraphServices.GenericResponse;
import com.voblr.api.rpc.client.GraphServices.User;
import com.voblr.api.rpc.client.GraphServices.UserService;
import com.voblr.api.rpc.client.controller.auth.AuthUser;

import static com.voblr.api.redis.ClientMain.client;

@Path("/follow")
public class ClientFollowAction {

	private static Jedis jedis = new Jedis("localhost", 6379);
	private static Kryo kryo = new Kryo();

//	private static Client client = new Client();
	static {
		kryo.register(ShortMovieCompleteResponse.class);
	}

	@GET
    @Produces("application/javascript")
	@Path("{follow_id}")
	public JSONWithPadding doFollow(@QueryParam("callback") String callback,
			@CookieParam("vob_common") String userCookie,
            @PathParam("follow_id") String follow_id) throws ServiceException{

		String user_id = AuthUser.getUserID(userCookie);
        if (user_id == null)
            return new JSONWithPadding(new NonLoginUser() , callback);
        
        APIResponse response = _doFollow(user_id, follow_id);
        
        return new JSONWithPadding(response, callback);
        
		
	}

	
	public APIResponse _doFollow(String user1,
			String user2) throws ServiceException {

        UserService.BlockingInterface followsService = UserService.newBlockingStub(client.channel);
        
        RpcController controller = client.channel.newRpcController();

        GenericEvent request = GenericEvent.newBuilder()
                .setObjectId(user1)
                .setSubjectId(user2)
                .setTimestamp(System.currentTimeMillis())
                .build();

        GenericResponse response = followsService.doFollow(controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        

        String responseCode;
        if (response.getStatus().equals("OK"))
            responseCode = ResponseConstants.SUCCESS;
        else
            responseCode = ResponseConstants.FAILURE;

      return new FOLLOWStatus(responseCode ,response.getMessage());
	}


	@GET
	@Produces("application/json")
	@Path("{movieName}/{movieID}/{imageURL}/{tagLine}")
	public ShortMovieCompleteResponse setMovieDetails(
			@PathParam("movieName") String movieName, 
			@PathParam("movieID") String movieID,
			@PathParam("imageURL") String imageURL,
			@PathParam("tagLine") String tagLine){

		ShortMovieCompleteResponse a = new ShortMovieCompleteResponse(movieID,
				movieName, imageURL, tagLine);

		ByteBuffer serializedMovie = ByteBuffer.allocate(256);
		kryo.writeObjectData(serializedMovie, a);

		String response = jedis.set(movieName.getBytes(),
				serializedMovie.array());

		return a;
	}

}
//channel.close();
//
//ShortMovieCompleteResponse ans = kryo.readObjectData(
//    ByteBuffer.wrap(jedis.get(user2.getBytes())),
//    ShortMovieCompleteResponse.class);

//jedis.set("vinit", "bothra");
//return new ShortMovieCompleteResponse(1, movieName, "imageURL1",
//"this is tagline", 100);
//return new FOLLOWStatus(response.getEvents(0).getSubjectId(),response.getEvents(0).getObjectId());
//return new FOLLOWStatus(response.getStatus(),response.getMessage());






//channel.close();
//
//ShortMovieCompleteResponse ans = kryo.readObjectData(
//		ByteBuffer.wrap(jedis.get(user2.getBytes())),
//		ShortMovieCompleteResponse.class);

//jedis.set("vinit", "bothra");
//return new ShortMovieCompleteResponse(1, movieName, "imageURL1",
//"this is tagline", 100);
//return new FOLLOWStatus(response.getEvents(0).getSubjectId(),response.getEvents(0).getObjectId());
