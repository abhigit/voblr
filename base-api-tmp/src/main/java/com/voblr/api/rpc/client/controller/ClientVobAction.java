package com.voblr.api.rpc.client.controller;

import static com.voblr.api.redis.ClientMain.client;

import java.nio.ByteBuffer;
import java.util.Map.Entry;

import javax.ws.rs.Consumes;
import javax.ws.rs.CookieParam;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import redis.clients.jedis.Jedis;

import com.esotericsoftware.kryo.Kryo;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.api.common.ResponseConstants;
import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.NonLoginUser;
import com.voblr.api.common.response.VOBStatus;
import com.voblr.api.redis.data.ShortMovieCompleteResponse;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;
import com.voblr.api.rpc.client.GraphServices.GenericResponse;
import com.voblr.api.rpc.client.GraphServices.MovieService;
import com.voblr.api.rpc.client.GraphServices.MovieVobCount;
import com.voblr.api.rpc.client.GraphServices.User;
import com.voblr.api.rpc.client.GraphServices.UserService;
import com.voblr.api.rpc.client.controller.auth.AuthUser;

@Path("/vob")
public class ClientVobAction {

    private static Jedis jedis = new Jedis("localhost", 6379);
    private static Kryo kryo = new Kryo();

    static {
        kryo.register(ShortMovieCompleteResponse.class);
    }

    private APIResponse _doVob(String movie_id, String userCookie)
            throws ServiceException {
//        System.out.println("Cookie" + userCookie);
        String user_id = AuthUser.getUserID(userCookie);
        if (user_id == null)
            return new NonLoginUser();

        UserService.BlockingInterface followsService = UserService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        GenericEvent request = GenericEvent.newBuilder().setObjectId(user_id)
                .setSubjectId(movie_id)
                .setTimestamp(System.currentTimeMillis()).build();

        GenericResponse response = followsService.doVob(controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        String responseCode;
        if (response.getStatus().equals("OK"))
            responseCode = ResponseConstants.SUCCESS;
        else
            responseCode = ResponseConstants.FAILURE;

        System.out.println("status :" + response.getStatus());
        System.out.println(responseCode);
        return new VOBStatus(responseCode, response.getMessage());
    }

    // @GET
    // @Consumes("text/html")
    // @Produces("application/json")
    // @Path("{movie_id}")
    // public APIResponse doVob(
    // // @PathParam("user_id") String user_id,
    // @PathParam("movie_id") String movie_id,
    // @QueryParam("vob_common") String userCookie)
    // throws ServiceException {
    // return _doVob(movie_id, userCookie);
    // }

    @GET
    @Consumes("text/html")
    @Produces("application/javascript")
    @Path("{movie_id}")
    public JSONWithPadding doVobs(@QueryParam("callback") String callback,
            @PathParam("movie_id") String movie_id,
            @CookieParam("vob_common") String userCookie,
            @QueryParam("tmp") String tmp)
            throws ServiceException {
        APIResponse response = _doVob(movie_id, userCookie);
        response.tmp = tmp;
        
        MovieService.BlockingInterface followsService = MovieService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        // User request = User.newBuilder()
        // .setObjectId(user_id)
        // .build();

        com.voblr.api.rpc.client.GraphServices.Movie request = com.voblr.api.rpc.client.GraphServices.Movie
                .newBuilder().setMovieId(movie_id).build();

        MovieVobCount responseMovieVobCount = followsService
                .getVobCount(controller, request);

        for (Entry<FieldDescriptor, Object> a : responseMovieVobCount.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        response.vobCount = responseMovieVobCount.getCount();
        response.movieId = movie_id;
        return new JSONWithPadding(response, callback);
    }

    @POST
    @Consumes("text/html")
    @Produces("application/json")
    public APIResponse getUserVobs(@CookieParam("vob_common") String userCookie)
            throws ServiceException {

        System.out.println(userCookie);
        String user_id = AuthUser.getUserID(userCookie);
        System.out.println(user_id);
        if (user_id == null)
            return new NonLoginUser();

        System.out.println("get user vobs");
        UserService.BlockingInterface followsService = UserService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        // User request = User.newBuilder()
        // .setObjectId(user_id)
        // .build();

        User request = User.newBuilder().setUserId(user_id).build();

        GenericResponse response = followsService.getUserVobsForAllMovies(
                controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        return new VOBStatus(response.getStatus(), response.getMessage(),
                response.getEventsList());
    }

    @GET
    @Produces("application/json")
    @Path("{movieName}/{movieID}/{imageURL}/{tagLine}")
    public ShortMovieCompleteResponse setMovieDetails(
            @PathParam("movieName") String movieName,
            @PathParam("movieID") String movieID,
            @PathParam("imageURL") String imageURL,
            @PathParam("tagLine") String tagLine) {

        ShortMovieCompleteResponse a = new ShortMovieCompleteResponse(movieID,
                movieName, imageURL, tagLine);

        ByteBuffer serializedMovie = ByteBuffer.allocate(256);
        kryo.writeObjectData(serializedMovie, a);

        String response = jedis.set(movieName.getBytes(),
                serializedMovie.array());

        return a;
    }

    @GET
    // @POST
    // @Consumes("text/html")
    @Produces("application/json")
    @Path("{user_id}/{movie_id}")
    public APIResponse doVobGet(@PathParam("user_id") String user_id,
            @PathParam("movie_id") String movie_id) throws ServiceException {

        System.out.println(user_id);
        System.out.println(movie_id);

        UserService.BlockingInterface followsService = UserService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        GenericEvent request = GenericEvent.newBuilder().setObjectId(user_id)
                .setSubjectId(movie_id)
                .setTimestamp(System.currentTimeMillis()).build();

        GenericResponse response = followsService.doVob(controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        String responseCode;
        if (response.getStatus().equals("OK"))
            responseCode = ResponseConstants.SUCCESS;
        else
            responseCode = ResponseConstants.FAILURE;

        System.out.println("status :" + response.getStatus());
        System.out.println(responseCode);
        return new VOBStatus(responseCode, response.getMessage());
    }

    // @POST
    // @Consumes("text/html")
    @GET
    @Produces("application/json")
    @Path("{user_id}/i")
    public APIResponse getUserVobsForAll(@PathParam("user_id") String user_id)
            throws ServiceException {

        System.out.println("get user vobs");
        UserService.BlockingInterface followsService = UserService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        // User request = User.newBuilder()
        // .setObjectId(user_id)
        // .build();

        User request = User.newBuilder().setUserId(user_id).build();

        GenericResponse response = followsService.getUserVobsForAllMovies(
                controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        String responseCode;
        if (response.getStatus().equals("OK"))
            responseCode = ResponseConstants.SUCCESS;
        else
            responseCode = ResponseConstants.FAILURE;

        return new VOBStatus(responseCode, response.getMessage(),
                response.getEventsList());
    }

    @GET
    @Produces("application/json")
    @Path("{user_id}/dffgg")
    public JSONWithPadding getUserVobsForAll(
            @PathParam("user_id") String user_id,
            @QueryParam("callback") String callback) throws ServiceException {

        System.out.println("get user vobs");
        UserService.BlockingInterface followsService = UserService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        // User request = User.newBuilder()
        // .setObjectId(user_id)
        // .build();

        User request = User.newBuilder().setUserId(user_id).build();

        GenericResponse response = followsService.getUserVobsForAllMovies(
                controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        String responseCode;
        if (response.getStatus().equals("OK"))
            responseCode = ResponseConstants.SUCCESS;
        else
            responseCode = ResponseConstants.FAILURE;

        return new JSONWithPadding(new VOBStatus(responseCode,
                response.getMessage(), response.getEventsList()), callback);
    }

    @GET
    @Path("/jsonp")
    @Produces("application/javascript")
    public JSONWithPadding testJSONP(@QueryParam("callback") String callback) {
        String retVal = "YES";
        return new JSONWithPadding(new APIResponse(retVal), callback);

        // return new JSONWithPadding(
        // new GenericEntity("{\"test\":\"" + retVal + "\"}"){},callback);
    }

    // channel.close();
    //
    // ShortMovieCompleteResponse ans = kryo.readObjectData(
    // ByteBuffer.wrap(jedis.get(user2.getBytes())),
    // ShortMovieCompleteResponse.class);

    // jedis.set("vinit", "bothra");
    // return new ShortMovieCompleteResponse(1, movieName, "imageURL1",
    // "this is tagline", 100);
    // return new
    // FOLLOWStatus(response.getEvents(0).getSubjectId(),response.getEvents(0).getObjectId());
}
