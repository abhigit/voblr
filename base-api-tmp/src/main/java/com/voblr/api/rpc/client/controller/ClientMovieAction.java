package com.voblr.api.rpc.client.controller;

import static com.voblr.api.redis.ClientMain.client;

import java.nio.ByteBuffer;
import java.util.Map.Entry;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;

import redis.clients.jedis.Jedis;

import com.esotericsoftware.kryo.Kryo;
import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.api.common.response.APIResponse;
import com.voblr.api.common.response.VOBStatus;
import com.voblr.api.redis.data.ShortMovieCompleteResponse;
import com.voblr.api.rpc.client.GraphServices.MovieService;
import com.voblr.api.rpc.client.GraphServices.MovieVobCount;

@Path("/movie/vobcount/")
public class ClientMovieAction {

    private static Jedis jedis = new Jedis("localhost", 6379);
    private static Kryo kryo = new Kryo();

    // private static Client client = new Client();
    static {
        kryo.register(ShortMovieCompleteResponse.class);
    }

    @GET
    @Produces("application/json")
    @Path("{movie_id}/d")
    public APIResponse getMovieVobCount(@PathParam("movie_id") String movie_id)
            throws ServiceException {

        MovieService.BlockingInterface followsService = MovieService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        // User request = User.newBuilder()
        // .setObjectId(user_id)
        // .build();

        com.voblr.api.rpc.client.GraphServices.Movie request = com.voblr.api.rpc.client.GraphServices.Movie
                .newBuilder().setMovieId(movie_id).build();

        MovieVobCount response = followsService
                .getVobCount(controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        return new VOBStatus("21", "", String.valueOf(response.getCount()));
    }

    @GET
    @Path("{movie_id}")
    @Produces("application/javascript")
    public JSONWithPadding testJSONP(@PathParam("movie_id") String movie_id,
            @QueryParam("callback") String callback) throws ServiceException {
        MovieService.BlockingInterface followsService = MovieService
                .newBlockingStub(client.channel);

        RpcController controller = client.channel.newRpcController();

        // User request = User.newBuilder()
        // .setObjectId(user_id)
        // .build();

        com.voblr.api.rpc.client.GraphServices.Movie request = com.voblr.api.rpc.client.GraphServices.Movie
                .newBuilder().setMovieId(movie_id).build();

        MovieVobCount response = followsService
                .getVobCount(controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }

        return new JSONWithPadding(new VOBStatus("21", "",
                String.valueOf(response.getCount())), callback);
        // return new JSONWithPadding(
        // new GenericEntity("{\"test\":\"" + retVal + "\"}"){},callback);
    }

    @GET
    @Produces("application/json")
    @Path("{movieName}/{movieID}/{imageURL}/{tagLine}")
    public ShortMovieCompleteResponse setMovieDetails(
            @PathParam("movieName") String movieName,
            @PathParam("movieID") String movieID,
            @PathParam("imageURL") String imageURL,
            @PathParam("tagLine") String tagLine) {

        ShortMovieCompleteResponse a = new ShortMovieCompleteResponse(movieID,
                movieName, imageURL, tagLine);

        ByteBuffer serializedMovie = ByteBuffer.allocate(256);
        kryo.writeObjectData(serializedMovie, a);

        String response = jedis.set(movieName.getBytes(),
                serializedMovie.array());

        return a;
    }

}
