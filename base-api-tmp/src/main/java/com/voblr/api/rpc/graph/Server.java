package com.voblr.api.rpc.graph;

import java.io.IOException;
import java.util.concurrent.Executors;

import org.jboss.netty.channel.socket.nio.NioServerSocketChannelFactory;

import com.google.protobuf.RpcCallback;
import com.google.protobuf.RpcController;
import com.googlecode.protobuf.pro.duplex.PeerInfo;
import com.googlecode.protobuf.pro.duplex.execute.ThreadPoolCallExecutor;
import com.googlecode.protobuf.pro.duplex.server.DuplexTcpServerBootstrap;
import com.voblr.api.rpc.graph.Follows.FollowReq;
import com.voblr.api.rpc.graph.Follows.Response;

public class Server {
    static PeerInfo server = new PeerInfo("localhost", 7080);
    static ThreadPoolCallExecutor executor = new ThreadPoolCallExecutor(10, 10);

    static DuplexTcpServerBootstrap bootstrap = new DuplexTcpServerBootstrap(
            server, 
            new NioServerSocketChannelFactory(
    Executors.newCachedThreadPool(),
    Executors.newCachedThreadPool()),
    executor);
    public Server() throws IOException{
    bootstrap.getRpcServiceRegistry().registerService(new Follows.FollowService() {
        
        @Override
        public void getFollowing(RpcController controller, FollowReq request,
                RpcCallback<Response> done) {
            System.out.println(request.getUserId1()+"  "+request.getUserId2());
            
            Follows.Response response = Follows.Response.newBuilder()
                    .setMessage("ok")
                    .setStatus(request.getUserId1()+" followed  "+request.getUserId2())
                    .build();//.set....build();            
            done.run(response); 
            
//            = new RpcCallback<Follows.Response>(){
//
//                @Override
//                public void run(Response response) {
//                    response.newBuilder()
//                    .setMessage("ok")
//                    .setStatus(request.getUserId1()+" followed  "+request.getUserId2())
//                    .build();//.set....build();            
//                }
//                
//            }
//            ;
        }
        
        @Override
        public void follow(RpcController controller, FollowReq request,
                RpcCallback<Response> done) {
            System.out.println(request.getUserId1()+"  "+request.getUserId2());
            Follows.Response response = Follows.Response.newBuilder()
                    .setMessage("ok")
                    .setStatus(request.getUserId1()+" followed  "+request.getUserId2())
                    .addUser("2")
                    .addUser("3")
                    .addUser("4")
                    .addUser("5")
                    .build();//.set....build();            
            done.run(response); 
        }
    });
    
    bootstrap.bind();
    }
    
    public static void main(String[] args) throws Exception{
                        
        new Server();
    }
}
