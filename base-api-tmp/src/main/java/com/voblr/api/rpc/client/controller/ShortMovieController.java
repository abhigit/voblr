package com.voblr.api.rpc.client.controller;

import java.nio.ByteBuffer;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;

import redis.clients.jedis.Jedis;

import com.esotericsoftware.kryo.Kryo;
import com.voblr.api.redis.data.ShortMovieCompleteResponse;

@Path("/shortMovie")
public class ShortMovieController {

    	private static Jedis jedis = new Jedis("localhost", 6379);
    	private static Kryo kryo = new Kryo();
    	static {
    		kryo.register(ShortMovieCompleteResponse.class);
    	}

	@GET
	@Produces("application/json")
	@Path("{movieID}")
	public ShortMovieCompleteResponse getMovieDetails(
			@PathParam("movieID") String movieName) {

		
		ShortMovieCompleteResponse ans = kryo.readObjectData(
				ByteBuffer.wrap(jedis.get(movieName.getBytes())),
				ShortMovieCompleteResponse.class);

//		jedis.set("vinit", "bothra");
//		return new ShortMovieCompleteResponse(1, movieName, "imageURL1",
//		 "this is tagline", 100);
		return ans;
	}

	@GET
	@Produces("application/json")
	@Path("{movieName}/{movieID}/{imageURL}/{tagLine}")
	public ShortMovieCompleteResponse setMovieDetails(
			@PathParam("movieName") String movieName, 
			@PathParam("movieID") String movieID,
			@PathParam("imageURL") String imageURL,
			@PathParam("tagLine") String tagLine){

		ShortMovieCompleteResponse a = new ShortMovieCompleteResponse(movieID,
				movieName, imageURL, tagLine);

		ByteBuffer serializedMovie = ByteBuffer.allocate(256);
		kryo.writeObjectData(serializedMovie, a);

		String response = jedis.set(movieName.getBytes(),
				serializedMovie.array());

		return a;
	}

}
