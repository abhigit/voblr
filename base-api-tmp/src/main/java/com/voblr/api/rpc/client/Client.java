package com.voblr.api.rpc.client;

import java.io.IOException;
import java.util.Map.Entry;
import java.util.concurrent.Executors;

import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import com.google.protobuf.Descriptors.FieldDescriptor;
import com.google.protobuf.RpcController;
import com.googlecode.protobuf.pro.duplex.PeerInfo;
import com.googlecode.protobuf.pro.duplex.RpcClientChannel;
import com.googlecode.protobuf.pro.duplex.client.DuplexTcpClientBootstrap;
import com.googlecode.protobuf.pro.duplex.execute.ThreadPoolCallExecutor;
import com.voblr.api.rpc.client.GraphServices.GenericEvent;
import com.voblr.api.rpc.client.GraphServices.GenericResponse;
import com.voblr.api.rpc.client.GraphServices.UserService;

public class Client {
    public static PeerInfo client = new PeerInfo(System.getProperty("client", "localhost"), 10234);
    public static PeerInfo server = new PeerInfo(System.getProperty("server", "api.iabhi.me"), 7080);
    public static ThreadPoolCallExecutor executor = new ThreadPoolCallExecutor(3, 10);

    public static DuplexTcpClientBootstrap bootstrap = new DuplexTcpClientBootstrap(
            client, new NioClientSocketChannelFactory(
                    Executors.newCachedThreadPool(),
                    Executors.newCachedThreadPool()), executor);
    public static RpcClientChannel channel;

    public Client() {
    	
        try {
			channel = bootstrap.peerWith(server);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    public static void main(String[] args) throws Exception {
        new Client();
        UserService.BlockingInterface followsService = UserService.newBlockingStub(channel);
        
        RpcController controller = channel.newRpcController();

        GenericEvent request = GenericEvent.newBuilder()
                .setObjectId("1")
                .setSubjectId("2")
                .setTimestamp(System.currentTimeMillis())
                .build();
        
        
        GenericResponse response = followsService.doFollow(controller, request);

        for (Entry<FieldDescriptor, Object> a : response.getAllFields()
                .entrySet()) {
            System.out.println(a.getKey() + "\n " + a.getValue());
        }
        
        channel.close();
        bootstrap.releaseExternalResources();
    }
}
