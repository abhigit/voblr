package com.voblr.api.services.common;

import java.io.IOException;
import java.util.Iterator;

import javax.management.relation.Relation;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.RelationshipType;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.StopEvaluator;
import org.neo4j.graphdb.TraversalPosition;
import org.neo4j.graphdb.Traverser;
import org.neo4j.graphdb.Traverser.Order;
import org.neo4j.graphdb.traversal.Evaluators;
import org.neo4j.graphdb.traversal.TraversalDescription;
import org.neo4j.kernel.Traversal;
import org.neo4j.kernel.Uniqueness;
import org.springframework.data.neo4j.core.EntityPath;
import org.springframework.data.neo4j.support.Neo4jTemplate;

import com.voblr.api.entity.Movie;
import com.voblr.api.entity.User;
import com.voblr.api.entity.relations.EVENT;
import com.voblr.api.entity.relations.FOLLOW;
import com.voblr.api.entity.relations.constants.Rels;
import com.voblr.api.repositories.movie.MovieRepository;
import com.voblr.api.repositories.user.UserRepository;

import static org.neo4j.graphdb.DynamicRelationshipType.withName;

public class VobService {

    private UserRepository repo;
    private MovieRepository mrepo;
    private Neo4jTemplate template;

    public void setUserRepository(UserRepository repo) {
        this.repo = repo;
    }

    public void setMovieRepository(MovieRepository repo) {
        this.mrepo = repo;
    }

    public void setNeo4jTemplate(Neo4jTemplate template) {
        this.template = template;
    }

    public void main(String[] args) throws IOException {

        Movie m0 = mrepo.createyOrGetMovie("matrix");

        for (User u : m0.users)
            System.out.println("m0 : " + u);

        User a = repo.createyOrGetUser("a");
        User b = repo.createyOrGetUser("b");
        User c = repo.createyOrGetUser("c");
        User d = repo.createyOrGetUser("d");
        User f = repo.createyOrGetUser("f");
        User gh = repo.createyOrGetUser("gh");

        repo.follows(d, a);

        repo.follows(d, b);

        repo.follows(a, c);

        repo.follows(b, f);

        Movie m2 = mrepo.createyOrGetMovie("m2");
        Movie m3 = mrepo.createyOrGetMovie("gh");

        repo.vobs(b, m2);
        repo.vobs(a, m3);

        User e = repo.createyOrGetUser("d");

        for (FOLLOW u : e.followees)
            System.out.println("m1 : " + u.getEndEntity());

        Movie m1 = mrepo.createyOrGetMovie("matrix");

        repo.vobs(d, m1);

        // mrepo.(a, m1);
        // mrepo.vobs(b, m1);
        // mrepo.vobs(e, m1);
        //
        // b.vobs(m1);
        // a.vobs(m1);
        //
        // repo.save(a);
        // mrepo.save(m1);
        //
        //
        // repo.save(b);
        // repo.save(c);
        // repo.save(d);
        // repo.save(e);
        //
        // Movie m2 = mrepo.createyOrGetMovie("matrix");
        // //
        // for (User u : m2.users)
        // System.out.println("m2 : " + u);

        TraversalDescription traversal = Traversal.description().breadthFirst()
                .uniqueness(Uniqueness.NODE_GLOBAL)
                .relationships(withName(Rels.FOLLOWS))
                .evaluator(Evaluators.toDepth(1))
                .evaluator(Evaluators.excludeStartPosition());

        // Iterable result = repo.findAllPathsByTraversal(e,traversal);
        Iterable<User> result = repo.findAllByTraversal(e, traversal);

        // Iterable<EntityPath<User, User>> friends = (Iterable<EntityPath<User,
        // User>>) result;
        // for (EntityPath<User, User> path : friends) {
        for (User path : result) {
            // int weight = 2 - path.length();
            // User friend = path.endEntity();
            // aggregateRatings(friend, weight);
            System.out.println("as-d-ad" + path);
        }

        Traverser traverser = findHackers(template.getNode(e.id));
        for (Node n : traverser) {
            System.out.println("--------------");
            System.out.println("--------------");
            System.out.println("child node : " + n.getProperty("name"));
            for (Relationship r : n.getRelationships(Direction.OUTGOING)) {
                Iterator<String> a1 = r.getPropertyKeys().iterator();
                while (a1.hasNext()) {
                    String temp = a1.next();
                    System.out.println("prop name : " + temp + " : "
                            + r.getProperty(temp));
                }
                System.out.println("node relationship : " + " "
                        + r.getStartNode().getProperty("name") + " --- "+ r.getEndNode().getProperty("name"));

            }

        }

        // for (Rating rating : user.getRatings()) {
        // ratings.remove(rating.getMovie());
        // }
    }

    private static Traverser findHackers(final Node startNode) {
        return startNode.traverse(Order.BREADTH_FIRST, new StopEvaluator() {

            @Override
            public boolean isStopNode(TraversalPosition currentPos) {
                return currentPos.depth() > 1;
            }
        }, new ReturnableEvaluator() {
            @Override
            public boolean isReturnableNode(final TraversalPosition currentPos) {
                return (currentPos.depth() > 0 && !currentPos.isStartNode());
            }
        }, withName(Rels.VOBS), Direction.OUTGOING,
                withName(Rels.FOLLOWS), Direction.OUTGOING);
    }
}
