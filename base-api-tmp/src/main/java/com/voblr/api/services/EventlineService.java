package com.voblr.api.services;

import java.util.Iterator;



/**
 * This is a simple interface for a user's eventline.
 * Implementing classes should return an list of
 * events which are ordered chronologically or by some
 * other logic.
 * 
 * @author abhi
 * @since 2012.04.07
 *
 */
public interface EventlineService {
	
	public Iterator<Object> getEventLine();

}
