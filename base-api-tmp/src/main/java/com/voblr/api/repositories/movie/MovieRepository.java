package com.voblr.api.repositories.movie;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;

import com.voblr.api.entity.Movie;

/**
 * @author me
 * @since 01.04.11
 */
public interface MovieRepository extends MovieManager, GraphRepository<Movie>, NamedIndexRepository<Movie> {
}
