package com.voblr.api.repositories.user;

import static com.voblr.api.common.GenericActions.template;
import static com.voblr.api.common.GenericActions.userRepository;
import static org.neo4j.graphdb.DynamicRelationshipType.withName;

import java.util.Collection;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

import org.neo4j.graphdb.Direction;
import org.neo4j.graphdb.Node;
import org.neo4j.graphdb.Relationship;
import org.neo4j.graphdb.ReturnableEvaluator;
import org.neo4j.graphdb.StopEvaluator;
import org.neo4j.graphdb.TraversalPosition;
import org.neo4j.graphdb.Traverser;
import org.neo4j.graphdb.Traverser.Order;
import org.springframework.data.neo4j.annotation.RelationshipEntity;
import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.entity.Entity;
import com.voblr.api.entity.Movie;
import com.voblr.api.entity.User;
import com.voblr.api.entity.relations.EVENT;
import com.voblr.api.entity.relations.FOLLOW;
import com.voblr.api.entity.relations.VOB;
import com.voblr.api.entity.relations.constants.Rels;

/**
 * Spring Data Neo4j backed application context for Worlds.
 */
public class UserRepositoryImpl implements UserManager {

    @Override
    @Transactional
    public User createyOrGetUser(String name) {
    	
        User user = get(name);
        if (user == null) {
        	user = new User(name);
        	userRepository.save(user);
        }
    	return user;
    }

    @Override
    public User get(String name) {
        return userRepository.findByPropertyValue("name", name);
    }

    @Override
    public Iterator<EVENT> getFeeds(String uid) {
//        TraversalDescription traversal = Traversal.description().relationships(("FOLLOWS"), Direction.OUTGOING);
//        return  null;// userRepository.findAllByTraversal(user, traversal);
        
        final User u = userRepository.get(uid);
        
        Node userNode = template.getNode(u.id);
        
        Traverser traverser = userNode.traverse(Order.BREADTH_FIRST, new StopEvaluator() {

            @Override
            public boolean isStopNode(TraversalPosition currentPos) {
                return currentPos.depth() > 0;
            }
        }, new ReturnableEvaluator() {
            @Override
            public boolean isReturnableNode(final TraversalPosition currentPos) {
                return (currentPos.depth() > 0 && !currentPos.isStartNode());
            }
        }, withName(Rels.VOBS), Direction.OUTGOING,
                withName(Rels.FOLLOWS), Direction.OUTGOING);
        
        Collection<EVENT> events = new PriorityQueue<EVENT>(9,
                new Comparator<EVENT>() {

            @Override
            public int compare(EVENT o1, EVENT o2) {
                long diff = o2.getEventTimestamp() - o1.getEventTimestamp();

                if (diff == 0l) {
                    return 0;
                }
                if (diff > 0)
                    return 1;

                return -1;
            }
        });
        
        
        for (final Node n : traverser) {
            for (final Relationship r : n.getRelationships(Direction.OUTGOING)) {
                events.add(new EVENT(){
                    @Override
                    public String name() {
                        try {
                            return Class.forName(r.getProperty("__type__").toString())
                                    .getAnnotation(RelationshipEntity.class).type();
                        } catch (ClassNotFoundException e) {
                            return "EVENT";
                        }
                    }

                    @Override
                    public Entity getStartEntity() {
                        
                        Entity re = new Entity (){
                            
                            String eid = r.getStartNode().getProperty("name").toString();

                            @Override
                            public String id() {
                                return eid;
                            }

                            @Override
                            public Type type() {
                                return userRepository.get(eid)== null ? Type.MOVIE:Type.USER;
                            }
                            
                        };
                        
                        return re ;
                    }

                    @Override
                    public Entity getEndEntity() {
                        // return
                        // repo.get(r.getEndNode().getProperty("name").toString());
                        Entity re = new Entity() {

                            String eid = r.getEndNode().getProperty("name")
                                    .toString();

                            @Override
                            public String id() {
                                return eid;
                            }

                            @Override
                            public Type type() {
                                return userRepository.get(eid) == null ? Type.MOVIE
                                        : Type.USER;
                            }

                        };

                        return re;
                    }

                    @Override
                    public long getEventTimestamp() {
   // TODO Auto-generated method stub
                        return Long.valueOf(r.getProperty("stamp").toString()).longValue();
                    }
                    
                });
            }
        }
        
        return events.iterator();
    }
    
    public UserRepository getRepository(){
        return userRepository;
    }

//    @Override
//    public Iterable<Event> getTimeLine(User user) {
//        // TODO Auto-generated method stub
//        return null;
//    }

    @Override
    public void follows(User user1, User user2) {
//        VOB avob= new VOB(this,other,System.currentTimeMillis());
        long now = now();
        FOLLOW f = new FOLLOW(user1,user2,now);
        template.save(f);
    }
    
    @Override
    public void vobs(User user, Movie movie) {
        long now = now();
        VOB avob= new VOB(user,movie,now);
        template.save(avob);
    }
    
    private long now(){
        return System.currentTimeMillis();
    }

}
