package com.voblr.api.repositories.movie;

import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.entity.Movie;

/**
 * @author abhi
 * @since 2012.04.07
 */
public interface MovieManager {
    /*
     * @Transactional is special annotation to denote the modification
     * to the DB, reading however doesnt require it.
     */
    @Transactional
    public Movie createyOrGetMovie(String name);

    public Movie get(String name);

    public Iterable<Movie> vobbers(Movie user);
    
}
