package com.voblr.api.repositories.user;

import java.util.Iterator;

import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.entity.Movie;
import com.voblr.api.entity.User;
import com.voblr.api.entity.relations.EVENT;

/**
 * @author abhi
 * @since 2012.04.07
 */
public interface UserManager {
    /*
     * @Transactional is special annotation to denote the modification
     * to the DB, reading however doesnt require it.
     */
    @Transactional
    public User createyOrGetUser(String name);

    public User get(String name);
    
    @Transactional
    public void follows(User user1,User user2);
    
    @Transactional
    public void vobs(User user,Movie movie);
    
    public  Iterator<EVENT> getFeeds(String uid);
    
//    public  Iterable<Event> getTimeLine(User user);
}
