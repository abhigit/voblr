package com.voblr.api.repositories.movie;

import static com.voblr.api.common.GenericActions.movieRepository;

import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.entity.Movie;

/**
 * Spring Data Neo4j backed application context for Worlds.
 */
public class MovieRepositoryImpl implements MovieManager {

    @Override
    @Transactional
    public Movie createyOrGetMovie(String name) {
    	
        Movie movie = get(name);
        if (movie == null) {
        	movie = new Movie(name);
        	movieRepository.save(movie);
        }
    	return movie;
    }

    @Override
    public Movie get(String name) {
        return movieRepository.findByPropertyValue("name", name);
    }

    
    public MovieRepository getRepository(){
        return movieRepository;
    }

   
    @Override
    public Iterable<Movie> vobbers(Movie user) {
        // TODO Auto-generated method stub
        return null;
    }
    
}
