package com.voblr.api.repositories.user;

import org.springframework.data.neo4j.repository.GraphRepository;
import org.springframework.data.neo4j.repository.NamedIndexRepository;

import com.voblr.api.entity.User;

/**
 * @author me
 * @since 01.04.11
 */
public interface UserRepository extends UserManager, GraphRepository<User>, NamedIndexRepository<User> {
}
