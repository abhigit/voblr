package com.voblr.api.repositories.movie;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bson.BSONObject;

import com.mongodb.DBObject;

public class MovieMongoDbObject implements DBObject{

    public String movie_name;
    public String movie_desc;
    public String movie_last_modified_at;
    public String movie_tagline;
    public String movie_released;
    public String movie_trailer;
    public Integer movie_runtime;
    public String movie_homepage;
    public String movie_imdb_id;
    public Integer movie_id;
    public List movie_posters;
    public List movie_directors;
    public List movie_genres;
    
    @Override
    public Object put(String key, Object v) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void putAll(BSONObject o) {
        // TODO Auto-generated method stub
        
    }

    @Override
    public void putAll(Map m) {
        movie_id    = (Integer) m.get("movie_id");
        movie_name  = (String) m.get("movie_name");
        movie_desc  = (String) m.get("movie_desc");
        movie_last_modified_at = (String) m.get("movie_last_modified_at");
        movie_tagline = (String) m.get("movie_tagline");
        movie_released = (String) m.get("movie_released");
        movie_trailer = (String) m.get("movie_trailer");
        movie_runtime = (Integer) m.get("movie_runtime");
        movie_homepage = (String) m.get("movie_homepage");
        movie_imdb_id = (String) m.get("movie_imdb_id");
        movie_posters = (List) m.get("movie_posters");
        movie_directors = (List)m.get("movie_directors");
        movie_genres = (List) m.get("movie_genres");
        // TODO Auto-generated method stub
        
    }

    @Override
    public Object get(String key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Map toMap() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Object removeField(String key) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean containsKey(String s) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean containsField(String s) {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public Set<String> keySet() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public void markAsPartialObject() {
        // TODO Auto-generated method stub
        
    }

    @Override
    public boolean isPartialObject() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public String toString() {
        return "MovieMongoDbObject [movie_name=" + movie_name + ", movie_desc="
                + movie_desc + ", movie_last_modified_at="
                + movie_last_modified_at + ", movie_tagline=" + movie_tagline
                + ", movie_released=" + movie_released + ", movie_trailer="
                + movie_trailer + ", movie_runtime=" + movie_runtime
                + ", movie_homepage=" + movie_homepage + ", movie_imdb_id="
                + movie_imdb_id + ", movie_id=" + movie_id + ", movie_posters="
                + movie_posters.toString() + ", movie_directors="
                + movie_directors.toString() + ", movie_genres="
                + movie_genres.toString() + "]";
    }

}
