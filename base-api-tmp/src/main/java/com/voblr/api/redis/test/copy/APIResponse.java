package com.voblr.api.redis.test.copy;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class APIResponse {
    public String status = "OK";
    public String message = null;
    
    public APIResponse(){
    }
    
    public APIResponse(String message){
        this.message = message;
    }
    
    public APIResponse(String status,String message){
        this.status = status;
        this.message = message;
    }
}
