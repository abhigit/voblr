package com.voblr.api.redis;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.grizzly.http.server.HttpServer;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.voblr.api.rpc.client.Client;

/**
 * Hello world(s)!
 * <p/>
 * An example application for exploring Spring Data Neo4j.
 */
// java -cp lib/*:BaseRedis-0.0.1-SNAPSHOT.jar com.voblr.api.redis.Main

public class ClientYoovi {
	public static Client client ;
	private static URI getBaseURI() {

		// return
		// UriBuilder.fromUri("http://"+System.getProperty("HOST","localhost")+"/").port(9998).build();
		return UriBuilder
				.fromUri(
						"http://" + System.getProperty("HOST", "192.168.1.16")
								+ "/").port(9998).build();
	}

	public static final URI BASE_URI = getBaseURI();

	protected static HttpServer startServer() throws IOException {
		System.out.println("Starting grizzly...");

		ResourceConfig rc = new PackagesResourceConfig(
				"com.voblr.api.rpc.client");
		client = new Client();
		return GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
	}

	public static void main(String[] args) throws IOException {
		HttpServer httpServer = startServer();
		System.in.read();
		httpServer.stop();
	}
}
