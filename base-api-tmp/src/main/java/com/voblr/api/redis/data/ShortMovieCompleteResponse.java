package com.voblr.api.redis.data;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class ShortMovieCompleteResponse  {
	private String id;
	private String name;
	private String imageURL;
	private String tagline;
	private int noOfVobs;

	// private short description

	public ShortMovieCompleteResponse() {

	}

	public ShortMovieCompleteResponse(String id, String name, String imageURL,
			String tagline, int noOfVobs) {
		this.id=id;
		this.name=name;
		this.imageURL=imageURL;
		this.tagline=tagline;
		this.noOfVobs=noOfVobs;
	}

	public ShortMovieCompleteResponse(String id, String name, String imageURL,
			String tagline) {
		this.id=id;
		this.name=name;
		this.imageURL=imageURL;
		this.tagline=tagline;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImageURL() {
		return imageURL;
	}

	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}

	public String getTagline() {
		return tagline;
	}

	public void setTagline(String tagline) {
		this.tagline = tagline;
	}

	public int getNoOfVobs() {
		return noOfVobs;
	}

	public void setNoOfVobs(int noOfVobs) {
		this.noOfVobs = noOfVobs;
	}

}
