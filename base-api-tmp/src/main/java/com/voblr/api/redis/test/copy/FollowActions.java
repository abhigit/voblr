package com.voblr.api.redis.test.copy;


import java.text.MessageFormat;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;


@Path("/follows")
public class FollowActions {
    @GET
    @Produces("application/json")
    @Path("{user_id1}/{user_id2}")
    public APIResponse doFollows(@PathParam("user_id1") String uid1,
            @PathParam("user_id2") String uid2) {

        try {
//            userRepository.follows((userRepository.createyOrGetUser(uid1)),
//                    userRepository.createyOrGetUser(uid2));
            return new APIResponse(MessageFormat.format("{0} followed {1}", uid1, uid2));
        } catch (Exception e) {
            return new APIResponse("ERROR");
        }
    }

//    @GET
//    @Produces("application/json")
//    @Path("{user_id}")
//    public APIResponse getFollowees(@PathParam("user_id") String uid) {
//
//        try {
//            Set<FOLLOW> followee = userRepository.createyOrGetUser(uid).followees;
//            return new UserFollowees(followee);
//        } catch (Exception e) {
//            return new NGResponse(e);
//        }
//    }

}
