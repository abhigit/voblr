package com.voblr.api.redis;

import java.io.IOException;
import java.net.URI;
import java.util.logging.Logger;

import javax.ws.rs.core.UriBuilder;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.glassfish.grizzly.http.server.HttpServer;
import org.slf4j.LoggerFactory;

import com.googlecode.protobuf.pro.duplex.logging.CategoryPerServiceLogger;
import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import com.voblr.api.rpc.client.Client;

/**
 * Hello world(s)!
 * <p/>
 * An example application for exploring Spring Data Neo4j.
 */
// java -cp lib/*:BaseRedis-0.0.1-SNAPSHOT.jar com.voblr.api.redis.Main

public class ClientMain {
	public static Client client ;
	private static URI getBaseURI() {

		// return
		// UriBuilder.fromUri("http://"+System.getProperty("HOST","localhost")+"/").port(9998).build();
		return UriBuilder
				.fromUri(
						"http://" + System.getProperty("client", "localhost")
								+ "/").port(9998).build();
	}

	 static {
	      System.setProperty("org.apache.commons.logging.Log",
	                         "org.apache.commons.logging.impl.NoOpLog");
	   }
	public static final URI BASE_URI = getBaseURI();

	protected static HttpServer startServer() throws IOException {
		System.out.println("Starting grizzly...");

		ResourceConfig rc = new PackagesResourceConfig(
				"com.voblr.api.rpc.client");
		client = new Client();
		return GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
	}

	public static void main(String[] args) throws Exception {
		Log log = LogFactory.getLog(CategoryPerServiceLogger.class);

		System.out.println("info enabled : " + log.isInfoEnabled());
//		LoggerFactory.getLogger(Client.class).
		HttpServer httpServer = startServer();

//		System.in.read();
//		Thread.sleep(31536000000L);
//		httpServer.stop();
	}
}
