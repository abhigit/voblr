package com.voblr.api.mongo.model;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.mongodb.BasicDBList;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.Mongo;
import com.mongodb.MongoException;
import com.voblr.api.common.response.Movie;
import com.voblr.api.repositories.movie.MovieMongoDbObject;

public class MongoConnection {

    public static Mongo mongo;
    public static DB voblrDB;
    public static DBCollection movieCollection;

    static {
        try {
            mongo = new Mongo("dev.ibhupi.com", 27017);
        } catch (UnknownHostException e) {
            e.printStackTrace();
        } catch (MongoException e) {
            e.printStackTrace();
        }

        voblrDB = mongo.getDB("voblr");
        movieCollection = voblrDB.getCollection("movie");
    }

    public MongoConnection() {
    }

    public static List<Movie> getMovieListByIDs(List<Movie> movieIdList) {
        BasicDBList values = new BasicDBList();
        Set<Integer> newSet = new HashSet<Integer>();
        Map<Integer, Movie> movieMap = new HashMap<Integer, Movie>();
        for(Movie movie : movieIdList){
            newSet.add(movie.getMovieId());
            movieMap.put(movie.getMovieId(), movie);
        }
        
        values.addAll(newSet);
        System.out.println("Set of keys for mongo :" + newSet);
        System.out.println("values list : " + values.toString());
        BasicDBObject in = new BasicDBObject("$in", values);
        
        System.out.println("basic DB object list : " + in.toString());
        
        DBCursor result = movieCollection.find(new BasicDBObject("movie_id", in));
        
        System.out.println("result count : " + result.count());
        MovieMongoDbObject a = new MovieMongoDbObject();

        List<Movie> returnList = new ArrayList<Movie>();
        while(result.hasNext()){
//            System.out.println(result.next().toString());
            a.putAll(result.next().toMap());
            returnList.add(new Movie(a.movie_id,
                                    movieMap.get(a.movie_id).getTimestamp(),
                                    a.movie_name,
                                    (String)a.movie_posters.get(0),
                                    a.movie_homepage));
        }
        
        return returnList;
    }
}
