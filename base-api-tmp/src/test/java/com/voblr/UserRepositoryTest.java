package com.voblr;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.neo4j.support.Neo4jTemplate;
import org.springframework.data.neo4j.support.node.Neo4jHelper;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.BeforeTransaction;
import org.springframework.transaction.annotation.Transactional;

import com.voblr.api.entity.Movie;
import com.voblr.api.entity.User;
import com.voblr.api.repositories.movie.MovieRepository;
import com.voblr.api.repositories.user.UserRepository;

/**
 * Exploratory testing of Spring Data Neo4j using
 * the WorldRepositoryImpl.
 */
@ContextConfiguration(locations = "classpath:default/context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class UserRepositoryTest
{

    @Autowired
	private UserRepository repo;
    @Autowired
    private MovieRepository mrepo;

	@Autowired
	private Neo4jTemplate template;

	@Rollback(false)
    @BeforeTransaction
    public void clearDatabase()
    {
		System.out.println("Clearing all db");
		Neo4jHelper.cleanDb(template);
    }

    @SuppressWarnings("unchecked")
    @Test
    public void shouldAllowDirectUserCreation()
    {
        assertEquals(0, (long) template.count(User.class));
        User me = repo.save(new User( "mine" ));
        assertEquals(1, (long) template.count(User.class));
        User foundUser = repo.findOne(me.id);
        
    }

    @Test
//    @Transactional
    public void atest()
    {
//        Movie m0 = mrepo.createyOrGetMovie("matrix");
//        
//        for (User u : m0.users)
//        System.out.println("m0 : "+u);
      
//        User a = repo.createyOrGetUserNamed( "a" );
//        User b = repo.createyOrGetUserNamed( "b" );
//        User c = repo.createyOrGetUserNamed( "c" );
//        User d = repo.createyOrGetUserNamed( "d" );
//        
//        repo.follows(d, a);
//        
//        repo.follows(d, b);
//        
//        repo.follows(d, c);
//       
//        User e = repo.createyOrGetUserNamed( "d" );
//        
//        for (User u : e.friends)
//          System.out.println("m0 : "+u);
//        
////        a.follows(b);
////        repo.save(a);
////        
////        c.follows(a);
////        
////        d.follows(d);
////        
//        Movie m1 = mrepo.createyOrGetMovie("matrix");
//        
//        mrepo.vobs(a, m1);
//        mrepo.vobs(b, m1);
//        mrepo.vobs(e, m1);
////        
////        b.vobs(m1);
////        a.vobs(m1);
////       
////        repo.save(a);
////        mrepo.save(m1);
////       
////        
////        repo.save(b);
////        repo.save(c);
////        repo.save(d);
////        repo.save(e);
////
//        Movie m2 = mrepo.createyOrGetMovie("matrix");
////       
//        for (User u : m2.users)
//        System.out.println("m2 : "+u);
//////        System.out.println("a.follows b : "+a.isFollowing(b));
    }


//    @Test
//    public void shouldHaveCorrectNumberOfWorlds()
//    {
//        repo.makeSomeUsers();
//        assertEquals(13, (long) repo.count());
//    }
//
//    @Test
//    public void shouldFindAllWorlds()
//    {
//        Collection<User> madeWorlds = repo.makeSomeUsers();
//        Iterable<User> foundWorlds = repo.findAll();
//
//        int countOfFoundWorlds = 0;
//        for ( User foundWorld : foundWorlds )
//        {
//            assertTrue( madeWorlds.contains( foundWorld ) );
//            countOfFoundWorlds++;
//        }
//
//        assertEquals( madeWorlds.size(), countOfFoundWorlds );
//    }
//
//    @Test
//    public void shouldFindWorldsByName()
//    {
//        for ( User w : repo.makeSomeUsers() )
//        {
//            assertNotNull( repo.get( w.getName() ) );
//        }
//    }
//
//    @Test
//    public void shouldReachMarsFromEarth()
//    {
//        repo.makeSomeUsers();
//
//        User earth = repo.get( "Earth" );
//        User mars = repo.get( "Mars" );
//        System.err.println(earth.friends);
//        System.err.println(mars.friends);
//
//        assertTrue("mars can be reached from earth", mars.isFollowing( earth ) );
//        assertTrue("earth can be reached from mars", earth.isFollowing( mars ) );
//    }

}
