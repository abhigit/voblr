#!/bin/bash
# Date: 14 Feb 2013
# Time: 10:45 PM JST
#

# A simple runner script, use it after mvn clean package
# We need a better way to get things working

#
# Explaination

# server is the graph db host, port used is 7040, default for now
# client is the api server's host ip, set it to 0.0.0.0 to accept from anywhere
# client_port is the api's access port, defaults to grizzly's for now


JAVA_OPTS="-Dserver=graph.voblr.com -Dclient=0.0.0.0 -Dclient_port=9998"

LOG=`date +%y%m%d%H%M%S`.log

# Bootstraping from BaseAPIMain class
MAINCLASS=com.voblr.base.api.BaseAPIMain

# Store command to be run first before executing (evaling) it

CP="`echo target/*.jar | tr ' ' ':'`"
CP=$CP:"`echo target/lib/*.jar | tr ' ' ':'`"

cmd="${JAVA_OPTS} -cp $CP ${MAINCLASS}"

# so that we can echo it, for debugging
echo $cmd

# run it now --> boom...
# do some simple log rotating :)
if [ -f ~/logs/voblr.log ];
then
    mv ~/logs/voblr.log ~/logs/$LOG
fi

nohup java $cmd &> ~/logs/voblr.log &

# check log file gerenerated
