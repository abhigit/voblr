package com.voblr.base.api.v1.common;

import com.voblr.base.api.v1.data.mongo.model.user.User;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

/**
 * Abhijeet Parande
 * Date: 3/12/13
 * Time: 10:57 PM
 * (c) Voblr, Inc
 */
public class VoblrMySql {

    String url = "jdbc:mysql://voblrmovies.db.7935083.hostedresource.com:3306/voblrmovies";
    String driverClass = "com.mysql.jdbc.Driver";

    public final java.sql.Connection con;

    public static VoblrMySql getInstance() throws Exception {
        if (instance == null) {
            synchronized (VoblrMySql.class) {
                if (instance == null) {
                    instance = new VoblrMySql();
                }
            }
        }
        return instance;
    }

    private static volatile VoblrMySql instance;

    private VoblrMySql() throws ClassNotFoundException, SQLException {
        System.out.println("URL: " + url);
        Class.forName(driverClass);
        // Get a connection to the database for a
        // user named root with a blank password.
        // This user is the default administrator
        // having full privileges to do anything.
        con = DriverManager.getConnection(url, "voblrmovies", "SocialMovie12");
    }

    public List<User> getAllMySqlUsers() throws Exception {
        List<User> userList = new ArrayList<User>();
        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("Select * from User");
        while (rs.next()) {
            User user = new User();
            user.setUserId(rs.getString("id"));
            user.setPicURL(String.format("http://graph.facebook.com/%s/picture", rs.getString("username")));
            user.setName(rs.getString("name"));
            user._createdTimeStamp = toSolr(rs.getString("create_ts"));
            user._updatedTimeStamp = toSolr(rs.getString("update_ts"));
            userList.add(user);
        }
        return userList;
    }

    static String toSolr(String mysqlDate) throws ParseException {
        Date d = mysqlFormat.parse(mysqlDate) ;
//        solrFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
        return solrFormat.format(d);
    }

    static SimpleDateFormat mysqlFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static SimpleDateFormat solrFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

    public static void main(String[] args) throws  Exception{
        VoblrMySql mySql = VoblrMySql.getInstance();

        for(User user : mySql.getAllMySqlUsers()){
            System.out.println(user.getUserId() + "\t" + user.getName()
                    + "\t" + user.getPicURL() + "\t" + mysqlFormat.parse(user._createdTimeStamp) + "\t" + mysqlFormat.parse(user._updatedTimeStamp));
        }

    }

}
