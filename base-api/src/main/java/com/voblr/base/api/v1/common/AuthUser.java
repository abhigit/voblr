package com.voblr.base.api.v1.common;

import redis.clients.jedis.Jedis;

/**
 * User: abhi@voblr.com
 * Date: 11/10/12
 * Time: 9:18 PM
 * (c) Voblr, Inc
 */
public class AuthUser {
    private static Jedis jedis = new Jedis("54.248.110.27", 6379);

    public static String getUserID(String userCookie) {

        System.out.println("cookie = " + userCookie);
        if (userCookie == null) {
            return null;
        }
        String userId = jedis.get(userCookie);
        System.out.println(jedis.get(userCookie));
        System.out.println("userId=" + userId);
        return userId;
    }

    public static void cleanUp() {
        System.out.println(jedis.dbSize());
        System.out.println(jedis.flushAll());
        System.out.println(jedis.dbSize());
    }
}
