package com.voblr.base.api.v1.common;

import com.google.protobuf.RpcController;
import com.google.protobuf.ServiceException;
import com.voblr.base.api.RpcClient;
import com.voblr.base.api.v1.data.mongo.controller.UserInfoController;
import com.voblr.base.api.v1.data.mongo.controller.VobController;
import com.voblr.base.api.v1.data.mongo.model.vob.VobComments;
import com.voblr.base.api.v1.data.mongo.model.vob.VobData;
import com.voblr.base.api.v1.data.mongo.response.*;
import com.voblr.base.api.v1.data.mongo.services.UserService;
import com.voblr.base.api.v1.response.APIResponse;
import com.voblr.common.GraphServices;
import com.voblr.common.constants.GraphConstants;
import com.voblr.common.constants.ResponseConstants;
import com.voblr.common.proto.Comment;
import com.voblr.common.proto.Movie;
import com.voblr.common.proto.User;
import com.voblr.common.proto.Vob;
import org.neo4j.helpers.collection.IterableWrapper;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * User: abhi@voblr.com Date: 11/10/12 Time: 11:29 PM (c) Voblr, Inc
 */
public class APIImpl {
    public APIResponse _doFollow(String user_id, String other_user_id)
            throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();

        GraphServices.FollowRequest request = GraphServices.FollowRequest
                .newBuilder().setUser1(user_id).setUser2(other_user_id)
                .setTs(System.currentTimeMillis()).build();

        GraphServices.SimpleResponse response = userService.doFollow(
                controller, request);
        String responseCode;
        if (response.getStatus().equals("OK")) {
            responseCode = ResponseConstants.OK;
            if (UserService
                    ._incPropertyInDocument(
                            com.voblr.base.api.v1.data.mongo.model.user.User.class,
                            user_id,
                            com.voblr.base.api.v1.data.mongo.model.user.User.NO_OF_FOLLOWING,
                            1) == ResponseCode.SUCCESS) {

                if (UserService
                        ._incPropertyInDocument(
                                com.voblr.base.api.v1.data.mongo.model.user.User.class,
                                other_user_id,
                                com.voblr.base.api.v1.data.mongo.model.user.User.NO_OF_FOLLOWERS,
                                1) == ResponseCode.SUCCESS) {

                }
            }

        } else
            responseCode = ResponseConstants.NG;

        return new APIResponse(responseCode, response.getMessage());
    }

    public APIResponse _doVoteUp(String user_id, String vob_id)
            throws ServiceException {
        System.out.println("Vote up called : " + user_id + " vob id : "
                + vob_id);

        GraphServices.VobService.BlockingInterface vobService = GraphServices.VobService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();

        GraphServices.VoteOnVobRequest request = GraphServices.VoteOnVobRequest
                .newBuilder().setUserId(user_id).setVobId(vob_id)
                .setTs(System.currentTimeMillis()).build();

        GraphServices.SimpleResponse response = vobService.doVoteUp(controller,
                request);
        String responseCode;
        if (response.getStatus().equals("OK")) {
            responseCode = ResponseConstants.OK;
            UserService._incPropertyInDocument(VobData.class, vob_id,
                    VobData.VOTE_UP_COUNT, 1);
        } else
            responseCode = ResponseConstants.NG;

        return new APIResponse(responseCode, response.getMessage());
    }

    public APIResponse _doVoteDown(String user_id, String vob_id)
            throws ServiceException {

        System.out.println("Vote down called : " + user_id + " vob id : "
                + vob_id);

        GraphServices.VobService.BlockingInterface vobService = GraphServices.VobService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();

        GraphServices.VoteOnVobRequest request = GraphServices.VoteOnVobRequest
                .newBuilder().setUserId(user_id).setVobId(vob_id)
                .setTs(System.currentTimeMillis()).build();

        GraphServices.SimpleResponse response = vobService.doVoteDown(
                controller, request);
        String responseCode;
        if (response.getStatus().equals("OK")) {
            responseCode = ResponseConstants.OK;
            UserService._incPropertyInDocument(VobData.class, vob_id,
                    VobData.VOTE_DOWN_COUNT, 1);
        } else
            responseCode = ResponseConstants.NG;

        return new APIResponse(responseCode, response.getMessage());
    }

    public APIResponse _doCreate(String userId, String name, String picURL)
            throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();

        User.UserProto request = User.UserProto.newBuilder()
                .setRecentActivityTs(0).setUserId(userId).build();

        GraphServices.SimpleResponse response = userService.createUser(
                controller, request);
        String responseCode;
        if (response.getStatus().equals("OK")) {
            responseCode = ResponseConstants.OK;
            UserInfoController uic = new UserInfoController();
            uic.setUserInfo(userId, name, picURL);
        } else
            responseCode = ResponseConstants.NG;

        return new APIResponse(responseCode, response.getMessage());
    }

    public APIResponse _remove(String type, String id)
            throws ServiceException {
        GraphServices.EntityService.BlockingInterface entityService = GraphServices.EntityService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();
        APIResponse resp = new APIResponse();
        GraphServices.EntityType eType = null;
        try {
            eType = GraphServices.EntityType.valueOf(type);
        } catch (IllegalArgumentException e) {
            resp.status = "NG";
            resp.message = "Unknown type " + type;
            return resp;
        }
        GraphServices.Entity request = GraphServices.Entity.newBuilder()
                .setEntityId(id).setEntityType(eType).build();

        GraphServices.SimpleResponse response = entityService.remove(controller, request);

        resp.status = response.getStatus();
        resp.message = response.getMessage();

        return resp;
    }

    public APIResponse _undoRemove(String type, String id)
            throws ServiceException {
        GraphServices.EntityService.BlockingInterface entityService = GraphServices.EntityService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();
        APIResponse resp = new APIResponse();
        GraphServices.EntityType eType = null;
        try {
            eType = GraphServices.EntityType.valueOf(type);
        } catch (IllegalArgumentException e) {
            resp.status = "NG";
            resp.message = "Unknown type " + type;
            return resp;
        }
        GraphServices.Entity request = GraphServices.Entity.newBuilder()
                .setEntityId(id).setEntityType(eType).build();

        GraphServices.SimpleResponse response = entityService.undoRemove(controller, request);

        resp.status = response.getStatus();
        resp.message = response.getMessage();

        return resp;
    }


    public APIResponse _doCreateMovie(String movieId) throws ServiceException {
        GraphServices.MovieService.BlockingInterface movieService = GraphServices.MovieService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();

        Movie.MovieProto request = Movie.MovieProto.newBuilder()
                .setMovieId(movieId).build();

        GraphServices.SimpleResponse response = movieService.createMovie(
                controller, request);
        String responseCode;
        if (response.getStatus().equals("OK")) {
            responseCode = ResponseConstants.OK;
        } else
            responseCode = ResponseConstants.NG;

        return new APIResponse(responseCode, response.getMessage());
    }


    public CommonApiResponse _getFollowers(String user_id, String offset, int limit)
            throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();

        User.UserProto request = User.UserProto.newBuilder()
                .setRecentActivityTs(0).setUserId(user_id).build();

        GraphServices.Users response = userService.getFollowers(
                controller, request);

        return new com.voblr.base.api.v1.data.mongo.response.UserListResponse(
                response);
    }

    public CommonApiResponse _getFollowing(String user_id, String offset, int limit)
            throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());

        RpcController controller = RpcClient.getController();

        User.UserProto request = User.UserProto.newBuilder()
                .setRecentActivityTs(0).setUserId(user_id).build();

        GraphServices.Users response = userService.getFollowing(
                controller, request);

        return new com.voblr.base.api.v1.data.mongo.response.UserListResponse(
                response);
    }

    public APIResponse _doVob(String user_id, String objectId,
                              String objectType, String vobTxt) throws ServiceException {
        GraphServices.VobService.BlockingInterface vobService = GraphServices.VobService
                .newBlockingStub(RpcClient.getClientChannel());
        long vob_time = System.currentTimeMillis();
        GraphServices.VobRequest request = GraphServices.VobRequest
                .newBuilder().setSubjectId(user_id).setObjectId(objectId)
                .setObjectType(GraphServices.EntityType.valueOf(objectType))
                .setVobText(vobTxt).setVobTime(vob_time).build();
        GraphServices.SimpleResponse response = vobService.doVob(
                RpcClient.getController(), request);

        String responseCode;
        if (response.getStatus().equals("OK")) {
            responseCode = ResponseConstants.OK;
            VobController vc = new VobController();
            vc.doVob(objectId, null, null, vobTxt, user_id, vob_time);
        } else
            responseCode = ResponseConstants.NG;
        return new APIResponse(responseCode, response.getMessage());
    }

    public APIResponse _doComment(String user_id, String objectId,
                                  String objectType, String message) throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());
        long comment_time = System.currentTimeMillis();
        GraphServices.CommentRequest request = GraphServices.CommentRequest
                .newBuilder().setSubjectId(user_id).setObjectId(objectId)
                .setObjectType(GraphServices.EntityType.valueOf(objectType))
                .setMessage(message).setCommentTime(comment_time).build();
        GraphServices.SimpleResponse response = userService.doComment(
                RpcClient.getController(), request);

        String responseCode;
        if (response.getStatus().equals("OK")) {
            responseCode = ResponseConstants.OK;
            UserService.doComment(objectId, user_id, message, new Date(
                    comment_time));
        } else
            responseCode = ResponseConstants.NG;
        return new APIResponse(responseCode, response.getMessage());
    }

    public CommonApiResponse _getMovieVobs(String movieId, String offset,
                                           int limit) throws ServiceException {
        return _getMovieVobs(movieId, null, offset, limit);
    }

    public List<com.voblr.base.api.v1.data.mongo.model.user.User>
    getUsers(GraphServices.Entities entities) throws ServiceException {

        GraphServices.EntityService.BlockingInterface entityService = GraphServices.EntityService
                .newBlockingStub(RpcClient.getClientChannel());

        GraphServices.Users response = entityService
                .getUsers(RpcClient.getController(), entities);

        if (response.getStatus().equals("OK")) {
            return new UserListResponse(response).getUserList();
        }
        return new ArrayList<com.voblr.base.api.v1.data.mongo.model.user.User>();
    }

    public CommonApiResponse _getMovieVobs(String movieId, String userId,
                                           String offset, int limit) throws ServiceException {
        GraphServices.MovieService.BlockingInterface movieService = GraphServices.MovieService
                .newBlockingStub(RpcClient.getClientChannel());

        if (userId == null)
            userId = "";
        if (offset == null)
            offset = "$start";
        if (limit >= 15 || limit < 1)
            limit = 15;
        GraphServices.VobRequest request = GraphServices.VobRequest
                .newBuilder().setObjectId(movieId).setSubjectId(userId)
                .setOffset(offset).setLimit(limit).build();

        long start1 = System.nanoTime();
        GraphServices.Vobs response = movieService.getAllVobs(
                RpcClient.getController(), request);

        System.out.println("All Movie Vobs(Graph)  "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                + " ms");

        if (response.getStatus().equals("OK")) {
            VobListResponse resp = new VobListResponse();
            resp.nextId = response.getNext();
            List<VobData> vobDatas = new ArrayList<VobData>();
            for (Vob.VobProto proto : response.getVobsList()) {
                VobData vobData = new VobData(proto);
                List<com.voblr.base.api.v1.data.mongo.model.vob.Comment> comments = new ArrayList<com.voblr.base.api.v1.data.mongo.model.vob.Comment>();
                for (Comment.CommentProto commentProto : proto.getComments().getCommentsList()) {
                    comments.add(new com.voblr.base.api.v1.data.mongo.model.vob.Comment(
                            commentProto));
                }
                vobData.vobComments = new VobComments(null, comments);
                vobDatas.add(vobData);
            }
            resp.vobList = vobDatas;
            resp.count = vobDatas.size();
            return resp;
        } else {
        }
        return new VobController().getMovieVobs(movieId);
    }

    public CommonApiResponse _getUserVotedUpVobs(String userId, String offset,
                                                 int limit) throws ServiceException {
        return _getUserVotedUpVobs(userId, null, offset, limit);
    }

    public CommonApiResponse _getUserVotedUpVobs(String objectUserId, String subjectUserId,
                                                 String offset, int limit) throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());

        boolean isFollowing = false;
        if (subjectUserId == null)
            subjectUserId = "";
        else {
            List<String> user = new ArrayList<String>();
            user.add(objectUserId);
            List<GraphServices.DirectedRelationship> rel =
                    new ArrayList<GraphServices.DirectedRelationship>();
            rel.add(GraphServices.DirectedRelationship.newBuilder()
                    .setDir(GraphServices.Direction.OUT)
                    .setRelType(GraphConstants.FOLLOWS).build());
            Iterable<List<Boolean>> t
                    = getCompositeResponse(subjectUserId,
                    GraphServices.EntityType.USER, user, GraphServices.EntityType.USER, rel);
            isFollowing = t.iterator().next().get(0);
        }
        if (offset == null)
            offset = "$start";
        if (limit >= 15 || limit < 1)
            limit = 15;
        GraphServices.VobRequest request = GraphServices.VobRequest
                .newBuilder().setObjectId(objectUserId).setSubjectId(subjectUserId)
                .setOffset(offset).setLimit(limit).build();

        long start1 = System.nanoTime();
        GraphServices.Vobs response = userService.getVotedUpVobs(
                RpcClient.getController(), request);

        System.out.println("All VotedUp Vobs(Graph)  "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                + " ms");

        if (response.getStatus().equals("OK")) {
            VobListResponse resp = new VobListResponse();
            resp.nextId = response.getNext();
            List<VobData> vobDatas = new ArrayList<VobData>();
            for (Vob.VobProto proto : response.getVobsList()) {
                VobData vobData = new VobData(proto);
                List<com.voblr.base.api.v1.data.mongo.model.vob.Comment> comments = new ArrayList<com.voblr.base.api.v1.data.mongo.model.vob.Comment>();
                for (Comment.CommentProto commentProto : proto.getComments().getCommentsList()) {
                    comments.add(new com.voblr.base.api.v1.data.mongo.model.vob.Comment(
                            commentProto));
                }
                vobData.vobComments = new VobComments(null, comments);
                vobDatas.add(vobData);
            }
            resp.vobList = vobDatas;
            resp.isFollowing = isFollowing;
            resp.count = vobDatas.size();
            return resp;
        } else {
        }
        return new VobController().getMovieVobs(objectUserId);
    }


    public CommonApiResponse _getMoviePopularVobs(String movieId, String offset,
                                                  int limit) throws ServiceException {
        return _getMoviePopularVobs(movieId, null, offset, limit);
    }

    public CommonApiResponse _getMoviePopularVobs(String movieId, String userId,
                                                  String offset, int limit) throws ServiceException {
        GraphServices.MovieService.BlockingInterface movieService = GraphServices.MovieService
                .newBlockingStub(RpcClient.getClientChannel());

        if (userId == null)
            userId = "";

        if (offset == null)
            offset = "$start";
        if (limit >= 15 || limit < 1)
            limit = 15;
        GraphServices.VobRequest request = GraphServices.VobRequest
                .newBuilder().setObjectId(movieId).setSubjectId(userId)
                .setOffset(offset).setLimit(limit).build();

        long start1 = System.nanoTime();
        GraphServices.Vobs response = movieService.getPopularVobs(
                RpcClient.getController(), request);

        System.out.println("Popular Movie Vobs(Graph)  "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                + " ms");

        if (response.getStatus().equals("OK")) {
            VobListResponse resp = new VobListResponse();
            resp.nextId = response.getNext();
            List<VobData> vobDatas = new ArrayList<VobData>();
            for (Vob.VobProto proto : response.getVobsList()) {
                VobData vobData = new VobData(proto);
                List<com.voblr.base.api.v1.data.mongo.model.vob.Comment> comments = new ArrayList<com.voblr.base.api.v1.data.mongo.model.vob.Comment>();
                for (Comment.CommentProto commentProto : proto.getComments().getCommentsList()) {
                    comments.add(new com.voblr.base.api.v1.data.mongo.model.vob.Comment(
                            commentProto));
                }
                vobData.vobComments = new VobComments(null, comments);
                vobDatas.add(vobData);
            }
            resp.vobList = vobDatas;
            resp.count = vobDatas.size();
            return resp;
        } else {
        }
        return new VobController().getMovieVobs(movieId);
    }

    public CommonApiResponse _getNewsFeedVobs(String userId, String offset,
                                              int limit) throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());

        if (offset == null)
            offset = "$start";
        if (limit >= 15 || limit < 1)
            limit = 15;
        GraphServices.VobRequest request = GraphServices.VobRequest
                .newBuilder().setSubjectId(userId).setOffset(offset)
                .setLimit(limit).build();

        long start1 = System.nanoTime();
        GraphServices.Vobs response = userService.getNewsFeedVobs(
                RpcClient.getController(), request);

        System.out.println("News Feed Vobs(Graph)  "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                + " ms");

        if (response.getStatus().equals("OK")) {
            VobListResponse resp = new VobListResponse();
            resp.nextId = response.getNext();
            List<VobData> vobDatas = new ArrayList<VobData>();
            for (Vob.VobProto proto : response.getVobsList()) {
                VobData vobData = new VobData(proto);
                List<com.voblr.base.api.v1.data.mongo.model.vob.Comment> comments = new ArrayList<com.voblr.base.api.v1.data.mongo.model.vob.Comment>();
                for (Comment.CommentProto commentProto : proto.getComments().getCommentsList()) {
                    comments.add(new com.voblr.base.api.v1.data.mongo.model.vob.Comment(
                            commentProto));
                }
                vobData.vobComments = new VobComments(null, comments);
                vobDatas.add(vobData);
            }
            resp.vobList = vobDatas;
            resp.count = vobDatas.size();
            return resp;
        } else {
        }
        return StaticResponse.InvalidRquest;
    }

    public CommonApiResponse _getUserVobs(String user_id, String offset,
                                          int limit) throws ServiceException {
        return _getUserVobs(null, user_id, offset, limit);
    }

    public CommonApiResponse _getUserVobs(String logged_in_user,
                                          String user_id, String offset, int limit) throws ServiceException {
        GraphServices.UserService.BlockingInterface userService = GraphServices.UserService
                .newBlockingStub(RpcClient.getClientChannel());

        boolean isFollowing = false;
        if (logged_in_user == null)
            logged_in_user = "";
        else {
            List<String> user = new ArrayList<String>();
            user.add(user_id);
            List<GraphServices.DirectedRelationship> rel =
                    new ArrayList<GraphServices.DirectedRelationship>();
            rel.add(GraphServices.DirectedRelationship.newBuilder()
                    .setDir(GraphServices.Direction.OUT)
                    .setRelType(GraphConstants.FOLLOWS).build());
            Iterable<List<Boolean>> t
                    = getCompositeResponse(logged_in_user,
                    GraphServices.EntityType.USER, user, GraphServices.EntityType.USER, rel);
            isFollowing = t.iterator().next().get(0);
        }
        if (offset == null)
            offset = "$start";
        if (limit > 15 || limit < 0)  ///NOTE return immediately, if limit = 0
            limit = 15;

        GraphServices.VobRequest request = GraphServices.VobRequest
                .newBuilder()
                .setSubjectId(logged_in_user)
                .setObjectId(user_id)
                .setOffset(offset)
                .setLimit(limit)
                .build();

        GraphServices.Vobs response = userService.getAllVobs(
                RpcClient.getController(), request);

        if (response.getStatus().equals("OK")) {
            VobListResponse resp = new VobListResponse();
            resp.nextId = response.getNext();
            List<VobData> vobDatas = new ArrayList<VobData>();
            for (Vob.VobProto proto : response.getVobsList()) {
                VobData vobData = new VobData(proto);
                List<com.voblr.base.api.v1.data.mongo.model.vob.Comment> comments = new ArrayList<com.voblr.base.api.v1.data.mongo.model.vob.Comment>();
                for (Comment.CommentProto commentProto : proto.getComments()
                        .getCommentsList()) {
                    comments.add(new com.voblr.base.api.v1.data.mongo.model.vob.Comment(
                            commentProto));
                }
                vobData.vobComments = new VobComments(null, comments);
                vobDatas.add(vobData);
            }
            resp.vobList = vobDatas;
            resp.isFollowing = isFollowing;
            resp.count = vobDatas.size();
            return resp;
        }
        return new VobController().getUserVobs(user_id);
    }

    /**
     * @param subject_id
     * @param subject_type
     * @param object_ids
     * @param object_type
     * @param rel_types
     * @return
     * @throws ServiceException
     */
    public Iterable<List<Boolean>> getCompositeResponse(String subject_id,
                                                        GraphServices.EntityType subject_type, List<String> object_ids,
                                                        GraphServices.EntityType object_type,
                                                        List<GraphServices.DirectedRelationship> rel_types)
            throws ServiceException {
        GraphServices.EntityService.BlockingInterface entityService = GraphServices.EntityService
                .newBlockingStub(RpcClient.getClientChannel());

        GraphServices.CompositeRequest request = GraphServices.CompositeRequest
                .newBuilder().setEntityId(subject_id)
                .setEntityType(subject_type).addAllOtherIds(object_ids)
                .setOtherType(object_type).addAllRels(rel_types).build();

        GraphServices.CompositeResponse response = entityService
                .getCompositeResponse(RpcClient.getController(), request);

        if (response.getStatus().equals("OK")) {
            return new IterableWrapper<List<Boolean>, GraphServices.InnerCompositeResponse>(
                    response.getRelatesToRelTypesList()) {
                @Override
                protected List<Boolean> underlyingObjectToObject(
                        GraphServices.InnerCompositeResponse object) {
                    return object.getRelatesToOtherIdsList();
                }
            };
        }
        return new LinkedList<List<Boolean>>();
    }
}
