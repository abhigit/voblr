package com.voblr.base.api.v2.api;

import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v2.objects.User;
import com.voblr.common.GraphServices;
import com.voblr.common.proto.User.UserProto;

import java.util.ArrayList;
import java.util.List;

/**
 * Abhijeet Parande
 * Date: 3/18/13
 * Time: 2:23 AM
 * (c) Voblr, Inc
 */
public class UserAdapter implements EntityAdapter<User, UserProto, List<User>, GraphServices.Users> {

    private MongoConnector mongoConnector;

    public UserAdapter(MongoConnector mongoConnector) {
        this.mongoConnector = mongoConnector;
    }

    @Override
    public User underlyingEntity(UserProto some) {

        MovieBasicInfo movieBasicInfo = mongoConnector.getSingle(MovieBasicInfo.class, "_id", some.getUserId());
        User ret = new User();
        ret.setMovieId(some.getUserId());
        ret.setName(movieBasicInfo.getName());
        ret.setCast(movieBasicInfo.getCast());
        ret.setDescription(movieBasicInfo.getDescription());
        ret.setPicUrl(movieBasicInfo.getPicUrl());
        ret.setReleaseDate(movieBasicInfo.getReleaseDate());
        ret.setDirectors(movieBasicInfo.getDirectors());
        ret.setNoOfVobs(some.getTotalVobs());
        ret.setLang(movieBasicInfo.getLang());
        ret.set_createdTimeStamp(String.valueOf(some.getCreatedTs()));
        ret.set_updatedTimeStamp(String.valueOf(some.getCreatedTs()));
        return ret;
    }

    @Override
    public List<User> underlyingEntities(GraphServices.Users some) {
        List<UserProto> protos = some.getUsersList();
        List<User> ret = new ArrayList<User>();
        for (UserProto proto : protos) {
            ret.add(underlyingEntity(proto));
        }
        return ret;
    }

    public static void main(String[] args) {
        UserAdapter movieWrapper = new UserAdapter(new MongoConnector());
        UserProto some = UserProto.newBuilder().setUserId("146819").build();
        User movie = movieWrapper.underlyingEntity(some);
        System.out.println(movie);
    }
}
