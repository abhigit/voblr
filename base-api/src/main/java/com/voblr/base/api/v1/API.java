/**
 * Date : Aug 14, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.base.api.v1;

import com.google.protobuf.ServiceException;
import com.voblr.base.api.v1.common.APIImpl;
import com.voblr.base.api.v1.common.AuthUser;
import com.voblr.base.api.v1.data.mongo.model.vob.VobData;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.response.VobListResponse;
import com.voblr.base.api.v1.data.mongo.services.UserService;
import com.voblr.base.api.v1.response.APIResponse;
import org.jboss.netty.handler.codec.http.HttpResponseStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * @author abhi@voblr.com
 */
@Path("/v1")
public class API {

    public Logger LOGGER = LoggerFactory.getLogger(API.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/follow/{follow_id}")
    public APIResponse GET_doFollow(@QueryParam("callback") String callback,
                                    @CookieParam("vob_common") String userCookie,
                                    @PathParam("follow_id") String follow_id) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            return APIResponse.fail("User authorization failed");
        }

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._doFollow(user_id, follow_id);
        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_doFollow : %s --follow--> %s",
                    user_id, follow_id), e);
            return APIResponse.fail("Follow failed");
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/create/{user_id}")
    public APIResponse GET_doCreateUser(
            // @CookieParam("vob_common") String userCookie,
            @PathParam("user_id") String user_id,
            @QueryParam("name") String name, @QueryParam("picURL") String picURL) {

        // String user_id = AuthUser.getUserID(userCookie);

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._doCreate(user_id, name, picURL);
        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_doCreate : %s", user_id), e);
            return APIResponse.fail("createUser failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/voteup/{vob_id}")
    public APIResponse GET_doVoteUp(@PathParam("vob_id") String vob_id,
                                    @CookieParam("vob_common") String userCookie) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            return APIResponse.fail("User authorization failed");
        }
        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._doVoteUp(user_id, vob_id);
        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_doVoteUp : %s", user_id), e);
            return APIResponse.fail("doVoteUp failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/votedown/{vob_id}")
    public APIResponse GET_doVoteDown(@PathParam("vob_id") String vob_id,
                                      @CookieParam("vob_common") String userCookie) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            return APIResponse.fail("User authorization failed");
        }
        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._doVoteDown(user_id, vob_id);
        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_doVoteUp : %s", user_id), e);
            return APIResponse.fail("doVoteUp failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/create/m/{movie_id}")
    public APIResponse GET_doCreateMovie(
            // @CookieParam("vob_common") String userCookie,
            @PathParam("movie_id") String movie_id) {

        // String user_id = AuthUser.getUserID(userCookie);

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._doCreateMovie(movie_id);
        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_doCreateMovie : %s", movie_id), e);
            return APIResponse.fail("createMovie failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/followers")
    public CommonApiResponse GET_getFollowers(
            @CookieParam("vob_common") String userCookie,
            @QueryParam("nextId") String offset, @QueryParam("count") int limit) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            return new CommonApiResponse(
                    HttpResponseStatus.BAD_REQUEST.getCode(),
                    "User authorization failed");
        }

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._getFollowers(user_id, offset, limit);

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_getFollowers : %s", user_id), e);
            return new CommonApiResponse(
                    HttpResponseStatus.BAD_REQUEST.getCode(),
                    "getFollowers failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/followers/{user_id}")
    public CommonApiResponse GET_getFollowersFor(
            @PathParam("user_id") String user_id,
            @QueryParam("nextId") String offset, @QueryParam("count") int limit) {

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._getFollowers(user_id, offset, limit);

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_getFollowers : %s", user_id), e);
            return new CommonApiResponse(
                    HttpResponseStatus.BAD_REQUEST.getCode(),
                    "getFollowers failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/following")
    public CommonApiResponse GET_getFollowing(
            @CookieParam("vob_common") String userCookie,
            @QueryParam("nextId") String offset, @QueryParam("count") int limit) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            return new CommonApiResponse(
                    HttpResponseStatus.BAD_REQUEST.getCode(),
                    "User authorization failed");
        }

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._getFollowing(user_id, offset, limit);

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_getFollowers : %s", user_id), e);
            return new CommonApiResponse(
                    HttpResponseStatus.BAD_REQUEST.getCode(),
                    "getFollowers failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/following/{user_id}")
    public CommonApiResponse GET_getFollowingFor(
            @PathParam("user_id") String user_id,
            @QueryParam("nextId") String offset, @QueryParam("count") int limit) {

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._getFollowing(user_id, offset, limit);

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_getFollowers : %s", user_id), e);
            return new CommonApiResponse(
                    HttpResponseStatus.BAD_REQUEST.getCode(),
                    "getollowers failed");
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/vob")
    public APIResponse POST_doVob(@CookieParam("vob_common") String userCookie,
                                  @FormParam("vobTxt") String vobTxt,
                                  @FormParam("objectId") String objectId,
                                  @FormParam("type") String objectType) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            return APIResponse.fail("User authorization failed");
        }

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._doVob(user_id, objectId, objectType, vobTxt);

        } catch (ServiceException e) {
            LOGGER.error(String.format("POST_doVob : %s", user_id), e);
            return APIResponse.fail("doVob failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/vob/movie/{movie_id}")
    public CommonApiResponse GET_movieVobs(
            @CookieParam("vob_common") String userCookie,
            @PathParam("movie_id") String movieId,
            @QueryParam("nextId") String offset, @QueryParam("count") int limit) {
        long start = System.nanoTime();
        String user_id = userCookie == null ? null : AuthUser.getUserID(userCookie);
        System.out.println("user authentication "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                + " ms");
        long start1 = System.nanoTime();
        try {
            APIImpl apiImpl = new APIImpl();
            if (user_id == null) {
                return apiImpl._getMovieVobs(movieId, offset, limit);
            } else {
                return apiImpl._getMovieVobs(movieId, user_id, offset, limit);
            }

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_movieVobs : %s", movieId), e);
            return StaticResponse.noMovieInfo;
        } finally {
            System.out.println("Vobs  "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                    + " ms");
            System.out.println("Total "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                    + " ms");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/vob/movie/popular/{movie_id}")
    public CommonApiResponse GET_moviePopularVobs(
            @CookieParam("vob_common") String userCookie,
            @PathParam("movie_id") String movieId,
            @QueryParam("nextId") String offset, @QueryParam("count") int limit) {
        long start = System.nanoTime();
        String user_id = userCookie == null ? null : AuthUser.getUserID(userCookie);
        System.out.println("user authentication "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                + " ms");
        long start1 = System.nanoTime();
        try {
            APIImpl apiImpl = new APIImpl();
            if (user_id == null) {
                return apiImpl._getMoviePopularVobs(movieId, offset, limit);
            } else {
                return apiImpl._getMoviePopularVobs(movieId, user_id, offset, limit);
            }

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_movieVobs : %s", movieId), e);
            return StaticResponse.noMovieInfo;
        } finally {
            System.out.println("Vobs  "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                    + " ms");
            System.out.println("Total "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                    + " ms");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/remove/{type}/{id}")
    public APIResponse GET_remove(@PathParam("type") String type,
                                  @PathParam("id") String id,
                                  @CookieParam("vob_common") String userCookie
    ) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            //return APIResponse.fail("User authorization failed");
        }
        LOGGER.debug("Removing " + type + "/" + id);

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._remove(type, id);

        } catch (ServiceException e) {
            e.printStackTrace();
            LOGGER.error(String.format("GET_remove : user : %s, %s.%s", user_id, type, id), e);
            return APIResponse.fail("doVob failed");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/undo_remove/{type}/{id}")
    public APIResponse GET_undoRemove(@PathParam("type") String type,
                                      @PathParam("id") String id,
                                      @CookieParam("vob_common") String userCookie
    ) {
        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
           // return APIResponse.fail("User authorization failed");
        }
        LOGGER.debug("Removing " + type + "/" + id);

        try {
            APIImpl apiImpl = new APIImpl();
            return apiImpl._undoRemove(type, id);

        } catch (ServiceException e) {
            e.printStackTrace();
            LOGGER.error(String.format("GET_remove : user : %s, %s.%s", user_id, type, id), e);
            return APIResponse.fail("doVob failed");
        }

    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/vob/user/{user_id}")
    public CommonApiResponse GET_userVobs(
            @PathParam("user_id") String vobs_of_user,
            @CookieParam("vob_common") String userCookie,
            @QueryParam("nextId") String offset,
            @QueryParam("count") int limit) {

        long start = System.nanoTime();
        String logged_in_user = userCookie == null ? null : AuthUser.getUserID(userCookie);
        System.out.println("user authentication "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                + " ms");
        long start1 = System.nanoTime();
        try {
            APIImpl apiImpl = new APIImpl();
            if (logged_in_user == null) {
                return apiImpl._getUserVobs(vobs_of_user, offset, limit);
            } else {
                return apiImpl._getUserVobs(logged_in_user, vobs_of_user,
                        offset, limit);
            }

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_userVobs : %s", vobs_of_user), e);
            return StaticResponse.noMovieInfo;
        } finally {
            System.out.println("Vobs  "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                    + " ms");
            System.out.println("Total "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                    + " ms");
        }
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/vob/{vob_id}")
    public CommonApiResponse GET_SingleVob(@PathParam("vob_id") String vobId,
                                           @CookieParam("vob_common") String userCookie) {

        VobData vob = UserService.getVobData(vobId);
        if (vob != null) {
            String logged_in_user = AuthUser.getUserID(userCookie);
            long start1 = System.nanoTime();
            System.out.println("user authentication "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                    + " ms");
            try {
                // if (logged_in_user != null) {
                // List<String> object_ids = new ArrayList<String>();
                //
                // Map<String, VobData> vobMap = new HashMap<String, VobData>();
                // object_ids.add(vob.getVobId());
                // vobMap.put(vob.getVobId(), vob);
                //
                //
                // List<GraphServices.DirectedRelationship> rel_types = new
                // ArrayList<GraphServices.DirectedRelationship>();
                // GraphServices.DirectedRelationship.Builder dr =
                // GraphServices.DirectedRelationship.newBuilder();
                // dr.setDir(Direction.OUT).setRelType(GraphConstants.VOTE_UP);
                //
                // rel_types.add(dr.build());
                //
                // GraphServices.DirectedRelationship.Builder dr1 =
                // GraphServices.DirectedRelationship.newBuilder();
                // dr1.setDir(Direction.OUT).setRelType(GraphConstants.VOTE_DOWN);
                //
                // rel_types.add(dr1.build());
                // APIImpl apiImpl = new APIImpl();
                // Iterable<List<Boolean>> b =
                // apiImpl.getCompositeResponse(logged_in_user, EntityType.USER,
                // object_ids, EntityType.VOB_ENTITY, rel_types);
                // apiImpl.getCompositeResponse(subject_id, subject_type,
                // object_ids, object_type, rel_types);
                // int count = 0;
                // for(List<Boolean> list : b){
                // System.out.println("List : " + count + "  size : " +
                // list.size());
                // vob.userVotedDown
                // }
                // }

            } finally {
                System.out.println("Vobs  "
                        + TimeUnit.NANOSECONDS.toMillis(System.nanoTime()
                        - start1) + " ms");
                // System.out.println("Total " +
                // TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start) +
                // " ms");
            }

        }

        VobListResponse vobList = new VobListResponse();
        vobList.count = 1;
        vobList.vobList = new ArrayList<VobData>();
        vobList.vobList.add(vob);

        return vobList;
    }

}
