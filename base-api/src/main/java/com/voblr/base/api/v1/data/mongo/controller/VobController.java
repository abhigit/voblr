/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.controller;

import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.base.api.v1.common.AuthUser;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.response.VobListResponse;
import com.voblr.base.api.v1.data.mongo.services.UserService;

import javax.ws.rs.CookieParam;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import java.util.Date;

/**
 * 
 * @author vinit
 * Update Time : Nov 11, 2012 : 7:24:48 PM
 * TODO
 */
//@Path("/v1/vobs")
public class VobController {

//	@POST
//	@Produces("application/javascript")
//	@Path("{movie_id}")
	public JSONWithPadding doVob(@PathParam("movie_id") String movieId,
			@CookieParam("vob_common") String vobCommon,
			@QueryParam("callback") String callback,
			@QueryParam("vobTxt") String vobText,
			String user_id,
			long vob_time) {

		String userId ;
		if(user_id == null)
			userId= AuthUser.getUserID(vobCommon);
		else
			userId = user_id;

		JSONWithPadding json = new UserInfoController().getUserInfo(userId, "callback");
		System.out.println(json.getJsonSource().toString());
		System.out.println("Request came with following parameter");
		System.out.println("movie_id : " + movieId);
        System.out.println("user id : " + userId);
        System.out.println("vobtext : " + vobText);
		if (userId == null){
			return new JSONWithPadding(StaticResponse.noLoggedInUser, callback);
		} 

        Date timestamp = new Date(vob_time);

		return new JSONWithPadding(UserService.doVob(userId, movieId, vobText, timestamp), callback);
	}

//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("{movie_id}")
	public CommonApiResponse getMovieVobs(@PathParam("movie_id") String movieId){
		VobListResponse vobs = new VobListResponse();
		vobs.vobList = UserService.getMovieVobs(movieId);
		vobs.count = vobs.vobList.size();
		
		return vobs;
	}


//	@GET
//	@Produces(MediaType.APPLICATION_JSON)
//	@Path("{user_id}")
	public CommonApiResponse getUserVobs(@PathParam("user_id") String userId){
		VobListResponse vobs = new VobListResponse();
		vobs.vobList = UserService.getUserVobs(userId);
		vobs.count = vobs.vobList.size();

		return vobs;
	}

}
