/**
 *
 */
package com.voblr.base.api.v1.data.mongo.controller;

import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.base.api.v1.common.APIImpl;
import com.voblr.base.api.v1.common.DateUtil;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.MovieInfoResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.services.MovieSearch;
import com.voblr.base.api.v1.data.mongo.services.MovieService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.*;

/**
 * @author vinit Update Time : Nov 3, 2012 : 11:30:27 AM TODO
 */
@Path("/v1/movies")
public class MovieController {


    @GET
    @Produces("application/javascript")
    public JSONWithPadding getMovieList(
            @QueryParam("q") String q,
            @QueryParam("nextId") int start,
            @QueryParam("count") int count,
            @QueryParam("callback") String callback) {

        System.out.println("Movie search called with q=" + q + " nextId=" + start + " count=" + count);
        if (q == null || "".equals(q.trim()))
            return new JSONWithPadding(StaticResponse.InvalidRquest, callback);
        else {
            try {
                return new JSONWithPadding(MovieSearch.INSTANCE().query(q, start, count), callback);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONWithPadding(StaticResponse.failed, callback);
            }
        }
    }

    @GET
    @Produces("application/javascript")
    @Path("{movie_id}")
    public JSONWithPadding getMovieInfo(@PathParam("movie_id") String movieId,
                                        @QueryParam("callback") String callback) {

        System.out.println("Got a request for movie details for : " + movieId);
        MovieBasicInfo movie = MovieService.getMovieById(movieId);
        if (movie != null) {
            MovieInfoResponse movieInfo = new MovieInfoResponse();
            movieInfo.setMovieInfo(movie);
            return new JSONWithPadding(movieInfo, callback);
        } else {
            return new JSONWithPadding(StaticResponse.noMovieInfo, callback);
        }
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/addOrUpdate")
    public CommonApiResponse setMovieInfo(@FormParam("movieId") String movieId,
                                          @FormParam("name") String name,
                                          @FormParam("picUrls") String picUrls,
                                          @FormParam("releaseDate") String releaseDate,
                                          @FormParam("cast") String cast,
                                          @FormParam("directors") String directors,
                                          @FormParam("desc") String description,
                                          @FormParam("lang") String lang,
                                          @FormParam("videoUrls") String urls,
                                          @FormParam("isUpdate") boolean update,
                                          @FormParam("secret") String secret) throws Exception {

        String SECRET = System.getProperty("MOVIE_INSERT_SECRET", "voblrapi2012");

        System.out.println("secret : " + secret);
        if (secret == null || !SECRET.equals(secret))
            return StaticResponse.secretInvalid;

        if (movieId == null || movieId.trim().equals(""))
            return StaticResponse.InvalidRquest;

//        if (MongoConnector.getSingleObject(MovieBasicInfo.class, "movieId",
//                movieId) != null && !update)
//            return StaticResponse.alreadyPresent;
        MovieBasicInfo m = MovieService.getMovieById(movieId);
        if (update) {
            if (!(name == null || name.trim().isEmpty())) m.setName(name);
            if (!(picUrls == null || picUrls.isEmpty())){
                m.setPicUrl(Arrays.asList(picUrls.split(";")));
            }
            if (!(releaseDate == null || releaseDate.trim().isEmpty()))
                m.setReleaseDate(DateUtil.dateFromMongoString(releaseDate));
            if (!(cast == null || cast.trim().isEmpty())) m.setCast(cast);
            if (!(directors == null || directors.trim().isEmpty())) m.setDirectors(directors);
            if (!(description == null || description.trim().isEmpty())) m.setDescription(description);
            if (!(lang == null || lang.trim().isEmpty())) m.setLang(lang);
            if (!(urls == null || urls.isEmpty())) {
                m.setUrls(Arrays.asList(urls.split(";")));
            }
        }
        System.out.println("Got a request for movie details for : " + movieId + " update " + update);
        CommonApiResponse c = null;

        if (!update) c = MovieService.setMovieInfo(movieId, name, description, Arrays.asList(picUrls.split(";")),
                releaseDate, cast, directors, lang, Arrays.asList(urls.split(";")));
        else c = MovieService.updateMovieInfo(m);

        if (c.status == 200) {
            try {
                // add it to Solr for search.
                MovieBasicInfo movie = MovieService.getMovieById(movieId);
                movie._updatedTimeStamp = "NOW";
                MovieSearch.INSTANCE().add(movie, true);
                // also add to graph
                new APIImpl()._doCreateMovie(movieId);
            } catch (Exception e) {
                e.printStackTrace();
                c = StaticResponse.movieNotSaved;
            }
        }
        return c;
    }

    public static void main(String[] args) throws Exception {
        //IMP test locally only
        System.setProperty("SolrHost", "localhost");

        List<MovieBasicInfo> list = MovieService.getMovieList();
        MovieSearch movieSearch = MovieSearch.INSTANCE();
        for (MovieBasicInfo m : list) {

            // movieSearch.add(m);
            for (MovieBasicInfo i : movieSearch.query(m.getName().split("\\s+")[0], 0, 10).getMovieList()) {
                System.out.println(i.getName());
            }

//            Thread.sleep(1000*2);
        }
    }

    public static void updateUsers() {

    }


}
