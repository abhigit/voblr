package com.voblr.base.api.v1.data.mongo.model.vob;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import java.util.ArrayList;
import java.util.List;

@Entity(value="VobLikes", noClassnameStored=true)
public class VobVoteUps {
	@Id String vobLikeId;
	List<Vote> voteUps;

	public VobVoteUps() {}

	/**
	 * @param vobLikeId
	 */
	public VobVoteUps(String vobLikeId) {
		this.vobLikeId = vobLikeId;
		this.setLikes(new ArrayList<Vote>());
	}

	/**
	 * @return the vobLikeId
	 */
	public String getVobLikeId() {
		return vobLikeId;
	}

	/**
	 * @param vobLikeId the vobLikeId to set
	 */
	public void setVobLikeId(String vobLikeId) {
		this.vobLikeId = vobLikeId;
	}

	/**
	 * @return the likes
	 */
	public List<Vote> getLikes() {
		return voteUps;
	}

	/**
	 * @param likes the likes to set
	 */
	public void setLikes(List<Vote> likes) {
		this.voteUps = likes;
	}

	/**
	 * @param vobId
	 * @return
	 */
	public static String generateVobLikeId(String vobId) {
		return vobId + "_" + "likes";
	}
	

	@Override
	public int hashCode(){
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj){
		return EqualsBuilder.reflectionEquals(this, obj);
	}
	
	@Override
	public String toString(){
		return ToStringBuilder.reflectionToString(this);
	}

	
}
