package com.voblr.base.api.v1.data.mongo.response;

import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.vob.VobData;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class UserMovieStat {
	public MovieBasicInfo movie;
	public int vobCount;
	public VobData latestVob;

	public UserMovieStat() {

	}
}
