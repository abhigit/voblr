package com.voblr.base.api.v1.data.mongo.controller;

import com.sun.jersey.api.json.JSONConfiguration;
import com.sun.jersey.api.json.JSONJAXBContext;
import com.voblr.base.api.v1.data.mongo.model.vob.VobComments;
import com.voblr.base.api.v1.data.mongo.response.MovieListResponse;
import com.voblr.base.api.v1.data.mongo.response.UserListResponse;
import com.voblr.base.api.v1.data.mongo.response.VobListResponse;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;

import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;
import javax.xml.bind.JAXBContext;

@Provider
public class JAXBContextResolver implements ContextResolver<JAXBContext> {

	private JAXBContext context;
	private Class<?>[] types = { VobListResponse.class, VobComments.class, UserListResponse.class, MovieListResponse.class,
    MovieBasicInfo.class,MovieBasicInfo.class};

	public JAXBContextResolver() throws Exception {
		this.context = new JSONJAXBContext(JSONConfiguration.mapped()
				.arrays("vobList","comments","userList","movieList","picUrl","urls").build(), types);

	//	System.out.println("JAXB called 1");
	}

	public JAXBContext getContext(Class<?> objectType) {
	//	System.out.println("Class : " + objectType);
		for (Class<?> type : types) {
			if (type == objectType) {
				return context;
			}
		}
	//	System.out.println("JAXB called 2");
		return null;
	}
}
