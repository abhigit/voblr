package com.voblr.base.api.v1.response;

import com.voblr.common.constants.ResponseConstants;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class APIResponse {
    public String status;
    public String message;

    public APIResponse() {
        status = "OK";
    }

    public APIResponse(String status, String message) {
        this.status = status;
        this.message = message;
    }

    public static APIResponse fail(String message) {
        return new APIResponse(ResponseConstants.NG, message);
    }

    public static APIResponse success(String message) {
        return new APIResponse(ResponseConstants.OK, message);
    }
}
