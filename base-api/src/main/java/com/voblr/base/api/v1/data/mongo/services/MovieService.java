/**
 *
 */
package com.voblr.base.api.v1.data.mongo.services;

import com.google.code.morphia.query.Query;
import com.voblr.base.api.v1.common.DateUtil;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

/**
 * @author vinit Update Time : Sep 16, 2012 : 11:51:08 PM TODO
 */
public class MovieService {
    public static List<MovieBasicInfo> getMovieList() {

        Query<MovieBasicInfo> q = MongoConnector.ds
                .createQuery(MovieBasicInfo.class).order("-releaseDate");
//				.limit(10);
        List<MovieBasicInfo> list = q.asList();

        return list;

    }

    public static MovieBasicInfo getMovieById(String movieId) {

        Query<MovieBasicInfo> q = MongoConnector.ds.createQuery(
                MovieBasicInfo.class).filter("_id", movieId);
        MovieBasicInfo movie = q.get();

        return movie;

    }

    public static CommonApiResponse updateMovieInfo(MovieBasicInfo movie){
        MongoConnector.ds.save(movie);
        return StaticResponse.success;
    }
    public static CommonApiResponse setMovieInfo(String movieId, String name,
                                                 String description,
                                                 List<String> picUrls,
                                                 String releaseDate, String cast,
                                                 String directors,
                                                 String lang,
                                                 List<String> urls) {
        MovieBasicInfo movie = new MovieBasicInfo();

        Date date = null;
        try {
            date = DateUtil.dateFromMongoString(releaseDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        if (date == null)
            return StaticResponse.incorrectDateFormat;

        movie.setMovieId(movieId);
        movie.setDescription(description);
        movie.setName(name);
        movie.setPicUrl(picUrls);
        movie.setReleaseDate(date);
        movie.setCast(cast);
        movie.setDirectors(directors);
        movie.setNoOfVobs(0);
        movie.setLang(lang);
        movie.setUrls(urls);

        MongoConnector.ds.save(movie);
        MovieBasicInfo m = MongoConnector.getSingleObject(MovieBasicInfo.class, "movieId",
                movieId);
        if (m == null)
            return StaticResponse.movieNotSaved;

        return StaticResponse.success;
    }

    private static Date makeDateFromString(String releaseDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");

        try {
            return sdf.parse(releaseDate);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

}
