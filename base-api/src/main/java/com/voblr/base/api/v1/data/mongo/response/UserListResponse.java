/**
 *
 */
package com.voblr.base.api.v1.data.mongo.response;

import com.voblr.base.api.v1.data.mongo.model.user.User;
import com.voblr.base.api.v1.data.mongo.services.UserService;
import com.voblr.common.GraphServices;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author vinit Update Time : Nov 4, 2012 : 7:17:08 PM TODO
 */
@XmlRootElement
public class UserListResponse extends CommonApiResponse {

    private List<User> userList;
    private int count;
    private String nextId;


    public UserListResponse() {
        super(ResponseCode.SUCCESS, "SUCCESS");
        userList = null;
    }

    public UserListResponse(GraphServices.Users users) {
        super(ResponseCode.SUCCESS, "SUCCESS");

        this.userList = new ArrayList<User>();
        if (users.getUsersList().size() == 0) {
            message = "No users found";
        }
        for (com.voblr.common.proto.User.UserProto user : users.getUsersList()) {
            User t = UserService.getUserById(user.getUserId());
            t.setNoOfVobs(user.getTotalVobs());
            t.setNoOfFollowers(user.getFollowersCount());
            t.setNoOfFollowing(user.getFollowingCount());
            t.recent_activity_ts = user.getRecentActivityTs();
            t.reputation = user.getReputation();
            this.userList.add(t);
        }
        this.nextId = users.getNext();
        count = userList.size();
    }

    /**
     * @return the userList
     */
    public List<User> getUserList() {
        return userList;
    }


    /**
     * @param userList the userList to set
     */
    public void setUserList(List<User> userList) {
        this.userList = userList;
        this.count = userList.size();
    }


    public String getNextId() {
        return nextId;
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    public void setNextId(String nextId) {
        this.nextId = nextId;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }


}
