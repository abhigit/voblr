package com.voblr.base.api.v2.objects;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Indexed;
import com.google.code.morphia.annotations.NotSaved;
import org.apache.solr.client.solrj.beans.Field;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;
import java.util.List;

/**
 * Abhijeet Parande
 * Date: 3/18/13
 * Time: 2:26 AM
 * (c) Voblr, Inc
 */

@XmlRootElement
@Entity(value = "MovieBasicInfo", noClassnameStored = true)
public class User {
    @Indexed
    @Id
    @Field("movie_id")
    private String movieId;
    @Field("movie_name")
    private String name;
    @Field("movie_desc")
    private String description;
    @Field("movie_pic_url")
    private List<String> picUrl;
    @Indexed
    @Field
    private Date releaseDate;
    @Field
    private String cast;
    @Field
    private String directors;
    private String lang;
    private int noOfVobs;

    @NotSaved
    @Field("_createdTimeStamp")
    private String _createdTimeStamp;
    @NotSaved
    @Field("_updatedTimeStamp")
    private String _updatedTimeStamp;


    public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<String> getPicUrl() {
        return picUrl;
    }

    public void setPicUrl(List<String> picUrl) {
        this.picUrl = picUrl;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getCast() {
        return cast;
    }

    public void setCast(String cast) {
        this.cast = cast;
    }

    public String getDirectors() {
        return directors;
    }

    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public String get_createdTimeStamp() {
        return _createdTimeStamp;
    }

    public void set_createdTimeStamp(String _createdTimeStamp) {
        this._createdTimeStamp = _createdTimeStamp;
    }

    public String get_updatedTimeStamp() {
        return _updatedTimeStamp;
    }

    public void set_updatedTimeStamp(String _updatedTimeStamp) {
        this._updatedTimeStamp = _updatedTimeStamp;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public int getNoOfVobs() {
        return noOfVobs;
    }

    public void setNoOfVobs(int noOfVobs) {
        this.noOfVobs = noOfVobs;
    }


}
