package com.voblr.base.api.v1.data.mongo.controller;

import com.google.protobuf.ServiceException;
import com.voblr.base.api.v1.API;
import com.voblr.base.api.v1.common.APIImpl;
import com.voblr.base.api.v1.common.AuthUser;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

@Path("/v1/newsfeed")
public class NewsFeedController {
    public Logger LOGGER = LoggerFactory.getLogger(API.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public CommonApiResponse GET_newsFeed(
            @CookieParam("vob_common") String userCookie,
            @QueryParam("nextId") String offset,
            @QueryParam("count") int limit) {

        String user_id = AuthUser.getUserID(userCookie);

        if (user_id == null) {
            return StaticResponse.noLoggedInUser;
        }

        System.out.println("user news feed for :" + user_id);

        try {
            APIImpl apiImpl = new APIImpl();

            return apiImpl._getNewsFeedVobs(user_id, offset, limit);

        } catch (ServiceException e) {
            LOGGER.error(String.format("GET_getFollowers : %s", user_id), e);
            return StaticResponse.noLoggedInUser;
        }
//
//		VobListResponse vobs = new VobListResponse();
//		vobs.vobList = newsFeed;
//		vobs.count = vobs.vobList.size();
//
//		return vobs;
    }

}
