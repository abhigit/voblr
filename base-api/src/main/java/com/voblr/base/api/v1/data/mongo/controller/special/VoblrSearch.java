package com.voblr.base.api.v1.data.mongo.controller.special;

import com.voblr.base.api.v1.common.APIImpl;
import com.voblr.base.api.v1.common.VoblrMongDB;
import com.voblr.base.api.v1.common.VoblrMySql;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.user.User;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.services.MovieSearch;
import com.voblr.base.api.v1.data.mongo.services.UserSearch;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import java.util.List;

/**
 * Abhijeet Parande
 * Date: 3/12/13
 * Time: 11:29 PM
 * (c) Voblr, Inc
 */
@Path("/v1/special")
public class VoblrSearch {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/userReset")
    public CommonApiResponse resetAllUsers(@QueryParam("secret") String secret) throws Exception {
        if (secret == null || !secret.equals("reset")) {
            return StaticResponse.failed;
        }
        UserSearch userSearch = UserSearch.INSTANCE();
        userSearch.getSolrServer().deleteByQuery("*:*");
        Thread.sleep(10 * 1000);
        for (User user : VoblrMySql.getInstance().getAllMySqlUsers()) {
            userSearch.add(user, false);
//            new APIImpl()._doCreate(user.getUserId(), user.getName(), user.getPicURL());
        }
        userSearch.getSolrServer().commit();

        return StaticResponse.success;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/moviePublish")
    public CommonApiResponse resetAllMovies(@QueryParam("secret") String secret) throws Exception {
        if (secret == null || !secret.equals("reset")) {
            return StaticResponse.failed;
        }
//        MovieSearch movieSearch = MovieSearch.INSTANCE();
////        movieSearch.getSolrServer().deleteByQuery("*:*");
////        Thread.sleep(10 * 1000);
//        for (MovieBasicInfo movieBasicInfo : VoblrMongDB.getAllMovies()) {
//            movieSearch.add(movieBasicInfo, false);
//            System.out.println(movieBasicInfo.getName()+" "+movieBasicInfo.getNoOfVobs());
//        }
//        movieSearch.getSolrServer().commit();

        return StaticResponse.success;
    }


    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/moviePublissh")
    public CommonApiResponse resetAllus(@QueryParam("secret") String secret) throws Exception {
        if (secret == null || !secret.equals("reset")) {
            return StaticResponse.failed;
        }
//        MovieSearch movieSearch = MovieSearch.INSTANCE();
////        movieSearch.getSolrServer().deleteByQuery("*:*");
////        Thread.sleep(10 * 1000);
//        List<User> userList = VoblrMongDB.getAllUsers();
//        System.out.println(userList.size());
//        for (User movieBasicInfo :userList ) {
//            if(movieBasicInfo.getUserId().equals("73")){
//                movieBasicInfo.setName("కిషోర్ సీపాన");
//                MongoConnector.ds.save(movieBasicInfo);
//            }
//            System.out.println(movieBasicInfo.getUserId()+ " "+movieBasicInfo.getName()+" "+movieBasicInfo.getNoOfVobs());
//        }

        return StaticResponse.success;
    }

    public static void main(String[] afs) throws Exception {
//        System.setProperty("SolrHost", "localhost");
//        CommonApiResponse response1 = new VoblrSearch().resetAllUsers("reset");
//        System.out.println(response1.message);

        CommonApiResponse response2 = new VoblrSearch().resetAllUsers("reset");
        System.out.println(response2.message);
        System.exit(0);
    }

}
