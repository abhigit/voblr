/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import com.voblr.base.api.v1.data.mongo.model.vob.VobData;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author vinit
 * Update Time : Nov 22, 2012 : 11:53:50 PM
 * TODO
 */
@XmlRootElement
public class VobListResponse extends CommonApiResponse{

	public List<VobData> vobList;
	public int count;
    public String nextId;
    public boolean isFollowing = false;

	public VobListResponse() {
		super(ResponseCode.SUCCESS, "SUCCESS");
		vobList = null;
	}


}
