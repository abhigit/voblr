/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author vinit
 * Update Time : Nov 2, 2012 : 12:07:43 AM
 * TODO
 */
@XmlRootElement
public class CommonApiResponse {
	public int status;
    public String message;

	public CommonApiResponse(){}
	
	public CommonApiResponse(int status, String string) {
		this.status = status;
		this.message=string;
	}

}
