package com.voblr.base.api.v1.data.mongo.model.user;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.NotSaved;
import com.voblr.base.api.v1.data.mongo.services.UserService;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;

import javax.xml.bind.annotation.XmlRootElement;

//@Entity("UserBasicInfo")
@XmlRootElement
@Entity(value = "UserBasicInfo", noClassnameStored = true)
public class UserBasicInfo {
    @Id
    @Field("user_id")
    String userId;
    @Field("user_name")
    String name;
    @Field("user_picurl")
    String picURL;
    @NotSaved
    @Field("_createdTimeStamp")
    public String _createdTimeStamp;
    @NotSaved
    @Field("_updatedTimeStamp")
    public String _updatedTimeStamp;

    public UserBasicInfo() {
    }

    /**
     * @param userId2
     * @param name2
     * @param picUrl2
     */
    public UserBasicInfo(String userId2, String name2, String picUrl2) {
        this.userId = userId2;
        this.name = name2;
        this.picURL = picUrl2;
    }

    /**
     * @return the userId
     */
    public String getUserId() {
        return userId;
    }

    /**
     * @param userId the userId to set
     */
    public void setUserId(String userId) {
        this.userId = userId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the picURL
     */
    public String getPicURL() {
        return picURL;
    }

    /**
     * @param picURL the picURL to set
     */
    public void setPicURL(String picURL) {
        this.picURL = picURL;
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
