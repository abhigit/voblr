/**
 *
 */
package com.voblr.base.api.v1.data.mongo.controller;

import com.google.code.morphia.query.Query;
import com.google.protobuf.ServiceException;
import com.sun.jersey.api.json.JSONWithPadding;
import com.voblr.base.api.v1.common.APIImpl;
import com.voblr.base.api.v1.common.AuthUser;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.user.User;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.base.api.v1.data.mongo.response.UserInfoResponse;
import com.voblr.base.api.v1.data.mongo.response.UserListResponse;
import com.voblr.base.api.v1.data.mongo.services.UserSearch;
import com.voblr.base.api.v1.data.mongo.services.UserService;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author vinit Update Time : Nov 4, 2012 : 7:02:46 PM TODO
 */

@Path("/v1/users")
public class UserInfoController {
    //    @GET
//   @Produces("application/javascript")
    public JSONWithPadding getUserListResponse(
            @QueryParam("callback") String callback,
            @CookieParam("vob_common") String cookie) {

        System.out.println("cookie : " + cookie);
        System.out.println("Userid : " + AuthUser.getUserID(cookie));
        System.out.println("User List called with callback");
        return new JSONWithPadding(getUserList(AuthUser.getUserID(cookie)),
                callback);
    }

    @GET
    @Produces("application/javascript")
    public JSONWithPadding getUserList(
            @QueryParam("q") String q,
            @QueryParam("nextId") int start,
            @QueryParam("count") int count,
            @CookieParam("vob_common") String cookie,
            @QueryParam("callback") String callback) {

        System.out.println("User search called with q=" + q + " nextId=" + start + " count=" + count);
        if (q == null || "".equals(q.trim()))
            return new JSONWithPadding(StaticResponse.InvalidRquest, callback);
        else {
            try {
                String forUserId = null;
                if (cookie != null) {
                    forUserId = AuthUser.getUserID(cookie);
                }
                return new JSONWithPadding(UserSearch.INSTANCE().query(forUserId, q, start, count), callback);
            } catch (Exception e) {
                e.printStackTrace();
                return new JSONWithPadding(StaticResponse.failed, callback);
            }
        }
    }

    public UserListResponse getUserList(String userId) {
        UserListResponse userList = new UserListResponse();
        userList.setUserList(UserService.getRandomUserList());
        System.out.println("User List called without callback");

        if (userId != null) {
            APIImpl apiImpl = new APIImpl();
            try {
                UserListResponse followers = (UserListResponse) apiImpl
                        ._getFollowing(userId, "$start", 0);

                for (User u : userList.getUserList()) {
                    for (User u1 : followers.getUserList()) {

                        // System.out.println("user list id : " + u.getUserId()
                        // + " followers list : " + u1.user_id + ".");
                        if (u.getUserId().equals(u1.getUserId())) {
                            u.isFollowing = true;
                        } else {
                            // System.out.println("user list id : " +
                            // u.getUserId() + "   followers list : " +
                            // u1.user_id);
                        }
                    }
                }
            } catch (ServiceException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

        return userList;
    }

    @GET
    @Produces("application/javascript")
    @Path("/{user_id}")
    public JSONWithPadding getUserInfo(@PathParam("user_id") String userId,
                                       @QueryParam("callback") String callback) {

        System.out.println("Got a request for user details for : " + userId);
        UserService us = new UserService();
        User user = us.getUserById(userId);
        if (user != null) {
            UserInfoResponse userInfo = new UserInfoResponse();
            userInfo.setUser(user);
            return new JSONWithPadding(userInfo, callback);

        } else {
            return new JSONWithPadding(StaticResponse.noUserInfo, callback);
        }
    }

    // @POST
    // @Produces("application/json")
    // @Path("{userId}")
    public CommonApiResponse setUserInfo(@PathParam("userId") String userId,
                                         @QueryParam("name") String name,
                                         @QueryParam("picURL") String picURL) {

        UserService us = new UserService();
        System.out.println("Userid : " + userId);
        System.out.println("Name : " + name);
        System.out.println("picURL : " + picURL);
        us.setUserInfo(userId, name, picURL);
        // add it to Solr for search.
        try {
            UserSearch.INSTANCE().add(UserService.getUserById(userId));
        } catch (Exception e) {
            System.out.println("Error during user add to search, userId=" + userId + ", error=" + e.getMessage());
        }
        return StaticResponse.success;
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/stats/{user_id}")
    public CommonApiResponse GET_UserMovieStats(
            @PathParam("user_id") String userId) {

        return UserService.getMoviesWatched(userId);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/stats")
    public CommonApiResponse GET_SelfMovieStats(
            @CookieParam("vob_common") String userCookie) {

        String userId = AuthUser.getUserID(userCookie);

        if (userId == null) {
            return StaticResponse.noLoggedInUser;
        }

        return GET_UserMovieStats(userId);
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/vobs/votedup/{user_id}")
    public CommonApiResponse GET_userVotedUpVobs(
            @PathParam("user_id") String vobs_of_user,
            @CookieParam("vob_common") String userCookie,
            @QueryParam("nextId") String offset,
            @QueryParam("count") int limit) {

        long start = System.nanoTime();
        String logged_in_user = userCookie == null ? null : AuthUser.getUserID(userCookie);
        System.out.println("user authentication "
                + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                + " ms");
        long start1 = System.nanoTime();
        try {
            APIImpl apiImpl = new APIImpl();
            if (logged_in_user == null) {
                return apiImpl._getUserVotedUpVobs(vobs_of_user, offset, limit);
            } else {
                return apiImpl._getUserVotedUpVobs(vobs_of_user, logged_in_user,
                        offset, limit);
            }

        } catch (ServiceException e) {
            System.out.println(String.format("GET_userVotedUpVobs : %s", vobs_of_user) + e);
            return StaticResponse.noMovieInfo;
        } finally {
            System.out.println("votedUpVobs  "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start1)
                    + " ms");
            System.out.println("Total "
                    + TimeUnit.NANOSECONDS.toMillis(System.nanoTime() - start)
                    + " ms");
        }
    }

    public static void main(String[] args) throws Exception {
        //IMP test locally only
        System.setProperty("SolrHost", "localhost");

        Query<UserBasicInfo> query = MongoConnector.ds.createQuery(UserBasicInfo.class);
        List<UserBasicInfo> list = query.asList();
        Collections.shuffle(list);
        UserSearch userSearch = UserSearch.INSTANCE();
        UserBasicInfo a = list.remove(0);
        for (UserBasicInfo u : list) {

            userSearch.add(u);
//            for (User i : userSearch.query(null, u.getName().split("\\s+")[0], 0, 10).getUserList()) {
//                System.out.println(i.getName());
//            }

            Thread.sleep(100);
        }
        userSearch.add(a);

    }


}
