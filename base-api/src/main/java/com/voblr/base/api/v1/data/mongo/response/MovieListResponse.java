/**
 *
 */
package com.voblr.base.api.v1.data.mongo.response;

import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * @author vinit
 *         Update Time : Nov 3, 2012 : 12:36:46 AM
 *         TODO
 */

@XmlRootElement
public class MovieListResponse extends CommonApiResponse {

    private List<MovieBasicInfo> movieList;
    private int count;
    private String nextId;

    /**
     *
     */
    public MovieListResponse() {
        super(ResponseCode.SUCCESS, "SUCCESS");
        movieList = null;
    }

    /**
     * @return the movieList
     */
    public List<MovieBasicInfo> getMovieList() {
        return movieList;
    }

    /**
     * @param movieList the movieList to set
     */
    public void setMovieList(List<MovieBasicInfo> movieList) {
        this.movieList = movieList;
        this.count = movieList.size();
    }

    /**
     * @return the count
     */
    public int getCount() {
        return count;
    }

    /**
     * @param count the count to set
     */
    public void setCount(int count) {
        this.count = count;
    }

    /**
     * @return the nextId
     */
    public String getNextId() {
        return nextId;
    }

    /**
     * @param next the nextId to set
     */

    public void setNextId(String next) {
        this.nextId = String.valueOf(next);
    }


}
