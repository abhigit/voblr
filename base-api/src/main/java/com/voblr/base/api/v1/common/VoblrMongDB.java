package com.voblr.base.api.v1.common;

import com.google.code.morphia.query.Query;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.user.User;

import java.util.List;

/**
 * Abhijeet Parande
 * Date: 3/12/13
 * Time: 11:21 PM
 * (c) Voblr, Inc
 */
public class VoblrMongDB {

    public static List<MovieBasicInfo> getAllMovies() {
        Query<MovieBasicInfo> q = MongoConnector.ds
                .createQuery(MovieBasicInfo.class);
        List<MovieBasicInfo> list = q.asList();
        if(list.isEmpty()) throw new RuntimeException("MovieList isEmpty");
        for(MovieBasicInfo movie : list){
            movie._createdTimeStamp = "NOW";
            movie._updatedTimeStamp = "NOW";
        }
        return list;
    }

    public static List<User> getAllUsers() {
        Query<User> q = MongoConnector.ds
                .createQuery(User.class);
        List<User> list = q.asList();
        if(list.isEmpty()) throw new RuntimeException("MovieList isEmpty");
        for(User movie : list){
            movie._createdTimeStamp = "NOW";
            movie._updatedTimeStamp = "NOW";
        }
        return list;
    }
}
