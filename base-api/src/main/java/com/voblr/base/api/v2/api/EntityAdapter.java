package com.voblr.base.api.v2.api;

/**
 * Abhijeet Parande
 * Date: 3/18/13
 * Time: 2:20 AM
 * (c) Voblr, Inc
 */
public interface EntityAdapter<T, V, TT, VV> {
    T underlyingEntity(V some);

    TT underlyingEntities(VV some);
}
