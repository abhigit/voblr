package com.voblr.base.api.v1.data.mongo.model.vob;

import com.google.code.morphia.annotations.*;
import com.google.code.morphia.utils.IndexDirection;
import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;
import com.voblr.base.api.v1.data.mongo.services.MovieService;
import com.voblr.base.api.v1.data.mongo.services.UserService;
import com.voblr.common.proto.Vob;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * @author vinit Update Time : Sep 2, 2012 : 9:21:32 PM
 */
@XmlRootElement
@Entity(value = "VobData", noClassnameStored = true)
public class VobData implements Comparable {
	@Indexed
	@Id
	String vobId;
	@Indexed
	Date timestamp;

	@Indexed(value = IndexDirection.DESC)
	Date latestActivityTimestamp;

	@Reference
	UserBasicInfo user;
	@Reference
	MovieBasicInfo movie;



	String vobText;

	int noOfLikes;
	int noOfComments;
	double popularityIndex;

	public int voteUpCount;
	public int voteDownCount;

	@NotSaved
	public boolean userVotedDown = false;

	@NotSaved
	public boolean userVotedUp = false;

	@NotSaved
    public VobComments vobComments;

	@NotSaved
	public static final String VOTE_UP_COUNT = "voteUpCount";
	@NotSaved
	public static final String VOTE_DOWN_COUNT = "voteDownCount";
	@NotSaved
	public static final String NO_OF_COMMENTS = "noOfComments";
	@NotSaved
	public static final String POPULARITY_INDEX = "popularityIndex";

	public VobData() {
	}

	public VobData(Vob.VobProto vobProto) {
		this.vobId = vobProto.getVobId();
		this.timestamp = new Date(vobProto.getCreatedTs());
		this.latestActivityTimestamp = new Date(vobProto.getLatestActivityTs());
		// this.user = new UserBasicInfo(vobProto.getSubjectId());
		// this.movie = new MovieBasicInfo(vobProto.getObjectId());
		this.user = MongoConnector.getSingleObject(UserBasicInfo.class,
				"userId", vobProto.getSubjectId());
		this.movie = MongoConnector.getSingleObject(MovieBasicInfo.class,
				"movieId", vobProto.getObjectId());
		this.vobText = vobProto.getVobText();
		this.noOfLikes = vobProto.getLikeCount();
		voteUpCount = vobProto.getVoteUpCount();
		voteDownCount = vobProto.getVoteDownCount();
		userVotedDown = vobProto.getUserVotedDown();
		userVotedUp = vobProto.getUserVotedUp();
		this.popularityIndex = vobProto.getPopularityIdx();
	}

	public VobData(String userId, String movieId, String vobText, Date timestamp) {
		this.vobId = generateVobId(userId, movieId, timestamp);
		this.timestamp = timestamp;
		this.latestActivityTimestamp = timestamp;
		this.user = UserService.getUserById(userId);
		this.movie = MovieService.getMovieById(movieId);
		this.vobText = vobText;
		noOfLikes = 0;
		noOfComments = 0;
		this.popularityIndex = calculatePopularityIndex(vobText);
		voteDownCount = 0;
		voteUpCount = 0;
	}

	/**
	 * @param vobText
	 * @return
	 */
	private int calculatePopularityIndex(String vobText) {
		int popularity = 0;
		if (vobText.length() == 0)
			popularity = 1;
		else if (vobText.length() < 20)
			popularity = 2;
		else
			popularity = 3;

		return popularity;
	}

	/**
	 * @param userId
	 * @param movieId
	 * @param timestamp
	 * @return
	 */
	public static String generateVobId(String userId, String movieId,
			Date timestamp) {
		return movieId + "_" + userId + "_" + timestamp.getTime();
	}

	/**
	 * @return the vobId
	 */
	public String getVobId() {
		return vobId;
	}

	/**
	 * @param vobId
	 *            the vobId to set
	 */
	public void setVobId(String vobId) {
		this.vobId = vobId;
	}

	/**
	 * @return the timestamp
	 */
	public Date getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the latestActivityTimestamp
	 */
	public Date getLatestActivityTimestamp() {
		return latestActivityTimestamp;
	}

	/**
	 * @param latestActivityTimestamp
	 *            the latestActivityTimestamp to set
	 */
	public void setLatestActivityTimestamp(Date latestActivityTimestamp) {
		this.latestActivityTimestamp = latestActivityTimestamp;
	}

	/**
	 * @return the user
	 */
	public UserBasicInfo getUser() {
		return user;
	}

	/**
	 * @param user
	 *            the user to set
	 */
	public void setUser(UserBasicInfo user) {
		this.user = user;
	}

	/**
	 * @return the movie
	 */
	public MovieBasicInfo getMovie() {
		return movie;
	}

	/**
	 * @param movie
	 *            the movie to set
	 */
	public void setMovie(MovieBasicInfo movie) {
		this.movie = movie;
	}

	/**
	 * @return the vobText
	 */
	public String getVobText() {
		return vobText;
	}

	/**
	 * @param vobText
	 *            the vobText to set
	 */
	public void setVobText(String vobText) {
		this.vobText = vobText;
	}

	/**
	 * @return the noOfLikes
	 */
	public int getNoOfLikes() {
		return noOfLikes;
	}

	/**
	 * @param noOfLikes
	 *            the noOfLikes to set
	 */
	public void setNoOfLikes(int noOfLikes) {
		this.noOfLikes = noOfLikes;
	}

	/**
	 * @return the noOfComments
	 */
	public int getNoOfComments() {
		return noOfComments;
	}

	/**
	 * @param noOfComments
	 *            the noOfComments to set
	 */
	public void setNoOfComments(int noOfComments) {
		this.noOfComments = noOfComments;
	}

	/**
	 * @return the popularityIndex
	 */
	public double getPopularityIndex() {
		return popularityIndex;
	}

	/**
	 * @param popularityIndex
	 *            the popularityIndex to set
	 */
	public void setPopularityIndex(double popularityIndex) {
		this.popularityIndex = popularityIndex;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}

	@Override
	public int compareTo(Object o) {
		VobData p = (VobData) o;

		return -this.timestamp.compareTo(p.timestamp);
	}

}
