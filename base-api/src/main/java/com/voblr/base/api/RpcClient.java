/**
 * Date : Jul 29, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.base.api;

import com.google.protobuf.RpcController;
import com.googlecode.protobuf.pro.duplex.PeerInfo;
import com.googlecode.protobuf.pro.duplex.RpcClientChannel;
import com.googlecode.protobuf.pro.duplex.client.DuplexTcpClientBootstrap;
import com.googlecode.protobuf.pro.duplex.execute.ThreadPoolCallExecutor;
import com.googlecode.protobuf.pro.duplex.listener.TcpConnectionEventListener;
import org.jboss.netty.channel.socket.nio.NioClientSocketChannelFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

/**
 * @author abhi@voblr.com
 *         <p/>
 *         This class implements a RPC client over google's protobuf.
 */
public class RpcClient {

    public static final RpcClient rpcClient = new RpcClient();

    private PeerInfo client = new PeerInfo(System.getProperty("client",
            "0.0.0.0"), 10234);
    private PeerInfo server = new PeerInfo(System.getProperty("server",
            "graph.voblr.com"), 7040);

    private static int reconnTryInterval = Integer.parseInt(System.getProperty(
            "retryInterval", "5"));

    private static int reconnTries = Integer.parseInt(System.getProperty(
            "retryAttempts", "-1"));

    private ThreadPoolCallExecutor executor = new ThreadPoolCallExecutor(3, 10);

    private DuplexTcpClientBootstrap bootstrap = new DuplexTcpClientBootstrap(
            client, new NioClientSocketChannelFactory(
            Executors.newCachedThreadPool(),
            Executors.newCachedThreadPool()), executor);
    private RpcClientChannel channel;

    private RpcClient() {

        /**
         * Added retry logic for peering with server
         */
        List<TcpConnectionEventListener> list = new ArrayList<TcpConnectionEventListener>();
        list.add(new TcpConnectionEventListener() {

            @Override
            public void connectionOpened(RpcClientChannel clientChannel) {
                System.err.println("Connected" + clientChannel);
            }

            @Override
            public void connectionClosed(RpcClientChannel clientChannel) {
                System.err.println("Closed " + clientChannel);
                new Thread(new Runnable() {
                    int wait = reconnTryInterval; // every 5 secs
                    int count = reconnTries;

                    @Override
                    public void run() {
                        while (true) {
                            try {
                                channel = bootstrap.peerWith(server);
                                break;
                            } catch (IOException e) {
                                System.err.println("Closed "
                                        + e.getMessage());
                            }
                            try {
                                Thread.sleep(wait * 1000);
                                if (count < 0)
                                    continue;
                                if (--count == 0)
                                    break;
                            } catch (InterruptedException e1) {
                            }
                        }
                    }
                }).start();

            }
        });
        bootstrap.setConnectionEventListeners(list);
        try {
            channel = bootstrap.peerWith(server);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static synchronized RpcClient getInstance() {
        return rpcClient;
    }

    public static RpcClientChannel getClientChannel() {
        return getInstance().channel;
    }

    public static RpcController getController() {
        return getClientChannel().newRpcController();
    }

}
