package com.voblr.base.api.v1.data.mongo.services;

import com.google.protobuf.ServiceException;
import com.voblr.base.api.v1.common.APIImpl;
import com.voblr.base.api.v1.data.mongo.model.user.User;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;
import com.voblr.base.api.v1.data.mongo.response.UserListResponse;
import com.voblr.common.GraphServices;
import com.voblr.common.constants.GraphConstants;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Abhijeet Parande
 * Date: 2/15/13
 * Time: 10:04 PM
 * (c) Voblr, Inc
 */
public class UserSearch {

    private int queueSize = Integer.parseInt(System.getProperty("SolrQSize", "100"));
    private int nThreads = Integer.parseInt(System.getProperty("SolrNThreads", "10"));
    private String serverUrl = "http://" + System.getProperty("SolrHost", "graph.voblr.com") + ":8983/solr/users";
    private SolrServer solr;

    private static UserSearch movieSearch;

    public static UserSearch INSTANCE() throws MalformedURLException {
        if (movieSearch == null) {
            synchronized (UserSearch.class) {
                if (movieSearch == null)
                    movieSearch = new UserSearch();
            }
        }
        return movieSearch;
    }

    public SolrServer getSolrServer() throws MalformedURLException {
        return solr;
    }

    private UserSearch() throws MalformedURLException {
        solr = new ConcurrentUpdateSolrServer(serverUrl, queueSize,
                nThreads);
    }


    public UserListResponse query(String forUserId, String q, int start, int rows) throws Exception {
        SolrQuery query = new SolrQuery();

        StringBuffer b = new StringBuffer();

        String[] arr = q.split("\\s+");

        b.append(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            b.append(' ').append(arr[i]);
        }
        query.setQuery(b.toString());
        query.setStart(start);
        query.setRows(rows);
        query.setSortField("_createdTimeStamp", SolrQuery.ORDER.desc);
        QueryResponse rsp = solr.query(query);
        SolrDocumentList docs = rsp.getResults();
//            System.out.println(docs);
        long found = docs.getNumFound();
        UserListResponse resp = new UserListResponse();
        if (found > (start + rows)) {
            resp.setNextId(String.valueOf(start + rows));
        }
        List<User> list = null;
        GraphServices.Entities.Builder entities_bldr = GraphServices.Entities.newBuilder();
        if (forUserId != null) {
            Iterable<List<Boolean>> resp1 = null;
            List<String> uids = new ArrayList<String>();
            for (SolrDocument doc : docs) {
                GraphServices.Entity.Builder entity_builder =
                        GraphServices.Entity.newBuilder();
                entity_builder.setEntityId((String) doc.getFieldValue("user_id"))
                        .setEntityType(GraphServices.EntityType.USER);
                entities_bldr.addEntities(entity_builder.build());

                uids.add((String) doc.getFieldValue("user_id"));
            }
            List<GraphServices.DirectedRelationship> rels = new ArrayList<GraphServices.DirectedRelationship>();
            rels.add(GraphServices.DirectedRelationship.newBuilder()
                    .setDir(GraphServices.Direction.OUT)
                    .setRelType(GraphConstants.FOLLOWS).build());
            try {
                resp1 = new APIImpl()
                        .getCompositeResponse(forUserId, GraphServices.EntityType.USER,
                                uids, GraphServices.EntityType.USER, rels
                        );
            } catch (ServiceException e) {
                e.printStackTrace();
                throw new RuntimeException(e);
            }
            list = new APIImpl().getUsers(entities_bldr.build());
            List<Boolean> followsRel = resp1.iterator().next();
            for (int i = 0 ; i < list.size(); i++) {
                list.get(i).isFollowing = followsRel.get(i);
            }
        } else {
            for (SolrDocument doc : docs) {
                GraphServices.Entity.Builder entity_builder =
                        GraphServices.Entity.newBuilder();
                entity_builder.setEntityId((String) doc.getFieldValue("user_id"))
                        .setEntityType(GraphServices.EntityType.USER);
                entities_bldr.addEntities(entity_builder.build());
            }
            list = new APIImpl().getUsers(entities_bldr.build());
        }
        resp.setUserList(list);
        return resp;
    }

    public void add(UserBasicInfo user) throws IOException, SolrServerException {
        add(user, true);
    }

    public void add(UserBasicInfo user, boolean commit) throws IOException, SolrServerException {
        solr.addBean(user);
        if (commit) solr.commit();
    }
}
