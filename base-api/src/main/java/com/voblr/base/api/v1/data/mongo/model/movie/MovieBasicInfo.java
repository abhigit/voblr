package com.voblr.base.api.v1.data.mongo.model.movie;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Indexed;
import com.google.code.morphia.annotations.NotSaved;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.solr.client.solrj.beans.Field;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@XmlRootElement
@Entity(value = "MovieBasicInfo", noClassnameStored = true)
public class MovieBasicInfo {
    @Indexed
    @Id
    @Field("movie_id")
    String movieId;
    @Field("movie_name")
    String name;
    @Field("movie_desc")
    String description;
    @Field("movie_pic_url")
    List<String> picUrl;
    @Indexed
    @Field
    Date releaseDate;
    @Field
    String cast;
    @Field
    String directors;
    @NotSaved
    @Field("_createdTimeStamp")
    public String _createdTimeStamp;
    @NotSaved
    @Field("_updatedTimeStamp")
    public String _updatedTimeStamp;

    public List<String> getUrls() {
        return urls;
    }

    public String getLang() {
        return lang;
    }

    public List<String> getPicUrl() {
        return picUrl;
    }

    String lang;

    public void setUrls(List<String> urls) {
        this.urls = urls;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    List<String> urls = new ArrayList<String>(2);

    int noOfVobs;

    public static final String NO_OF_VOBS = "noOfVobs";

//	List <String> genres;


    public MovieBasicInfo() {
    }
    /**
     * @return the movieId
     */
    public String getMovieId() {
        return movieId;
    }

    /**
     * @param movieId the movieId to set
     */
    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param description the name to set
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }


    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }



    public void setPicUrl(List<String> picUrl) {
        this.picUrl = picUrl;
    }

    /**
     * @return the releaseDate
     */
    public Date getReleaseDate() {
        return releaseDate;
    }

    /**
     * @param releaseDate the releaseDate to set
     */
    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    /**
     * @return the cast
     */
    public String getCast() {
        return cast;
    }

    /**
     * @param cast the cast to set
     */
    public void setCast(String cast) {
        this.cast = cast;
    }

    public void setCast(List<String> cast) {
        boolean f = false;
        for (String person : cast) {
            if (f) {
                this.cast += ", ";
            }
            this.cast += person;
            f = true;
        }
    }

    /**
     * @return the directors
     */
    public String getDirectors() {
        return directors;
    }

    /**
     * @param directors the directors to set
     */
    public void setDirectors(String directors) {
        this.directors = directors;
    }

    public void setDirectors(List<String> directors) {
        boolean f = false;
        for (String director : directors) {
            if (f) {
                this.directors += ", ";
            }
            this.directors += directors;
            f = true;
        }
    }

    /**
     * @return the noOfVobs
     */
    public int getNoOfVobs() {
        return noOfVobs;
    }

    /**
     * @param noOfVobs the noOfVobs to set
     */
    public void setNoOfVobs(int noOfVobs) {
        this.noOfVobs = noOfVobs;
    }


    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }

}
