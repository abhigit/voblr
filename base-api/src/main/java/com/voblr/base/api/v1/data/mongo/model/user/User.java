package com.voblr.base.api.v1.data.mongo.model.user;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.NotSaved;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;

//@Entity("User")
@XmlRootElement
@Entity(value = "User", noClassnameStored = true)
public class User extends UserBasicInfo {
	// @Id String userId;
	// String name;
	// String picUrl;

	public static final String NO_OF_FOLLOWERS = "noOfFollowers";
	public static final String NO_OF_FOLLOWING = "noOfFollowing";
	public static final String NO_OF_VOBS = "noOfVobs";

	int noOfFollowers;
	int noOfFollowing;
	int noOfVobs;

    public long reputation;

	@NotSaved
	public boolean isFollowing = false;
	
    // required for constructing newsfeed
	@NotSaved
    public long recent_activity_ts;

	
	public User() {
		super();
	}

	public User(String userId) {
		super(userId, null, null);
	}

	public User(String userId, String name, String picUrl) {
		super(userId, name, picUrl);
		noOfFollowers = 0;
		noOfFollowing = 0;
		noOfVobs = 0;
	}

	public User(UserBasicInfo userBasicInfo) {
		super(userBasicInfo.userId, userBasicInfo.name, userBasicInfo.picURL);
		noOfFollowers = 0;
		noOfFollowing = 0;
		noOfVobs = 0;
	}

	
//    public User(com.voblr.common.proto.User.UserProto user) {
//		super(userBasicInfo.userId, userBasicInfo.name, userBasicInfo.picURL);
//		noOfFollowers = 0;
//		noOfFollowing = 0;
//		noOfVobs = 0;
//		this.userId = userId;
//		User u = UserService.getUserById(userId);
//		if (u == null) {
//			
//		} else {
//			this.name = u.getName();
//			this.picURL = u.getPicURL();
//		}
//        this.recent_activity_ts = user.getRecentActivityTs();
//    }

	/**
	 * @return the noOfFollowers
	 */
	public int getNoOfFollowers() {
		return noOfFollowers;
	}

	/**
	 * @param noOfFollowers
	 *            the noOfFollowers to set
	 */
	public void setNoOfFollowers(int noOfFollowers) {
		this.noOfFollowers = noOfFollowers;
	}

	/**
	 * @return the noOfFollowing
	 */
	public int getNoOfFollowing() {
		return noOfFollowing;
	}

	/**
	 * @param noOfFollowing
	 *            the noOfFollowing to set
	 */
	public void setNoOfFollowing(int noOfFollowing) {
		this.noOfFollowing = noOfFollowing;
	}

	/**
	 * @return the noOfVobs
	 */
	public int getNoOfVobs() {
		return noOfVobs;
	}

	/**
	 * @param noOfVobs
	 *            the noOfVobs to set
	 */
	public void setNoOfVobs(int noOfVobs) {
		this.noOfVobs = noOfVobs;
	}

	@Override
	public int hashCode() {
		return HashCodeBuilder.reflectionHashCode(this);
	}

	@Override
	public boolean equals(Object obj) {
		return EqualsBuilder.reflectionEquals(this, obj);
	}

	@Override
	public String toString() {
		return ToStringBuilder.reflectionToString(this);
	}
}
