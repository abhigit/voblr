/**
 *
 */
package com.voblr.base.api.v1.data.mongo.model.vob;

import com.google.code.morphia.annotations.Embedded;
import com.google.code.morphia.annotations.Reference;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;
import com.voblr.base.api.v1.data.mongo.services.UserService;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * @author vinit
 *         Update Time : Sep 17, 2012 : 12:11:04 AM
 *         TODO
 */
@XmlRootElement
@Embedded
public class Comment {
    Date timestamp;
    @Reference
    UserBasicInfo user;
    String commentText;
    String commentId;

    public Comment() {
    }

    /**
     * @param timestamp
     * @param user
     * @param commentText
     */
    public Comment(Date timestamp, UserBasicInfo user, String commentText) {
        this.timestamp = timestamp;
        this.user = user;
        this.commentText = commentText;
    }

    public Comment(Date timestamp, String userId, String commentText) {
        this(timestamp, UserService.getUserById(userId), commentText);
    }

    public Comment(com.voblr.common.proto.Comment.CommentProto commentProto) {
        this(new Date(commentProto.getCreatedTime()), UserService.getUserById(commentProto.getFrom()), commentProto.getMessage());
        commentId = commentProto.getCommentId();
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(Date timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return the user
     */
    public UserBasicInfo getUser() {
        return user;
    }

    /**
     * @param user the user to set
     */
    public void setUser(UserBasicInfo user) {
        this.user = user;
    }

    /**
     * @return the commentText
     */
    public String getCommentText() {
        return commentText;
    }

    /**
     * @param commentText the commentText to set
     */
    public void setCommentText(String commentText) {
        this.commentText = commentText;
    }


}
