/**
 * Date : Jul 29, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.base.api;

import com.sun.jersey.api.container.grizzly2.GrizzlyServerFactory;
import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.glassfish.grizzly.http.server.HttpServer;

import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;

/**
 * @author abhi@voblr.com
 */
public class BaseAPIMain {
    public static RpcClient client;

    private static URI getBaseURI() {
        String host = System.getProperty("client", "0.0.0.0");
        int port = Integer.parseInt(System.getProperty("client_port", "9998"));
        return UriBuilder
                .fromUri("http://" + host + "/").port(port).build();
    }

    {
        System.setProperty("org.apache.commons.logging.Log",
                "org.apache.commons.logging.impl.NoOpLog");
    }

    public static final URI BASE_URI = getBaseURI();

    protected static HttpServer startServer() throws IOException {
        System.out.println("Starting grizzly...");

        ResourceConfig rc = new PackagesResourceConfig("com.voblr.base.api");
        client = RpcClient.getInstance();
        return GrizzlyServerFactory.createHttpServer(BASE_URI, rc);
    }

    public static void main(String[] args) {
//        Log log = LogFactory.getLog(CategoryPerServiceLogger.class);

//        System.out.println("info enabled : " + log.isInfoEnabled());
        try {
            startServer();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
