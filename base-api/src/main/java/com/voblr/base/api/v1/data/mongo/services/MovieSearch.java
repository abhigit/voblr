package com.voblr.base.api.v1.data.mongo.services;

import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.response.MovieListResponse;
import org.apache.solr.client.solrj.SolrQuery;
import org.apache.solr.client.solrj.SolrServer;
import org.apache.solr.client.solrj.SolrServerException;
import org.apache.solr.client.solrj.impl.ConcurrentUpdateSolrServer;
import org.apache.solr.client.solrj.response.QueryResponse;
import org.apache.solr.common.SolrDocument;
import org.apache.solr.common.SolrDocumentList;

import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Abhijeet Parande
 * Date: 2/15/13
 * Time: 10:04 PM
 * (c) Voblr, Inc
 */
public class MovieSearch {

    private int queueSize = Integer.parseInt(System.getProperty("SolrQSize", "100"));
    private int nThreads = Integer.parseInt(System.getProperty("SolrNThreads", "10"));
    private String serverUrl = "http://" + System.getProperty("SolrHost", "graph.voblr.com") + ":8983/solr/movies";
    private SolrServer solr;

    private static MovieSearch movieSearch;

    public static MovieSearch INSTANCE() throws MalformedURLException {
        if (movieSearch == null) {
            synchronized (MovieSearch.class) {
                if (movieSearch == null)
                    movieSearch = new MovieSearch();
            }

        }
        return movieSearch;
    }

    public SolrServer getSolrServer() throws MalformedURLException {
        return solr;
    }

    private MovieSearch() throws MalformedURLException {
        solr = new ConcurrentUpdateSolrServer(serverUrl, queueSize,
                nThreads);
    }


    public MovieListResponse query(String q, int start, int rows) throws SolrServerException {
        SolrQuery query = new SolrQuery();

        StringBuffer b = new StringBuffer();

        String[] arr = q.split("\\s+");

        b.append(arr[0]);
        for (int i = 1; i < arr.length; i++) {
            b.append(' ').append(arr[i]);
        }
        query.setQuery(b.toString());
        query.setStart(start);
        query.setRows(rows);
        query.setSortField("_createdTimeStamp", SolrQuery.ORDER.desc);
        QueryResponse rsp = solr.query(query);
        SolrDocumentList docs = rsp.getResults();
//            System.out.println(docs);
        long found = docs.getNumFound();
        MovieListResponse resp = new MovieListResponse();
        if (found > (start + rows)) {
            resp.setNextId(String.valueOf(start + rows));
        }
        List<MovieBasicInfo> list = new ArrayList<MovieBasicInfo>();
        for (SolrDocument doc : docs) {
            MovieBasicInfo movie = MovieService.getMovieById((String) doc.getFieldValue("movie_id"));
            list.add(movie);
        }
        resp.setMovieList(list);
        return resp;
    }

    public void add(MovieBasicInfo movie) throws IOException, SolrServerException {
        add(movie, false);
    }

    public void add(MovieBasicInfo movie, boolean commit) throws IOException, SolrServerException {
        solr.addBean(movie);
        if (commit)
            solr.commit();
    }
}
