package com.voblr.base.api.v1.data.mongo.response;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement
public class MoviesWatchedResponse extends CommonApiResponse {

	public List<UserMovieStat> movieStats;
	public int count;

	/**
	 * 
	 */
	public MoviesWatchedResponse() {
		super(ResponseCode.SUCCESS, "SUCCESS");
		movieStats = new ArrayList<UserMovieStat>();
	}

}
