package com.voblr.base.api.v1.data.mongo.controller;

import com.google.protobuf.ServiceException;
import com.voblr.base.api.v1.common.APIImpl;
import com.voblr.base.api.v1.common.AuthUser;
import com.voblr.base.api.v1.data.mongo.response.CommonApiResponse;
import com.voblr.base.api.v1.data.mongo.response.StaticResponse;
import com.voblr.common.GraphServices;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;

/**
 * @author bothravinit
 */
@Path("/v1/vob/comment")
public class CommentController {

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public CommonApiResponse GET_newsFeed(
            @CookieParam("vob_common") String userCookie,
            @FormParam("vobId") String vobId,
            @FormParam("comment") String commentText) {

        String userId = AuthUser.getUserID(userCookie);

        if (userId == null) {
            return StaticResponse.noLoggedInUser;
        }

        System.out.println("user comment for :" + userId);
        if (commentText == null || commentText.trim().length() == 0)
            return StaticResponse.nullCommentNotAllowed;

        if (vobId == null)
            return StaticResponse.vobIdNull;

        try {
            APIImpl apiImpl = new APIImpl();
            apiImpl._doComment(userId, vobId, GraphServices.EntityType.VOB_ENTITY.name(), commentText);
        } catch (ServiceException e) {
            return StaticResponse.noUserInfo;
        }

        return StaticResponse.success;

    }
}
