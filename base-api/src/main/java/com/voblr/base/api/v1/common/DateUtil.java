package com.voblr.base.api.v1.common;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Abhijeet Parande
 * Date: 3/13/13
 * Time: 12:42 AM
 * (c) Voblr, Inc
 */
public class DateUtil {


    public static String toSolr(String mysqlDate) throws ParseException {
        Date d = mysqlFormat.parse(mysqlDate) ;
        return solrFormat.format(d);
    }
    public static String toMongo(Date d) throws ParseException{
        return mongoFormat.format(d);
    }
    public static Date dateFromMongoString(String date) throws ParseException {
        return  mongoFormat.parse(date);
    }

    static SimpleDateFormat mysqlFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    static SimpleDateFormat mongoFormat = new SimpleDateFormat("yyyy-MM-dd");
    static SimpleDateFormat solrFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
}
