/**
 * 
 */
package com.voblr.base.api.v1.data.mongo.response;

import org.jboss.netty.handler.codec.http.HttpResponseStatus;

/**
 * @author vinit Update Time : Nov 4, 2012 : 6:41:22 PM TODO
 */

public class StaticResponse {

	public static final CommonApiResponse success = new CommonApiResponse(
			HttpResponseStatus.OK.getCode(), "Action Success");

    public static final CommonApiResponse failed = new CommonApiResponse(
            HttpResponseStatus.INTERNAL_SERVER_ERROR.getCode(), "Action Failed");

	public static final CommonApiResponse noLoggedInUser = new CommonApiResponse(
			HttpResponseStatus.UNAUTHORIZED.getCode(), "User is not logged in");

	public static final CommonApiResponse noUserInfo = new CommonApiResponse(
			HttpResponseStatus.BAD_REQUEST.getCode(),
			"User ID found from cookie/parameters. But User info could not be found. Reason can be that while creating user, user info was not inserted in Mongo DB.");

	public static final CommonApiResponse noMovieInfo = new CommonApiResponse(
			HttpResponseStatus.BAD_REQUEST.getCode(),
			"Movie ID in the request could not be found in the DB");

	public static final CommonApiResponse vobIdNull = new CommonApiResponse(HttpResponseStatus.BAD_REQUEST.getCode(), "Vob id is null");

	public static final CommonApiResponse vobIdInvalid = new CommonApiResponse(HttpResponseStatus.BAD_REQUEST.getCode(), "Vob id is invalid");

	public static CommonApiResponse nullCommentNotAllowed = new CommonApiResponse(
			HttpResponseStatus.BAD_REQUEST.getCode(), "null Comment Not Allowed");

    public static CommonApiResponse InvalidRquest = new CommonApiResponse(
            HttpResponseStatus.BAD_REQUEST.getCode(), "Bad Request");

	public static CommonApiResponse secretInvalid = new CommonApiResponse(
            HttpResponseStatus.BAD_REQUEST.getCode(), "Secret is not correct.");

	public static CommonApiResponse alreadyPresent= new CommonApiResponse(
            HttpResponseStatus.BAD_REQUEST.getCode(), "Movie id is already present.");

	public static CommonApiResponse incorrectDateFormat =  new CommonApiResponse(
            HttpResponseStatus.BAD_REQUEST.getCode(), "Date is in incorrect format");

	public static CommonApiResponse movieNotSaved =  new CommonApiResponse(
            HttpResponseStatus.BAD_REQUEST.getCode(), "Could not save movie. Please try again.");



	public StaticResponse() {

	}
}
