package com.voblr.base.api.v1.data.mongo.model.vob;

import com.google.code.morphia.annotations.Entity;
import com.google.code.morphia.annotations.Id;
import com.google.code.morphia.annotations.Indexed;
import org.apache.commons.lang.builder.EqualsBuilder;
import org.apache.commons.lang.builder.HashCodeBuilder;
import org.apache.commons.lang.builder.ToStringBuilder;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement
@Entity(value = "VobComments", noClassnameStored = true)
public class VobComments {
    @Indexed
    @Id
    String vobCommentId;
    private List<Comment> comments;
    public int count;

    public VobComments() {
    }

    public VobComments(String vobCommentId, List<Comment> comments) {
        this.vobCommentId = vobCommentId;
        this.setComments(comments);
        count = comments.size();
    }

    public VobComments(String vobCommentId) {
        this.vobCommentId = vobCommentId;
        this.setComments(null);
        count = 0;
    }

    /**
     * @return the vobCommentId
     */
    public String getVobCommentId() {
        return vobCommentId;
    }

    /**
     * @param vobCommentId the vobCommentId to set
     */
    public void setVobCommentId(String vobCommentId) {
        this.vobCommentId = vobCommentId;
    }

    /**
     * @return the comments
     */
    public List<Comment> getComments() {
        return comments;
    }

    /**
     * @param comments the comments to set
     */
    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    /**
     * @param vobId
     * @return
     */
    public static String generateVobCommentId(String vobId) {
        return vobId + "_" + "comments";
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this);
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj);
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this);
    }


}
