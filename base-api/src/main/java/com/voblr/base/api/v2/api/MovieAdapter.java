package com.voblr.base.api.v2.api;

import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v2.objects.Movie;
import com.voblr.common.GraphServices;
import com.voblr.common.proto.Movie.MovieProto;

import java.util.ArrayList;
import java.util.List;

/**
 * Abhijeet Parande
 * Date: 3/18/13
 * Time: 2:23 AM
 * (c) Voblr, Inc
 */
public class MovieAdapter implements EntityAdapter<Movie, MovieProto, List<Movie>,GraphServices.Movies> {

    private MongoConnector mongoConnector;

    public MovieAdapter(MongoConnector mongoConnector) {
        this.mongoConnector = mongoConnector;
    }

    @Override
    public Movie underlyingEntity(MovieProto some) {

        MovieBasicInfo movieBasicInfo = mongoConnector.getSingle(MovieBasicInfo.class, "_id", some.getMovieId());
        Movie ret = new Movie();
        ret.setMovieId(some.getMovieId());
        ret.setName(movieBasicInfo.getName());
        ret.setCast(movieBasicInfo.getCast());
        ret.setDescription(movieBasicInfo.getDescription());
        ret.setPicUrl(movieBasicInfo.getPicUrl());
        ret.setReleaseDate(movieBasicInfo.getReleaseDate());
        ret.setDirectors(movieBasicInfo.getDirectors());
        ret.setNoOfVobs(some.getTotalVobs());
        ret.setLang(movieBasicInfo.getLang());
        ret.set_createdTimeStamp(String.valueOf(some.getCreatedTs()));
        ret.set_updatedTimeStamp(String.valueOf(some.getCreatedTs()));
        return ret;
    }

    @Override
    public List<Movie> underlyingEntities(GraphServices.Movies some) {
        List<MovieProto> protos = some.getMoviesList();
        List<Movie> ret = new ArrayList<Movie>();
        for(MovieProto proto : protos){
            ret.add(underlyingEntity(proto));
        }
        return ret;
    }

    public static void main(String[] args){
        MovieAdapter movieWrapper = new MovieAdapter(new MongoConnector());
        MovieProto some = MovieProto.newBuilder().setMovieId("146819").build();
        Movie movie = movieWrapper.underlyingEntity(some);
        System.out.println(movie);
    }
}
