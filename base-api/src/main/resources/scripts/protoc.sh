
PRJ=`dirname $0`/../../../../

cd $PRJ
PRJ=$PWD
SRC=$PRJ/src/main/resources/proto
#ls -al $SRC
OUT=$PRJ/src/main/java
PROTO=$SRC/GraphServices.proto

echo "PRJ= "$PRJ
echo "SRC= "$SRC
echo "OUT= "$OUT
echo "PROTO= "$PROTO
echo "protoc -I=$SRC --java_out=$OUT $PROTO"
protoc -I=$SRC --java_out=$OUT $PROTO
