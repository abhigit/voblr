import com.voblr.base.api.v1.data.mongo.DAO.MongoConnector;
import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.user.User;
import com.voblr.base.api.v1.data.mongo.model.user.UserBasicInfo;
import com.voblr.base.api.v1.data.mongo.model.vob.VobComments;
import com.voblr.base.api.v1.data.mongo.model.vob.VobData;
import com.voblr.base.api.v1.data.mongo.model.vob.VobVoteUps;


public class CleanMongoDB {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

//		MongoConnector.ds.
		MongoConnector.ds.delete(MongoConnector.ds.createQuery(VobComments.class));
		MongoConnector.ds.delete(MongoConnector.ds.createQuery(VobData.class));
		MongoConnector.ds.delete(MongoConnector.ds.createQuery(User.class));
		MongoConnector.ds.delete(MongoConnector.ds.createQuery(UserBasicInfo.class));
		MongoConnector.ds.delete(MongoConnector.ds.createQuery(VobVoteUps.class));
        MongoConnector.ds.delete(MongoConnector.ds.createQuery(MovieBasicInfo.class));
//		MongoConnector.ds.update(MongoConnector.ds.createQuery(MovieBasicInfo.class),MongoConnector.ds
//				.createUpdateOperations(MovieBasicInfo.class).set("noOfVobs", 0));
	}

}
