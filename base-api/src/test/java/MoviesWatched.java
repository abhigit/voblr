import com.esotericsoftware.minlog.Log;
import com.voblr.base.api.v1.data.mongo.response.MoviesWatchedResponse;
import com.voblr.base.api.v1.data.mongo.response.UserMovieStat;
import com.voblr.base.api.v1.data.mongo.services.UserService;


public class MoviesWatched {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		MoviesWatchedResponse m = UserService.getMoviesWatched("1");
		for(UserMovieStat u : m.movieStats){
			Log.info(u.movie.getMovieId() + ":" + u.vobCount + ":" + u.latestVob.getTimestamp());
		}
		Log.info("Total movies watched : " + m.count);
	}

}
