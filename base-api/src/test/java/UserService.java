import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response;
 
@Path("/users")
public class UserService {
 
	@GET
	@Path("/get")
	public Response addUser(@Context HttpHeaders headers) {
 
		String userAgent = headers.getRequestHeader("user-agent").get(0);
 
		for(String header : headers.getRequestHeaders().keySet()){
			System.out.println(header);
		}
		
		for(String header : headers.getRequestHeaders().keySet()){
			System.out.println(header);
		}
		return Response.status(200)
			.entity("addUser is called, userAgent : " + userAgent)
			.build();
 
	}
 
}