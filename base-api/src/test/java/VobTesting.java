import java.util.Date;
import java.util.List;

import com.voblr.base.api.v1.data.mongo.model.vob.VobData;
import com.voblr.base.api.v1.data.mongo.services.UserService;

/**
 * 
 */

/**
 * @author vinit Update Time : Nov 21, 2012 : 10:42:36 PM TODO
 */
public class VobTesting {
	public static void main(String args[]) {
		
//		UserService.doVob("0002", "tt0144117", "Very nice movie", new Date());
		
		List<VobData> a= UserService.getMovieVobs("tt1055369");
		for (VobData b : a ){
			System.out.println(b.getTimestamp() + " : " + b.getVobId());
		}

		System.out.println("-------------------");
		a= UserService.getUserVobs("0001");
		for (VobData b : a ){
			System.out.println(b.getTimestamp() + " : " + b.getVobId());
		}
	}
}
