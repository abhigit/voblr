import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import com.voblr.base.api.v1.data.mongo.model.movie.MovieBasicInfo;
import com.voblr.base.api.v1.data.mongo.services.MovieService;

public class MovieInsertTest {

	/**
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		List<MovieBasicInfo> m = MovieService.getMovieList();
		for (MovieBasicInfo a : m) {
			URL u = new URL("http://api.voblr.com:9998/v1/create/m/"
					+ a.getMovieId());
			System.out.println(u.openStream().read());
		}
	}

}
