import com.voblr.base.api.v1.common.AuthUser;
import com.voblr.base.api.v1.common.VoblrMySql;

import java.sql.ResultSet;
import java.sql.SQLException;

public class CleanMySQL {
    /**
     * @param args
     * @throws ClassNotFoundException
     * @throws SQLException
     */
    public static void main(String[] args) throws Exception {
        java.sql.Connection con = VoblrMySql.getInstance().con;

        // Get a Statement object
        java.sql.Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("Select * from User");
        System.out.println("Display all results:");
        while (rs.next()) {
            String name = rs.getString("name");

        }//end while loop

        String[] list = new String[]{"Snsinfo", "User"};

        for (String table : list) {
            truncate(table, stmt);
        }
//
        con.close();

        AuthUser.cleanUp();
    }

    static void truncate(String table, java.sql.Statement stmt) throws SQLException {
        int a = stmt.executeUpdate("TRUNCATE TABLE " + table);
        System.out.println(a + " records deleted from " + table + " table");
    }

}
