<?php
/**
 * Copyright 2011 Facebook, Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

require_once "base_facebook.php";

/**
 * Extends the BaseFacebook class with the intent of using
 * PHP sessions to store user ids and access tokens.
 */
class Facebook_ApiFacebook extends BaseFacebook
{

  const APP_ID = '270237853057532';
  const APP_SECRET = '88f5532d51c9be808771522acb412a5b';
  const APP_NAME = 'facebook';

  private $access_params = array(
      'token'  => null,
      'secret' => null
  );

  public static $LOGIN_SCOPE = array(
      'scope' => 'email, publish_actions'
  );


  /**
   * Identical to the parent constructor, except that
   * we start a PHP session to store the user ID and
   * access token if during the course of execution
   * we discover them.
   *
   * @param Array $config the application configuration.
   * @see BaseFacebook::__construct in facebook.php
   */
  public function __construct($config = null) {
    //     if (!session_id()) {
    //     	Log::info(__METHOD__ . ", session id");
    //       session_start();
    //     }
    if (!$config) {
      $config = array();
      $config['appId'] = self::APP_ID;
      $config['secret'] = self::APP_SECRET;
      $config['fileUpload'] = false; // optional
    }
    parent::__construct($config);
  }

  protected static $kSupportedKeys =
  array('state', 'code', 'access_token', 'user_id');

  /**
   * Provides the implementations of the inherited abstract
   * methods.  The implementation uses PHP sessions to maintain
   * a store for authorization codes, user ids, CSRF states, and
   * access tokens.
   */
  protected function setPersistentData($key, $value) {
    //     if (!in_array($key, self::$kSupportedKeys)) {
    //       self::errorLog('Unsupported key passed to setPersistentData.');
    //       return;
    //     }

    //     $session_var_name = $this->constructSessionVariableName($key);
    //     $_SESSION[$session_var_name] = $value;
  }

  protected function getPersistentData($key, $default = false) {
    //     if (!in_array($key, self::$kSupportedKeys)) {
    //       self::errorLog('Unsupported key passed to getPersistentData.');
    //       return $default;
    //     }

    //     $session_var_name = $this->constructSessionVariableName($key);
    //     return isset($_SESSION[$session_var_name]) ?
    //       $_SESSION[$session_var_name] : $default;
  }

  protected function clearPersistentData($key) {
    //     if (!in_array($key, self::$kSupportedKeys)) {
    //       self::errorLog('Unsupported key passed to clearPersistentData.');
    //       return;
    //     }

    //     $session_var_name = $this->constructSessionVariableName($key);
    //     unset($_SESSION[$session_var_name]);
  }

  protected function clearAllPersistentData() {
    //     foreach (self::$kSupportedKeys as $key) {
    //       $this->clearPersistentData($key);
    //     }
    //   }

    //   protected function constructSessionVariableName($key) {
    //     return implode('_', array('fb',
    //                               $this->getAppId(),
    //                               $key));
  }



  public function getFbUrl($name, $path='', $params=array())
  {
    return $this->getUrl($name, $path, $params);
  }


  public function getAccessTokenByCode($code, $redirect_uri = null)
  {
    is_null($redirect_uri) && $redirect_uri = $this->getCurrentUrl();

    $params = array(
        'client_id'      => $this->getAppId(),
        'client_secret'  => $this->getAppSecret(),
        'redirect_uri'   => $redirect_uri,
        'code'           => $code
    );

    $uri = $this->getFbUrl('graph', '/oauth/access_token', $params);
    $config = array(
        'adapter'   => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => array(
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CONNECTTIMEOUT => 5
        ),
    );

    $client = new Zend_Http_Client($uri, $config);
    $result = $client->request()->getBody();
    parse_str($result, $access_param);
    if (isset($access_param['access_token'])){
      $this->access_params['token'] = $access_param['access_token'];
      return $this->getExtendedToken();
    }
    else {
      return false;
    }
  }
  public function getExtendedToken($token = null)
  {
    is_null($token) && $token = $this->access_params['token'];

    $params =  array(
        'client_id'         => $this->getAppId(),
        'client_secret'     => $this->getAppSecret(),
        'grant_type'        => 'fb_exchange_token',
        'fb_exchange_token' => $token
    );

    $uri = $this->getFbUrl('graph', '/oauth/access_token', $params);
    $config = array(
        'adapter'   => 'Zend_Http_Client_Adapter_Curl',
        'curloptions' => array(
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_CONNECTTIMEOUT => 5
        ),
    );

    $client = new Zend_Http_Client($uri, $config);
    $result = $client->request()->getBody();

    parse_str($result, $access_param);
    if (isset($access_param['access_token'])) {
      $this->access_params['token'] = $access_param['access_token'] ;
      $this->setAccessToken($this->access_params['token']);
      return true;
    } else {
      return false;
    }
  }

  /**
   * @see Sns_model::getUserInfo()
   */
  public function getAuthInfo()
  {
    $user_profile = '';
    if ($this->getUser()) {
      try {
        // Proceed knowing you have a logged in user who's authenticated.
        return array(
            'uid'      => $this->getUser(),
            'token'    => $this->access_params['token'],
            'secret'   => $this->access_params['secret'],
            'service'  => self::APP_NAME
        );
      } catch (FacebookApiException $e) {
        log_message('warn', $e->__toString());
        $user = null;
        return false;
      }
    }
    return false;
  }


  public function getUserInfo($detail = false)
  {
    $user_profile = '';
    if ($this->getUser()) {
      try {
        // Proceed knowing you have a logged in user who's authenticated.
        $user_profile = $this->api('/me');
        if ($detail) {
        	return $user_profile;
        }
        $user_name = isset($user_profile['username']) ? $user_profile['username'] : underscore(mb_strtolower($user_profile['username']));
        $user_email = $user_profile['email'];
        $user = compact('user_email', 'user_name');
        return $user;
      } catch (FacebookApiException $e) {
        log_message('warn', $e->__toString());
        return false;
      }
    }
    return false;
  }
}
