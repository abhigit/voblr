<?php

/**
 * For making debugging easier in the application development
 *
 * @author Rishabh Mahajan<rishabhmhjn@voblr.com>
 * @param mixed $var the mixed type that has to be print_r or var_dumped
 * @param int $print_r_flag (1 default for print_r | 0 for var_dump)
 * @param int $die 0|1 to set whether to die or not
 */
function app_debug($var, $print_r_flag = 1, $die = 0) {
  $bt = debug_backtrace();
  echo "<h3>{$bt[0]['file']}:{$bt[0]['line']}</h3>";
  echo '<pre style="text-align:left;">';
  if ($print_r_flag > 0)
    print_r($var);
  else var_dump($var);
  echo '</pre>';
  if ($die) die;
}