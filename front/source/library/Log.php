<?php
class Log {
        public static function debug($msg) {
            $writer = new Zend_Log_Writer_Stream('/usr/local/zend/var/log/custom/debug_log');
            $logger = new Zend_Log($writer);
            if(is_array($msg)) {
                $logger->debug(print_r($msg, true));
            } else {
                $logger->debug($msg);
            }
        }
	
	
        public static function info($msg) {
            $writer = new Zend_Log_Writer_Stream('/usr/local/zend/var/log/custom/info_log');
            $logger = new Zend_Log($writer);
            if(is_array($msg)) {
                $logger->info(print_r($msg, true));
            } else {
                $logger->info($msg);
            }
        }
        public static function err($msg) {
            $writer = new Zend_Log_Writer_Stream('/usr/local/zend/var/log/custom/err_log');
            $logger = new Zend_Log($writer);
            if(is_array($msg)) {
                $logger->err(print_r($msg, true));
            } else {
                $logger->err($msg);
            }            
        }
        
        public static function alert($msg) {
            $writer = new Zend_Log_Writer_Stream('/usr/local/zend/var/log/custom/alert_log');
            $logger = new Zend_Log($writer);
            if(is_array($msg)) {
                $logger->alert(print_r($msg, true));
            } else {
                $logger->alert($msg);
            }
        }
        
        public static function error_handler($errno, $errstr, $errfile, $errline) {
            $writer = new Zend_Log_Writer_Stream('/usr/local/zend/var/log/custom/err_log');
            $logger = new Zend_Log($writer);
                $logger->err("ERROR :" . $errstr . ", Filename: " . $errfile . ", LineNum: " . $errline);
        }
/*
        public static function imageshared($msg) {
                $writer = new Zend_Log_Writer_Stream('/usr/local/zend/apache2/logs/image_shared');
                $logger = new Zend_Log($writer);
                $logger->info($msg);
        }
*/

}
