<?php


class Project_Mailer_Mail extends Project_Mailer_Base
{
  function __construct() {
    $options = array(
        'auth'     => 'login',
        'username' => 'ibhupi@voblr.com',
        'password' => 'socialmovie2012',
        'ssl'      => 'tls',
        'port' => 587
    );
    $mailTransport = new Zend_Mail_Transport_Smtp('smtp.gmail.com', $options);
    Zend_Mail::setDefaultTransport($mailTransport);
    Zend_Mail::setDefaultFrom('ibhupi@voblr.com', 'ibhupi-voblr');
    Zend_Mail::setDefaultReplyTo('ibhupi@voblr.com', 'ibhupi-voblr');
  }
  
  private function send($subject, $bodyText, $emailid, $name) {
    if (!$subject || $subject == "") {
      Log::alert(__METHOD__ . ", email subject is missing");
      return null;
    }
    if (!$bodyText || $bodyText == "") {
      Log::alert(__METHOD__ . ", email body is missing");
      return null;
    }
    if (!$emailid || $emailid == "") {
      Log::alert(__METHOD__ . ", email recipient is missing");
      return null;
    }
    if (!$name || $name == "") {
      Log::alert(__METHOD__ . ", email recipient name is missing");
      return null;
    }
    Log::info(__METHOD__ .", Sending email:[" . $subject . "]" . " to [ " . $emailid . " ]");
  	
    $mailer = new Zend_Mail('utf-8');
  	if (!$mailer) {
  		$mailerNull = "mailer is empty can't send mail";
  		Log::err(__METHOD__ . ", " . $mailerNull);
  		return array("error" => $mailerNull);
  	}
  	
    $mailer->setBodyHtml($bodyText);
    $mailer->addTo($emailid, $name);
		$mailer->setSubject($subject);
		$mailer->send();
    Log::info(__METHOD__ .", Email:[" . $subject . "]" . " sent successfully to [ " . $emailid . " ]");
		
		return array("mailer" => "mail sent");
  }
  
  public function sendVerificationLink($veificationLink, $emailid, $name) {
  	Log::info(__METHOD__ . ", " . $veificationLink . ", " . $emailid . ", " . $name);
  	
  	if (!$veificationLink || $veificationLink == "") {
  		return null;
  	}
    if (!$emailid || $emailid == "") {
      return null;
    }
    if (!$name || $name == "") {
      return null;
    }
    $subject = "Verify email address";
  	// create view object
    $html = new Zend_View();
    $html->setScriptPath(APPLICATION_PATH . '/views/emails/');
    // assign valeues
    $html->assign('name', $name);
    $html->assign('verificationlink', $veificationLink);
    
    // render view
    $bodyText = $html->render('verificationlink.phtml');
    
    return self::send($subject, $bodyText, $emailid, $name);
  }
  
  
}