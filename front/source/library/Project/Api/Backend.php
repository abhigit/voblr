<?php

class Project_Api_Backend extends Project_Api_Base
{
    //const BACKEND_URL = "http://api.voblr.com/v1/";
    const BACKEND_URL = "http://api.voblr.com:9998/v1/";
    
    function __construct() {
//        parent::init(self::BACKEND_URL);
  }
    
    
    public function getMovieInfo($movieid) {
        $response = $this->getResponse(self::BACKEND_URL . 'movies/' . $movieid);
        
        return $response;
    }
    
    public function getUserStats($userid) {
//        http://api.voblr.com/v1/vob/user/1
        
        $response = $this->getResponse(self::BACKEND_URL . 'users/' . $userid);
        return $response;
    }
    
   /**
    * vob details
    *
    */
   public function getVobDetail($vobId){
       $apiUrl = self::BACKEND_URL."vob/".$vobId."/";
       $response = $this->makeGetRequest($apiUrl, null);
       return $response;
   } 
}
