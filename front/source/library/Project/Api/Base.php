<?php


class Project_Api_Base
{
    private $_client = null;
    private $_CONFIG =  array( 'maxredirects' => 0,'timeout'      => 30);
    function __construct() {
        Log::info(__METHOD__);
    }

    public function init($url) {

    }

    public function getResponse($url) {
        $this->_client = new Zend_Http_Client($url, $this->_CONFIG);

        $request = $this->_client->request();
        if (!$request) {
            Log::err(__METHOD__ . ", error in request from url $url ,  request:" . print_r($request, true));
            return null;
        }

        $response = $request->getBody();
  //    $response = $response->
  //    $response = $request->getContent();
        if (!$response) {
            return null;
        }
        $startPos = 0;
        $endPos = 0;
        $haystack = $response;
        $needle = "callback(";
        if ($this->startsWith($haystack, $needle)) {
            $startPos = strlen($needle);
        }
        $needle = ")";
        if ($this->endsWith($haystack, $needle)) {
            $endPos = -strlen($needle);
        }
  //    Log::info("response: " . $response);

  //    Log::info("start, end pos: $startPos, $endPos");
        if ($startPos != $endPos) {
            $response = substr($response, $startPos, $endPos);
        }

        $response = json_decode($response, true);

        if (!isset($response["status"]) || !$response["status"] || $response["status"] != "200") {
            Log::err(__METHOD__.", error in  url $url response: " . print_r($response, true));
            Log::err(__METHOD__);
            Log::err($request);
            return false;
        }

        return $response;
    }

    /**
     * Make Get Request
     *
     * @param string $apiUrl (Target Api Url)
     * @param array  $params (request parameters)
     */
    public function makeGetRequest($apiUrl, $params=NULL){
        $client   = new Zend_Http_Client($apiUrl);
        // Set Query Parameters
        $client->setParameterGet($params);
        // Set Cookie
        $vobCommon = isset($_COOKIE['vob_common']) ? $_COOKIE['vob_common'] : NULL;
        $client->setCookie('vob_common', $vobCommon);
        // HTTP Request
        $response = $client->request('GET');
        $response = $response->getbody();

        $response = json_decode($response, true);
        if (!isset($response["status"]) || !$response["status"] || $response["status"] != "200") {
            Log::err(__METHOD__.", error in  url $url response: " . print_r($response, true));
            Log::err(__METHOD__);
            Log::err($request);
            return false;
        }
        return $response;
    }


    //-------------------------------
    // Private Functions
    //-------------------------------
    private function startsWith($haystack, $needle)
    {
        return !strncmp($haystack, $needle, strlen($needle));
    }

    private function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }

        return (substr($haystack, -$length) === $needle);
    }

    private function isJson($data, $callback) {
        $tmp = explode('({', $data);
        if($tmp[0] == $callback){
            $data = str_replace( $callback.'(', '', $data );
            $data = substr( $data, 0, strlen( $data ) - 1 ); //strip out last paren
        }
        json_decode($data);
        return (json_last_error() == JSON_ERROR_NONE);
    }

}
