<?php


class Project_Business_Auth extends Project_Business_Base
{

    //----------------------------
    // Constants
    //---------------------------- 
    const USER_SESSION_NAMESPACE         = 'USER_SESSION_NAMESPACE';
    const SESS_COOKIE_NAME               = "vob_common";
    const SESS_COOKIE_PREFIX             = "SESSID_";
    const AUTH_VERIFICATION_EMAIL_PREFIX = "verify_email_";
    const SECONDS_IN_MONTH               = 2592000; // cookie, token expiry time for email approved login 
    const SECONDS_IN_DAY                 = 86400;   // cookie, token expiry time first time after registration
    const FBM_COOKIE_NAME                = 'fbm_270237853057532'; // 270237853057532 => voblr fb app id
    const FBSR_COOKIE_NAME               = 'fbsr_270237853057532'; 

    public $_auth;
  
    function __construct() {
      //      Log::info(__METHOD__);
      parent::init();
      //      $this->_auth = Zend_Auth::getInstance();
    }
  
    //----------------------------
    // Public Functions 
    //---------------------------- 
    public function getInstance(){
      //      return $this->_auth;
      return;
    }
  
    public function checkAuthenticatedUser() {
      return null;
      //            return $this->_auth->hasIdentity();
    }
  
    public function clearSession() {
        //      Zend_Session::namespaceUnset(self::USER_SESSION_NAMESPACE);
        //      Zend_Session::namespaceUnset(self::USER_SESSION_NAMESPACE);
        //      $this->_auth->clearIdentity();
        $domainName = $this->getDomainName();
        @setcookie(self::SESS_COOKIE_NAME,null, time()-3600, "/", $domainName);

        //$token = new Project_Business_Tokens();
        //$userD = $token->validateAccessToken($_COOKIE['vob_common']);
        //Log::info("Got Info. from Redis".print_r($userD, true));
        ////Get access Token
        //$user = new Project_Provider_User();
        //$result = $user->getAccessToken($userD['user']['id']);
        //$accessToken = $result['sns_access_token'];   
        //$params = array (
        //    "access_token" => ''.$accessToken.''
        //);        
        //$facebook  = new Facebook_ApiFacebook();
        //$logoutUrl = $facebook->getLogoutUrl($params);
        //Log::info("LogoutUrl".$logoutUrl);
        //header("location:".$logoutUrl);
        //@setcookie(self::FBM_COOKIE_NAME, "", time()-3600, "/", $domainName);
        //@setcookie(self::FBSR_COOKIE_NAME,"", time()-3600, "/", $domainName);
        // Destroy Facebook Session
        //$facebook->destroySession();
    }
  
    public function checkLogin($username, $password) {
        $user = new Project_Business_User();
        $result = $user->getUserdatabyUsernameAndPassword($username, $password);
        if(!$result) {
            //    Log::info("This user is NOT a valid user");
            return null;
        }
      
        if (!$result["id"]) {
            Log::err(__METHOD__ .", cannot find id");
        }
        
        if(!$result['emailid_verified']) {
            $auth = new Project_Business_Auth();
            $auth->sendverficationlink($result['email_id']);
        }
        
      
        $cookieId = self::SESS_COOKIE_PREFIX . $result["id"] . "_" .  self::generateRandomID();
      
        //Set cookiee for authentication
         
        if (!$result["id"]) {
            Log::err(__METHOD__ .", cannot find id");
            return null;
        }
        
        $registeredTime = strtotime($result["create_ts"]);
        $currentTime = strtotime(date("Y-m-d H:i:s"));
        $emailid_verified = false;
        $timeAfterRegisteration = $currentTime - $registeredTime;
        if ( $timeAfterRegisteration < self::SECONDS_IN_DAY || $result["emailid_verified"]) {
            if ($result["emailid_verified"]) {
                $emailid_verified = true;
            }
            
            $cookieId = self::SESS_COOKIE_PREFIX . $result["id"] . "_" .  self::generateRandomID();
            $domainName = $this->getDomainName();
            $expiryTime = $emailid_verified ? self::SECONDS_IN_MONTH : $timeAfterRegisteration;
            setcookie(self::SESS_COOKIE_NAME,$cookieId, $expiryTime + time(), "/", $domainName);
            $redis = new Project_Business_Redis();
            $redis->setAndExpire($cookieId, $result["id"], $expiryTime);
        }
        
        return array( "user" => array("id" => $result["id"], "name" => $result["name"]), 
                      "emailid_verified" => $emailid_verified, 
                      "tmp_vb" => $cookieId);
    }
      
  
    public function getInfoFromSession() {
        $result = self::checkAuthenticatedUser();
        return $result;
    }
  
  
    private function generateRandomID() {
        $id = uniqid();
        $id = base_convert($id, 16, 2);
        $id = str_pad($id, strlen($id) + (8 - (strlen($id) % 8)), '0', STR_PAD_LEFT);
  
        $chunks = str_split($id, 8);
        //$mask = (int) base_convert(IDGenerator::BIT_MASK, 2, 10);
  
        $id = array();
        foreach ($chunks as $key => $chunk) {
            //$chunk = str_pad(base_convert(base_convert($chunk, 2, 10) ^ $mask, 10, 2), 8, '0', STR_PAD_LEFT);
            if ($key & 1) {  // odd
              array_unshift($id, $chunk);
            }else{         // even
              array_push($id, $chunk);
            }
        }
        return base_convert(implode($id), 2, 36);
    }
  
    //$username can be email id
    public function sendverficationlink($username) {
        if (!$username) {
            return "username or email id empty";
        }
        $user = new Project_Business_User();
        $result = $user->getUserdatabyUsername($username);
        if (!$result || !is_array($result) || !isset($result["email_id"])) {
            $msg = "user not found";
            Log::info(__METHOD__ . ", " . $msg);
            return $msg;
        }
        if ($result["emailid_verified"]) {
            $msg = "emailid is already verified";
            Log::info(__METHOD__ .", " . $msg . ", emailid: " . $result["email_id"]);
            return $msg;
        }
        
        $emailid = $result["email_id"];
        $name = $result["name"];
        $id = self::generateRandomID();
        if (!$id) {
            Log::alert(__METHOD__ .", " . "could not create id for redis key");
            return "some error occured, code: 001284";
        }
        $key = self::AUTH_VERIFICATION_EMAIL_PREFIX . $id;
        $value = $emailid;
        $sec = 2592000;
        
        $redis = new Project_Business_Redis();
        $redis->setAndExpire($key, $value, $sec);
  
        $hostname = self::getHostUrl();
        $mail = new Project_Mailer_Mail();
        $result = $mail->sendVerificationLink($hostname . "/auth/verifyemail?linkid=" . $key, $emailid, $name);
        return $result;
    }
    
    //verify link id
    public function verifyemail($linkid) {
        if (!$linkid) {
            return null;
        }
        $key = $linkid;
        $redis = new Project_Business_Redis();
        $id = $redis->get($key);
        if ($id) {
            Log::info(__METHOD__ .", user verified emailid: ". $id);
            //TODO update email verified
            $user = new Project_Business_User();
            $user->emailIdVerified($id);
            $redis->delete($key);
        }
  
        return $id ? true : false;
    }
}
