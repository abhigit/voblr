<?php


/**
 *  Project_Business_User Class
 *
 *  handling user
 *
 */
class Project_Business_User extends Project_Provider_Base
{
    //----------------------------
    // Constants
    //----------------------------
    private $_provider = null;
    const TYPE = 'User';

    function __construct() {
        //   parent::init();
        //   $this->_auth = Zend_Auth::getInstance();
        $this->_provider = new Project_Provider_User();
    }

    //----------------------------
    // Public Functions
    //----------------------------
    /**
     *  
     * 
     */
    public function checkAuthenticatedUser() {
        $redis = new Project_Business_Redis();
        $fc = Zend_Controller_Front::getInstance();
        $request = $fc->getRequest();
        $key_name = Project_Business_Auth::SESS_COOKIE_NAME;
        $keyId = $request->getCookie($key_name, $default = null);
        if (!$keyId) { //check in access_token exist
            $key_name = Project_Business_Tokens::ACCESS_TOKEN;
            $keyId = $request->getParams($key_name, $default = null);
        }
        if (!$keyId) { //nither cookiee nor access_token exist
            return null;
        }
        $userid = $redis->get($keyId);
        if(!$userid) {
            return null;
        }
        return $userid;
    }

    /**
     *  
     * 
     */
    public function logMeOut(){
       $auth = new Project_Business_Auth();
       $auth->clearSession();
    }

    /**
     *  
     * 
     */
    public function getUserbyID($id) {
        if (!$id) {
            Log::err(__METHOD__ . ", User id can not be null");
            return null;
        }
        $result = $this->_provider->getUserbyID($id);
        if (!$result || !is_array($result) || count($result) < 1 ) {
            return null;
        }

        $userStats = new Project_Api_Backend();
        $userStats = $userStats->getUserStats($id);

        if (count($result) > 1) {
            Log::err(__METHOD__ . ", Two users can not have same id, " . print_r($result, true));
        }

        if (0 < count($result) && $userStats && isset($userStats["user"])
            && $userStats["user"] && is_array($userStats["user"]) ) {
            $result[0] = array_merge($result[0], $userStats["user"]);
        }

        $result = (object) $result[0];
        return $result;
    }

    /**
     *  
     * 
     */
    public function getUserdatabysnsid($sns_user_id = null, $sns_service_id = null){
        if(!($sns_service_id && $sns_user_id)) {
            Log::err(__METHOD__
                    . ', required parameters are empty sns_service_id: '
                    . $sns_service_id . ', sns_user_id: ' . $sns_user_id);
            return false;
        }
        $result = $this->_provider->getUserdatabysnsid($sns_user_id,$sns_service_id);
        if(!is_array($result) || count($result) < 1) {
            return false;
        }else if(count($result) > 1) {
            Log::err("There is some serious error in code,
                    could get two user information. Expected result is one, UserInfo "
                    . print_r($result, true));
        }else {
            return $result[0];
        }
        return $result;
    }

    /**
     *  
     * 
     */
    public function addUserinfo($params = null) {
        if(!$params) {
            $response = array( 'error'  => true,
                               'status' => '101'
                        );
            return $response;
        }

        // Username
        if(!isset($params['username']) || strlen($params['username']) < 5 ){
            $response = array( 'error'  => true,
                               'status' => '102'
                        );
            Log::info("Username is null or is less than 5 characters");
            return $response;
        }

        // email
        if (!filter_var($params['email_id'], FILTER_VALIDATE_EMAIL)) {
            $response = array( 'error'  => true,
                               'status' => '103'
                        );
            Log::info("Invalid email address");
            return $response;
        }

        // password
        if(!isset($params['password']) || strlen($params['password']) < 5 ){
            $response = array( 'error'  => true,
                               'status' => '104'
                        );
            Log::info("Password is null or is less than 5 characters");
            return $response;
        }


        $user = $this->_provider->getUserdatabyUsername($params['username']);
        if($user) {
            $response = array( 'error'  => true,
                               'status' => '105'
                        );
            Log::info("User with this name already exists in database");
            return $response;
        }

        $user = $this->_provider->getUserdatabyUsername($params['email_id']);
        if($user) {
            $response = array( 'error'  => true,
                               'status' => '106'
                        );
            Log::info("User with this email already exists in database");
            return $response;
        }

        $usersnsinfo = array();
        if (isset($params['signup_via']) && $params['signup_via'] == 'facebook_mobile') {
            if(isset($params['snsuserid']) && $params['snsuserid']) {
                $usersnsinfo['snsserviceid'] = 'FACEBOOK';
                $usersnsinfo['snsuserid'] = $params['snsuserid'];
            }
        } else if (isset($params['snsinfo']) && $params['snsinfo']){
            $usersnsinfo = $params['snsinfo'];
        }
        // result is last inserted id i.e., $user['id']
        $result = $this->_provider->addUserinfo($params, $usersnsinfo);
        if ($result) {
            Log::info("creating graph node params");
            $apiUrl   = "http://api.voblr.com:9998/v1/create/".$result;
            $name     = $params['name'];
            $picURL   = "http://stg.voblr.com/static/images/default/user.png?v=1";
            if(isset($usersnsinfo['snsuserid'])){
                $fbimgurl = 'http://graph.facebook.com/%s/picture';
                $picURL   = sprintf($fbimgurl,$usersnsinfo['snsuserid']);
            }
            // Create HTTP Client for request
            $client   = new Zend_Http_Client($apiUrl);
            // Set Query Parameters
            $client->setParameterGet(array('name'   => $name,
                                           'picURL' => $picURL));
            // HTTP Request
            // TODO response check
            $response = $client->request('GET');
            Log::info("new Ams_____________________________".$result);
            Log::info(print_r($response, true));
            Log::info("_____________________________");

        }
        return $result;
    }

    /**
     *  
     * 
     */
    public function addSnsInfo($uuid, $snsinfo) {
        $result = $this->_provider->addSnsInfo($uuid, $snsinfo);
        return $result;
    }

    /**
     *  
     * 
     */
    public function updateAccessTokenBySnsId($snsuserid, $token) {
        $result = $this->_provider->updateAccessTokenBySnsId($snsuserid, $token);
        return $result;
    }

    /**
     *  
     * 
     */
    public function updateUserLocation($id, $location) {
        $result = $this->_provider->updateUserLocation($id, $location);
        return $result;
    }

    /**
     *  
     * 
     */
    public function getUserdatabyUsernameAndPassword($username, $password){
        if (!$username) {
            Log::err(__METHOD__ . " username not specified");
            return false;
        }

        if (!$password) {
            Log::err(__METHOD__ . " password not specified");
            return false;
        }

        $result = $this->_provider->getUserdatabyUsernameAndPassword($username, $password);

        /*
         * Validate token //TODO
         */
        // Log::info($result);
        if (!$result || !is_array($result)) {
           return null;
        }
        $result = isset($result[0]) ? $result[0] : null;
        return $result;
    }

    /**
     *  
     * 
     */
    //input param $username can be email address
    public function getUserdatabyUsername($username){
        //This method is to link SNS sign to the exsiting email address

        if (!$username) {
            Log::err(__METHOD__ . " username or email not specified");
            return false;
        }

        $result = $this->_provider->getUserdatabyUsername($username);

        /*
         * Validate token //TODO
         */
        // Log::info($result);
        if (!$result || !is_array($result)) {
           return null;
        }
        $result = isset($result[0]) ? $result[0] : null;
        return $result;
    }


    /**
     * getExtraUserInfo
     *
     * @access public
     * @return $response array
     */
    public function getExtraUserInfo($userId){
        $response = array();
        $result = $this->_provider->getExtraUserInfo($userId);
        try {
            $facebook = new Facebook_ApiFacebook();
            $apiName  = $result['snsInfo']['sns_user_id'] . '?locale=en_US';
            $userdata = $facebook->api($apiName);

            // Assign Extra user info to response from FB
            if(isset($userdata['location']['name'])){
              $response['location'] = $userdata['location']['name'];
            }

            if(isset($userdata['bio'])){
              $response['about_me'] = $userdata['bio'];
            }

            if(isset($userdata['work'][0])){
              $response['work'] = $userdata['work'][0]['employer']['name'];
            }

            if(isset($userdata['education'][0])){
              $response['education'] = $userdata['education'][0]['school']['name'];
            }

            $response['username'] = isset($userdata['username']) ? $userdata['username'] : "";

            // For Debug If required
            // Log::info($userdata);
            Log::info("Extra User Info : ".print_r($response,true));
        } catch (FacebookApiException $e) {
            Log::err($e);
        }
        return $response;
    }


    /**
     *  
     * 
     */
    public function ifUserExists($userIdOrPassword) {
        return $this->_provider->getUserdatabyUsername($userIdOrPassword);
    }

    /**
     *  
     * 
     */
    public function changePassword($emailId, $password) {
        Log::info("in business. emailId=".$emailId."password=".$password);
        if($this->_provider->changePassword($emailId, $password)) {
            Log::info("[SUCCESS] Password changed for user_email = ".$emailId);
            return true;
        } else {
            Log::info("[Failed] Password change for user_email = ".$emailId);
        }
        return false;
    }

    /**
     *  
     * 
     */
    public function postRegisteration(array $params) {
        if ( ! isset($params['signup_via'])) return;

        switch ($params['signup_via']) {
            case 'facebook':
                $facebook = new Facebook_ApiFacebook();
                try {
                    $friends = $facebook->api("/me/friends");
                    Log::info('In unwanted facebook friends loop');
                    $friends_uids = array();
                    foreach ($friends['data'] as $uid) {
                      $friends_uids[] = $uid['id'];
                    }
                    app_debug($friends_uids);
                    die;
                } catch (Exception $e) {
                    Log::err($e->__toString());
                }
        }
    }

    /**
     *  
     * 
     */
    public function emailIdVerified($id) {
        if($this->_provider->emailIdVerified($id)) {
            return array("emailid_verified" => true);
        } else {
            return "already verified user";
        }
    }


    /**
     *  
     * 
     */
    public function checkLoginWithsns($snsuserid, $accessToken, $snsid) {
        if (!$snsuserid || !$accessToken || !$snsid) {
            $msg = "either snsuserid or accesstoken or snsid not recevied";
            Log::err(__METHOD__ .", msg: " . $msg);
            return $msg;
        }
        $response = "error in getting userinformation";
        try {
            $facebook = new Facebook_ApiFacebook();
            $facebook->setAccessToken($accessToken);
            //$userdata = $facebook->getUserInfo();

            $facebookUserInfo = $facebook->getUserInfo();
            if (isset($facebookUserInfo['user_email']) && $facebookUserInfo['user_email']) {
                $result = self::getUserdatabyUsername($facebookUserInfo['user_email']);
                if ($result) {
                    $username = isset($result['username']) && $result['username'] ? $result['username'] : null;
                    $password = isset($result['password']) && $result['password'] ? $result['password'] : null;
                    $login = new Project_Business_Tokens();
                    $response = $login->checkLogin($username, $password);
                    return $response;
                }
            }
        } catch (FacebookApiException $e) {
            $usersnsinfo = null;
        }
        return $response;
    }

    /**
     *  
     * 
     */
    public function getUsersList($userid) {
        if (!$userid) {
            $msg = "current userid can not be null";
            Log::err(__METHOD__ .", " . $msg);
            return $msg;
        }

        $userlist = $this->_provider->getUsersList($userid);
        if (!$userlist || !is_array($userlist)) {
            $msg = "Error in getting users list from DB";
            Log::err(__METHOD__ .", " . $msg);
            return $msg;
        }
        return $userlist;
    }

    //----------------------------
    // Private Functions
    //----------------------------
    /**
     * getRefreshToken For FB
     *
     * @access private
     * @return refreshed access Token
     */
    private function getRefreshToken(){
        $facebook = new Facebook_ApiFacebook();
        // get short-time accessToken
        $accessTokenFb = $facebook->getAccessToken();

        // Voblr App Constants
        $app_id = '270237853057532';
        $app_secret = '88f5532d51c9be808771522acb412a5b';

        // To get AccessToken with the increased validity period
        $curl_url = 'https://graph.facebook.com/oauth/access_token?client_id='.$app_id
                   .'&client_secret='.$app_secret
                   .'&grant_type=fb_exchange_token&fb_exchange_token='.$accessTokenFb;

        // Curl Start
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $curl_url);

        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        // $output contains the output string
        $output = curl_exec($ch);

        // close curl resource to free up system resources
        curl_close($ch);

        $strings = explode("&", $output);
        $accessTokenFb = explode("=",$strings[0]);
        $accessTokenFb = $accessTokenFb[1];
        $expireTime = explode("=",$strings[1]);
        $expireTime = $expireTime[1];
        $result['accessTokenFb']= $accessTokenFb;
        $result['expireTime']= $expireTime;
        return $result;
    }
}
//}
