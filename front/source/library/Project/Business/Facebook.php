<?php

class Project_Business_Facebook {

  /**
   *
   * Constants
   */
  CONST VOBLR_API_URL = 'http://api.voblr.com:9998/v1/';

  private $_provider = null;

  private $_defaultAutoFollow = null;


  function __construct() {
    // associaltive array for user_ids
    // Format:  index => 'user_id'
    $this->_defaultAutoFollow = array(  1 => '42',
                                        2 => '13',
                                        3 => '39',
                                        4 => '4',
                                        5 => '43'
                                      );
  }

  /**
   * postFeedFb (wraper for posting on facebook)
   *
   */
  public function postFeedFb($target, $userId, $type){
      $user        = new Project_Provider_User();
      $result      = $user->getAccessToken($userId);
      $accessToken = $result['sns_access_token'];

      // Call FB Api for fb accessTokenSET and post
      $facebook = new Facebook_ApiFacebook();
      $facebook->destroySession();
      $facebook->setAccessToken($accessToken);

      $apiUrl = '';
      switch($type){
          case 'vob'      : $apiUrl = '/me/video.watches';
                            $postData = array('movie'=>$target); 
                            break;

          case 'follow'   : $apiUrl   = '/me/og.follows';
                            $postData = array('profile'=>$target);
                            break;

          case 'vote'     : $apiUrl   = '/me/voblrapp:vote';
                            $postData = array('vob'=>$target);
                            break;
          
		  case 'like'     : $apiUrl = '/me/og.likes';
                            $postData = array('object'=>$target); 
                            break;

          default         : $apiUrl   = '/me/voblrapp:see';
                            $postData = array('object'=>$target);
                            break;
      }

      if($apiUrl != ''){
          try{
              $facebook->api($apiUrl, 'post', $postData);
              return true;
          }catch(Exception $e) {
              Log::info('FB vob post caught Exception.'.$e);
              Log::err('FB vob post caught Exception.'.$e);
              return false;
          }
      }
  }

  /**
   * AutoFollow Default Users
   *
   */
  public function autoFollowDefaultUsers($cookieVal){
      // Auto Follow Default User
      if($this->_defaultAutoFollow){
          foreach($this->_defaultAutoFollow as $user){
              Log::Info("Auto Follow Deafult User ".print_r($user,true));
              // Auto Follow  api Url 
              $apiUrl = self::VOBLR_API_URL."follow/".$user;

              // Http Request Client
              $client = new Zend_Http_Client($apiUrl);
              $client->setCookie('vob_common', $cookieVal);

              // Make 'GET' call and store response
              $response = $client->request('GET');

              // Logging
              $res      = $response->getbody();
              Log::info(print_r($res, true));
          }
      }
  }

  /**
   * AutoFollow
   *
   */
  public function autoFollow($userId, $cookieVal){
      Log::info("inside business facebook");
      $user        = new Project_Provider_User();
      $result      = $user->getAccessToken($userId);
      $accessToken = $result['sns_access_token'];

      // Call FB Api for fb accessTokenSET and post
      $facebook = new Facebook_ApiFacebook();
      $facebook->setAccessToken($accessToken);

      $apiUrl = '/me/friends?fields=installed';
      try{
          $result = $facebook->api($apiUrl, 'get');
          // For Debug if required
          // Log::info('faceBook Friends = '.print_r($result, true));
      }catch(Exception $e) {
          Log::err('Auto Follow failed.'.$e);
          return false;
      }

      if($result['data']){
          foreach($result['data'] as $friend){
              if(isset($friend['installed'])){
                  if($friend['installed'] === true){
                      // Get user_id by using snsid
                      $validFriend = $user->getUserdatabysnsid($friend['id'], 'FACEBOOK');
                      // For Debug if required
                      // Log::info('Valid Friend(App Installed) = '.print_r($validFriend, true));
                      if($validFriend){
                          // Auto Follow  api Url 
                          $apiUrl = self::VOBLR_API_URL."follow/".$validFriend[0]['id'];

                          // Http Request Client
                          $client = new Zend_Http_Client($apiUrl);
                          $client->setCookie('vob_common', $cookieVal);

                          // Make 'GET' call and store response
                          $response = $client->request('GET');
                          $res      = $response->getbody();

                          // Logging
                          Log::info("Facebook Friends Autofollow");
                          Log::info(print_r($res, true));
                      }
                  }
              }
          }
      }
  }

}
