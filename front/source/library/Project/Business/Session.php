<?php

/**
 * For Session Handling
 * methods getSession, clearSession, 
 * setSession, init 
 *
 */
class Project_Business_Session extends Project_Business_Base{

/**
 *
 *
 */
  public function init() {
    parent::init();
    Zend_session::start();
  }

/**
 *
 *
 */
  public function initializeSession(){
    // Initialize Session
  }
  
/**
 *
 *
 */
  public function setSession($key, $value){
   // Set session 
  } 

/**
 * Get Session values
 *
 */
  public function getSession(){
    $auth = new Zend_Session_Namespace('Zend_Auth');
    
    if(isset($auth->storage)){
      Log::info("session Id  : ". session_id() ."\n");
      Log::info("session data: ". print_r($_SESSION,true)."\n-----------\n");
      //echo session_id();
      return session_id();
    }else{
      Log::error("session Id ng!!");
      return null;
    }
  }

/**
 * Clear Session Values
 * For complete logout
 */
  public function clearSession(){
    Zend_session::destroy(true);
  }


/**
 * Update Session Id 
 * 
 */
  public function updateSessionId(){
    return session_regenerate_id();
  }
}
?>
