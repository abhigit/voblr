<?php


class Project_Business_Base
{
  protected function init() {
  }
  
  public function getDomainName() {
    $httpHost = new Zend_Controller_Request_Http();
    $url = $httpHost->getHttpHost();
    $split_url = explode(".", $url);
    $url = '.' . $split_url[count($split_url) - 2] . '.' .$split_url[count($split_url) - 1];
    return $url;
  }
  
  public static function getHostUrl() {
    $httpHost = new Zend_Controller_Request_Http();
    $url = $httpHost->getScheme() . "://" . $httpHost->getHttpHost();
    return $url;
  }
  
}
