<?php


class Project_Business_Tokens extends Project_Business_Base
{
  const TOKEN_LENGTH = 32;
  const SECONDS_IN_MONTH = 2592000; // cookie, token expiry time for email approved login 
  const SECONDS_IN_DAY = 86400; // cookie, token expiry time first time after registration
  const SESS_TOKEN_PREFIX = "SESSTOKEN_";
  const ACCESS_TOKEN = 'access_token';
  
  private $_provider = null;
  
  function __construct() {
    parent::init();
    $this->_provider = new Project_Provider_Tokens();
  } 
	
  public function validateAccessToken($access_token = null) {
  	if (!$access_token) {
  		return false;
  	}
    $redis = new Project_Business_Redis();
    $value = $redis->get($access_token);
  	
//  	$token = $this->_provider->checkToken($access_token);
    
  	/*
  	 * Validate token //TODO
  	 */
  	Log::info($value);
  	if ($value) {
  		return array("user" => array("id" => $value));
  	}
  	return null;
  }

  private function generateToken($length = self::TOKEN_LENGTH) {
  	$characters = array(
        "A","B","C","D","E","F","G","H","J","K","L","M",
        "N","P","Q","R","S","T","U","V","W","X","Y","Z",
        "a","b","c","d","e","f","g","h","i","j","k","m",
        "n","o","p","q","r","s","t","u","v","w","x","y","z",
        "1","2","3","4","5","6","7","8","9", "0", "-", "_");
  	shuffle($characters);
  	return implode("", array_slice($characters, 0, $length));
  }

  public function checkLogin($username, $password){
  	$provider = new Project_Provider_User();
  	$result = $provider->getUserdatabyUsernameAndPassword($username, $password);
  	
    if(is_array($result) && count($result) == 1) {
        $result = $result[0];     
    }
    
  	if(!$result) {
  		Log::info("This user is NOT a valid user");
  		return false;
  	}
  	//send verification mail if emailid notverififed
  	if(!$result['emailid_verified']) {
  		Log::info(__METHOD__ . ", sending verification link");
  		$auth = new Project_Business_Auth();
  		$auth->sendverficationlink($result['email_id']);
  	}
  	return self::createAccessToken($result);
  }
  
  
  public function checkLoginForSns($snsuserid, $snsid = 'FACEBOOK'){
    $provider = new Project_Provider_User();
    $result = $provider->getUserdatabysnsid($snsuserid, $snsid);
    Log::info($result);
    
    if(is_array($result) && count($result) == 1) {
        $result = $result[0];     
    }
    if($result) {
          Log::info($result);
          return self::createAccessToken($result);
    } else {
    Log::info("This user is NOT a valid user");
      return false;
    }
  }
  
  
  private function createAccessToken($user) {
  	
    $emailid_verified = false;
    $registeredTime = strtotime($user["create_ts"]);
    $currentTime = strtotime(date("Y-m-d H:i:s"));
    $token = false;
    $timeAfterRegisteration = $currentTime - $registeredTime;
    if ( $timeAfterRegisteration < self::SECONDS_IN_DAY || $user["emailid_verified"]) {
      if ($user["emailid_verified"]) {
        $emailid_verified = true;
      }
	    $tokenid = self::SESS_TOKEN_PREFIX . $user["id"] . "_" .  self::generateToken();
	    $params = Array();
	    $params['access_token'] = $tokenid;
	    $params['user_id'] = $user['id'];
	    $params['valid_flg'] = 1;
	    $params['expire_ts'] = $emailid_verified ? self::SECONDS_IN_MONTH : $timeAfterRegisteration;    
	    $redis = new Project_Business_Redis();
	    $token = $redis->setAndExpire($tokenid, $user["id"], $params['expire_ts']);
    }
    
    if ($emailid_verified && !$token) {
    	Log::alert(__METHOD__ .", error in creating token. User on mobile can not login.");
    	return null;
    }
    
    $result = array();
    if ($token) {
	    $result[self::ACCESS_TOKEN] = $params['access_token'];
	    $result['expiry_time'] = $params['expire_ts'];    	
    }
    $result['user'] = array("id" => $user["id"] , "name" => $user["name"]);
    $result['emailid_verified'] = $emailid_verified;
    return $result;
  }
}
