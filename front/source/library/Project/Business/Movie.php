<?php

class Project_Business_Movie
{
  private $_provider = null;
  const TYPE = 'Movie';

  function __construct() {
    //   parent::init();
    //   $this->_auth = Zend_Auth::getInstance();
    $this->_provider = new Project_Provider_Movie();
  }

  public function getmovieDetail($id) {
  	Log::info(__METHOD__ . ", $id");
  	$movieDetail = new Project_Api_Backend();
//		$movieId = "tt1055369";
  	$movieDetail = $movieDetail->getMovieInfo($id);
//  	Log::info($movieDetail);
  	if (!$movieDetail) {
  		Log::err(__METHOD__ . ", could not get moview detail, id: " . $id);
  		return null;
  	}
  	
  	
  	$movieDetail = isset($movieDetail["movieInfo"]) && is_array($movieDetail["movieInfo"]) && count($movieDetail["movieInfo"]) > 0 ? $movieDetail["movieInfo"] : null;
  	if (!$movieDetail) {
  		Log::err(__METHOD__ . ", could not get movieInfo from movieDetail , id: " . $id);
  		return null;
  	}
  	
  	$movieDetail['id'] = $movieDetail['movieId'];
  	unset($movieDetail['movieId']);
  	
  	$movieDetail['desc'] = $movieDetail['name'];
  	
  	$movieDetail['director'] = $movieDetail['directors'];
  	unset($movieDetail['directors']);
  	
  	$movieDetail['vobCount'] = $movieDetail['noOfVobs'];
  	unset($movieDetail['noOfVobs']);
  	
  	$movieDetail['imageURL'] = $movieDetail['picUrl'];
  	unset($movieDetail['picUrl']);
  	
  	$movieDetail['create_ts'] = $movieDetail['releaseDate'];
  	unset($movieDetail['releaseDate']);
  	
  	$movieDetail['update_ts'] = $movieDetail['create_ts'];
  	
		return $movieDetail;  

		
  	$movieDetail = $this->_provider->getmovieDetail($id);
  	if (count($movieDetail) != 1) {
  		
  		Log::err(__METHOD__ . ", could not get moview detail, id: " . $id);
  		return null;
  	}
  	
  	$movieDetail = $movieDetail[0];
  	$movieDetail['name'] = $movieDetail['title'];
  	unset($movieDetail['title']);
//  	Log::info($movieDetail);
  	 return $movieDetail;
  }
}
