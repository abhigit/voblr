<?php 


class Project_Business_Movies extends Project_Business_Base {
	
	private $_provider = null;
	
  function __construct() {
  	parent::init();
    $this->_provider = new Project_Provider_Movies();
  }	
  
  public function getAllMovies() {
  	$result = $this->_provider->getAllMovies();
  	
  	if($result) {
  		foreach ($result as &$movie) {
  			$movie['name'] = $movie['title'];
  			unset($movie['title']);
  			$movie['type'] = 'movie';
  		}
  		
      return $result;
  	} else {
//    Log::info("Could not get result");
      return false;
  	}
  }
  
  public function incrementMovieVob($id) {
    Log::info(__METHOD__);
    $result = $this->_provider->incrementMovieVob($id);
    if($result) {
      return $result;
    } else {
//    Log::info("Could not get result");
      return false;
    }
  }
  
  
}