<?php


class Project_Provider_Auth extends Project_Provider_Base
{
  public function __construct(){
    parent::__construct();
    if($this->_db) {

    } else {
      Log::err("DB connection NOT exists");
    }
  }

  public function checkValidUser($username, $password, $auth){

    $identity_type = 'email_id';
    if(!filter_var($username, FILTER_VALIDATE_EMAIL)) {
      $identity_type = 'username';
    }
    $adapter = new Zend_Auth_Adapter_DbTable(
            $this->_db,
            'User',
            $identity_type,
            'password'
    );
    Zend_Session::regenerateId();
    $adapter->setIdentity($username);
    $adapter->setCredential($password);
    $result = $auth->authenticate($adapter);
    if ($result->isValid()) {

      $result = get_object_vars($adapter->getResultRowObject());
      $userid = $result["id"];
      $auth->getStorage()->write($userid);
      return $result;
    } else {
      return false;
    }
    return false;
  }

  public function lookupUids($params)
  {

  }
}
