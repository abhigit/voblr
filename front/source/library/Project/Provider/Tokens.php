<?php 


class Project_Provider_Tokens extends Project_Provider_Base {
	
	const TABLE = 'Tokens';
	
	public function __construct(){
		parent::__construct();
		if($this->_db) {
//      Log::info("DB connection OK");
		} else {
			Log::err("DB connection NOT exists");
		}
	}
  
  
  public function getAllMovies() {
  	Log::info(__METHOD__);
    $result = null;
    $select = $this->_db->select()
        ->from(self::TABLE);
    return $this->_db->fetchAll($select);
  }
  
  public function checkToken($access_token) {
    Log::info(__METHOD__);
    $result = null;
    $select = $this->_db->select()
        ->from(self::TABLE)
        ->where('access_token = ?', $access_token);
    return $this->_db->fetchAll($select);
  }
 
  public function incrementMovieVob($id) {
    Log::info(__METHOD__);
    if(!$id) {
    	Log::err(" id is null");
    	return false;
    }
		$data      = array('times_vobbed' => new Zend_Db_Expr('times_vobbed + 1'));
		$where = $this->_db->quoteInto('id = ?', $id);
    $this->_db->insert(self::TABLE, $data, $where);
  }
  
  public function createAccessToken($params) {
    $params['create_ts'] = new Zend_Db_Expr('NOW()');
    
    $this->_db->insert(self::TABLE, $params);
    $result = $this->_db->lastInsertId();
    return $result;
  }
  
}