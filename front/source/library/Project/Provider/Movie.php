<?php

class Project_Provider_Movie extends Project_Provider_Base {
	
  const TABLE_NAME = 'TempMovies';
	private $_provider = null;
  const TYPE = 'Movie';

  
  public function __construct(){
    parent::__construct();
    if($this->_db) {
//      Log::info("DB connection OK");
    } else {
      Log::err("DB connection NOT exists");
    }
  }
  
  public function getmovieDetail($id) {
    $result = null;
    $select = $this->_db->select()
        ->from(self::TABLE_NAME)
        ->where('id = ?', $id);
    $result = $this->_db->fetchAll($select);
    return $result;
  }
}