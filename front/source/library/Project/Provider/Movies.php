<?php 


class Project_Provider_Movies extends Project_Provider_Base {
	
	const TABLE_NAME = 'TempMovies';
	
	public function __construct(){
		parent::__construct();
		if($this->_db) {
//      Log::info("DB connection OK");
		} else {
			Log::err("DB connection NOT exists");
		}
	}
  
  
  public function getAllMovies() {
    $result = null;
    $select = $this->_db->select()
        ->from(self::TABLE_NAME);
    return $this->_db->fetchAll($select);
  }
  
  public function incrementMovieVob($id) {
    Log::info(__METHOD__);
    if(!$id) {
    	Log::err(" id is null");
    	return false;
    }
		$data      = array('vobCount' => new Zend_Db_Expr('vobCount + 1'));
		$where = $this->_db->quoteInto('id = ?', $id);
    $this->_db->insert(self::TABLE, $data, $where);
//		$db->update($table, $data, $where); 
  }
  
}