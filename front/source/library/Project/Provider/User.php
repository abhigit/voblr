<?php

class Project_Provider_User extends Project_Provider_Base
{
    private $tablename;
    public function __construct(){
        parent::__construct();
        if($this->_db) {
            $this->tablename = "User";
        } else {
          Log::info("DB connection NOT exists");
        }
    }
    
    /**
     *  
     * 
     */
    public function getUserbyID($id = null){
        if(!$id) {
          Log::err(__METHOD__ . ', required parameters are empty user id ' . $id);
          return false;
        }
        $result = null;
        $select = $this->_db->select()
                  ->from('User')
                  ->where('id = ?', $id);
        $result = $this->_db->fetchAll($select);
        return $result;
    }
    
    /**
     *  
     * 
     */
    public function getUserbyUUID($uuid = null){
        if(!$uuid) {
          Log::err(__METHOD__ . ', required parameters are empty uid ' . $uid);
          return false;
        }
        $result = null;
        $select = $this->_db->select()
                  ->from('User')
                  ->where('uuid = ?', $uuid);
      
        ////  Belowis multicondition select    
        //  $select = $this->_db->select()
        //    ->from('User')
        //    ->where('uuid = ?', $uuid)
        //    ->orwhere('uuid = ?', 'abcd000000002');
        $result = $this->_db->fetchAll($select);
        return $result;
    }
    
    /**
     *  
     * 
     */
    public function getUserdatabysnsid($sns_user_id = null, $sns_service_id = null){
        if(!($sns_service_id && $sns_user_id)) {
          Log::err(__METHOD__ 
                  . ', required parameters are empty sns_service_id: ' 
                  . $sns_service_id . ', sns_user_id: ' . $sns_user_id);
          return false;
        }
        $result = null;
        $select = $this->_db->select()
                  ->from(array('U' => 'User'))
                  ->joinLeft(array('S'=>'Snsinfo'),'U.uuid = S.uuid', array())
                  ->where('S.sns_service_id = ?', $sns_service_id)
                  ->where('S.sns_user_id = ?', $sns_user_id);
        $result = $this->_db->fetchAll($select);
        return $result;
    }
    
    /**
     *  
     * 
     */
    public function getUserdatabyUsername($username){
        if(!$username) {
          Log::err(__METHOD__ . ', required parameters are empty username: ' . $username);
          return false;
        }
      
        $identity_type = 'email_id';
        if(!filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $identity_type = 'username';
        }
       
        $result = null;
        $select = $this->_db->select()
                  ->from(array('U' => 'User'))
                  ->where('U.'.$identity_type.' = ?', $username);
        $result = $this->_db->fetchAll($select);
        return $result;
    }
    
    /**
     *  
     * 
     */
    public function getUserdatabyUsernameAndPassword($username, $password){
        if(!$username) {
          Log::err(__METHOD__ . ', required parameters are empty username: ' . $username);
          return false;
        }
        
        if(!$password) {
          Log::err(__METHOD__ . ', required parameters are empty password: ' . $password);
          return false;
        }
        
        $identity_type = 'email_id';
        if(!filter_var($username, FILTER_VALIDATE_EMAIL)) {
            $identity_type = 'username';
        }
        
        $result = null;
        $select = $this->_db->select()
                 ->from(array('U' => 'User'))
                 ->where('U.'.$identity_type.' = ?', $username)
                 ->where('U.password = ?', $password);
            
        $result = $this->_db->fetchAll($select);
        return $result;
    }
    
    /**
     *  
     * 
     */
    public function addUserinfo($userinfo, $snsinfo = null){
        if(!$userinfo || !is_array($userinfo) || count($userinfo) < 4) {
          Log::info(__METHOD__ 
                    . ", cannot add new user info because some parameters are missing" 
                    . print_r($userinfo));
          return false;
        }
      
        if(!isset($userinfo['username']) || !$userinfo['username']) {
          Log::info("username undefined");
          return false;
        }
        if(!isset($userinfo['name']) || !$userinfo['name']) {
          Log::info("name undefined");
          return false;
        }
        if(!isset($userinfo['email_id']) || !$userinfo['email_id']) {
          Log::info("email_id undefined");
          return false;
        }
        if(!isset($userinfo['password']) || !$userinfo['password']) {
          Log::info("password undefined");
          return false;
        }
        $emailid_verified = 0;
        if($snsinfo) {
          if(!is_array($snsinfo) || count($snsinfo) < 2){
            Log::info("SNS info is set but some thing missing, input = " . print_r($snsinfo, true));
            return false;
          }
          $emailid_verified = 1;
        }
        $uuid = self::generate();
        //$username = $userinfo['username'];
        //$name     = $userinfo['name'];
        //$email_id = $userinfo['email_id'];
        //$password = $userinfo['password'];
        $create_ts = new Zend_Db_Expr('NOW()');
        if(!$uuid) {
          Log::err("Can not generate unique id");
          return false;
        }
        $data = array(
                  'uuid'             => $uuid,
                  'username'         => $userinfo['username'],
                  'name'             => $userinfo['name'],
                  'email_id'         => $userinfo['email_id'],
                  'password'         => $userinfo['password'],
                  'location'         => $userinfo['location'],
                  'create_ts'        => $create_ts,
                  'emailid_verified' => $emailid_verified
                );
        $this->_db->insert('User', $data);
        $result = $this->_db->lastInsertId();
        if($result && $snsinfo) {
            self::addSnsInfo($uuid, $snsinfo);
        } 
        return $result; //This is last insertid
    }
    
    /**
     *  
     * 
     */
    public function updateUserLocation($id, $location) {
        $data = array(
            "location" => $location
        );
        $where = array(
            "id = ? " => $id
        );
        return $this->_db->update('User', $data, $where);
    }
    
    /**
     *  
     * 
     */
    public function addSnsInfo($uuid, $snsinfo) {
        if(!isset($snsinfo['snsserviceid']) || !$snsinfo['snsserviceid']) {
          Log::info("snsservice id undefined");
          return false;
        }
        if(!isset($snsinfo['snsuserid']) || !$snsinfo['snsuserid']) {
          Log::info("snsuser id undefined");
          return false;
        }
        $snsserviceid = $snsinfo['snsserviceid'];
        $snsuserid = $snsinfo['snsuserid'];
        $snsaccesstoken = $snsinfo['snsaccesstoken'];
        $create_ts = new Zend_Db_Expr('NOW()');
        $data = array(
                  'uuid' => $uuid,
                  'sns_service_id' => $snsserviceid,
                  'sns_user_id' => $snsuserid,
                  'sns_access_token' => $snsaccesstoken,
                  'create_ts' => $create_ts
                );
        $this->_db->insert('Snsinfo', $data);
        $result = $this->_db->lastInsertId();
        return $result;
    }
  
    public function updateAccessTokenBySnsId($snsuserid, $token) {
        $data = array(
            "sns_access_token" => $token
        );
        $where = array(
            "sns_user_id = ? " => $snsuserid
        );
        return $this->_db->update('Snsinfo', $data, $where);
    }   
    
   /**
    * getExtraUserInfo 
    * @access public
    * @param $userId int
    * @return $resultset from DB 
    *
    */ 
    public function getExtraUserInfo($userId){
        $select = $this->_db->select()
                  ->from(array('U' => 'User'))
                  ->where('U.id = ?', $userId);
        $result = $this->_db->fetchAll($select);
        if(count($result) === 0) {
         return false;
        }
        $result = $result[0];
        Log::info("uuid for user : ".$result['uuid']);
        $select1 = $this->_db->select() 
                   ->from(array('SN' => 'Snsinfo'))
                   ->where('SN.uuid=?',$result['uuid']);
        $result1= $this->_db->fetchAll($select1);
        if(count($result1) === 0) {
         return false;
        }
        $result['snsInfo'] = $result1[0];
        return $result; 
    }
    
    /**
     * GetAccessToken for FB
     * @param userId
     * @return accessTokenFb:FB accessToken
     * @access public
     * @tables User, SnsInfo
     */ 
    public function getAccessToken($userId){
        $select = $this->_db->select()
                 ->from(array('U' => 'User'))
                 ->where('U.id = ?', $userId);
        
        $user = $this->_db->fetchAll($select);
        $uuid = $user[0]['uuid'];
        
        $select1 = $this->_db->select()
                  ->from(array('SN' => 'Snsinfo'))
                  ->where('SN.uuid = ?', $uuid);
  
        $sns = $this->_db->fetchAll($select1);
        return $sns[0];
    } 
    
    private function generate() {
        $id = uniqid();
        $id = base_convert($id, 16, 2);
        $id = str_pad($id, strlen($id) + (8 - (strlen($id) % 8)), '0', STR_PAD_LEFT);
        
        $chunks = str_split($id, 8);
        //$mask = (int) base_convert(IDGenerator::BIT_MASK, 2, 10);
        
        $id = array();
        foreach ($chunks as $key => $chunk) {
          //$chunk = str_pad(base_convert(base_convert($chunk, 2, 10) ^ $mask, 10, 2), 8, '0', STR_PAD_LEFT);
          if ($key & 1) {  // odd
             array_unshift($id, $chunk);
          }else{         // even
             array_push($id, $chunk);
          }
        }
        return base_convert(implode($id), 2, 36);
    } 
    
    public function changePassword($emailId, $password) {
        //$table = new Zend_Db_Table('User');
        $username = null;
        $result = null;
        $select = $this->_db->select()
        ->from('User')
        ->where('email_id = ?', $emailId);
        $result = $this->_db->fetchAll($select);
        $username = $result[0]['username'];
        Log::info("changing password for username =".$username);
        $data = array(
                 "password" => $password
                );
        $where = array(
                 "username = ? " => $username
                ); 
        return $this->_db->update('User', $data, $where); 
        
    }
    
    public function emailIdVerified($email_id) {
        $data = array(
             "emailid_verified" => 1
        );
        $where = array(
             "email_id = ? " => $email_id
        );
        return $this->_db->update('User', $data, $where);
    }
    
    public function getUsersList($userid) {
        $result = null;
        $select = $this->_db->select()
                  ->from('User')
                  ->where('id != ?', $userid);
        $result = $this->_db->fetchAll($select);
        //    Log::info($result);
        return $result;
    }
}
