<?php

/**
 * @author rishabhmhjn
 *
 */
class Project_Controller_Basemobile extends Zend_Controller_Action {

	const APP_SECRET = "wFgKHF5Fq8XsGko0kjxkJDWiV9DOekIZ";
  public $_user = null;

  /**
   * Application configurations stored in /application/configs/aconfig.ini
   *
   * @var Zend_Registry Object
   */
  protected $_aconfig;

  public function init() {
    //    $this->checkUserLogin();
    Log::info(__METHOD__);
    self::checkUserValidity();
//    $this->_aconfig = Zend_Registry::get('aconfig');
  }

  public function currPageURL() {
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && ($_SERVER["HTTPS"] == "on")) {
      $pageURL .= "s";
    }
    $pageURL .= "://";
    if (isset($_SERVER["SERVER_PORT"]) && ($_SERVER["SERVER_PORT"] != "80")) {
      $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
    } else {
      $pageURL .= $_SERVER["SERVER_NAME"];
    }
    return $pageURL;
  }

  public function staticpath() {
    return "/static/";
    //		return $this->currPageURL() ."/static/";
  }

  public function jspath() {
    return $this->staticpath() . 'js/';
  }

  public function csspath() {
    return $this->staticpath() . 'css/';
  }

  public function loadjs($files) {
    $jspath = $this->jspath() . $this->controllername . '/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headScript()->appendFile( $jspath . $value . '.js');
      }
    }
  }

  public function loadCommonJs($files) {
    $jspath = $this->jspath() . 'common/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headScript()->appendFile( $jspath . $value . '.js');
      }
    }
  }

  public function loadExternalJs($files) {
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headScript()->appendFile($value);
      }
    }
  }

  public function loadcss($files) {
    $csspath = $this->csspath() . $this->controllername . '/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headLink()->appendStylesheet($csspath . $value . '.css');
      }
    }
  }

  public function loadCommonCss($files) {
    $csspath = $this->csspath() . 'common/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headLink()->appendStylesheet($csspath . $value . '.css');
      }
    }
  }
  
  private function checkUserLogin() {

    // Facebook Check
    $config = array();
    $config['appId'] = '270237853057532';
    $config['secret'] = '88f5532d51c9be808771522acb412a5b';
    $config['fileUpload'] = false; // optional

    $this->facebook = new Facebook_ApiFacebook($config);

  }

  private function checkUserValidity(){
    //  	Log::info(__METHOD__);
    $client_access_token = $this->_request->getParam("access_token");
    if (!$client_access_token) {
    	return false;
    }
    
    $token = new Project_Business_Tokens();
    $result = $token->validateAccessToken($client_access_token);
    if($result) {
      //  		Log::info("User is valid user");
      $this->_user = true;
      return true;
    } else {
      //  		Log::info(__METHOD__ . ", User Not valid user");
    }
  }
}
