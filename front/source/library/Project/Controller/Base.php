<?php

/**
 * @author rishabhmhjn
 *
 */
class Project_Controller_Base extends Zend_Controller_Action {

  const ERROR_CONTROLLER    = "error";
  const LOGIN_CONTROLLER    = "index";
  const LOGIN_ACTION        = "login";
 
  public $_user = null;
  public $_hostname = null;
  

  /**
   * Application configurations stored in /application/configs/aconfig.ini
   *
   * @var Zend_Registry Object
   */
  protected $_aconfig;

  /**
   * The view mode string (smart|default)
   * @var string
   */
  protected $_view_mode;

  public function init() {
      
    $controller = $this->getRequest()->getControllerName();
    $action = $this->getRequest()->getActionName();
    $this->_hostname = Project_Business_Base::getHostUrl();
    
    // No login required controllers
    $noLoginControllers = array("movie","vob","stats");
    if (!self::noSessionaction($controller, $action)) {
        if(self::checkUserValidity()) {
            $this->view->user = $this->_user;
            $this->view->userid = $this->_user->id;
            $this->view->userdisplayname = $this->_user->name;
            $this->view->userimageURL = $this->_user->img;
        } else {
            if (strtolower($controller) == self::LOGIN_CONTROLLER) {
                $this->_forward(self::LOGIN_ACTION);
            } else {
                $this->_redirect(self::loginurl());
                exit;
            }
        }
        if ($this->view) {
            $this->view->popupstemplate = APPLICATION_PATH ."/views/templates/fanyboxpopup/popups.phtml";
        }
    }elseif (in_array(strtolower($controller), $noLoginControllers)) {
        if(self::checkUserValidity()) {
            $this->view->user = $this->_user;
            $this->view->userid = $this->_user->id;
            $this->view->userdisplayname = $this->_user->name;
            $this->view->userimageURL = $this->_user->img;
        }
    }
    
    
    $params = $this->_request->getParams();
    $fbcode = isset($params["code"]) && $params["code"];
    if(!$this->_user && $fbcode) {
    	$url = "/" . $controller . "/" . $action;
    	$url = urlencode($url);
    	Log::info($url);
    	$this->_redirect("/auth/facebooklogin/?redirect_url=" . $url);
    }
    
    
    $externaleJSfiles = array('http://code.jquery.com/jquery-1.7.1.min.js');
    $this->loadExternalJs($externaleJSfiles);
    // setting up environment specific vairables
    $this->_aconfig = Zend_Registry::get('aconfig')->{APPLICATION_ENV};
    $this->_setViewMode();
  }
  
  public function __call($method, $args) {
//      Log::info($method);
      if ('Action' == substr($method, -6)) {
      // If the action method was not found, forward to the
      // index action
//      return $this->_forward('notfound');
      if (method_exists($this, 'undefinedaction')) {
          $this->undefinedaction();
          return;
      }
    }
    // all other methods throw an exception
    throw new Exception('Invalid method "'
        . $method
        . '" called',
        500);
        exit;
  }
  
  
  private function loginurl() {
      return "/" . self::LOGIN_CONTROLLER . "/" . self::LOGIN_ACTION . "/";
  }
  
  private function errorurl() {
    return "/" . self::ERROR_CONTROLLER;
  }
  
  
  private function noSessionaction($controllerName, $actionName) {
      //Should always lower case
      $controllers = array("passwordreset", "movie","vob","stats");
      $actions = array("login",
                       "validatelogincredential",
                       "registration",
                       "registeruser",
                       "facebooklogin",
                       "sendverficationlink",
                       "loginwithsns",
                       "movievob",
                       "uservob",
                       "getpopularmovies"
                       );
      if (in_array(strtolower($controllerName), $controllers)) {
      return true;
    }
                       
      if (in_array(strtolower($actionName), $actions)) {
          return true;
      }
      return  false;
  }

  private function _setViewMode()
  {
      $this->_view_mode = 'default';
  }

  public function currPageURL() {
    $pageURL = 'http';
    if (isset($_SERVER["HTTPS"]) && ($_SERVER["HTTPS"] == "on")) {
      $pageURL .= "s";
    }
    $pageURL .= "://";
    if (isset($_SERVER["SERVER_PORT"]) && ($_SERVER["SERVER_PORT"] != "80")) {
      $pageURL .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"];
    } else {
      $pageURL .= $_SERVER["SERVER_NAME"];
    }
    return $pageURL;
  }

  public function staticpath() {
    return "/static/";
  }

  public function jspath() {
    return $this->staticpath() . 'js/';
  }

  public function csspath() {
    return $this->staticpath() . 'css/';
  }

  public function loadjs($files) {
    $jspath = $this->jspath() . strtolower($this->controllername) . '/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headScript()->appendFile( $jspath . $value . '.js');
      }
    }
  }

  public function loadCommonJs($files) {
    $jspath = $this->jspath() . 'common/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headScript()->appendFile( $jspath . $value . '.js');
      }
    }
  }
  
  public function loadLibJs($files) {
    $libjsPath = $this->staticpath() . 'res/js/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headScript()->appendFile( $libjsPath . $value . '.js');
      }
    }
  }
  

  public function loadExternalJs($files) {
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headScript()->appendFile($value);
      }
    }
  }

  public function loadcss($files) {
    $csspath = $this->csspath() . strtolower($this->controllername) . '/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headLink()->appendStylesheet($csspath . $value . '.css');
      }
    }
  }

  public function loadCommonCss($files) {
    $csspath = $this->csspath() . 'common/';
    if($files && is_array($files)) {
      foreach ($files as $value) {
        $this->view->headLink()->appendStylesheet($csspath . $value . '.css');
      }
    }
  }

  private function checkUserLogin() {

    // Facebook Check
    $config = array();
    $config['appId'] = '270237853057532';
    $config['secret'] = '88f5532d51c9be808771522acb412a5b';
    $config['fileUpload'] = false; // optional

    $this->facebook = new Facebook_ApiFacebook($config);

    // /Facebook Check
    // Twitter Check
  }

  private function checkUserValidity(){
    $user = new Project_Business_User();
    $userid = $user->checkAuthenticatedUser();
    if(!$userid) {
        return null;
    }
    $userDetail = $user->getUserbyID($userid);
    if (!$userDetail) {
      Log::alert(__METHOD__ . ", error in getting user detail for user id: ". $userid);
      return false;
    }
    $this->_user = $userDetail;
    
    $this->_user = $user->getUserbyID($userid);
    return true;
  }
}
