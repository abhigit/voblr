<?php

class Bootstrap extends Zend_Application_Bootstrap_Bootstrap
{
  protected function _initAutoload() {

    date_default_timezone_set('Asia/Tokyo');
    $autoloader = Zend_Loader_Autoloader::getInstance();
    $autoloader->registerNamespace('Project_');
    $autoloader->setFallbackAutoloader(true);
    $autoloader->suppressNotFoundWarnings(false);
    set_error_handler("Log::error_handler");
    
    $options = array(
//      'keyPrefix' => 'SESSIONS_', // Delete below
      'keyPrefix' => '',
      'lifetime'  => 100 * 60 * 15, // 15 minutes
      'rediska'   => array('servers' =>  array( array('host' => '54.248.110.27', 'port' => 6379)))
      );

    require_once 'Rediska.php';
    $saveHandler = new Rediska_Zend_Session_SaveHandler_Redis($options);
    Zend_Session::setSaveHandler($saveHandler);
    
  }

  protected function _initConfig()
  {
    $aconfig = new Zend_Config_Ini(APPLICATION_PATH . '/configs/aconfig.ini');
    Zend_Registry::set('aconfig', $aconfig);
  }

  protected function _initUserAgent() {
    $agent = $this->getPluginResource('useragent')->getUserAgent();
    Zend_Registry::set('useragent', $agent);
  }

  // doctype ams add

  protected function _initDoctype()
    {
        $this->bootstrap('view');
        $view = $this->getResource('view');
        $view->doctype('HTML5');
    }
}

