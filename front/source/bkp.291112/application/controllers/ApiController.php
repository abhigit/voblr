<?php

class ApiController extends Project_Controller_Base
{

  /**
   * @var Zend_Rest_Server
   *
   */
  public function init()
  {
    parent::init();
    //        Log::info(__METHOD__);

    // disable view rendering
    $this->_helper->viewRenderer->setNoRender(true);
    // disable layout
    $this->_helper->layout->disableLayout();
    $action = $this->getRequest()->getActionName();
  }

  public function unautherizeduserAction() {
    $final_response = '{"status"="0" , "message"="Login required", "code"="010101"}';
    $this->getResponse()->setHttpResponseCode(401)->setRawHeader('HTTP/1.1 401 Not Found'); // optional
    $this->getResponse()->appendBody($final_response);
  }
  
  public function __call($method, $args)
  {
    if ('Action' == substr($method, -6)) {
      // If the action method was not found, forward to the
      // index action
      return $this->_forward('notfound');
    }

    // all other methods throw an exception
    throw new Exception('Invalid method "'
        . $method
        . '" called',
        500);
  }  

  public function notfoundAction(){ 
    $data = array("message" => "invalid action");
    $this->_returnResponse(false, array("test", "data" => $data));
  }
  
  private function _returnResponse($success = false, $response = null)
  {
    $status = 400;
    $message = "Bad request";
    $data = array();
      if (!$success) { //this is error
      $this->getResponse()->setHttpResponseCode(400)->setRawHeader('HTTP/1.1 400 Bad Request'); // optional
      } else { //this is success
          $status = 200;
          $message = "OK";
      $this->getResponse()->setHttpResponseCode(200)->setRawHeader('HTTP/1.1 200 OK'); // optional
      }
      
      if($response && !is_array($response)) {
          $message = $response;
      }
      
      if (is_array($response)) {
          $data = $response;
      }
      
    $final_response = json_encode(array("status" => $status, "message" => $message , "data" => $data));
    $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8')->appendBody($final_response);
  }

  public function registeruserAction() {
    $params = $this->_request->getParams();
    $user = new Project_Business_User();
    $result = $user->addUserinfo($params);
    if($result && $result > 0) {
      $user->postRegisteration($params);
      $this->_forward('validatelogincredential');
      return;
    }
    $this->_returnResponse(false, "error in registering user");
  }

  private function getJSON($status, $msg){
    $ret = array("status"=>$status, "message"=>$msg);
    $ret = json_encode($ret);
    $this->getResponse()->clearBody();
    $this->getResponse()->setHeader('Content-Type', 'application/json')->appendBody($ret);
    //$this->setResponse($ret);
  }
   
   
  /**
   * authentication action
   *
   */
  public function validatelogincredentialAction(){
    $params = $this->_request->getParams();
    $username = isset($params['username']) ? $params['username'] : '';
    $password = isset($params['password']) ? $params['password'] : '';
    $msg = "username or password is incorrect";
    if (!$username) {
      $this->_returnResponse(false , $msg);
      return false;
    }
    if (!$password) {
      $this->_returnResponse(false , $msg);
      return false;
    }
    
    $login = null;
    
    
    if (isset($params['client_type']) && $params['client_type'] == "smartphone") {
      $login = new Project_Business_Tokens();
        
    } else {
      $login = new Project_Business_Auth();
    }
    $result = $login->checkLogin($username, $password);
    //Logger For Login result
    if($result){
      $this->_returnResponse(true , $result);
      return true;
    }
    $this->_returnResponse(false, $msg);
    return true;
  }
  
  
  public function loginwithsnsAction() {
    $params = $this->_request->getParams();
    $snsuserid = isset($params['sns_user_id']) ? $params['sns_user_id'] : null;
    $accessToken = isset($params['sns_access_token']) ? $params['sns_access_token'] : null;
    $snsid = isset($params['sns_id']) ?  $params['sns_id'] : 'FACEBOOK';
    
    if (!$snsuserid || !$accessToken || !$snsid) {
        $msg = "either snsuserid or accesstoken or snsid not recevied";
      Log::err(__METHOD__ .", msg: " . $msg);
      $this->_returnResponse(false, $msg);
      return true;
    } else {
        Log::info(__METHOD__ .", Checking user exits or not");
    }
    
    $user  = new Project_Business_User();
    $result = $user->checkLoginWithsns($snsuserid, $accessToken, $snsid);
    
    if($result && is_array($result)){
      $this->_returnResponse(true , $result);
      return true;
    }
    $this->_returnResponse(false, $result);
    return true;
  }

  /**
   * getuserinfo : Get UserInfo from mysql
   *
   * @param $requestuserId int
   * @return JSON
   * @access public
   * TODO 
   */
   public function getuserinfoAction(){
      $params = $this->_request->getParams();
      $callback = '';
      Log::info("request coming getuserinfo");
      if($this->_user->id){
        $callback = $params['callback'];
        Log::info(" getRequestUserInfo callback".$callback);
        $user = new Project_Business_User();
        Log::info(" getRequestUserInfo for ".$this->_user->id);
        if(isset($params['request_user_id'])){ 
          //UserInfo for other User 
          Log::info(" getRequestUserInfo for other User ".$params['request_user_id']);
          $userInfo = $user->getExtraUserInfo($params['request_user_id']);
          $this->responseWithCallback($userInfo, $callback); 
        }elseif(isset($params['request_user_id_arr'])){
          //UserInfo for array of other users
          $userInfoArr = array();
          foreach($params['request_user_id_arr'] as $requestUser){
            $userInfo = $user->getExtraUserInfo($resquestUser['request_user_id']);
            $userInfoArr = array_push($userInfo);
          }
          $this->responseWithCallback($userInfoArr, $callback); 
        }else{
          // Current Logged In user
          $userInfo = $user->getExtraUserInfo($this->_user->id);
          $this->responseWithCallback($userInfo, $callback); 
        }
      }else{
        Log::info("User not logged in");
        $this->getJSON(91, "No login User");
      }
   }

   /**
    * responseWithCallback
    *
    * @access private
    * @param  $result Array : result
    * @param  $callback string : callback name 
    * 
    */
    private function responseWithCallback($result, $callback){
      $this->getResponse()->clearBody();
      $res = array();
      $res['status']  = 21;
      $res['message'] = "OK";
      $res['result']  = $result;
      if($callback != ''){ 
        $resp = $callback.'('.json_encode($res).')';
      }else{
        $resp = json_encode($res);
      }
      $this->getResponse()->setHeader('Content-Type', 'application/json')->appendBody($resp);
    }
    
    public function sendverficationlinkAction() {
      $params = $this->_request->getParams();
      $username = isset($params['username']) ? $params['username'] : null;
      $emailid = isset($params['emailid']) ? $params['emailid'] : null;
      if ((!$username || $username == "") && (!$emailid || $emailid == "")) {
          $msg = "emailid or username empty";
          Log::info(__METHOD__ . ", " . $msg);
          $this->_returnResponse(false , $msg);
        return;
      }
      
      $username = $emailid ? $emailid : $username;
      $auth = new Project_Business_Auth();
      $result = $auth->sendverficationlink($username);
      if ($result && is_array($result)) {
        $this->_returnResponse(true , $result);
        return;
      }
      $this->_returnResponse(false , $result);
      return;
    }
    
   /**
    * Post Feed on FB
    * 
    */
    public function  postFbAction(){
       // Get Params
       $params = $this->_request->getParams();
 
       Log::info("request coming postVobFb");

       if($this->_user->id){
           Log::info(" user postVobFb_".$this->_user->id);
           if(isset($params['target']) && isset($params['type'])){
               Log::info(" target & type Is".$params['target']."&".$params['type']);
               $fb = new Project_Business_Facebook();
               $userId = $this->_user->id;
               $result = $fb->postFeedFb($target, $userId, $type);
               if($result){
                   Log::info("Posted on FB check"+$this->_user->id);
                   $this->getJSON("21", "Posted");
               }else{
                   $this->getJSON("31", "Problem in Posting");
               }  
           }
       }else{
         Log::info("User not logged in");
         $this->getJSON(91, "No login User");
       }
    }
}
