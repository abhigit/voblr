<?php

class MpiController extends Project_Controller_Basemobile
{

    /**
     * @var Zend_Rest_Server
     *
     */
    public function init()
    {
    	parent::init();
    	Log::info(__METHOD__);
//        Log::info($this->_user);

        // disable view rendering
        $this->_helper->viewRenderer->setNoRender(true);
        // disable layout
        $this->_helper->layout->disableLayout();
        $action = $this->getRequest()->getActionName();
      if(!$this->_user && ($action !== 'registeruser'  && $action != 'validatelogincredential' && $action != 'validatelogincredentialusingsns')) {
          Log::info("unauthorized user for action: " . $action);
          $this->_forward('unautherizeduser');
      }
    }
    
    public function unautherizeduserAction() {
      $final_response = '{"status"="0" , "message"="Login required", "code"="010101"}';
      $this->getResponse()->appendBody($final_response);
    }
    
    public function __call($method, $args)
    {
    	if ('Action' == substr($method, -6)) {
            // If the action method was not found, forward to the
            // index action
            return $this->_forward('notfound');
        }
 
        // all other methods throw an exception
        throw new Exception('Invalid method "'
                            . $method
                            . '" called',
                            500);
    }
    public function indexAction()
    {
    	$provider = new Project_Provider_User();
    	Log::info(__METHOD__);
    	$result = $provider->getUserbyUUID('abcd000000001');
    	Log::info($result);
    	$result = $provider->getUserdatabysnsid('1','1');
    	Log::info($result);
    	
		$this->_forward('notfound');
		return;
        $images = array("http://photos-f.ak.fbcdn.net/hphotos-ak-ash4/315964_2173209171496_1284785957_32013457_1202502561_n.jpg",
        				"http://photos-e.ak.fbcdn.net/hphotos-ak-ash4/385448_2173206331425_1284785957_32013453_1827814228_n.jpg",
        				"http://photos-e.ak.fbcdn.net/hphotos-ak-ash4/316582_2173223051843_1284785957_32013464_92537643_n.jpg");
        $this->_returnResponse(array("test", "data" => $images));
    }
    
    public function notfoundAction(){
    	$data = array("message" => "invalid action");
        $this->_returnResponse(array("test", "data" => $data));
    }
    private function _returnResponse($response = null)
    {
    	if($response) {
    		$message = array("status" => true , "message" => "FINISH-OK" , "response" => $response);
    	} else {
    		$message = array("status" => false , "message" => "some error occured");
    	}
    	$final_response = json_encode($message, JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE | JSON_FORCE_OBJECT);
      
    	if(!$final_response) {
    		$final_response = '{"status"="0" , "message"="some error occured", "code"="-1"}';
    	}
    	$this->getResponse()->appendBody($final_response);
    }

    private function _returnError($response)
    {
    	Log::info(__METHOD__);
      $message = array("status" => false , "message" => $response);
      $final_response = json_encode($message);
      if(!$final_response) {
        $final_response = '{"status"="0" , "message"="some error occured", "code"="-1"}';
      }
      $this->getResponse()->appendBody($final_response);
    }
    
    
    public function getimagesAction()
    {
    	$image_path = 'static/images/theme/';
//    	$images = array($image_path.'default/1.png',
//    					$image_path.'default/2.png',
//    					$image_path.'default/3.png',
//    					$image_path.'default/blank.png'
//    					);
//		$result = array("this is response");
    	  $result = array(array("id"=>100 , "icon" => $image_path.'default/blank.png'),
    	  				  array("id"=>101 , "icon" => $image_path.'default/1.png'),
    	  				  array("id"=>102 , "icon" => $image_path.'default/2.png'),
    	  				  array("id"=>103 , "icon" => $image_path.'default/3.png')
    	  				);
//        $images = array("http://photos-f.ak.fbcdn.net/hphotos-ak-ash4/315964_2173209171496_1284785957_32013457_1202502561_n.jpg",
//        				"http://photos-e.ak.fbcdn.net/hphotos-ak-ash4/385448_2173206331425_1284785957_32013453_1827814228_n.jpg",
//        				"http://photos-e.ak.fbcdn.net/hphotos-ak-ash4/316582_2173223051843_1284785957_32013464_92537643_n.jpg");
        $this->_returnResponse($result);
    }
    
    public function getredirectionurlAction() {
//    	 $prov = new Project_Business_Facebook();
//	     $result = $prov->getredirectionurl();
//		 if(!$result){
//			 $result = array("msg" => "can not retirve url");
//		 }
//	     $this->_returnResponse(array("url" => $result));
	     $this->_returnResponse();
    }
    public function getsessionAction() {
//    	 $prov = new Project_Business_Facebook();
//	     $result = $prov->getSession();
//		 if(!$result){
//			 $result = array("session" => "unable to get Session variables");
//		 }
//	     $this->_returnResponse(array("session" => $result));
		 $result = "Empty";
	     $this->_returnResponse(array("token" => $result));
    }
    public function gettokenAction() {
//    	 $prov = new Project_Business_Facebook();
//	     $result = $prov->getToken();
//		 if(!$result){
//			 $result = array("token" => "unable to get getToken");
//		 }
		 $result = "Empty";
	     $this->_returnResponse(array("token" => $result));
    }
    public function getalbumsAction() {
//    	 $prov = new Project_Business_Facebook();
//	     $result = $prov->getAlbums();
//		 if(!$result){
//			 $result = array("msg" => "unable to get Images");
//		 }
//	     $this->_returnResponse($result);
	     $this->_returnResponse(array());
    }
    public function getalbumphotosAction() {
//    	 $albumid = $_REQUEST[id];
//    	 $limit = $_REQUEST[limit];
//    	 if(!$albumid) {
//		     $this->_returnResponse(array("albumid" => "notdefined"));
//		     return true;
//    	 } else if(!$limit) {
//		     $this->_returnResponse(array("count" => "notdefined"));
//		     return true;
//    	 }
//    	 $prov = new Project_Business_Facebook();
//	     $result = $prov->getAlbumPhotos($albumid, $limit);
//		 if(!$result){
//			 $result = array("msg" => "unable to get photos");
//		 }
//	     $this->_returnResponse($result);
	     $this->_returnResponse(array());
    }
    public function gettaggedphotosAction() {
	     $result = "";
		 if(!$result){
			 $result = array("msg" => "unable to get tagged photos");
		 }
	     $this->_returnResponse($result);
    }
    public function deletefromfacebookAction() {
    	 $id = "none";
		 if(!$result){
			 $result = array("msg" => "unable to delete " . $id);
		 }
	     $this->_returnResponse($result);
    }
    
    public function gettimagefromurlAction() {
//    	$this->_redirect($url)
//    	return;
	     $this->_returnResponse("");
    }


    public function getpicsourceAction() {
    	Log::info(__METHOD__);
    	$response = file_get_contents("http://profile.ak.fbcdn.net/hprofile-ak-snc4/174140_1284785957_1678563435_q.jpg");
    	$this->getResponse()->appendBody($response);
    }
    public function picturepostedAction() {
    	$this->_returnResponse("success");
    }
    
    public function getallmoviesAction() {
      $result = Project_Business_Movie::getMovies(10,1);
    	$this->_returnResponse($result);
    }
    
    public function registeruserAction() {    	
      $params = $this->_request->getParams();
      if(!isset($params['app_secret']) || !$params['app_secret'] || $params['app_secret'] != self::APP_SECRET) {
      	$this->_returnResponse("app secret not specified or in correct");
      	return true;
      }

      $user = new Project_Business_User();
    	$result = $user->addUserinfo($params);
    	
    	if($result && $result > 0) {
          $this->_forward('validatelogincredential');
          return;
    	}
    	$this->_returnResponse();
    }
    
    public function vobitAction() {
      $this->_request->setParam('userid' , $this->_user->id);
    	$params = $this->_request->getParams();
    	$objectid = isset($params['id']) ? $params['id'] : null;
      $userid = isset($params['userid']) ? $params['userid'] : null;
    	if(!$objectid) {
    		$this->returnError("undefined id for vobbing");
    		return;
    	}
      Log::info("Current user id:" . $userid);
    	$vobs = new Project_Business_Vobs();
    	$result = $vobs->vobit($params);
      Log::info("#" . $objectid . " vobbed successfully ");
      $this->_returnResponse($result);
    }

   public function gettimelineAction(){
     if($this->_user->id){
    	$vobs= new Project_Business_Vobs();
    	$result = $vobs->getTimeLine($this->_user->id);
    	$this->_returnResponse($result);
     }else{
       // error json
       $this->getErrorJSON(91, "No user ID");
     }
   }
  

   public function getnewsfeedAction(){
        if($this->_user){
    	$subscribers = new Project_Business_Subscribers();
        $result = $subscribers->getSubscribers($this->_user);
       	$vobs = new Project_Business_Vobs();
        $comp_result = array();
        if($result){
          foreach($result as $u){
    	    $resultt = $vobs->getTimeLine($u['object_id']);
            array_push($comp_result, $resultt);
          }
        }
        $this->_returnResponse($comp_result && is_array($comp_result) && count($comp_result) == 1 ? $comp_result[0] : $comp_result);
     }else{
       $this->getErrorJSON(91, "No login User");
     }

   }

   public function getuserlistAction(){
     if($this->_user->id){
    	$user = new Project_Business_User();
        $userlist = $user->getUserList($this->_user->id);
        $this->_returnResponse($userlist);
     }else{
       $this->getErrorJSON(91, "No login User");
     }
   }
   
   public function subscribeuserAction(){
     if($this->_user->id){
        $this->_request->setParam('user_id',$this->_user->id);
        $params = $this->_request->getParams();
        if(isset($params['id']) && isset($params['user_id'])){
    	  $subscriber = new Project_Business_Subscribers();
          $result  = $subscriber->subscribe($params);
          if($result){
            $this->_returnResponse($result);
          }else{
            $this->getErrorJSON(91, "No result");
          }
        }else{
            $this->getErrorJSON(91, "param error");
        }
     }else{
       $this->getErrorJSON(91, "No login User");
     }
   }
   
   private function getErrorJSON($status, $msg){
     $ret = array("status"=>$status, "message"=>$msg);
     $this->getResponse()->appendBody($ret);
   }
   
   
  /**
   * authentication action
   *
   */
    public function validatelogincredentialAction(){
    	Log::info(__METHOD__);
        $params = $this->_request->getParams();
        
//        if(!isset($params['app_secret']) || !$params['app_secret'] || $params['app_secret'] != self::APP_SECRET) {
//	        $this->_returnResponse("app secret not specified or in correct");
//	        return true;
//        }

        $username = isset($params['username']) ? $params['username'] : '';
        $password = isset($params['password']) ? $params['password'] : '';
        if (!$username) {
        	Log::info("username not defined");
            $this->_returnResponse();
            return false;
        }
        if (!$password) {
          Log::info("password not defined");
            $this->_returnResponse();
            return false;
        }
        $login = new Project_Business_Tokens();
        $result = $login->checkLogin($username, $password);
        //Logger For Login result
        if($result){
        	 $result["login"] = "ok";
           $this->_returnResponse($result);
           return true;
        }
        $this->_returnResponse("username or password is incorrect");
        return true;
    }
    
    
  /**
   * authentication action
   * 
   */
    public function validatelogincredentialusingsnsAction(){
        Log::info(__METHOD__);
        $params = $this->_request->getParams();
        if(!isset($params['app_secret']) || !$params['app_secret'] || $params['app_secret'] != self::APP_SECRET) {
          $this->_returnResponse("app secret not specified or in correct");
          return true;
        }

        $snsuserid = isset($params['snsuserid']) ? $params['snsuserid'] : '';
        $snsid = isset($params['snsid']) ? $params['snsid'] : null;
        
        if (!$snsuserid) {
            Log::err("snsuserid not defined");
            $this->_returnResponse();
            return false;
        }
        
        if (!$snsid) {
            Log::err("snsid not defined");
            $this->_returnResponse();
            return false;
        }
        
        $login = new Project_Business_Tokens();
        $result = $login->checkLoginForSns($snsuserid, $snsid);
        //Logger For Login result
        if($result){
           $result["login"] = "ok";
           $this->_returnResponse($result);
           return true;
        }
        
        $this->_returnResponse("username or password is incorrect");
        return true;
    }
}
