<?php

class MovieController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'movie';


  
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
    
	$commonJsFiles = array('modernizr-2.6.1-respond-1.1.0.min','bootstrap.min','mustache','jquery.autosize-min','jquery.imagesloaded.min','mosaic.1.0.1.min','Api','Log','jquery.shopholic');
// removed Moviebase.js
    $this->loadCommonJs($commonJsFiles);
    
    
    $cssfiles = array('bootstrap','bootstrap-responsive','main');
    $this->loadCommonCss($cssfiles);
    
  }
  
  public function indexAction() {
  	//http://bhupi.voblr.com/movie/tt1055369/
    $requestURI  = $this->getRequest()->getRequestUri();
    $requestURI = explode("/", $requestURI);
    
//    Log::info($requestURI);
    if (!isset($requestURI[2])) {
      $this->_redirect("/movies/");
      return;
    }
    
    $movieid = $requestURI[2];
    $movie = new Project_Business_Movie();
    $movieDetail = $movie->getmovieDetail($movieid);
    if (!$movieDetail) {
    	$this->_redirect("/movies/");
      return;
    }
    
    $this->view->moviedetail = $movieDetail;
    
      $js = array("index");
      $this->loadjs($js);
//      $css = array("index");
//      $this->loadcss($css);
  }
  
  protected function undefinedaction() {
//  	Log::info(__METHOD__);
  	$this->_forward('index');
  	return;
  }
  
}
