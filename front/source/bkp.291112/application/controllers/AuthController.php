<?php

/***
 * AuthController for handling user Authentication
*
*/
class AuthController extends Zend_Controller_Action{
	
	const DEFAULT_PASSWORD_LENGTH = 8;

  /**
   * Initialize method
   * set view null, disable layout
   */
  public function init(){
    /* Initialize action controller here */
    $this->_helper->viewRenderer->setNoRender(true);
    // disable layout
    $this->_helper->layout->disableLayout();
  }

  /**
   *
   *
   */
  public function indexAction(){
    exit;
    // action body
  }

  /**
   * authentication action
   *
   */
  public function loginAction(){
    if($_POST){
      $params = $this->_request->getParams();
      $username = isset($params['username']) ? $params['username'] : '';

      $password = isset($params['password']) ? $params['password'] : '';
      if (!$username) {
        Log::info("username is missing");
        $this->_redirect('/');
      }
      if (!$password) {
        Log::info("password is missing");
        $this->_redirect('/');
      }
      $login = new Project_Business_Auth();
      $result = $login->checkLogin($_POST['username'], $_POST['password']);

      //Logger For Login result
      Log::info("Login_result " . ($result ? "TRUE" : "False") . "\n---------\n");
      if($result){
        //           Log::info($result);
          
        $this->_redirect('/');
      }else{
        // Redirect To Login error page
        //           echo "Login error";
        $this->_redirect('/');
      }
    }else{
      // Redirect to Home Page
      //         echo "No Post";
      $this->_redirect('/');
    }
  }

  /**
   *  check action
   *
   */


  public function isLogin(){

    if ($_COOKIE['vob_common'] != NULL || $_COOKIE['vob_common'] != "")
    {
      $_COOKIE['vob_common'] == session_id();
      Log::info("");
      return true;
    }
    else {
      return false;
    }

  }

  public function logoutAction(){
    $user = new Project_Business_User();
    $user->logMeOut();
    $this->_redirect('/');
  }


  public function facebookloginAction(){
    $userinfo = array();

    $this->facebook = new Facebook_ApiFacebook();

    $auth_info = null;
    $user_info = null;
    if ( ! $this->_request->getParam('code')) {
      $this->_redirect($this->facebook->getLoginUrl(Facebook_ApiFacebook::$LOGIN_SCOPE));
    } else {
      $this->facebook->getAccessTokenByCode($this->_request->getParam('code'));
      $auth_info = $this->facebook->getAuthInfo();
    }
    
    try {
      $snsuserid = $auth_info['uid'];
      if($snsuserid) {
        $user = new Project_Business_User();
        $result = $user->getUserdatabysnsid($snsuserid,'FACEBOOK');
        if (!$result) {
        	Log::debug("there is possibility to reduce load in facebook->getUserInfo");
        	$facebookUserInfo = $this->facebook->getUserInfo(true);
        	if (isset($facebookUserInfo['email']) && $facebookUserInfo['email']) {
	          $result = $user->getUserdatabyUsername($facebookUserInfo['email']);
	          $snsinfo = array();
	          $snsinfo['snsserviceid'] = 'FACEBOOK';
	          $snsinfo['snsuserid'] = $auth_info['uid'];
	          $snsinfo['snsaccesstoken'] = $auth_info['token'];
	          if ($result) {
	          	Log::info(__METHOD__ .", user already registered");
	          	$snsinfo = $user->addSnsInfo($result['uuid'], $snsinfo);
	          } else {
	          	Log::info(__METHOD__ .", creating FB user, details:");
//	          	Log::info($facebookUserInfo);
	          	$registerParams = array("name"=> $facebookUserInfo["name"], "username" => $facebookUserInfo["id"], "password" => self::generatePassword(), "email_id"=> $facebookUserInfo["email"], "snsinfo" => $snsinfo);
	          	self::registeruser($registerParams);
	          }
        	} else {
        		Log::err(__METHOD__ .", redirect user to error page");
        		$this->_redirect('/error/');
            die;
        	}
        }
        if($result) {
          $username = isset($result['username']) && $result['username'] ? $result['username'] : null;
          $password = isset($result['password']) && $result['password'] ? $result['password'] : null;
          if($username && $password) {
            $_POST['username'] = $username;
            $_POST['password'] = $password;
            $this->_forward('/login/');
            return;
          } else {
            $this->_redirect('/error/');
            die;
          }
        } else {
          Log::err(__METHOD__ .", users FB registration process die");
            die;
        }
      }
    }
    catch (FacebookApiException $e) {
      Log::err($e);
      $this->_forward('index');
      return;
    }
}

  private function registeruser($params) {
    $user = new Project_Business_User();
    $result = $user->addUserinfo($params);
    if($result && $result > 0) {
      $_POST['username'] = $params['username'];
      $_POST['password'] = $params['password'];
      self::loginAction();
      return;
    }
  }


	private function generateTempKey($str = '') {
		return substr(md5(time() . $str), 0, 20);
	}
	
	public function verifyemailAction () {
    /* Initialize action controller here */
    $this->_helper->viewRenderer->setNoRender(false);
    // disable layout
    $this->_helper->layout->enableLayout();

    $params = $this->_request->getParams();
    $linkid = isset($params['linkid']) ? $params['linkid'] : null;
    $errmsg = "invalid link id";
    if (!$linkid || $linkid == "") {
    	$this->view->errmsg = $errmsg;
    	return;
    }

    $auth = new Project_Business_Auth();
    $result = $auth->verifyemail($linkid);
    if ($result) {
    	return;
    }
    $this->view->errmsg = $errmsg;
    return;
	}
	
	
  private function generatePassword($length = self::DEFAULT_PASSWORD_LENGTH) {
  	$characters = array(
        "A","B","C","D","E","F","G","H","J","K","L","M",
        "N","P","Q","R","S","T","U","V","W","X","Y","Z",
        "a","b","c","d","e","f","g","h","i","j","k","m",
        "n","o","p","q","r","s","t","u","v","w","x","y","z",
        "1","2","3","4","5","6","7","8","9", "0", "-", "_");
  	shuffle($characters);
  	return implode("", array_slice($characters, 0, $length));
  }
	
}
