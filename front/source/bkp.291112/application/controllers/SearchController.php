<?php

class SearchController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'search';


  
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
    
	$commonJsFiles = array('modernizr-2.6.1-respond-1.1.0.min','bootstrap.min','mustache','jquery.autosize-min','jquery.imagesloaded.min','mosaic.1.0.1.min','Api','Log','jquery.shopholic');
// removed Moviebase.js Userbase.js
    $this->loadCommonJs($commonJsFiles);
    
    $cssfiles = array('bootstrap','bootstrap-responsive','main');
    $this->loadCommonCss($cssfiles);
  }
  
  public function indexAction() {
      $js = array("index");
      // View Title variable 
      $this->view->vTitle = "Search";
      $this->loadjs($js);
//      $css = array("index");
//      $this->loadcss($css);
  }
  protected function undefinedaction() {
    $this->_forward('index');
    return;
  }
}
