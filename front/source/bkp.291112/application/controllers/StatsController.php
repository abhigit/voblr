<?php

class StatsController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'stats';


  
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
   
	$commonJsFiles = array('modernizr-2.6.1-respond-1.1.0.min','bootstrap.min','mustache','jquery.autosize-min','jquery.imagesloaded.min','mosaic.1.0.1.min','Api','Log','jquery.shopholic');
// removed Moviebase.js Userbase.js 
// newsfeed same css and js files
    $this->loadCommonJs($commonJsFiles);
    
    
    $cssfiles = array('bootstrap','bootstrap-responsive','main');
    $this->loadCommonCss($cssfiles);
    
  }
  
  public function indexAction() {
         // FOR TESTING     	
         //      $fb = new Project_Business_Facebook();
         //      $userId = $this->_user->id;
         //      $result = $fb->autoFollow($userId);

    $requestURI  = $this->getRequest()->getRequestUri();
    $requestURI = explode("/", $requestURI);
    $userid = isset($requestURI[2]) && $requestURI[2] ? $requestURI[2] : $this->_user->id;
    $user = new Project_Business_User();
    $userinfo = $user->getUserbyID($userid);
    
//    Log::info(get_object_vars($userinfo));
    
    $this->view->statsuser = $userinfo;
    $this->view->statsuserid = $userinfo->id;
    $this->view->statsusername = $userid == $this->_user->id ? "you" : $userinfo->name;
    $js = array("index");
    $this->loadjs($js);
//
//    $css = array("index");
//    $this->loadcss($css);
  }
  
  
  protected function undefinedaction() {
    $this->_forward('index');
    return;
  }

}
