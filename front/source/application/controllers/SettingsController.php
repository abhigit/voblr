<?php

class SettingsController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'settings';

  
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
    $commonJsFiles = array('modernizr-2.6.1-respond-1.1.0.min', 
                           'wysihtml5-0.3.0_rc2.min',
                           'bootstrap.min',
                           'mustache',
                           'jquery.masonry.min',
                           'jquery.imagesloaded.min',
                           'timeago',
                           'mosaic.1.0.1.min',
                           'jquery.autosize-min',
                           'bootstrap-wysihtml5-0.0.2.min',
                           'Api',
                           'Log',
                           'jquery.shopholic',
                           'facebook',
                           'login'
                          );
   
    
	// removed Moviebase.js Userbase.js 
    // newsfeed same css and js files
    $this->loadCommonJs($commonJsFiles);
    
    
    $cssfiles = array('bootstrap','bootstrap-responsive','bootstrap-wysihtml5-0.0.2','main');
    $this->loadCommonCss($cssfiles);
    
  }
  
  public function indexAction() {
    $js = array("index");
      // View Title variable 
      $this->view->vTitle = "Settings";
      $this->loadjs($js);

    // $css = array("index");
    // $this->loadcss($css);
  }
  
  
  protected function undefinedaction() {
    $this->_forward('index');
    return;
  }

}
