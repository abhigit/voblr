<?php

/**
 *  AuthController Class
 *
 *  handling user Authentication
 *
 */
class AuthController extends Zend_Controller_Action{
    
    //----------------------------
    // Constants
    //---------------------------- 
    const DEFAULT_PASSWORD_LENGTH = 8;


    //----------------------------
    // Public Functions 
    //---------------------------- 
    /**
     * Initialize method
     * set view null, disable layout
     */
    public function init(){
        /* Initialize action controller here */
        $this->_helper->viewRenderer->setNoRender(true);
        // disable layout
        $this->_helper->layout->disableLayout();
    }

    /**
     *
     *
     */
    public function indexAction(){
        exit;
        // action body
    }

    /**
     * authentication action
     *
     */
    public function loginAction(){
        $redirect_url = "/";
        if($_POST){
            $params = $this->_request->getParams();
            $username = isset($params['username']) ? $params['username'] : '';
            $password = isset($params['password']) ? $params['password'] : '';
            if (!$username) {
                Log::info("username is missing");
                $this->_redirect('/');
            }
            if (!$password) {
                Log::info("password is missing");
                $this->_redirect('/');
            }
            $login = new Project_Business_Auth();
            $result = $login->checkLogin($_POST['username'], $_POST['password']);
  
            //Logger For Login result
            Log::info("Login_result " . ($result ? "TRUE" : "False") . "\n---------\n");
            if($result){
                //User For Debug
                //Log::info("Params: ".print_r($params, true));
                //Log::info("POST: ".print_r($_POST, true));
                $facebook = new Project_Business_Facebook();
                // Auto follow Friends from Facebook
                if(isset($params['snsuserid']) && isset($params['current_user_id']) && $params['current_user_id']){
                    $facebook->autoFollow($params['current_user_id'], $result['tmp_vb']);
                }
                // Auto follow Default Users
                $facebook->autoFollowDefaultUsers($result['tmp_vb']);

                $redirect_url = isset($params['redirect_url']) ? urldecode($params['redirect_url']) : $redirect_url;
                $this->_redirect($redirect_url);
            }else{
                // Redirect To Login error page
                $this->_redirect('/');
            }
        }else{
            // Redirect to Home Page
            $this->_redirect('/');
        }
    }

   // /**
   //  *  check action
   //  *
   //  */
   // public function isLogin(){
   //     if ($_COOKIE['vob_common'] != NULL || $_COOKIE['vob_common'] != "") {
   //         $_COOKIE['vob_common'] == session_id();
   //         Log::info("");
   //         return true;
   //     }else {
   //         return false;
   //     }
  
   // }

    public function logoutAction(){
        $user = new Project_Business_User();
        $user->logMeOut();
        $this->_redirect('/');
    }


    public function facebookloginAction(){
        $userinfo = array();
  
        $this->facebook = new Facebook_ApiFacebook();
  
        $auth_info = null;
        $user_info = null;
        if ( ! $this->_request->getParam('code')) {
            $this->_redirect($this->facebook->getLoginUrl(Facebook_ApiFacebook::$LOGIN_SCOPE));
        } else {
            $this->facebook->getAccessTokenByCode($this->_request->getParam('code'));
            $auth_info = $this->facebook->getAuthInfo();
        }
        
        try {
            $snsuserid = $auth_info['uid'];
            if($snsuserid) {
                $user = new Project_Business_User();
                $result = $user->getUserdatabysnsid($snsuserid,'FACEBOOK');
                if (!$result) {
                    Log::debug("there is possibility to reduce load in facebook->getUserInfo");
                    $facebookUserInfo = $this->facebook->getUserInfo(true);
                    if (isset($facebookUserInfo['email']) && $facebookUserInfo['email']) {
                        $result = $user->getUserdatabyUsername($facebookUserInfo['email']);
                        $snsinfo = array();
                        $snsinfo['snsserviceid'] = 'FACEBOOK';
                        $snsinfo['snsuserid'] = $auth_info['uid'];
                        $snsinfo['snsaccesstoken'] = $auth_info['token'];
                        if ($result) {
                            Log::info(__METHOD__ .", user already registered");
                            $snsinfo = $user->addSnsInfo($result['uuid'], $snsinfo);
                        } else {
                            Log::info(__METHOD__ .", creating FB user, details:");
                            // Log::info($facebookUserInfo);
                            $registerParams = array( "name"     => $facebookUserInfo["name"], 
                                                     "username" => $facebookUserInfo["id"], 
                                                     "password" => self::generatePassword(), 
                                                     "email_id" => $facebookUserInfo["email"], 
                                                     "location" => $facebookUserInfo["location"], 
                                                     "snsinfo"  => $snsinfo);
                            self::registeruser($registerParams);
                        }
                    } else {
                        Log::err(__METHOD__ .", redirect user to error page");
                        $this->_redirect('/error/');
                        die;
                    }
                } else {
                    // Returning user
                    // Update Access Token in snsinfo
                    $ret = $user->updateAccessTokenBySnsId($snsuserid, $auth_info['token']);
                    // get Location from facebook if null
                    if($result['location'] === null) {
                        Log::info("Updateing Location");
                        $facebookUserInfo = $this->facebook->getUserInfo(true);
                        $ret = $user->updateUserLocation($result['id'], $facebookUserInfo['location']['name']);
                    }
                    //Log::info($snsuserid);
                    //Log::info(print_r($auth_info,true));
                    $op  = ($ret=='1') ? "YES" : "NO(same as previous)";
                    Log::info("Update Token : ".$op);
                }
                if($result) {
                    $refHeader   = $this->getRequest()->getHeader('referer');
                    $temp        = explode("?",$refHeader);
                    $temp2       = explode("//",$temp[0]);
                    $referer     = $temp2[1];
                    Log::info("Referer : ".print_r($referer,true));
                    // Get Redirect Url
                    $redirectUrl = $this->getRedirectUrl($referer);
                    Log::info("Redirect Url : ".print_r($redirectUrl,true));

                    $username = isset($result['username']) && $result['username'] ? $result['username'] : null;
                    $password = isset($result['password']) && $result['password'] ? $result['password'] : null;
                    if($username && $password) {
                        $_POST['username']     = $username;
                        $_POST['password']     = $password;
                        $_POST['redirect_url'] = $redirectUrl;
                        $this->_forward('/login/');
                        return;
                    } else {
                        $this->_redirect('/error/');
                        die;
                    }
                } else {
                    Log::err(__METHOD__ .", users FB registration process die");
                    die;
                }
            }
        }
        catch (FacebookApiException $e) {
          Log::err($e);
          $this->_forward('index');
          return;
        }
    }

    public function verifyemailAction () {
        /* Initialize action controller here */
        $this->_helper->viewRenderer->setNoRender(false);
        // disable layout
        $this->_helper->layout->enableLayout();

        $params = $this->_request->getParams();
        $linkid = isset($params['linkid']) ? $params['linkid'] : null;
        $errmsg = "invalid link id";
        if (!$linkid || $linkid == "") {
            $this->view->errmsg = $errmsg;
            return;
        }

        $auth = new Project_Business_Auth();
        $result = $auth->verifyemail($linkid);
        if ($result) {
            return;
        }
        $this->view->errmsg = $errmsg;
        return;
    }
    
    //----------------------------
    // Private Functions 
    //---------------------------- 
    private function registeruser($params) {
        $user = new Project_Business_User();
        $result = $user->addUserinfo($params);

        $refHeader   = $this->getRequest()->getHeader('referer');
        $temp        = explode("?",$refHeader);
        $temp2       = explode("//",$temp[0]);
        $referer     = $temp2[1];
        Log::info("Referer : ".print_r($referer,true));
        // Get Redirect Url
        $redirectUrl = $this->getRedirectUrl($referer);
        Log::info("Redirect Url : ".print_r($redirectUrl,true));

        if($result && $result > 0) {
            $_POST['username'] = $params['username'];
            $_POST['password'] = $params['password'];
            $_POST['redirect_url'] = $redirectUrl;
            
            // For AutoFollow
            if (isset($params['signup_via']) && $params['signup_via'] == 'facebook_mobile') {
                if(isset($params['snsuserid']) && $params['snsuserid']) {
                    $_POST['snsuserid'] = $params['snsuserid'];
                }
             } elseif (isset($params['snsinfo']) && $params['snsinfo']){
                 $_POST['snsuserid'] = $params['snsinfo']['snsuserid'];
             }

            $_POST['current_user_id'] = $result;

            self::loginAction();

            return;
        }
    }


    private function generateTempKey($str = '') {
        return substr(md5(time() . $str), 0, 20);
    }
    
    
    private function generatePassword($length = self::DEFAULT_PASSWORD_LENGTH) {
        $characters = array(
          "A","B","C","D","E","F","G","H","J","K","L","M",
          "N","P","Q","R","S","T","U","V","W","X","Y","Z",
          "a","b","c","d","e","f","g","h","i","j","k","m",
          "n","o","p","q","r","s","t","u","v","w","x","y","z",
          "1","2","3","4","5","6","7","8","9", "0", "-", "_");
        shuffle($characters);
        return implode("", array_slice($characters, 0, $length));
    }


    private function getRedirectUrl($url) {
        $voblrBaseUrls    = array("voblr.com","stg.voblr.com","tarun.voblr.com","www.voblr.com");
        $voblrTargetPages = array("movie","stats","vob");

        $tmp           = explode("/",$url);
        $baseUrl       = $tmp[0];
        $redirectUrl   = "/";
        
        if(in_array($baseUrl, $voblrBaseUrls) && in_array($tmp[1], $voblrTargetPages)){
            $tmp1        = explode($baseUrl, $url);
            $redirectUrl = $tmp1[1]; 
        }

        return $redirectUrl;
    }
}
