<?php

class MovieController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'movie';
  const DEFAULT_USER_PIC_URL = "http://voblr.com/static/images/default/user.png?v=1";
  
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
   /* 
    $commonJsFiles = array(
                           'plugin.20130403.min'
                          );
*/
    $commonJsFiles = array(
                           'wysihtml5',
                           'bootstrap.min.copy',
                           'bootstrap-wysihtml5',
                           'mustache',
                           'jquery.masonry.min',
                           'timeago',
                           'login',
                           'Api',
                           'jquery.shopholic',
                           'apiConfig'
                          );
    
    //removed Moviebase.js
    $this->loadCommonJs($commonJsFiles);
    
    
    //$cssfiles = array('bootstrap','bootstrap-responsive','main');
/*
    $cssfiles = array(
                    'all.min'
                    );
*/
    $cssfiles = array(
                    'bootstrap-copy',
                    'bootstrap-wysihtml5',
                    'theme',
                    'blog-mod',
                    'external-pages',
                    'main'
                    ); 
    $this->loadCommonCss($cssfiles);
    
  }
  
  public function indexAction() {
    //http://bhupi.voblr.com/movie/tt1055369/
    $this->getResponse()->clearBody();
    $requestURI  = $this->getRequest()->getRequestUri();
    $requestURI = explode("/", $requestURI);
    
    //Log::info($requestURI);
    if (!isset($requestURI[2])) {
      $this->_redirect("/movies/");
      return;
    }
    
    $movieid = $requestURI[2];
    $movie = new Project_Business_Movie();
    $movieDetail = $movie->getmovieDetail($movieid);
    if (!$movieDetail) {
        $this->_redirect("/movies/");
      return;
    }

    // Default Pic URL for not logged in users
    $this->view->defaultpicURL   = self::DEFAULT_USER_PIC_URL;
    $this->view->defaultUserName = 'VoblrUser';
    
    $this->view->moviedetail = $movieDetail;
    
    // set Og tgas
    $this->view->ogtype="video.movie"; 
    $this->view->ogurl ="http://www.voblr.com/movie/".$movieDetail["id"]."/"; 
    $this->view->ogtitle=$movieDetail["name"]; 
    $this->view->ogimage=$movieDetail["imageURL"][2]; 

    // Page Title
    $this->view->vTitle = $movieDetail["name"] . " review";
   // $js = array("index");
   // $this->loadjs($js);

  }
  
  protected function undefinedaction() {
      // Log::info(__METHOD__);
      $this->_forward('index');
      return;
  }
  
}
