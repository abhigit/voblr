<?php
require_once 'AuthController.php';

class IndexController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'index';
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
/*
	$commonJsFiles = array(
                           'plugin.20130403.min'
                          ); 
*/
    $commonJsFiles = array(
                           'wysihtml5',
                           'bootstrap.min.copy',
                           'bootstrap-wysihtml5',
                           'mustache',
                           'jquery.masonry.min',
                           'timeago',
                           'login',
                           'Api',
                           'jquery.shopholic',
                           'apiConfig'
                          );
    $this->loadCommonJs($commonJsFiles);
/*
    $cssfiles = array(
                    'all.min'
                    );
 */
    $cssfiles = array(
                    'bootstrap-copy',
                    'bootstrap-wysihtml5',
                    'theme',
                    'blog-mod',
                    'external-pages',
                    'main'
                    ); 
    $this->loadCommonCss($cssfiles);
  }
  
  public function loginAction() {
 //     $externalJs = array("https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js");
 //     $this->loadExternalJs($externalJs);
 //     $libjs = array("jquery.fancybox");
 //     $this->loadCommonJs($libjs);

      //$jsfiles = array('facebook','login');
      //$this->loadjs($jsfiles);
  }
  public function indexAction() {
      $this->_redirect("/newsfeed/");
    //if user is not logged in redirect to login action
      $this->view->fbMe = array ("name" => $this->_user->name,
                                 "username" => $this->_user->username,
                                 "email_id" => $this->_user->email_id,
                                 "password" => '******************',
                                 "update_ts" => $this->_user->update_ts);
      $this->view->afterloginmsg = "Welcome back " . $this->_user->name;

      //Set View Title 
      $this->view->vTitle = "Voblr / Login";
  }

  public function registrationAction(){
    if($this->_user) {  //signed in user cannot register again
      $this->_redirect('/');
      return;
    }
    $params = $this->_request->getParams();
    $signup_service = isset($params['service']) ? $params['service'] : 'voblr';

    $redis_obj = new Project_Business_Redis();
    $auth_regist_key = isset($params['auth_regist_key']) && $params['auth_regist_key'] ? $params['auth_regist_key'] : "";
    $auth_info = json_decode($redis_obj->get('auth_regist_key_' . $auth_regist_key), true);
    
    if($signup_service == 'facebook' && isset($auth_info['service']) && $auth_info['service'] == 'facebook') {
      $facebook = new Facebook_ApiFacebook();
      $facebook->setAccessToken($auth_info['token']);
      try {
        $userdata = $facebook->api('/me');
        if($userdata && is_array($userdata) && isset($userdata['id']) && $userdata['id']) {
          $this->view->auth_regist_key = $auth_regist_key;
          $this->view->signup_via = $signup_service;
          $this->view->name = isset($userdata['name']) && $userdata['name'] ? $userdata['name'] : null;
          $this->view->email = isset($userdata['email']) && $userdata['email'] ? $userdata['email'] : null;
          $this->view->gender = isset($userdata['gender']) && $userdata['gender'] ? $userdata['gender'] : null;
        }
      } catch (FacebookApiException $e) {
        Log::err(__METHOD__  . ", ############ very serious error ###########??");
        Log::err($e);
        $this->_forward('index');
        return;
      }
    }
    $jsfiles = array('registration');
    $this->loadjs($jsfiles);
  }
}
