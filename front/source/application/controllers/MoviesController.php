<?php

class MoviesController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'movies';


  
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
    
    $commonJsFiles = array('jquery.fancybox', 'Log','Api', 'Moviebase','bootstrap');
    $this->loadCommonJs($commonJsFiles);
    
    $cssfiles = array('bootstrap','main', 'jquery.fancybox');
    $this->loadCommonCss($cssfiles);
    
  }
  
  public function indexAction() {
  //if user is not logged in redirect to login action
    $this->view->afterloginmsg = "Welcome back " . $this->_user->name;
      $js = array("index");
      $this->loadjs($js);

      $css = array("index");
      $this->loadcss($css);
  }
  

}
