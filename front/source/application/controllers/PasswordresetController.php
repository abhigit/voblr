<?php

/*  Password Recovery Module.
 By Avi
Functions:
- init()
- indexAction()
- sendAction()
- rand_string($len) returns string
- linkclickAction()
- changePasswordAction()
*/

class PasswordresetController extends Project_Controller_Base
{
    const CONTROLLER_NAME = 'Passwordreset';
    public $_auth;
    public $userCheck;

    public function getInstance(){
        return $this->_auth;
    }


    public function init()
    {
        $this->controllername = self::CONTROLLER_NAME;
        parent::init();
        $commonCssfiles = array('main', 'jquery.fancybox');
        $this->loadCommonCss($commonCssfiles);
        
        $cssfiles = array('passwordreset');
        $this->loadCss($cssfiles);
    }

    public function indexAction() {
        //Don't delete this action this is required to show all movies.
        //To show all movies. Don't delete
    }

    public function sendAction($msg = false) {

        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $emailId = $_POST["email"];
        $key = $this->rand_string(20);
        $rediska = new Project_Business_Redis();
        $userCheck = new Project_Business_User();
        $response = array(
                'status' => 200,
                'message' => "ok",
                'data' => array
                (
                        array
                        (
                                'errorCode' => -1,
                                'emailId' => $emailId,
                                'message' => ''
                        )
                )
        );
        
        if($userCheck) {
            if($userCheck->ifUserExists($emailId)) {
                if ($rediska->set($key,$emailId)) {
                    $rediska->expire($key,1000);
                    log::info("[Password Reset] Redis set successful. Key => ".$key." Email ID => ".$emailId);
                     $url =  $this->_hostname . "/passwordreset/linkclick?key=".$key;
                    $message = '
                    <!DOCTYPE html>
                    <html>
                    <body style="background:#FCFCFC ;font-family: Helvetica;margin: 0;padding:20px;color: #666666;font-size: 15px;">
                    Hello,<br><br>
                    To reset your Voblr account password, click the following link.
                    <br><br><a href="'. $url.'">'.$url.'</a>
                    <br><br>Thank you.
                    <br><br>

                    Voblr Team<br>
                    <a href="www.voblr.com">Voblr.com</a>  <br>
                    </body>
                    </html>
                    ';
                    $headers  = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'From: passwordreset@voblr.com' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
                    if (mail($emailId, 'Password reset instructions form Voblr.com', $message, $headers)){
                        // no unless in php thats why it sucks !
                        $response['data'][0]['errorCode'] = 0;
                        $response['data'][0]['message']   = 'Email sent';
                    } else {
                        $response['data'][0]['errorCode'] = 500;
                        $response['data'][0]['message']   = 'Email sending failed';
                    }
                    self::echoResponse($response);
                } else {
                    log::info("[Password Reset] Redis set unsuccessful. Key => ".$key." Email ID => ".$emailId);
                    $response['data'][0]['errorCode'] = 500;
                    $response['data'][0]['message']   = 'Redis access failed';
                    self::echoResponse($response);
                }
            } else {
                log::info("[Password Reset] User with email id => ".$emailId." does not exist!");
                //$this->render('/notregistered');
                $response['data'][0]['errorCode'] = 404;
                $response['data'][0]['message']   = 'User with email Id  <'.$emailId.'>  does not exist.';
                self::echoResponse($response);                 
            }
             
        }
    }

    public function rand_string( $length ) {
        $chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        $size = strlen( $chars );
        $str = "";
        for( $i = 0; $i < $length; $i++ ) {
            $str .= $chars[ rand( 0, $size - 1 ) ];
        }
        return $str;
    }

    public function linkclickAction() {

        $key = $_GET["key"];

        //require_once 'Rediska.php';
        $rediska = new Project_Business_Redis();

        if (! $rediska->exists($key)) {
            log::info("[Password Reset] The key => ".$key." Does not exists in Redis keystore");
            $this->render('linkclickfail');
        } else {
            // reverify the email
            $emailId = $rediska->get($key);
            $this->view->emailId = $emailId;
            log::info("[Password Reset] In linkClickAction. Key => ".$key." email Id => ".$emailId);
        }


    }

    public function changepasswordAction() {
        $password = $_POST["password"];
        $key = $_POST["key"];
        $rediska = new Project_Business_Redis();
        if ($rediska->exists($key)) {
            // get the email id from Redis keystore
            $emailId = $rediska->get($key);
            $userCheck = new Project_Business_User();
                
            if($userCheck->changePassword($emailId, $password)) {
                log::info("password successfully changed! for EmailId => ".$emailId);
                $rediska->delete($key);
                $this->_redirect("/");
            }
            else {
                $this->render('changefail');
                $rediska->delete($key);
            }
        } else {
            log::info("Invalid key. Key => ".$key);
            $rediska->delete($key);
            $this->render('error');
            // redirect to error page

        }
            
    }

    public function notregisteredAction() {
        // Do nothing just log.
        log::info("notregisteredAction() called.");
    }

    public function changefailAction() {
        log::info("changefailAction() called.");
    }

    public function linkclickfailAction() {
        log::info("linkclickfailAction() called.");
    }

    public function errorAction() {
        log::info("Some unknown issue occured..");
    }
    
    private function echoResponse($response) {
        $final_response = json_encode($response);
        $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8')->appendBody($final_response);
        $this->getResponse()->setHttpResponseCode(200)->setRawHeader('HTTP/1.1 200 OK'); // optional
    }
}

