<?php

class StatsController extends Project_Controller_Base
{
    const CONTROLLER_NAME = 'stats';
    const DEFAULT_USER_PIC_URL = "http://voblr.com/static/images/default/user.png?v=1";

    public function init()
    {
        //$auth = new AuthController();
        /* Initialize action controller here */
        $this->controllername = self::CONTROLLER_NAME;
        parent::init();
/*
        $commonJsFiles = array(
                                'plugin.20130403.min'
                              );
*/
    $commonJsFiles = array(
                           'wysihtml5',
                           'bootstrap.min.copy',
                           'bootstrap-wysihtml5',
                           'mustache',
                           'jquery.masonry.min',
                           'timeago',
                           'login',
                           'Api',
                           'jquery.shopholic',
                           'apiConfig'
                          );
        // removed Moviebase.js Userbase.js
        // newsfeed same css and js files
        $this->loadCommonJs($commonJsFiles);


        //    $cssfiles = array('bootstrap','bootstrap-responsive','main');
/*
        $cssfiles = array(
                           'all.min'
                          );
*/
    $cssfiles = array(
                    'bootstrap-copy',
                    'bootstrap-wysihtml5',
                    'theme',
                    'blog-mod',
                    'external-pages',
                    'main'
                    ); */
        $this->loadCommonCss($cssfiles);

    }

    public function indexAction() {
        $requestURI = $this->getRequest()->getRequestUri();
        $requestURI = explode("/", $requestURI);
        $userid     = isset($requestURI[2]) && $requestURI[2] ? $requestURI[2] : $this->_user->id;
        $user       = new Project_Business_User();
        // Get from API (mongo)
        $userinfo   = $user->getUserbyID($userid);

        Log::info("This User's Info: ".print_r($userinfo,true));

        // Get Extra UserInfo from Facebok
        // Hit only if request is not from Facebbok
        if(!preg_match("/.*facebookexternalhit.*/",$_SERVER['HTTP_USER_AGENT'])){
            // For Debugging If required
            // Log::info("Referer Current : ".print_r($_REQUEST,true));
            // Log::info("Referer Current : ".print_r($_SERVER,true));
            //$extraUserInfo = $user->getExtraUserInfo($userid);
            $extraUserInfo = array();
            $extraUserInfo['name'] = $userinfo->name;
            $extraUserInfo['location'] = $userinfo->location;
            $this->view->statuserextra = $extraUserInfo;
        }

        // Default Pic URL for not logged in users
        $this->view->defaultpicURL   = self::DEFAULT_USER_PIC_URL;
        $this->view->defaultUserName = 'VoblrUser';

        $this->view->statsactive   = $userid == (isset($this->_user->id) ? $this->_user->id : NULL) ? "active" : "";

        $this->view->statsuser     = $userinfo;
        $this->view->statsuserid   = $userinfo->id;
        $this->view->me            = isset($this->_user->id) ? $this->_user->id : NULL ;
        $this->view->statsusername = $userid == (isset($this->_user->id) ? $this->_user->id : NULL) ? "you" : $userinfo->name;

        $this->view->ogtype  = "profile";
        $this->view->ogurl   = "http://www.voblr.com/stats/".$userid."/";
        $this->view->ogtitle = $userinfo->name;
        $this->view->ogimage = $userinfo->picURL."?type=large";

        // $js = array("index");
        // $this->loadjs($js);
        //Set View Title
        $this->view->vTitle = $userinfo->name . " on Voblr";
    }


    protected function undefinedaction() {
        $this->_forward('index');
        return;
    }

}
