<?php 
class ApimockController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'Apimock';
  public function init(){
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
    $this->_helper->layout->disableLayout();
    $this->_helper->viewRenderer->setNoRender(true);

    $commonCssfiles = array('common');
  }

  public function followersmockAction() {
    $data = array('id'          => '0010',
        'displayName' => 'Avinash Varma',
        'imageURL'    => 'https://fbcdn-profile-a.akamaihd.net/hprofile-ak-snc4/572212_100000051127351_366250568_q.jpg',
        'follows'     => '100',
        'following'   => '500'
    );
    echo json_decode($data);

  }
  
  public function getuserslistAction() {
  	$user = new Project_Business_User();
  	$userList = $user->getUsersList($this->_user->id);
  	
  	if ($userList && is_array($userList)) {
  		$randFollow = array(true, false);
  		foreach ($userList as &$user) {
  			$user['following']  = $randFollow[rand(0, 1)];
  		}
	    $response = array(
	        'status' => 200,
	        'message' => "ok",
	        'data' => $userList
	    );
  	} else {
      $response = array(
          'status' => 400,
          'message' => "errror in getting users list",
      );
  	}
  	
    self::echoResponse($response);
  }
   
  public function moviesAction(){
  	
  	$movies = new Project_Business_Movies();
  	$data = $movies->getAllMovies();
    $response = array(
        'status' => 200,
        'message' => "ok",
        'data' => $data
    );
    
//    $final_response = json_encode(array("status" => $status, "message" => $message , "data" => $data));
    self::echoResponse($response);
  }
  
  public function followuserAction() {
    $params = $this->_request->getParams();
    $response = array(
        'status' => 200,
        'message' => "ok"
    );
    self::echoResponse($response);
  }
  public function unfollowuserAction() {
    $params = $this->_request->getParams();
    $response = array(
        'status' => 200,
        'message' => "ok"
    );
    self::echoResponse($response);
  }
  
  
  public function getfollowersAction() {
  	$params = $this->_request->getParams();
  	$userid = isset($params['userid']) && $params['userid'] ? $params['userid'] : null; 
  	if (!$userid) {
  		$badresponse =  array(
          'status' => 500,
          'message' => "user id can not be null"
      );
  		self::echoBadResponse($badresponse);
  		return;
  	}
    $user = new Project_Business_User();
    $userList = $user->getUsersList($userid);
    
    if ($userList && is_array($userList)) {
      $randFollow = array(true, false);
      foreach ($userList as &$user) {
        $user['following']  = $randFollow[rand(0, 1)];
      }
      $response = array(
          'status' => 200,
          'message' => "ok",
          'data' => $userList
      );
    } else {
      $badresponse = array(
          'status' => 400,
          'message' => "errror in getting users list",
      );
      self::echoBadResponse($badresponse);
      return;
    }
    self::echoResponse($response);
  }
  
  
  public function getfollowingsAction() {
    $params = $this->_request->getParams();
    $userid = isset($params['userid']) && $params['userid'] ? $params['userid'] : null; 
    if (!$userid) {
      $badresponse =  array(
          'status' => 500,
          'message' => "user id can not be null"
      );
      self::echoBadResponse($badresponse);
      return;
    }
    $user = new Project_Business_User();
    $userList = $user->getUsersList($userid);
    
    if ($userList && is_array($userList)) {
      $randFollow = array(true, false);
      foreach ($userList as &$user) {
        $user['following']  = $randFollow[rand(0, 1)];
      }
      $response = array(
          'status' => 200,
          'message' => "ok",
          'data' => $userList
      );
    } else {
      $badresponse = array(
          'status' => 400,
          'message' => "errror in getting users list",
      );
      self::echoBadResponse($badresponse);
      return;
    }
    self::echoResponse($response);
  }
  
  
  public function vobAction() {
  	$params = $this->_request->getParams();
    $response = array(
        'status' => 200,
        'message' => "ok"
    );
    self::echoResponse($response);
  }
  
  public function vobfeedsAction() {
    $user = array("id" => "1", "displayName" => "Bhupi" , "imageURL" => $this->_user->img);
    $movie = new Project_Business_Movie();
    $moviearray = array("10001", "10002", "10003", "10004", "10005");
    $movie1 = $movie->getmovieDetail($moviearray[rand(0,4)]);
    unset($movie1['desc']);
    unset($movie1['director']);
    unset($movie1['create_ts']);
    unset($movie1['update_ts']);
    unset($movie1['desc']);
    $movie1['type'] = "movie";
    $movie2 = $movie->getmovieDetail($moviearray[rand(0,4)]);
    unset($movie2['desc']);
    unset($movie2['director']);
    unset($movie2['create_ts']);
    unset($movie2['update_ts']);
    unset($movie2['desc']);
    $movie2['type'] = "movie";
//    Log::info($movie1);
//    Log::info($movie2);
    
    $c1 = array("commentId" => "2142313231", "commentText" => "wasted 10man hrs of developer", "timestamp" => "2122141241", "user" => $user);
    $c2 = array("commentId" => "2142313232", "commentText" => "enjoyed the movie", "timestamp" => "2122141261", "user" => $user);
    $c3 = array("commentId" => "2142313233", "commentText" => "and later you will regret for the time you wasted ", "timestamp" => "212214321", "user" => $user);
    
    $comments1 = array($c2);
    $comments2 = array($c1, $c3);
    $d1 = array('id' => "3252342342", "vobText" => "great movie", "likesCount" => 10, "commentsCount" => 1, "timestamp" => "201232322", "user" => $user, "movie" => $movie1, "comments" => $comments1);
    $d2 = array('id' => "5435234345", "vobText" => "this movie is total timewaste", "likesCount" => 100, "commentsCount" => 2, "timestamp" => "201232622", "user" => $user,  "movie" => $movie2, "comments" => $comments2);
    $data = array($d1, $d2);
    $response = array(
        'status' => 200,
        'message' => "ok",
        'data' => $data
    );
    self::echoResponse($response);
  }
  
  private function echoResponse($response) {
    $final_response = json_encode($response);
    $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8')->appendBody($final_response);
    $this->getResponse()->setHttpResponseCode(200)->setRawHeader('HTTP/1.1 200 OK'); // optional
  }

  private function echoBadResponse($response) {
    $final_response = json_encode($response);
    $this->getResponse()->setHeader('Content-Type', 'application/json; charset=UTF-8')->appendBody($final_response);
    $this->getResponse()->setHttpResponseCode(500)->setRawHeader('HTTP/1.1 500 Bad Request'); // optional
  }
  
}
