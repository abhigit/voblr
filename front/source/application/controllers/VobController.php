<?php

class VobController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'vob';
  const DEFAULT_USER_PIC_URL = "http://voblr.com/static/images/default/user.png?v=1";


  public function init()
  {
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
/*
    $commonJsFiles = array(
                           'plugin.20130403.min'
                          );
*/
    $commonJsFiles = array(
                           'wysihtml5',
                           'bootstrap.min.copy',
                           'bootstrap-wysihtml5',
                           'mustache',
                           'jquery.masonry.min',
                           'timeago',
                           'login',
                           'Api',
                           'jquery.shopholic',
                           'apiConfig'
                          );
    // removed Moviebase.js Userbase.js
    // newsfeed same css and js files
    $this->loadCommonJs($commonJsFiles);
/*
    $cssfiles = array(
                    'all.min'
                    ); 
*/
    $cssfiles = array(
                    'bootstrap-copy',
                    'bootstrap-wysihtml5',
                    'theme',
                    'blog-mod',
                    'external-pages',
                    'main'
                    ); 
    $this->loadCommonCss($cssfiles);

  }

  public function indexAction() {
    $requestURI  = $this->getRequest()->getRequestUri();
    $requestURI = explode("/", $requestURI);

    if (!isset($requestURI[2]) || $requestURI[2]=="") {
      if($this->_user->id){
          Log::info("redirecting to newsfeed");
          $this->_redirect("/newsfeed/");
      }else{
          Log::info("redirecting to Login");
          $this->_redirect("/");
      }
      return;
    }

    $vobId = isset($requestURI[2]) ? $requestURI[2] : NULL;

    // Get Vob details
    $backendAPI  = new Project_Api_Backend();
    $vobDetail   = $backendAPI->getVobDetail($vobId);

    // Validate result
    if($vobDetail && isset($vobDetail['count']) && (int)$vobDetail['count'] === 1){
        // Set view array vobDetail which has vobList info.
        $this->view->vobDetail = $vobDetail['vobList'][0];

        // For debugging, if required
        //Log::info("vobDtail=".print_r($vobDetail['vobList'][0], true));

        // View Title variable
        $this->view->vTitle = "Vob";

        // Default Pic URL for not logged in users
        $this->view->defaultpicURL = self::DEFAULT_USER_PIC_URL;
        $this->view->defaultUserName = 'VoblrUser';

        // Set og Tags
        $this->view->ogtype="object";
        $this->view->ogurl ="http://www.voblr.com/vob/".$vobId."/";
        $this->view->ogtitle = $vobDetail["vobList"][0]["movie"]["name"]." Review";
        $this->view->ogimage = $vobDetail["vobList"][0]["movie"]["picUrl"][2];
    }else{
        $this->_redirect("/");
        return;
    }

    // Load Script
    // $js = array("index");
    // $this->loadjs($js);
    // $css = array("index");
    // $this->loadcss($css);
  }

  protected function undefinedaction() {
    $this->_forward('index');
    return;
  }

}
