<?php

class NewsfeedController extends Project_Controller_Base
{
  const CONTROLLER_NAME = 'newsfeed';

  
  public function init()
  {
    //$auth = new AuthController();
    /* Initialize action controller here */
    $this->controllername = self::CONTROLLER_NAME;
    parent::init();
    /*
$commonJsFiles = array(
                           'plugin.20130403.min'
                          );
 */
    $commonJsFiles = array(
                           'wysihtml5',
                           'bootstrap.min.copy',
                           'bootstrap-wysihtml5',
                           'mustache',
                           'jquery.masonry.min',
                           'timeago',
                           'login',
                           'Api',
                           'jquery.shopholic',
                           'apiConfig'
                          );
    // removed Moviebase.js Userbase.js 
    // newsfeed same css and js files
    $this->loadCommonJs($commonJsFiles);
    //$cssfiles = array('bootstrap','bootstrap-responsive','main');
    /*
$cssfiles = array(
                    'all.min'
                    ); 
*/
    $cssfiles = array(
                    'bootstrap-copy',
                    'bootstrap-wysihtml5',
                    'theme',
                    'blog-mod',
                    'external-pages',
                    'main'
                    ); 
    $this->loadCommonCss($cssfiles);
    
  }
  
  public function indexAction() {
   // $js = array("index");
    // View Title variable 
    $this->view->vTitle = "Voblr";
    $this->view->newsactive= "active"; 
      
    // set Og tgas
    $this->view->ogtype=""; 
    $this->view->ogurl =""; 
    $this->view->ogtitle=""; 
    $this->view->ogimage=""; 

    //$this->loadjs($js);

  }
  
  
  protected function undefinedaction() {
    $this->_forward('index');
    return;
  }

}
