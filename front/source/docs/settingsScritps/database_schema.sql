Use `voblrmovies`;
DROP TABLE IF EXISTS `User`;
CREATE TABLE `User` (
  `id` INT(30) unsigned NOT NULL auto_increment UNIQUE,
  `uuid` varchar(30) NOT NULL,
  `username` varchar(50) NOT NULL UNIQUE,
  `name` varchar(50) NOT NULL,
  `email_id` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `service_access` BOOL default TRUE,
  `create_ts` DATETIME NOT NULL,
  `update_ts` TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (`uuid`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `Snsinfo`;
CREATE TABLE `Snsinfo` (
  `id` int(30) unsigned NOT NULL auto_increment,
  `uuid` varchar(30) NOT NULL,
  `sns_service_id` varchar(10) NOT NULL,
  `sns_user_id` varchar(255) NOT NULL,
  `sns_access_token` varchar(1000) default NULL,
  `sns_access_token_expire` int(50) default NULL,
  `sns_refresh_token` varchar(1000) default NULL,
  `sns_refresh_token_expire` int(50) default NULL,
  `create_ts` datetime NOT NULL,
  `update_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`uuid`,`sns_service_id`),
  UNIQUE KEY `id` (`id`),
  CONSTRAINT `UUID` FOREIGN KEY (`uuid`) REFERENCES `User` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



Use `voblrmovies`;
DROP TABLE IF EXISTS `TempMovies`;
CREATE TABLE `TempMovies` (
  `id` INT(30) unsigned NOT NULL auto_increment UNIQUE,
  `title` varchar(255) NOT NULL,
  `desc` varchar(255),
  `times_vobbed` int(9) default 0,
  `create_ts` DATETIME NOT NULL,
  `update_ts` TIMESTAMP default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;



Use `voblrmovies`;
DROP TABLE IF EXISTS `TempVobs`;
CREATE TABLE `TempVobs` (
  `id` int(30) unsigned NOT NULL auto_increment,
  `vobber_id` int(30) unsigned  NOT NULL,
  `object_id` int(30) unsigned  NOT NULL,
  `object_type` varchar(25) NOT NULL,
  `action_type` varchar(25) NOT NULL,
  `create_ts` datetime NOT NULL,
  `update_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  CONSTRAINT `VOBBER` FOREIGN KEY (`vobber_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `TempSubscribers`;
CREATE TABLE `TempSubscribers` (
  `id` int(30) unsigned NOT NULL auto_increment,
  `user_id` int(30) unsigned  NOT NULL,
  `object_id` int(30) unsigned  NOT NULL,
  `object_type` varchar(25) NOT NULL,
  `action_type` varchar(25) NOT NULL,
  `status_flg` tinyint NOT NULL,
  `create_ts` datetime NOT NULL,
  `update_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  CONSTRAINT `SUBSCRIBER` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`),
  CONSTRAINT `SUBSCRIBED_OBJECT` FOREIGN KEY (`object_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 0 kb';


DROP TABLE IF EXISTS `Tokens`;
CREATE TABLE `Tokens` (
  `id` int(30) unsigned NOT NULL auto_increment,
  `access_token` varchar(100) NOT NULL,
  `user_id` int(30) unsigned  NOT NULL,
  `valid_flg` tinyint NOT NULL,
  `expire_ts` datetime NOT NULL,
  `create_ts` datetime NOT NULL,
  `update_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`),
  UNIQUE KEY (`access_token`),
  CONSTRAINT `ACCESS_TOKEN_CONSTRAINT` FOREIGN KEY (`user_id`) REFERENCES `User` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 0 kb';

DROP TABLE IF EXISTS `GraphNode`;
CREATE TABLE `GraphNode` (
  `id` int(30) unsigned NOT NULL,
  `type` varchar(5) NOT NULL,
  `local_id` int(30) unsigned  NOT NULL,
  `create_ts` datetime NOT NULL,
  `update_ts` timestamp NOT NULL default CURRENT_TIMESTAMP on update CURRENT_TIMESTAMP,
  PRIMARY KEY  (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8 COMMENT='InnoDB free: 0 kb';
