 var Movies = new function() {
	this.movies = null;
	this.getmoviebyid = function(id) {
		for(var i = 0; i < this.movies.length; i++) {
			var movie = this.movies[i];
			if(movie.id == id) {
				return movie;
			}
		}
		return null;
	};
};

$(document).ready(function(){
	var count = 0;
	var params = {'count' : count};
	Api.getmovieslist(getmovieslistCallback, null, params);
});

var getmovieslistCallback = function(response, params) {
	if(!response || response.status != Api.SUCCESS) {
		Log.info("Some problem in login response: " + response.status);
		return;
	}
	
	if(!response.data) {
		Log.alert("No movies list recieved from the server");
		Log.info(response.status);
		return;
	}
	
	var movies = response.data;
	Log.info("Total movies recevied: " + movies.length);
	
	$("#total_movies").text(movies.length);
	for(var i = 0; i < movies.length; i++) {
		var movie = movies[i];
		var element = '<dt>' + i + ') ' + '<a href="/movie/'+movie.id+'/">'+movie.name+'</a>&nbsp;&nbsp;'+ 'vobs('+movie.vobCount+')&nbsp;&nbsp;'+ '<a href="#movie-vob-popup" class="fancybox vob-link" movieid="'+movie.id+'" moviename="'+movie.name+'">vobIt</a>' + '</dt>';
		$("#movies_list").append(element);
	}
}
