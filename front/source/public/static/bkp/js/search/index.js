/*var userslist;
var movieslist;
var UsersVar = function() {
	this.lastId = null;
	this.timeStamp = null;
	this.users = new Array();
    this.getuserslist = function (callback, callbackparams) {
//    	var lastId = this.lastId;
//    	var timeStamp = this.timeStamp;
    	var params = {};
    	Api.getuserslist(callback, params, callbackparams);
    }
}

var RecomondedMoviesVar = function() {
	this.lastId = null;
	this.timeStamp = null;
	this.movies = new Array();
    this.getmovieslist = function (callback, callbackparams) {
//    	var lastId = this.lastId;
//    	var timeStamp = this.timeStamp;
    	var params = {};
    	Api.getmovieslist(callback, params, callbackparams);
    }
}


$(document).ready(function(){
	userslist = new UsersVar();
	movieslist = new RecomondedMoviesVar();
	userslist.getuserslist(getuserslistCallback, null);
	$("#recomendation-users").click(function(){
		userslist.getuserslist(getuserslistCallback, null);
		$("#recomendation-movies-list").hide();
		$("#recomendation-users-list").show();
	});
	
	$("#recomendation-movies").click(function(){
		movieslist.getmovieslist(getmovieslistCallback, null);
		$("#recomendation-users-list").hide();
		$("#recomendation-movies-list").show();
	});
	
});



var getuserslistCallback = function(response, params) {
	if(!response || response.status != Api.SUCCESS) {
		Log.info("Some problem in getting userslist response: " + response.status);
		return;
	}
	
	if(!response.data) {
		Log.alert("No users list recieved from the server");
		Log.info(response.status);
		return;
	}
	
	var users = response.data;
	Log.info("Total users recevied: " + users.length);
	
	var i = userslist.users.length;
	
	userslist.users = $.merge(userslist.users, users);
	$("#totalUsers").text(userslist.users.length);
	
	for(; i < userslist.users.length; i++) {
		var user = userslist.users[i];
		var followunfollow = user.following ? "unfollow_user" : "follow_user";
		var followunfollowText = {"unfollow_user" : "Unfollow", "follow_user" : "Follow"};
		var element = '<dt style="border: 1px solid gray; padding: 10px; font-size: 11px; margin:10px;">' + i + ') ' + '<a href="/stats/'+user.id+'/">'+user.name+'</a>&nbsp;&nbsp;'+ 'vobs('+user.vobCount+')&nbsp;&nbsp;'
					  + '<a href="#followers-following-popup" class="fancybox followers-popup" userid="'+user.id+'" name="'+user.name+'" img="'+user.img+'" followersCount="'+user.followersCount+'" >followers('+user.followersCount+')&nbsp;&nbsp</a>'
					  + '<a href="#followers-following-popup" class="fancybox followings-popup" userid="'+user.id+'" name="'+user.name+'" img="'+user.img+'" followingsCount="'+user.followeingCount+'" >followings('+user.followeingCount+')&nbsp;&nbsp</a>'
					  + '<a class="'+followunfollow+'" userid="'+user.id+'">'+followunfollowText[followunfollow]+'</a>'
					  + '</dt>';
		$("#recomendation-users-list").append(element);
	}
}



var getmovieslistCallback = function(response, params) {
	if(!response || response.status != Api.SUCCESS) {
		Log.info("Some problem in getting movieslist response: " + response.status);
		return;
	}
	
	if(!response.data) {
		Log.alert("No movies list recieved from the server");
		Log.info(response.status);
		return;
	}
	
	var movies = response.data;
	Log.info("Total movies recevied: " + movies.length);
	
	var i = movieslist.movies.length;
	
	movieslist.movies = $.merge(movieslist.movies, movies);
	$("#totalMovies").text(movieslist.movies.length);
	
	for(; i < movieslist.movies.length; i++) {
		var movie = movieslist.movies[i];
		var element = '<dt>' + i + ') ' + '<a href="/movie/'+movie.id+'/">'+movie.name+'</a>&nbsp;&nbsp;'+ 'vobs('+movie.vobCount+')&nbsp;&nbsp;'+ '<a href="#movie-vob-popup" class="fancybox vob-link" movieid="'+movie.id+'" moviename="'+movie.name+'">vobIt</a>' + '</dt>';
		$("#recomendation-movies-list").append(element);
	}
}
var getMoviesListCallback = function (response){
	console.log(response);
	var template =$('#searchItemMovieTmpl').html();
	var html = Mustache.to_html(template,response);
	$('.searchResult').html(html);
	console.log(response);
}
Api.getMoviesList(getMoviesListCallback, {}, null);
*/

jQuery(function($){ 
		$('.searchResult').shopholic({
			apis: {
				searchItemMovie: {
					type: "GET",
					url: '/api/movies/',
			//		cache: false,
				//	data: data,
					dataType:'jsonp',
					contentType : 'text/html',
					dataType : 'jsonp',
					jsonp : 'callback',
					jsonpCallback: 'callback'
				},
				searchItemPeople : {
					type: "GET",
					url: '/api/users/',
			//		cache: false,
				//	data: data,
					dataType:'jsonp',
					contentType : 'text/html',
					dataType : 'jsonp',
					jsonp : 'callback',
					jsonpCallback: 'callback'
				},
				vobIt : {
					type: "POST",
					url: 'http://api.voblr.com/v1/vob/',
				//	dataType: 'json',
					data: {
						vobTxt: '',
						objectId: '',
						type: 'MOVIE'		
					}
				},
				followIt : {
					type: "GET",
					url: 'http://api.voblr.com/v1/follow/',
			//		dataType: 'json',
			//		data: {
			//			vobTxt: '',
			//			objectId: '',
			//			type: 'MOVIE'		
			//		}
				}
			}
		});
});


