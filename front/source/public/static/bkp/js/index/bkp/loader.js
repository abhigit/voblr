(function( $ ){
  var methods = {
    load : function() { 
    	$.ajax({
    		  url: 'testdata/test.html',
    		  success: function(data) {
    		    $('section').html(data);
    		    $('section').css({ opacity: 0 });
    		    var $container = $('section');
    			$container.imagesLoaded(function(){
    				$('section').animate({ opacity: 1 });
        			$('.init-loader').hide();
    			    $container.masonry({
    			    itemSelector : 'article',
    			    columnWidth: 222,
    			    isFitWidth: true,
    			    gutterWidth:30
    			  });
    			});

    		  }
    		});
    }
    
  };

  $.fn.loader = function( method ) {
    
    // Method calling logic
    if ( methods[method] ) {
      return methods[ method ].apply( this, Array.prototype.slice.call( arguments, 1 ));
    } else if ( typeof method === 'object' || ! method ) {
      return methods.init.apply( this, arguments );
    } else {
      $.error( 'Method ' +  method + ' does not exist on jQuery.tooltip' );
    }    
  
  };

})( jQuery ); 
