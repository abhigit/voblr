/*$("#loginRegister").click(function() {
	var selectedEffect = "blind";
     // most effect types need no options passed by default
	var options = {};
	$("#loginWithVoblr").toggle(selectedEffect, options, 500);
	console.log("toggled?");
});
*/

$('document').ready(function(){
	$('.dialogCtrl').bind('click',function(event){
		event.preventDefault()
		var targetModal = $(this).attr('href');
		var inDialogMode= $('.modal').hasClass('in');
		if(inDialogMode){
			$('.modal.in').each(function(index){
				$(this).modal('hide');
				$(this).on('hidden', function(){
					$(targetModal).modal({'show':true});
				});	
				$(this).off('hidden');
			});	
		}else{
			$(targetModal).modal({'show':true});
		}
	});	
});
var validateLoginCallback = function(response, params) {
	if(response && response.status == Api.SUCCESS) {
		if(!response.data || !response.data.emailid_verified) {
			console.log("emailid not verified: " + response.data.emailid_verified);
			alert("your email id is not verified");
		}
		top.location.href = "/"; // Login success
		return;
	} 
//	alert("Some problem in login response: " + response);
	$('.wNamePass').css('display','block');
	$('.signIn').button('reset');
	// there is some problem in login handle 
}

function submit_form(val, id){
	$('.signIn').button('loading');
	$('.signIn').addClass('disabled');
	var form    = document.getElementById(id);
	var username   = document.getElementById("login_field").value;
	var password   = document.getElementById("password_field").value;
	var params = {'username' : username, 'password' : password};
	if(params['username']  && params['password']){
		Api.validatelogincredntial(validateLoginCallback, params);
	}else{ 
//	  alert("input user name and password");
		$('.wNamePass').css('display','block');
		$('.signIn').removeClass('disabled');
		$('.signIn').button('reset');
	}
	return false; //Don't remove this (it is for preventing page self submit action.)
}


function validFormat() {
  if (password !== re_password) {
    alert("password and confirm password dose not match");
    return false;
  }
  var password = $("#password").val();
  var re_password = $("#re_password").val();
  var name = $("#name").val();
  var username = $("#username").val();
  var email_id = $("#email_id").val();
  var auth_regist_key = $("#auth_regist_key").val();
  var signup_via = $("#signup_via").val();
  return true;
}

function highlight_errors()
{
  return true;
}

var registerUserCallback = function(response, params) {
	if(response && response.status == Api.SUCCESS) {
		top.location.href = "/"; // Login success
		return;
	}else{
            //alert(response.status);
            //alert(response.message);
            alert("Authentication failed! Please check your details.!!");
        } 
	// there is some problem in registration 
}

$(document).ready(function() {
 // $(".registration_form input").blur(function(value) {
    // alert("loosing focus");
 // });

  $(".submit_registration_form").click(function() {
//    if (!validFormat()) {
//		console.log("validate format");
 //   	highlight_errors();
  //  	return;
   // }
    var password = $("#password").val();
    var re_password = $("#re_password").val();
    var name = $("#name").val();
    var username = $("#username").val();
    var email_id = $("#email_id").val();
    var auth_regist_key = $("#auth_regist_key").val();
    var signup_via = $("#signup_via").val();
    var params = {'name' : name, 'username' : username, 'password' : password, 'email_id' : email_id, 'signup_via' : signup_via, 'auth_regist_key' : auth_regist_key};
//    $(".submit_registration_form").attr("disabled", "disabled");
	Api.registerUser(registerUserCallback, params);
  });
});

