var ub;
var followers;
var followings;

$(document).ready(function(){

	$(".followers-popup").live({
		click: function(){
			$("#followers_following_list").empty();
			$("#loading_followers_followings").show();
			followers = new Array(); 
			var userid = $(this).attr("userid");
			var displayname = $(this).attr("name");
			var userimg = $(this).attr("img");
			var followersCount = $(this).attr("followersCount");
			
			if(userid == undefined) {
				alert("could not find userid id for getting followers list, please contact us for this problem");
				return;
			}
			
			var ff_user_name = displayname;
			var ff_user_id = userid;
			var ff_user_img = userimg;
			Log.info("being followed by: " + followersCount);
			$("#ff-user-img").attr("src", ff_user_img);
			$("#ff-user-id").attr("href", '/stats/'+ff_user_id+'/');
			$("#ff-user-id").text(ff_user_name);
			$("#ff_action_list").text('being followed by ('+followersCount+') users');
			if (!ub) {
				ub = new Userbase();
			}
			$("#followers_following_list").empty();
			ub.getFollowersList(userid, this);
		}
	});
	
	$(".followings-popup").live({
		click: function(){
			$("#followers_following_list").empty();
			$("#loading_followers_followings").show();
			followings = new Array();
			var userid = $(this).attr("userid");
			var displayname = $(this).attr("name");
			var userimg = $(this).attr("img");
			var followingsCount = $(this).attr("followingsCount");
			Log.info("following: " + followingsCount);
			
			if(userid == undefined) {
				alert("could not find userid id for getting followers list, please contact us for this problem");
				return;
			}
			
			var ff_user_name = displayname;
			var ff_user_id = userid;
			var ff_user_img = userimg;
			
			$("#ff-user-img").attr("src", ff_user_img);
			$("#ff-user-id").attr("href", '/stats/'+ff_user_id+'/');
			$("#ff-user-id").text(ff_user_name);
			$("#ff_action_list").text('following ('+followingsCount+') users');
//			$("#vob-from-user-moviename").text(vob_from_user_moviename);
			if (!ub) {
				ub = new Userbase();
			}
			ub.getFollowingsList(userid, this);
		}
	});
	
	
	$(".follow_user").live({
		click: function() {
		if (!ub) {
			ub = new Userbase();
		}
		$(this).text("wait...");
		ub.follow($(this).attr("userid"), this);
		}
	});
	$(".unfollow_user").live({
		click: function() {
			if (!ub) {
				ub = new Userbase();
			}
			$(this).text("wait...");
			ub.unfollow($(this).attr("userid"), this);
		}
	});
	
});

var Userbase = function() {
	var followunfollowCallback = function(response, params) {
		
		if(!response || response.status != Api.SUCCESS) {
			alert("Some problem in following user response: " + response);
			// there is some problem in vobbign a movie
			ub.closefollowUnfollowUsernotification(params['action'], params['element'], false);
			return;
		}
		ub.closefollowUnfollowUsernotification(params['action'], params['element'], true);
	}
	
    this.follow = function(userid, element) {
    	if(userid == null) {
    		alert("could not get user id, plese contact us to fix this problem");
    		this.closefollowUnfollowUsernotification('follow', element, false);
    	}
		var params = {'userid' : userid};
		var callbackparams = {'action' : 'follow', 'element' : element};
		Api.followuser(followunfollowCallback, params, callbackparams);
    }
    
    this.unfollow = function(userid, element) {
    	if(userid == null) {
    		alert("could not get user id, plese contact us to fix this problem");
    		this.closefollowUnfollowUsernotification('unfollow', element, false);
    	}
		var params = {'userid' : userid};
		var callbackparams = {'action' : 'unfollow', 'element' : element};
		Api.unfollowuser(followunfollowCallback, params, callbackparams);
    }
    
    this.closefollowUnfollowUsernotification = function(action, element, success) {
    	if(!success) {
    		alert("some error occured while following/unfollowing user");
        	if(action && action == 'follow') {
        		$(element).text("Follow");
        		$(element).attr('class', 'follow_user');
        	}
        	if(action && action == 'unfollow') {
        		$(element).text("Unfollow");
        		$(element).attr('class', 'unfollow_user');
        	}
        	return;
    	}
    	if(action && action == 'follow') {
    		$(element).text("Unfollow");
    		$(element).attr('class', 'unfollow_user');
    	}
    	if(action && action == 'unfollow') {
    		$(element).text("Follow");
    		$(element).attr('class', 'follow_user');
    	}
    }
    
	var followersFollowingsCallback = function(response, params) {
		$("#loading_followers_followings").hide();
		if(!response || response.status != Api.SUCCESS) {
			alert("Some problem in retriving followers/follwings list response: " + response);
			// there is some problem in vobbign a movie
			return;
		}
		
		var users = response.data;
		Log.info("Total users recevied: " + users.length);
		
		var targetlist = params['action'] == 'followers' ? followers : followings;
		var i = targetlist.length;
		targetlist = $.merge(targetlist, users);
		
		var followunfollowText = {"unfollow_user" : "Unfollow", "follow_user" : "Follow"};
		
		for(; i < targetlist.length; i++) {
			var user = targetlist[i];
			var followunfollow = user.following ? "unfollow_user" : "follow_user";
			var element = '<dt>' + i + ') ' + '<a href="/stats/'+user.id+'/">'+user.name+'</a>&nbsp;&nbsp;'+ 'vobs('+user.vobCount+')&nbsp;&nbsp;'
						  + '<a href="#followers-following-popup" class="fancybox followers-popup" userid="'+user.id+'" name="'+user.name+'" img="'+user.img+'" followersCount="'+user.followersCount+'" >followers('+user.followersCount+')&nbsp;&nbsp</a>'
						  + '<a href="#followers-following-popup" class="fancybox followings-popup" userid="'+user.id+'" name="'+user.name+'" img="'+user.img+'" followingsCount="'+user.followeingCount+'" >followings('+user.followeingCount+')&nbsp;&nbsp</a>'
						  + '<a class="'+followunfollow+'" userid="'+user.id+'">'+followunfollowText[followunfollow]+'</a>'
						  + '</dt>';
			$("#followers_following_list").append(element);
		}
		
	}

    this.getFollowersList = function(userid, element){
    	if(userid == null) {
    		alert("could not get user id to get followers list, plese contact us to fix this problem");
    		ub.closefollowUnfollowPopup();
    	}
		var params = {'userid' : userid};
		var callbackparams = {'action' : 'followers', 'element' : element};
		Api.getFollowers(followersFollowingsCallback, params, callbackparams);
    }

    this.getFollowingsList = function(userid, element){
    	if(userid == null) {
    		alert("could not get user id to get followings list, plese contact us to fix this problem");
    		ub.closefollowUnfollowPopup();
    	}
		var params = {'userid' : userid};
		var callbackparams = {'action' : 'followings', 'element' : element};
		Api.getFollowings(followersFollowingsCallback, params, callbackparams);
    }
    
	this.closefollowUnfollowPopup = function(){
		$.fancybox.close();
	}
    
}