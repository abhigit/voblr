;(function(w,d,$){

	$.fn.extend({ 
		shopholic: function (options) { 
			if ( ! $(this).length ) return; // no elements
			return this.each(function(){
				new $.ShopholicPlugin( $(this) , options );
			});
		} 
	});

	$.ShopholicPlugin = function ( node , options ){
		this.$target = node;
		this.settings = $.extend( {} , $.ShopholicPlugin.defaults , options );
		this.init();
	};
		
	$.extend($.ShopholicPlugin, { 
		prototype: { 
			$target: null,  
			settings: null,
			isLoading: false,
			loadedItemNum: 0,
			totalItemNum: 0,
//			targetLoadingPage: 0,
			targetAPI: null,
			targetTmpl: null,
			noMoreItems: null,
	
			init: function(){
				var self = this;
		
				//init autosize
				$('textarea').autosize();
				//init mosaic
				$('.bar2').mosaic({
					animation	:	'slide'
				});
				//init tooltip
				$("[rel=tooltip]").tooltip();
											
				//apply InfiniteScroll event
				if(self.settings.isInfiniteScroll){
				$(w).bind('scroll',$.proxy(self.onWindowScroll,self));
					$('.scrollTopButton').bind('click',$.proxy(self.onScrollTop,self));
				} 
				
				//apply loading event
				self.$target.bind('loadStart loadComplete loadError',$.proxy(self.onLoadProgress,self));

				//bind vob it, follow-unfollow, voteup, votedown, cancel(voteup/votedown),followers,following, movies you reviewed, people who reviewed, reply, points  
				$(".vobItInit").live('click', function(){
					var objId = $(this).attr('tgt-objid');
					self.settings.apis.vobIt.data.objectId = objId;
					var mvName = $(this).attr('tgt-mvname');
					$('span#movieName').text(mvName);
					$("#vobItModal").modal('toggle');
					$('#vobItNow').bind('click',$.proxy(self.onVobChange,self));
				});
		//		$('#vobItNow').click(function(){console.log("clicked");});
		

				$('.followInit').live('click',$.proxy(self.onFollowChange,self));

				//bind ui-controlgroup event
				if($('.ui-controlgroup')[0]){
					$('a','.ui-controlgroup')
						.bind('click',$.proxy(self.onViewChange,self))
						.eq(0).trigger('click');
				} 
				//when there is no controlgroup division (ex: stats page and newsfeedpage)
				else {
					for(key in self.settings.apis){
						self.targetAPI = key;
						self.targetTmpl = '#'+key+'Tmpl';
						break;
					}
					self.reloadList();
				} 
				
			},

			onVobChange: function(e){
				var self = this;
				var $btn = $(e.currentTarget);
				$('textarea#mytextarea').attr('disabled','disabled');
				$btn.attr('disabled','disabled');	
				$btn.button('loading');
				var text = $('textarea#mytextarea').val();
				
				self.settings.apis.vobIt.data.vobTxt = text;
				e.preventDefault();
				if(!$btn[disabled='disabled']) {
					switch(e.type){
						case 'click':
					//		console.log("lcame inside");
							var api = $btn.attr('href').slice(1);
		//							console.log(api);
							if(api in self.settings.apis){
								self.targetAPI = api;
								console.log(self.settings.apis);	
								var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],{
									success: function(json){		
										if(json.status=="OK"){
											console.log(json);
											$("#vobItModal").modal('hide');
										}			
									//	self.appendItem(json);
									}, error:function(){
										console.log('error');
									}
								});
							//	console.log(ajaxParams);
								$.ajax(ajaxParams);
							}  
					}
				} 
			},
			onFollowChange: function(e){
				console.log("in onFollowChange");
				var self = this;
				var $btn = $(e.currentTarget);
				$btn.attr('disabled','disabled');	
				var uId = $btn.attr('tgt-uid');
				console.log(uId);
				self.settings.apis.followIt.url =  "http://api.voblr.com/v1/follow/" + uId;
			//	console.log(uId);
				var api = $btn.attr('href').slice(1);
				e.preventDefault();
				if(!$btn[disabled='disabled']) {
					switch(e.type){
						case 'click':
							if(api in self.settings.apis){
								self.targetAPI = api;
								console.log(self.settings.apis);	
								var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],{
									success: function(json){		
											console.log(json);
										//	$("#vobItModal").modal('hide');
									//	self.appendItem(json);
									}, error:function(){
										console.log('error');
									}
								});
							//	console.log(ajaxParams);
								$.ajax(ajaxParams);
							}  
					}
				} 
			},
			
			onViewChange: function(e){
				var self = this;
				var $btn = $(e.currentTarget);
				
				e.preventDefault();
				if(!$btn.hasClass('active')) {
					switch(e.type){
						case 'click':
							var api = $btn.attr('href').slice(1);
							var tmpl = $btn.attr('href')+'Tmpl';
							console.log(tmpl);
						//if any ui-controls	
						//$btn.removeClass('ui-btn-up').addClass('ui-btn-active');
					
							//self.targetLoadingPage = 0;
							//self.loadedItemNum = 0;
							//self.totalItemNum = 0;

							if(api in self.settings.apis && $(tmpl)[0]){
								self.targetAPI = api;
								self.targetTmpl = tmpl; 
								self.reloadList();
							} 
					}
				}
			},
			
			onWindowScroll: function(e){
				var self = this;
				var doLoading = ($(w).scrollTop() + $(w).height() > $(d).height() - self.settings.loadingBottomY);
				var showButton = ($(w).scrollTop() > self.settings.scrollTopButton.top)
				 && ($(w).scrollTop() + $(w).height() < $(d).height() - self.settings.scrollTopButton.bottom);
				var showButtonAnime = showButton ? 'show' : 'hide';
				
				if(self.targetAPI && !self.isLoading && doLoading && !$('html').hasClass('menuOpen')) {
					console.log("start loading items");
				//	self.loadItem();
				}
				$('.scrollTopButton').animate({
					height: showButtonAnime
				},100);				

			}, 
			
			onScrollTop: function(e){
				e.preventDefault();
				switch(e.type){
					case 'click':
						$('html,body').animate({
							scrollTop: 0
						}, 200);
						break;
				}
			},
			
				
			onLoadProgress: function(e){
				var self = this;
				switch(e.type){
					case 'loadStart':
				//		self.isLoading = true;
						$('.spinner').css('display','block');
				//		self.loadedTimes++;
		/*	//			var spinner = new Spinner({
			//				color: '#ffffff'
						}).spin();
						var $progress = $('<div id="loadingProgress">')
							.fadeTo(0,0)
							.append('<p class="msg">Loading...</p>')
							.append($(spinner.el));
						
						//self.$target.hide();
						$('#loadingProgress').remove();
						$progress
							.appendTo(self.$target.parent())
							.stop(true).animate({
								opacity: 0.5
							},200); */
						break;
					case 'loadComplete':
				/*		var $progress = $('#loadingProgress');
						var $nodata = $('#no-data');
						$progress.animate({
							opacity: 0
						},200,function(){
							$progress.remove();
							self.isLoading = false;
						});
						if(self.loadedItemNum === 0){
							$nodata.slideDown();
						} else {
							$nodata.slideUp();
						} */
						$('.spinner').css('display','none');
						break;
				}
			},
			
			reloadList: function(params){
				var self = this;
				
				if(self.$target.children()[0]){
					self.$target.empty();
					//	.masonry('remove',self.$target.children() )
					//	.masonry('reload');
				} 
				self.loadItem(params);
			},
			
			loadItem: function(params){
				var self = this;
	//			console.log(self.settings);
	//			console.log(self.targetAPI);
				var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],params,{
					success: function(json){				
					//	if('summary' in json){
				//			self.totalItemNum = json.summary.all_count;
				//			if(json.summary.get_count < 1){
				//				self.$target.trigger('loadComplete');
				//				return false;
				//			}
			//			}
				//		self.$target.trigger('loadComplete');
						self.appendItem(json);
					}, error:function(){
						//console.log('error');
					}
				});
				
		//		if( /* self.targetLoadingPage === 0 || /* self.loadedItemNum < self.totalItemNum){
					self.$target.trigger('loadStart');
				/*	self.targetLoadingPage++;
					if('data' in ajaxParams) ajaxParams.data.page = self.targetLoadingPage; */
					$.ajax(ajaxParams);
		//		}
		/*		else {
					if(!self.noMoreItems){
						self.noMoreItems = 1;
						$("#nomore").css("display","block");
					}
				} */
			},
			
			appendItem: function(listData){

// have to add switch with cases (getMovielist api or howzitapi or followapi)
// vote unvote follow unfollow
// howzit reply cancelvote
// bannerAPIs
// detailed list APIs (followers,following,vobs)
// login APIs register validate login credential
				var self = this;
//				console.log(listData);
				template = $(self.targetTmpl).html();
				var html = Mustache.to_html(template,listData);
				self.$target.html(html).css({opacity: 0});
//				$boxes.css({ opacity: 0 });
//				$boxes.animate({ opacity: 1 });
	

/*				var $newItem = $(self.targetTmpl)
					.tmpl(listData,{
						index: function(){
							var i = 0;
							return function(){
								return ++i;
							}
						}()
					})
					.filter(function(){ return this.nodeType !== 3})
					.appendTo(self.$target); */
				
				self.$target.imagesLoaded(function(){
			//		var currentItemNum = self.loadedItemNum;
			//		self.loadedItemNum += $newItem.length;
					self.$target.trigger('loadComplete');
			//		$('.banner').animate({ opacity: 1});
					self.$target.animate({ opacity: 1});
				//	$(this).masonry('appended',$newItem,currentItemNum === 0 ? false : true);
					
				}); 
			} 
			
		}
	});
	
	$.ShopholicPlugin.defaults = {
		isInfiniteScroll: true,
		loadingBottomY: 800,
		scrollTopButton: {
			top: 100,
			bottom: 200
		}
	};

})(window,document,jQuery);
