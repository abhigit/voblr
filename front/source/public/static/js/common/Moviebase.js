$(document).ready(function(){
	
	$(".vob-link").live({
		click: function(){
			var movieid = $(this).attr("movieid");
			var moviename = $(this).attr("moviename");
			if(movieid == undefined) {
				alert("could not find movie id for vob, please contact us for this problem");
				Moviebase.closeVobPopup();
				return;
			}
			
			Moviebase.movieid = movieid;
			
			var vob_from_user_name = $('input[name=userdisplayname]').val();
			var vob_from_user_img = $('input[name=userimageURL]').val();
			
			var vob_from_user_movieurl = "/movie/"+movieid+"/";
			var vob_from_user_moviename = moviename;
			
			$("#vob-from-user-name").text(vob_from_user_name);
			$("#vob-from-user-img").attr("src", vob_from_user_img);
			$("#vob-from-user-movieurl").attr("href", vob_from_user_movieurl);
			$("#vob-from-user-moviename").text(vob_from_user_moviename);
		}
	});
	
	$(".vob_movie").live({
		click: function() {
			Moviebase.vobit();
		}
	});
});

var Moviebase = new function() {
	$(".fancybox").fancybox();
	this.movieid = null;

	var vobMovieCallback = function(response, params) {
		if(!response || response.status != Api.SUCCESS) {
			alert("Some problem in vobbing movie response: " + response);
			// there is some problem in vobbign a movie
			return;
		}
		Log.alert("movie vobbed success fully");
		Moviebase.closeVobPopup();
	}
    this.vobit = function() {
    	if(this.movieid == null) {
    		alert("could not get movie id, plese contact us to fix this problem");
    		this.closeVobPopup();
    	}
    	var vobtext = $("#vob-from-user-vobtext").val();
    	Log.info("vobbing: " + this.movieid +", text: " + vobtext);
		var params = {'movieid' : this.movieid, 'vob-text' : vobtext};
		Api.vobit(vobMovieCallback, params);
    }
    this.closeVobPopup = function() {
    	this.movieid = null;
    	$("#vob-from-user-vobtext").val("");
    	$.fancybox.close();
    }
}