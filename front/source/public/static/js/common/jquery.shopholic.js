;(function(w,d,$){

   $.fn.extend({ 
      shopholic: function (options) { 
         if ( ! $(this).length ) return; // no elements
         return this.each(function(){
            new $.ShopholicPlugin( $(this) , options );
         });
      } 
   });

   $.ShopholicPlugin = function ( node , options ){
      this.$target = node;
      this.settings = $.extend( {} , $.ShopholicPlugin.defaults , options );
      this.init();
   };
      
   $.extend($.ShopholicPlugin, { 
      prototype: { 
         $target: null,  
         settings: null,
         isLoading: false,
         nextId: "",
         q: "",
         targetAPI: null,
         targetTmpl: null,
         vobLoading: false,
         commentLoading: false,
         followLoading: false,
         voteLoading: false,
         listLoading: false,
   
         init: function(){
            var self = this;
			
			//init popover 
            $("[data-toggle=popover]").popover('show');
			if(document.URL.indexOf("/search") >= 0){ 
				$(".popoverOne[data-toggle=popover]").popover('destroy');
			}
			if(document.URL.indexOf("/movie") >= 0){ 
				$(".vobItInit[data-toggle=tooltip]").tooltip('show');
			}
			// apply focus to search 
			$("input:text:visible:first").focus();
			
			//init timeago
			$('time').timeago();

			//init wysihtml5

			$('#mytextarea').wysihtml5({
				"font-styles": true, //Font styling, e.g. h1, h2, etc. Default true
				"emphasis": true, //Italics, bold, etc. Default true
				"lists": true, //(Un)ordered lists, e.g. Bullets, Numbers. Default true
				"html": false, //Button which allows you to edit the generated HTML. Default false
				"link": false, //Button to insert a link. Default true
				"image": false, //Button to insert an image. Default true,
				"color": false //Button to change color of font  
			});
		
			$('#target').submit(function() {
				self.$target.empty();
				self.q = "*";
				self.nextId = "";

				if("nextId" in self.settings.apis[self.targetAPI].data){
					delete self.settings.apis[self.targetAPI].data.nextId;
				}
				self.settings.apis[self.targetAPI].data.q = "*";
			//	console.log("in input" +self.q);
			var flag=	$('.ui-controlgroup a.active')
					.attr('flag');
				if($('.ui-controlgroup')[0]){
					$('a','.ui-controlgroup')
					.unbind();
					$('.ui-controlgroup a')
					.removeClass('active');
					$('a','.ui-controlgroup')
					.bind('click',$.proxy(self.onViewChange,self))
				    .eq(flag).trigger('click');
				} 
				return false;
			});
	
			//apply and init masonry
			self.$target.masonry({itemSelector: '.applyMasonry'});


            //apply InfiniteScroll event
            if(self.settings.isInfiniteScroll){
            $(w).bind('scroll',$.proxy(self.onWindowScroll,self));
               $('.scrollTopButton').bind('click',$.proxy(self.onScrollTop,self));
            } 
            
			//apply loading event
            self.$target.bind('loadStart loadComplete loadError',$.proxy(self.onLoadProgress,self));

            //bind vob it, follow-unfollow, voteup, votedown, cancel(voteup/votedown),followers,following, movies you reviewed, people who reviewed, reply, points  
            $(".vobItInit").live('click', function(){
			if($('#loginCheck').text() === "1"){
                // need objid , movie name
				$("#vobEdit").removeClass('hide');	
				$("#vobSuc").addClass('hide');	
				var objId = $(this).attr('tgt-objid');
				self.settings.apis.vobIt.data.objectId = objId;
				var mvName = $(this).attr('tgt-mvname');
				$('span#movieName').text(mvName);
                // enable textarea and vob button - textarea value , height, button , reset loading
				$("#vobItModal").modal('toggle');
				$('#vobItNow').bind('click',$.proxy(self.onVobChange,self));
//				tinyMCE.activeEditor.setContent('<p>Verdict: One time watch.<span id="caret_pos_holder"></span></p>');
//				tinyMCE.activeEditor.selection.select(tinyMCE.activeEditor.dom.select('span#caret_pos_holder')[0]); //select the span
//				tinyMCE.activeEditor.dom.remove(tinyMCE.activeEditor.dom.select('span#caret_pos_holder')[0]); //remove the span
				//tinyMCE.activeEditor.focus();
			}
			else {
				$('.dialogCtrl').trigger('click');
			}
            });
            
            $('.followInit').live('click',$.proxy(self.onFollowChange,self));
            $('.voteUpInit').live('click',$.proxy(self.onVoteUpChange,self));
            $('.voteDownInit').live('click',$.proxy(self.onVoteDownChange,self));
            $('.replyInit').live('click',$.proxy(self.onReplyChange,self));
            $('.showComment').live('click',$.proxy(self.showCommentChange,self));

            //bind ui-controlgroup event
            if($('.ui-controlgroup')[0]){
               $('a','.ui-controlgroup')
                  .bind('click',$.proxy(self.onViewChange,self))
                  .eq(0).trigger('click');
            } 
         },

         onVobChange: function(e){
            var self = this;
			if(self.vobLoading == false) {
			self.vobLoading = true;
            var $btn = $(e.currentTarget);
            $btn.button('loading');
            self.settings.apis.vobIt.data.vobTxt = $('#mytextarea').val();
            e.preventDefault();
               switch(e.type){
                  case 'click':
                     var api = $btn.attr('href').slice(1);
                     if(api in self.settings.apis){
                        self.targetAPI = api;
                        var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],{
                           success: function(json){      
                              if(json.status=="OK"){
                                $btn.attr('disabled','disabled');   
                                var movieUrl = '/movie/'+self.settings.apis.vobIt.data.objectId+'/'; 
                                $.ajax({
                                    url     : "/api/postonfacebook",
                                    data    : { target: movieUrl, 
                                                type  : 'vob' },
                                    success : function(data){
                                //                console.log('Posted on FB successFully');
                                              },
                                    error   : function(){
                                 //               console.log('Error in FB posting');
                                              }
                                });
								//$('.alertDiv').html('<div class="alert alert-success span4 offset2"> You have successfully shared your opinion </div>');
                                window.setTimeout(function(){
									//$("#vobItModal").modal('hide');
									$('#movieRedir').attr('href',movieUrl);
									$("#vobEdit").addClass('hide');	
									$("#vobSuc").removeClass('hide');	
									$btn.button('reset');
								},1000);
                                // FB post Vote Up
                              }         
                           }, error:function(){
                               // console.log('error');
                           }
                        });
                        $.ajax(ajaxParams);
                     }  
               }
         }
		 self.vobLoading = false;
         },
         onFollowChange: function(e){
            var self = this;
			if(self.followLoading==false){
			self.followLoading = true;
            var $btn = $(e.currentTarget);
            var uId = $btn.attr('tgt-uid');
            self.settings.apis.followIt.data.userId = uId;
            var api = $btn.attr('href').slice(1);
            e.preventDefault();
           // if(!$btn[disabled='disabled']) {
               switch(e.type){
                  case 'click':
                     if(api in self.settings.apis){
                        self.targetAPI = api;
                        var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],{
                           success: function(json){
                                if(json.status=="OK"){
                                    $btn.attr('disabled','disabled');   
                                    $btn.removeClass('btn-danger').text(' Following').prepend("<i></i>").find('i').addClass('icon-ok');
                                var followUrl = '/stats/'+self.settings.apis.followIt.data.userId;
                                $.ajax({
                                    url     : "/api/postonfacebook",
                                    data    : { target: followUrl, 
                                                type  : 'follow' },
                                    success : function(data){
											//	console.log("facebook response"+data);
                                             //   console.log('Posted on FB successFully');
                                              },
                                    error   : function(){
                                                //console.log('Error in FB posting');
                                              }
                                });
                                }      
                           }, error:function(){
                              //console.log('error');
                           }
                        });
                        $.ajax(ajaxParams);
                     }  
               }
         } 
		self.followLoading = false;
         },
         onVoteUpChange: function(e){
			if($('#loginCheck').text() === "1"){
            var self = this;
			if(self.voteLoading==false){
			self.voteLoading=true;
            var $btn = $(e.currentTarget);
            var vId = $btn.attr('tgt-vid');
            var mId = $btn.attr('tgt-mid');
            self.settings.apis.voteUpIt.data.vobId = vId;
            var api = $btn.attr('href').slice(1);
            e.preventDefault();
               switch(e.type){
                  case 'click':
                     if(api in self.settings.apis){
                        self.targetAPI = api;
                        var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],{
                           success: function(json){      
                                if(json.status=="OK"){
                                    //$btn.attr('disabled','disabled');   
                                    var voteUpCount = $('#voteUpCount'+vId).text();    
                                    $('#voteUpCount'+vId).text(parseInt(voteUpCount)+1);    
                                    $('#rmUp'+vId).hide();    
                                    $('#rmDown'+vId).hide();    
									$('#voteResult'+vId).html('<i class="icon-chevron-up"></i>You liked this');
                                var voteUpUrl = '/vob/'+self.settings.apis.voteUpIt.data.vobId;
                                $.ajax({
                                    url     : "/api/postonfacebook",
                                    data    : { target: voteUpUrl, 
                                                type  : 'like' },
                                    success : function(data){
                                                //console.log('Posted on FB successFully');
                                              },
                                    error   : function(){
                                                //console.log('Error in FB posting');
                                              }
                                });
                                    
                                }      
                           }, error:function(){
                              //console.log('error');
                           }
                        });
                        $.ajax(ajaxParams);
                     }  
               }
            }
			self.voteLoading = false;
            } 
			else {
				$('.dialogCtrl').trigger('click');
			}
         },
         onVoteDownChange: function(e){
            var self = this;
            var $btn = $(e.currentTarget);
            $btn.attr('disabled','disabled');   
            var vId = $btn.attr('tgt-vid');
            self.settings.apis.voteDownIt.data.vobId = vId;
            var api = $btn.attr('href').slice(1);
            e.preventDefault();
            if(!$btn[disabled='disabled']) {
               switch(e.type){
                  case 'click':
                     if(api in self.settings.apis){
                        self.targetAPI = api;
                        var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],{
                           success: function(json){      
                                if(json.status=="OK"){
                                    var totalCount = $('#totalCount'+vId).text();    
                                    $('#totalCount'+vId).text(parseInt(totalCount)+1);    
                                    $('#rmUp'+vId).hide();    
                                    $('#rmDown'+vId).hide();    
									$('#voteResult'+vId).html('<i class="icon-chevron-down"></i>You voted down this');
                                var voteDownUrl = '/vob/'+self.settings.apis.voteDownIt.data.vobId;
                                $.ajax({
                                    url     : "/api/postonfacebook",
                                    data    : { target: voteDownUrl, 
                                                type  : 'vote' },
                                    success : function(data){
                                                //console.log('Posted on FB successFully');
                                              },
                                    error   : function(){
                                                //console.log('Error in FB posting');
                                              }
                                });

                                }      
                                 //console.log(json);
                           }, error:function(){
                              //console.log('error');
                           }
                        });
                        $.ajax(ajaxParams);
                     }  
               }
            } 
         },
         
         onReplyChange: function(e){
            var self = this;
			if(self.commentLoading==false){
			self.commentLoading = true;
            var $btn = $(e.currentTarget);
            var vId = $btn.attr('tgt-vobid');
            var vComment = $('#txt'+vId).val();    
            self.settings.apis.replyIt.data.vobId = vId;
            self.settings.apis.replyIt.data.comment = vComment;
            var api = $btn.attr('href').slice(1);
            e.preventDefault();
            if(vComment!="") {
               switch(e.type){
                  case 'click':
                     if(api in self.settings.apis){
                        self.targetAPI = api;
                        var ajaxParams = $.extend({},self.settings.apis[self.targetAPI],{
                           success: function(json){     
                            if(json.status=="200"){ 
                                $('#txt'+vId).val("");    
                                $('#txt'+vId).css("height","20px");    
                                $btn.removeAttr('disabled');   
                                var userImgURL = $("#userImgURL").text();
                                var userName = $("#userName").text();
                                $("#userReplies"+vId).append('<div class="userReply"><img class="userImageSmall" alt="" src="'+userImgURL+'"> <div class="userReplyText"><b><a href="/stats">'+userName+ '</a></b> '+vComment+'</div></div>');
								self.$target.masonry('reload');
                            }
                           }, error:function(){
                              console.log('error');
                           }
                        });
                        $.ajax(ajaxParams);
                     }  
               }
            } 
         } 
			self.commentLoading = false;
         },
         showCommentChange: function(e){
			if($('#loginCheck').text() === "1"){
            var self = this;
            var $btn = $(e.currentTarget);
            e.preventDefault();
           // if(!$btn.hasClass('active')) {
               switch(e.type){
                  case 'click':
                     var vId = $btn.attr('tgt-vid');
                    // var tmpl = $btn.attr('href')+'Tmpl';
                    var status = $("#showThis"+vId).attr("class");
					if(status==="hide"){$("#showThis"+vId).attr("class","show");}
					else{$("#showThis"+vId).attr("class","hide");}
			        self.$target.masonry('reload');
					break;
               }
            }
			else {
				$('.dialogCtrl').trigger('click');
			}
         },
         onViewChange: function(e){
            e.preventDefault();
			if($('#loginCheck').text() === "1"){
            var self = this;
		//	if(!self.isLoading){
            var $btn = $(e.currentTarget);
            //$('a','.ui-controlgroup').button('reset');
            $btn.button('toggle');
            if($btn.hasClass('active')) {
               switch(e.type){
                  case 'click':
                     var api = $btn.attr('href').slice(1);
                     var tmpl = $btn.attr('href')+'Tmpl';
                     if(api in self.settings.apis && $(tmpl)[0]){
                        self.targetAPI = api;
                        self.targetTmpl = tmpl; 
                        self.reloadList();
                     } 
               }
            }
         //   }
            }
			else {
				setTimeout(function(){$('.dialogCtrl').trigger('click');},1000);
			}
         },
         
         onWindowScroll: function(e){
            var self = this;
            var doLoading = ($(w).scrollTop() + $(w).height() > $(d).height() - self.settings.loadingBottomY);
            var showButton = ($(w).scrollTop() > self.settings.scrollTopButton.top)
             && ($(w).scrollTop() + $(w).height() <= $(d).height() - self.settings.scrollTopButton.bottom);
            var showButtonAnime = showButton ? 'show' : 'hide';
            if($('.ui-controlgroup')[0]){
               var $btn = $('a.active');
               var api = $btn.attr('href').slice(1);
                var tmpl = $btn.attr('href')+'Tmpl';
                        self.targetAPI = api;
                        self.targetTmpl = tmpl; 
                if(self.targetAPI && !self.isLoading && doLoading) {
                    if(self.nextId != ""){
                        params = {data: {nextId:self.nextId}};
                        self.loadItem(params);
                    }    
                    else {
                       self.$target.trigger('loadComplete');
                        //console.log("No items to load");
                    }
                }
            } 
            $('.scrollTopButton').animate({
               height: showButtonAnime
            },100);            
			if(document.URL.indexOf("/newsfeed") >= 0 || document.URL.indexOf("/movie") >= 0 || document.URL.indexOf("/stats") >= 0 ){ 
				$(".popoverOne[data-toggle=popover]").popover('destroy');
			}
			

         }, 
         
         onScrollTop: function(e){
            e.preventDefault();
            switch(e.type){
               case 'click':
                  $('html,body').animate({
                     scrollTop: 0
                  }, 200);
                  break;
            }
         },
         
            
         onLoadProgress: function(e){
            var self = this;
            switch(e.type){
               case 'loadStart':
                  self.isLoading = true;
                  $('.spinner').css('display','block');
                  break;
               case 'loadComplete':
                  $('.spinner').css('display','none');
                  var str= $('#tgt-userhideId').text();
                  $(".hideFollow"+str).hide();    
                  break;
            }
         },
         
         reloadList: function(params){
            var self = this;
			self.nextId = "";
			$('.noitems').addClass('hide');
			// Dont delete this - its a fix
							if(document.URL.indexOf("/search") >= 0){ 
								self.settings.apis[self.targetAPI].data.q = "*";
								self.q=$('#target input').attr('value');
								if(self.q != "*" && self.q != ""){
									if("q" in self.settings.apis[self.targetAPI].data)	{
										self.settings.apis[self.targetAPI].data.q = self.q;
									}
									// self.loadItem(params);
								}    
							}
			if(document.URL.indexOf("/search") >= 0 ||  document.URL.indexOf("/movie") >= 0 || document.URL.indexOf("/stats") >= 0 ){ 
				if("nextId" in self.settings.apis[self.targetAPI].data){
					delete self.settings.apis[self.targetAPI].data.nextId;
				}
			}

            if(self.$target.children()[0]){
               self.$target
               .masonry('remove',self.$target.children() )
               .masonry('reload');
            } 
            self.loadItem(params);
         },
         
         loadItem: function(params){
            var self = this;
            var ajaxParams = $.extend(true,self.settings.apis[self.targetAPI],params,{
               success: function(json){
		//			console.log(json);  
					if(json.count==="0"){
						$('.noitems').removeClass('hide');
					}          
					if("nextId" in json){
						self.nextId = json.nextId;
						//console.log(self.nextId);
					}
					else {
						self.nextId = "";
					}
					if(json.count==0){
						self.$target.trigger('loadComplete');
						return false;
					} 
					self.filterItem(json);
               }, error:function(){
                  //console.log('error response');
               }
            });
               self.$target.trigger('loadStart');
               $.ajax(ajaxParams);
         },
    
        filterItem : function(listData){
            var self = this;
            var    stringToBoolean= function(value,index){
                switch(value.isFollowing){
                    case "true": value.isFollowing= true ; 
                    break;
                    case "false": value.isFollowing= false;
                    break;
                }
            }    
            var    stringToBoolean1= function(value,index){
                switch(value.userVotedUp){
                    case "true": value.userVotedUp= true ; 
                    break;
                    case "false": value.userVotedUp= false;
                    break;
                }
            }    
            var    stringToBoolean2= function(value,index){
                switch(value.userVotedDown){
                    case "true": value.userVotedDown= true ; 
                    break;
                    case "false": value.userVotedDown= false;
                    break;
                }
            }    
            
            if(listData.hasOwnProperty("userList")){
                listData.userList.forEach(stringToBoolean);
            }    
            if(listData.hasOwnProperty("vobList")){
                    $.each(listData.vobList,function(index,value){
                        if(value.hasOwnProperty("vobComments")){
                            if(value.vobComments.hasOwnProperty("comments")){
                                value.vobComments.comments.reverse();
                            }    
                        }
                    });
            }    
            if(listData.hasOwnProperty("vobList")){
                        listData.vobList.forEach(stringToBoolean1);
            }
            if(listData.hasOwnProperty("vobList")){
                        listData.vobList.forEach(stringToBoolean2);
            }
            self.appendItem(listData);
        },
     
         appendItem: function(listData){
            var self = this;
            template = $(self.targetTmpl).html();
            var htmlCon = Mustache.to_html(template,listData);
            $(htmlCon).appendTo(self.$target).timeago();
            //$(htmlCon).appendTo(self.$target).css({opacity: 0}).timeago();
            var $newItem = $(htmlCon);
			        self.$target.masonry('reload');
					self.$target.trigger('loadComplete');
					//self.$target.trigger('loadComplete').find('li , .itemIterate').animate({ opacity: 1});
					self.isLoading = false;

			//init tooltip
            $("[data-toggle=tooltip]").tooltip(); 
			
			//fb dialog temp
				if($('#loginCheck').text() === "0"){
					setTimeout(function(){
						if(!$('#logInDialog').hasClass('in')){
							$('.dialogCtrl').trigger('click');
						}
					},5000);
				}
         } 
         
      }
   });
   
   $.ShopholicPlugin.defaults = {
      isInfiniteScroll: true,
      loadingBottomY: 800,
      scrollTopButton: {
         top: 100,
         bottom: 0 
      }
   };

})(window,document,jQuery);
