//search
jQuery(function($){ 
		var movieStr= $('#tgt-movieId').text();
		var userStr= $('#tgt-userId').text();
		$('.queryResult').shopholic({
			apis: {
				searchItemMovie: {
					type: "GET",
					url: '/api/movies/',
					dataType:'jsonp',
					data : {
						q: '*',
						count: 10 
					}, 
					dataType : 'jsonp',
					jsonp : 'callback',
					jsonpCallback: 'moviesList'
				},
				searchItemPeople : {
					type: "GET",
					url: '/api/users/',
					dataType:'jsonp',
					data : {
						q: '*',
						count: 10 
					//	lastid will be when you scroll from shopholic
					}, 
					jsonp : 'callback',
					jsonpCallback: 'usersList'
				},
				vobIt : {
					type: "POST",
					url: '/api/vob/',
					dataType: 'json',
					data: {
						vobTxt: null,
						objectId: null,
						type: 'MOVIE'		
					}
				},
				followIt : {
					type: "GET",
					url: '/api/follow/',
					dataType: 'json',
					data : { 
						userId: null 
					}
				},
				replyIt : {
					type: "GET",
					url: '/api/vobcomment/',
					dataType: 'json',
					data : { 
						vobId: '', 
						comment:''
					}
				},
				voteUpIt : {
					type: "GET",
					url: '/api/voteup/',
					dataType: 'json',
					data : { 
						vobId: ''
					}
				},
				voteDownIt : {
					type: "GET",
					url: '/api/votedown/',
					dataType: 'json',
					data : { 
						vobId: ''
					}
				},
				feedItemRecent: {
					type: "GET",
					url: '/api/movievob/',
					dataType : 'json',
                    data : {
                        movieId : movieStr,
				//		nextId : 
						count: 5
					}
				},
				feedItemPopular: {
					type: "GET",
					url: '/api/getpopularmovies/',
					dataType : 'json',
                    data : {
                        movieId : movieStr,
				//		nextId : 
						count: 5
					}
				},
				newsFeedRecent: {
					type: "GET",
					url: '/api/newsfeed/',
					dataType : 'json',
					data: {
					//  nextId: ,
						count: 5 
					}
				},
				feedItemMoviesWatched : {
					type: "GET",
					url: '/api/movieswatched/',
					dataType : 'json',
					data: {			
						count: 5,
						userId: userStr
					}
				},
				feedItemTimeline : {
					type: "GET",
					url: '/api/uservob/',
					dataType : 'json',
					data: {			
						count: 5,
						userId: userStr
					}
				},
				feedItemFollowers : {
					type: "GET",
					url: '/api/getfollowers/',
					dataType : 'json',
					data: {			
						userId: userStr,
						count: 5
					}
				},
				feedItemFollowing : {
					type: "GET",
					url: '/api/getfollowing/',
					dataType : 'json',
					data: {			
						userId: userStr,
						count: 5
					}
				}
			}
		});
});


