jQuery(function () {
	(function(d) {
		var js, id = 'facebook-jssdk', ref = d.getElementsByTagName('script')[0];
		if (d.getElementById(id)) {
			return;
		}
		js = d.createElement('script');
	    js.id = id;
	    js.async = true;
	    js.src = "//connect.facebook.net/en_US/all.js#appId=270237853057532";
	    ref.parentNode.insertBefore(js, ref);
	}(document));
	$(".fbLogin").bind('click', function() {
		$('.ajaxfb').removeClass('hide').css('display','inline-block'); 
		window.fbAsyncInit = function() {
		//	  var FB;
			FB._initialized = false;
			FB.init();
			FB.init({
				appId : '270237853057532',
				cookie : true,
				xfbml : true,
				oauth : true
			});
			FB.Event.subscribe('auth.login', function(response) {
				top.location.href = "/auth/facebooklogin/";
			});
		};
		FB.getLoginStatus(function(response) {
			if (response.status === 'connected') {
				top.location.href = "/auth/facebooklogin/";
			}
		}, true);
	});

    $('.dialogCtrl').bind('click',function(event){
        event.preventDefault()
        var targetModal = $(this).attr('href');
        var inDialogMode= $('.modal').hasClass('in');
        if(inDialogMode){
            $('.modal.in').each(function(index){
                $(this).modal('hide');
                $(this).on('hidden', function(){
                    $(targetModal).modal({'show':true});
                });    
                $(this).off('hidden');
            });    
        }else{
            $(targetModal).modal({'show':true});
        }
    });    

    $(window).scroll(function(){
        // add navbar opacity on scroll
        if ($(this).scrollTop() > 100) {
            $(".navbar.navbar-fixed-top").addClass("scroll");
        } else {
            $(".navbar.navbar-fixed-top").removeClass("scroll");
        }

        // global scroll to top button
        if ($(this).scrollTop() > 300) {
            $('.scrolltop').fadeIn();
        } else {
            $('.scrolltop').fadeOut();
        }        
    });

    $('#myCarousel').carousel('cycle');

    // scroll back to top btn
    $('.scrolltop').click(function(){
        $("html, body").animate({ scrollTop: 0 }, 700);
        return false;
    });
    
    // scroll navigation functionality
    $('.scroller').click(function(){
    	var section = $($(this).data("section"));
    	var top = section.offset().top;
        $("html, body").animate({ scrollTop: top }, 700);
        return false;
    });

    // FAQs
    var $faqs = $("#faq .faq");
    $faqs.click(function () {
        var $answer = $(this).find(".answer");
        $answer.slideToggle('fast');
    });

    if (!$.support.leadingWhitespace) {
        //IE7 and 8 stuff
        $("body").addClass("old-ie");
    }
});
