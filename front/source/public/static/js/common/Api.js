var Api = new function() {
	this.SUCCESS = "200";
    this.timeout = 18E3; // 18 second timeout
    this.api_base_url = 'http://api.ibhupi.com:9998/';
    this.api_base_url_java = 'http://api.voblr.com:9998/v1/';
    
	
	this.hostname = function() {
		var domain = window.location.hostname;
		var parts = domain.toString().split('.'.toString());
		var hostname = "." +parts[parts.length -2] + "." + parts[parts.length-1]
		return hostname;
	}
    
    /********************
     * Jsonp request  
     * @callback(response)
     ********************/
    this.jsonprequest = function (type, url, data, callback) {
        $.ajax({
            type         : type,
            url          : url,
            data         : data,
            contentType  : 'text/html',
            dataType     : 'jsonp',
            jsonp        : 'callback',
            jsonpCallback: 'callback',
			success : callback 
        });
    };

    /********************
     * Normal request  
     * internal callback(response)
     ********************/
    this.callback_request = function (type, url, data, callback, callback_params) {
		console.log(data);
        $.ajax({
            type    : type,
            url     : url,
            data    : data,
            timeout : this.timeout,
            success : function(response) {
                //alert(callback);
                callback(response, callback_params);
            },
            error   : function(response) {
            	callback(response, callback_params);
            }
        });
    };





    /********************
     * Test request  
     * internal callback(response)
     ********************/
    this.request = function (type, url, data, callback) {
        $.ajax({
            type     : type,
            url      : url,
            data     : data,
            dataType : 'jsonp',
            timeout  : this.timeout,
            callback : callback,
            jsonpCallback: callback 
        });
    };



    this.getAllMovies = function (callback, params, callback_params) {
        this.callback_request('post', '/api/getallmovies/', params, callback, callback_params);
    }
    
    this.vobit = function (callback, params, callback_params) {
    	this.callback_request('post','/apimock/vob/',params,callback,callback_params);
    }
    
    this.validatelogincredntial = function (callback, params, callback_params) {
        this.callback_request('post','/api/validatelogincredential/',params,callback,callback_params);
    }
    
    this.getmovieslist = function (callback, params, callback_params) {
        this.callback_request('post','/apimock/movies/',params,callback,callback_params);
    } 
    
    this.getvobfeedlist = function (callback, params, callback_params) {
        this.callback_request('post','/apimock/vobfeeds/',params,callback,callback_params);
    } 
    
    this.getuserslist = function (callback, params, callback_params) {
        this.callback_request('post','/apimock/getuserslist/',params,callback,callback_params);
    } 
    
    this.registerUser = function (callback, params, callback_params) {
    	this.callback_request('post','/api/registeruser/',params,callback,callback_params);
    }
    this.passwordReset = function (callback, params, callback_params) {
    	this.callback_request('post','/passwordreset/send/',params,callback,callback_params);
    }
    	
    
    this.vobCount = function(callback, params, callback_params){
        var api_name      = 'vobcount/';
        var api_local_url = 'http://api.ibhupi.com:9998/movie/';
        var target_url    = api_local_url+api_name+params['id'];
        this.jsonprequest('get', target_url, {}, callback);
    }
   
    this.getNotifications = function(callback, params){
        var api_name      = 'newsfeed/';
        var api_local_url = 'http://api.ibhupi.com:9998/user/';
        var target_url    = api_local_url+api_name;
        this.jsonprequest('get',target_url, params, callback); 
    }
    
    this.vobPostFb = function (callback, params , callback_params){
        this.callback_request('post', '/api/postvobfeedfb', params, callback, callback_params);
    }

    this.getTimeline = function (callback, params){
        var api_name      = 'vobline';
        var api_local_url = 'http://api.ibhupi.com:9998/';
        var target_url    = api_local_url+api_name;
        this.jsonprequest('get',target_url, params, callback); 
    }
  
    this.getMoviesList = function (callback, params){
        var api_name      = 'movies';
        var api_local_url = 'http://api.voblr.com:9998/';
        var target_url    = api_local_url+api_name;
        this.jsonprequest('get',target_url, params, callback); 
    }


    this.getUserInfo = function (callback, params, callback_params){
        this.request('get', '/api/getUserInfo', params, callback);
    }

    this.followuser = function (callback, params, callback_params){
        this.callback_request('post','/apimock/followuser/',params,callback,callback_params);
    }

    this.unfollowuser = function (callback, params, callback_params){
        this.callback_request('post','/apimock/unfollowuser/',params,callback,callback_params);
    }
    
    this.getFollowers = function (callback, params, callback_params){
        //this.callback_request('post','/apimock/getfollowers/',params,callback,callback_params);
        this.callback_request('post','http://api.voblr.com/v1/followers',params,callback,callback_params);
    }
    
    this.getFollowings = function (callback, params, callback_params){
        //this.callback_request('post','/apimock/getfollowings/',params,callback,callback_params);
        this.callback_request('post','http://api.voblr.com/v1/following',params,callback,callback_params);
    }

    /********************
     * common Functions
     ********************/
    this.getCookie = function (name) {
      var nameEQ = name + "=";
      var ca = document.cookie.split(';');
      for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
      }   
      return null;
    }
     
    this.redirectTo = function(url){
        //OK
        window.location = url;
    }

    this.isNumber   = function(n) {
      return !isNaN(parseFloat(n)) && isFinite(n);
    }
}


