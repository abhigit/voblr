#!/bin/bash
PRJ="`dirname \"${0}\"`/../../../../"

#echo PRJ=$PRJ

msg="VOBLR"
#$(cat <<EOF
#     ____   ____ ________    __________  .____      __________
#     \   \ /   / \_____  \   \______   \ |    |     \______   \
#      \   Y   /   /   |   \   |    |  _/ |    |      |       _/
#       \     /   /    |    \  |    |   \ |    |___   |    |   \
#        \___/    \_______  /  |______  / |_______ \  |____|_  /
#                         \/          \/          \/         \/
#EOF
#)

echo $msg
cd "$PRJ"
PRJ="$PWD"
SRC=$PRJ/src/main/resources/proto
#ls -al $SRC
OUT=$PRJ/src/main/java
PROTO_ROOT=$SRC

echo PRJ= $PRJ
echo SRC= $SRC
echo OUT= $OUT
#echo PROTO= $PROTO
#echo protoc -I="$SRC" --java_out="$OUT" "$PROTO"
#protoc -I="$SRC" --java_out="$OUT" "$PROTO"

#--proto_path "$ROOT/build/$CONFIG/protobuf/include"\

find "$PROTO_ROOT" -name \*.proto |\
 xargs protoc \
   -I="${PROTO_ROOT}" \
   --java_out="${OUT}"


#cd $PRJ && svn ci -m "Deploying to nexus"


