/**
 * Date : Sep 2, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.common.constants;

/**
 * @author abhi@voblr.com
 * 
 * 
 * 
 */
public class ResponseConstants {

    public static final String OK = "OK";

    public static final String NG = "NG";

    public static final int MAX_OBJECT_COUNT = 30;

    public static final int DEFAULT_OBJECT_COUNT = 10;

    public static final long CONSECUTIVE_VOB_DURATION = 1000 * 2l;

}
