/**
 * Date : Sep 2, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.common.constants;

/**
 * @author abhi@voblr.com
 */
public class GraphConstants {
    public static final String ENTITY_ID = "eid";
    public static final String ENTITY_TYPE = "etype";
    public static final String SUBJECT_ID = "sid";
    public static final String SUBJECT_TYPE = "stype";
    public static final String OBJECT_ID = "oid";
    public static final String OBJECT_TYPE = "otype";
    public static final String LIKE_FLAG = "lf";
    public static final String DELETED_FLAG = "df";
    public static final String FOLLOWERS_COUNT = "fwc";
    public static final String FOLLOWING_COUNT = "flc";
    public static final String FOLLOWS = "fws";
    public static final String FOLLOWS_TS = "fwsts";
    public static final String COMMENTS = "cmnts";
    public static final String NEXT_COMMENT = "nxtcmnt";
    public static final String FIRST_COMMENT = "fcmnt";
    public static final String VOB_COMMENT = "vbcmnt";
    public static final String COMMENTS_COUNT = "cmntsc";
    public static final String POPULARITY_INDEX = "pidx";
    public static final String REPUTATION = "rptn";
    public static final String LATEST_ACTIVITY_TS = "lats";
    public static final String CREATED_TS = "cts";
    public static final String VOB_TS = "vobts";
    public static final String VOB_TXT = "vobtxt";
    public static final String COMMENT_TXT = "cmnttxt";
    public static final String VOB_MOVIE = "vobmv";
    public static final String USER_VOB = "uvob";
    public static final String FIRST_VOB = "fvob";
    public static final String NEXT_VOB = "nvob";
    public static final String VOB_COUNT = "vobc";
    public static final String NEXT_VOB_MOVIE = "nvobm";
    public static final String LIKE_COUNT = "lc";
    public static final String VOTE_UP = "vtu";
    public static final String VOTE_DOWN = "vtd";
    public static final String VOTE_UP_COUNT = "vtuc";
    public static final String VOTE_DOWN_COUNT = "vtdc";
    public static final String CURRENT_FEED = "cfd";
    public static final String NEXT_FEED = "nfd";
    public static final String START_FEED = "sfd";
}
