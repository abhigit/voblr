/**
 * Date : Jul 29, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.common.annotations;

/**
 * @author abhi@voblr.com
 * 
 * 
 * 
 */
public @interface GraphEntity {
    public Class<?> type();
}
