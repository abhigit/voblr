/**
 * Date : Jul 29, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.common.builders;

import com.voblr.common.GraphServices.Entity;
import com.voblr.common.GraphServices.EventInfo;
import com.voblr.common.GraphServices.GenericEvent;
import com.voblr.common.exception.NotInitialized;
import com.voblr.common.proto.Event.EventType;

/**
 * @author abhi@voblr.com
 * 
 * 
 * 
 */
public class EventBuilder {

    public static GenericEvent buildEvent(Entity subject, Entity Object,
            long timestamp, EventType type) throws NotInitialized {

        GenericEvent.Builder builder = GenericEvent.newBuilder();

        builder.setSubject(subject);
        builder.setObject(Object);

        EventInfo.Builder infoBuilder = EventInfo.newBuilder();
        infoBuilder.setTimestamp(timestamp);
        infoBuilder.setType(type);
        if (infoBuilder.isInitialized()) {
            builder.setEventInfo(infoBuilder.build());
        }
        if (builder.isInitialized()) {
            return builder.build();
        }
        throw new NotInitialized("Event not Intialized");
    }

}
