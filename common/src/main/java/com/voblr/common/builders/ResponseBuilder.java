/**
 * Date : Sep 2, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.common.builders;


/**
 * @author abhi@voblr.com
 *         <p/>
 *         <br>
 *         Builds a generic response for various requests
 *         <p/>
 *         <pre>
 *                 message GenericResponse {
 *                     required string status = 1;
 *                     optional string message = 2;
 *
 *                     repeated GenericEvent events = 3;
 *                     repeated Entity entities = 4;
 *                     optional int32 number = 5;
 *                 }
 *                 </pre>
 */
public class ResponseBuilder {

}
