/**
 * Date : Jul 29, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */
package com.voblr.common.builders;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import com.typesafe.config.ConfigRenderOptions;
import com.typesafe.config.ConfigResolveOptions;
import com.voblr.common.GraphServices.Entity;
import com.voblr.common.GraphServices.EntityType;
import com.voblr.common.exception.NotInitialized;

/**
 * @author abhi@voblr.com
 * 
 *         This will build entities to be transferred via RPC.
 * 
 */
public class EntityBuilder {
    public static void main(String[] a){
        Config conf = ConfigFactory.load("profile");
        System.out.println(conf.resolve().root().render(ConfigRenderOptions.concise().setFormatted(true).setOriginComments(false)));
    }

    public static Entity buildEntity(String entity_id, EntityType type,
            int intNum, long longNum, String text) throws NotInitialized {
        Entity.Builder builder = Entity.newBuilder();
        builder.setEntityId(entity_id);
        builder.setEntityType(type);
        builder.setINumber(intNum);
        builder.setLNumber(longNum);
        builder.setText(text);
        if (builder.isInitialized()) {
            return builder.build();
        }
        throw new NotInitialized("Entity not Initialized");

    }

    public static Entity buildUserEntity(String entity_id, EntityType type,
            int intNum, long longNum, String text) throws NotInitialized {
        Entity.Builder builder = Entity.newBuilder();
        builder.setEntityId(entity_id);
        builder.setEntityType(type);
        builder.setINumber(intNum);
        builder.setLNumber(longNum);
        builder.setText(text);
        if (builder.isInitialized()) {
            return builder.build();
        }
        throw new NotInitialized("Entity not Initialized");
    }

    public static Entity buildEntity(String entity_id, EntityType type)
            throws NotInitialized {
        Entity.Builder builder = Entity.newBuilder();
        builder.setEntityId(entity_id);
        builder.setEntityType(type);
        if (builder.isInitialized()) {
            return builder.build();
        }
        throw new NotInitialized("Entity not Initialized");
    }

    public static Entity buildEntity(String entity_id, String type)
            throws NotInitialized {
        return buildEntity(entity_id, EntityType.valueOf(type));
    }
}
