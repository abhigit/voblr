package com.voblr.common.exception;

/**
 * Date : Sep 2, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */

/**
 * @author abhi@voblr.com
 * 
 * 
 * 
 */
public class NodeEntityNotFound extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -6518447587017027207L;

    /**
     * 
     */

    public NodeEntityNotFound(String message) {
        super(message);
    }

    public NodeEntityNotFound(String message, Throwable throwable) {
        super(message, throwable);
    }

}
