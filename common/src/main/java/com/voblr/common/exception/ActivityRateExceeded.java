package com.voblr.common.exception;

import com.voblr.common.proto.Event.EventType;

/**
 * Date : Sep 2, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */

/**
 * @author abhi@voblr.com
 * 
 *         This exception will be thrown if a certain activity rate exceeds the
 *         allowed rate.
 * 
 */
public class ActivityRateExceeded extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = 4452218745203418668L;

    public ActivityRateExceeded(EventType type) {
        super("ActivityRateExceeded for " + type.name());
    }

    public ActivityRateExceeded(EventType type, Throwable throwable) {
        super("ActivityRateExceeded for " + type.name(), throwable);
    }

}
