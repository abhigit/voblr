package com.voblr.common.exception;

/**
 * Date : Sep 2, 2012
 *
 * All rights reserved (c)2012 Voblr, Inc.
 */

/**
 * @author abhi@voblr.com
 * 
 * 
 * 
 */
public class NotInitialized extends Exception {

    /**
     * 
     */
    private static final long serialVersionUID = -6510447587017027207L;

    /**
     * 
     */

    public NotInitialized(String message) {
        super(message);
    }

    public NotInitialized(String message, Throwable throwable) {
        super(message, throwable);
    }

}
